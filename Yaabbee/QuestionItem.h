

#import <Foundation/Foundation.h>
#import "AnswerItem.h"

@interface QuestionItem : NSObject  {

}

@property (nonatomic, assign) BOOL isAnswered;
@property (nonatomic, assign) int answeredIndex;

@property(nonatomic,retain)NSString *Text;
@property(nonatomic,retain)NSString *CorrectAnswer;
@property(nonatomic,retain)NSMutableArray *Answers;

- (BOOL)isAnsweredCorrect;
- (NSDictionary *)getItems;

@end


//<a:Question>
//<a:Answers>
//<a:Answer>
//<a:Points>0</a:Points>
//<a:Text>True</a:Text>
//<a:isCorrect>false</a:isCorrect>
//</a:Answer>
//<a:Answer>
//<a:Points>0</a:Points>
//<a:Text>False</a:Text>
//<a:isCorrect>true</a:isCorrect>
//</a:Answer>
//</a:Answers>
//<a:CorrectAnswer>2</a:CorrectAnswer>
//<a:Text>It is useless to bother setting goals at any time.</a:Text>