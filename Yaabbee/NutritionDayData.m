//
//  claimActivityOBJ.m
//  Acclaris
//
//  Created by Sarfaraj Biswas on 09/11/10.
//  Copyright 2010 ObjectSol Technologies. All rights reserved.
//

#import "NutritionDayData.h"

#define ERROR_STATUS @"a:ErrorStatus"
#define ERROR_MSG @"a:ErrorMessage"

#define WEEK_LABEL @"a:WeekLabel"
#define MEMBER_ID @"a:MemberId"
#define DATE @"a:Date"
#define DAY_LABEL @"a:DayLabel"

//

//<a:AmSnack><a:Breakfast><a:Dinner><a:Lunch><a:PmSnack></a:Vitamin></a:Water>
#define AM_SNACK @"a:AmSnack"
#define BREAKFAST @"a:Breakfast"
#define DINNER @"a:Dinner"
#define LUNCH @"a:Lunch"
#define PM_SNACK @"a:PmSnack"
#define VITAMIN @"a:Vitamin"
#define WATER @"a:Water"
#define MEALS_PHOTO_URL @"a:PhotoUrl"
#define MEALS_PHOTO_DATE @"a:PhotoDateTime"

//
#define CALORIES_CONSUMED @"a:CaloriesConsumed"
#define DESCRIPTION @"a:Description"
#define HEALTH_RANKING @"a:HealthRanking"
#define TARGET_CALORIES @"a:TargetCalories"
#define WAS_EATEN @"a:wasEaten"



//static TeamProgressData *_sharedInstance = nil;

@implementation NutritionDayData

@synthesize nutritionDayDict, Date;

@synthesize WeekLabel, MemberId,DayLabel;

@synthesize responseStatus;

#pragma mark Parse Xml Data

- (void)parseXMLFileAtData:(NSMutableData *)data parseError:(NSError **)error
{
	NSXMLParser *parser;
	parser = [[NSXMLParser alloc] initWithData:data];
	[parser setDelegate:self];
	[parser setShouldProcessNamespaces:NO];
	[parser setShouldReportNamespacePrefixes:NO];
	[parser setShouldResolveExternalEntities:NO];
	[parser parse];
	
	NSError *parseError = [parser parserError];
	
	if (parseError && error) 
	{
		*error = parseError;
	}
	
	[parser release];
}

- (void)parserDidStartDocument:(NSXMLParser *)parser
{
	responseStatus = [[ResponseStatus alloc]init];
	self.nutritionDayDict = [[NSMutableDictionary alloc]init];
}		

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
	if (qName)
	{
		elementName = qName;
	}
	else 
		if([elementName isEqualToString:ERROR_STATUS])
		{
			contentOfString=[NSMutableString string];
			[contentOfString retain];
			return;		
				
		}
	else 
		if([elementName isEqualToString:ERROR_MSG])
		{
			contentOfString=[NSMutableString string];
			[contentOfString retain];
			return;		
		}
		else if([elementName isEqualToString:DATE])
		{
			contentOfString=[NSMutableString string];
			[contentOfString retain];
			return;		
			
		}
		else		
		if([elementName isEqualToString:DAY_LABEL])
		{
			contentOfString=[NSMutableString string];
			[contentOfString retain];
			return;		
			
		}
		else if([elementName isEqualToString:WEEK_LABEL])
		{
			contentOfString=[NSMutableString string];
			[contentOfString retain];
			return;		
			
		}else	
			if([elementName isEqualToString:MEMBER_ID])
			{
				contentOfString=[NSMutableString string];
				[contentOfString retain];
				return;		
				
			}else 		
				if([elementName isEqualToString:AM_SNACK])
				{
					currDict = [[NSMutableDictionary alloc] init];
					//contentOfString=[NSMutableString string];
					//[contentOfString retain];
					return;							
				}else 		
					if([elementName isEqualToString:BREAKFAST])
					{
						currDict = [[NSMutableDictionary alloc] init];
						//contentOfString=[NSMutableString string];
						//[contentOfString retain];
						return;		
					}else 		
						if([elementName isEqualToString:DINNER])
						{
							currDict = [[NSMutableDictionary alloc] init];
							//contentOfString=[NSMutableString string];
							//[contentOfString retain];
							return;		
						}else 		
							if([elementName isEqualToString:LUNCH])
							{
								currDict = [[NSMutableDictionary alloc] init];
								//contentOfString=[NSMutableString string];
								//[contentOfString retain];
								return;		
							}else 		
								if([elementName isEqualToString:PM_SNACK])
								{
									currDict = [[NSMutableDictionary alloc] init];
									//contentOfString=[NSMutableString string];
									//[contentOfString retain];
									return;		
								}else 		
									if([elementName isEqualToString:WATER])
									{
										//currDict = [[NSMutableDictionary alloc] init];
										contentOfString=[NSMutableString string];
										[contentOfString retain];
										return;		
									}else 	
						if([elementName isEqualToString:VITAMIN])
						{
							contentOfString=[NSMutableString string];
							[contentOfString retain];
							return;		
							
						}else 
							if([elementName isEqualToString:CALORIES_CONSUMED])
							{
								contentOfString=[NSMutableString string];
								[contentOfString retain];
								return;		
								
							}else //		
								if([elementName isEqualToString:DESCRIPTION])
								{
									contentOfString=[NSMutableString string];
									[contentOfString retain];
									return;		
									
								}
								else 
									if([elementName isEqualToString:HEALTH_RANKING])
									{
										contentOfString=[NSMutableString string];
										[contentOfString retain];
										return;		
										
									}
									else //		
										if([elementName isEqualToString:TARGET_CALORIES])
										{
											contentOfString=[NSMutableString string];
											[contentOfString retain];
											return;		
											
										}else //		
											if([elementName isEqualToString:WAS_EATEN])
											{
												contentOfString=[NSMutableString string];
												[contentOfString retain];
												return;		
												
											}
                                            else //		
                                                if([elementName isEqualToString:MEALS_PHOTO_URL])
                                                {
                                                    contentOfString=[NSMutableString string];
                                                    [contentOfString retain];
                                                    return;		
                                                    
                                                }
                                                else //		
                                                    if([elementName isEqualToString:MEALS_PHOTO_DATE])
                                                    {
                                                        contentOfString=[NSMutableString string];
                                                        [contentOfString retain];
                                                        return;		
                                                        
                                                    }
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName     
{	
	if (qName)
	{
		elementName = qName;
	}
	else 
			if([elementName isEqualToString:ERROR_STATUS])
			{
				if(contentOfString)
				{
					responseStatus.returnCode = contentOfString;
					[contentOfString release];
					contentOfString = nil;
				}				
			}
			else 
				if([elementName isEqualToString:ERROR_MSG])
				{
					if(contentOfString)
					{
						responseStatus.errorText = contentOfString;
						[contentOfString release];
						contentOfString = nil;
					}		
				}
	else 
		if([elementName isEqualToString:DAY_LABEL])
		{
			if(contentOfString)
			{
				self.DayLabel = contentOfString;
				[contentOfString release];
				contentOfString = nil;
			}
		}
		else
		if([elementName isEqualToString:DATE])
		{
			if(contentOfString)
			{
				self.Date = contentOfString;
				[contentOfString release];
				contentOfString = nil;
			}
		}
		else 
			if([elementName isEqualToString:MEMBER_ID])
			{
				if(contentOfString)
				{
					self.MemberId = contentOfString;
					[contentOfString release];
					contentOfString = nil;
				}
			}else 
				if([elementName isEqualToString:WEEK_LABEL])
				{
					if(contentOfString)
					{
						self.WeekLabel = contentOfString;
						[contentOfString release];
						contentOfString = nil;
					}
				}
			else 
				if([elementName isEqualToString:AM_SNACK])
				{
					if(currDict)
					{
						[self.nutritionDayDict setObject:currDict forKey:AM_SNACK_KEY];
						[currDict release];
						currDict = nil;
					}
				}
				else 
					if([elementName isEqualToString:BREAKFAST])
					{
						if(currDict)
						{
							[self.nutritionDayDict setObject:currDict forKey:BREAKFAST_KEY];
							[currDict release];
							currDict = nil;
						}
					}
					else 
						if([elementName isEqualToString:DINNER])
						{
							if(currDict)
							{
								[self.nutritionDayDict setObject:currDict forKey:DINNER_KEY];
								[currDict release];
								currDict = nil;
							}
						}
						else 
							if([elementName isEqualToString:LUNCH])
							{
								if(currDict)
								{
									[self.nutritionDayDict setObject:currDict forKey:LUNCH_KEY];
									[currDict release];
									currDict = nil;
								}
							}
							else 
								if([elementName isEqualToString:PM_SNACK])
								{
									if(currDict)
									{
										[self.nutritionDayDict setObject:currDict forKey:PM_SNACK_KEY];
										[currDict release];
										currDict = nil;
									}
								}
								else 
									if([elementName isEqualToString:WATER])
									{
										if(contentOfString)
										{
											[self.nutritionDayDict setObject:contentOfString forKey:WATER_KEY];
											[contentOfString release];
											contentOfString = nil;
										}
									}
									else 
										if([elementName isEqualToString:VITAMIN])
										{
											if(contentOfString)
											{
												[self.nutritionDayDict setObject:contentOfString forKey:VITAMIN_KEY];
												[contentOfString release];
												contentOfString = nil;
											}											
										}
				else 
					if([elementName isEqualToString:CALORIES_CONSUMED])
					{
						if(contentOfString)
						{
							[currDict setObject:contentOfString forKey:CALORIES_CONSUMED_KEY];
							[contentOfString release];
							contentOfString = nil;
						}
					}
					else 
						if([elementName isEqualToString:DESCRIPTION])
						{
							if(contentOfString)
							{
								[currDict setObject:contentOfString forKey:DESCRIPTION_KEY];
								[contentOfString release];
								contentOfString = nil;
							}
						}else 
							if([elementName isEqualToString:HEALTH_RANKING])
							{
								if(contentOfString)
								{
									[currDict setObject:contentOfString forKey:HEALTH_RANKING_KEY];
									[contentOfString release];
									contentOfString = nil;									
								}
							}else 
								if([elementName isEqualToString:TARGET_CALORIES])
								{
									if(contentOfString)
									{
										[currDict setObject:contentOfString forKey:TARGET_CALORIES_KEY];
										[contentOfString release];
										contentOfString = nil;										
									}
								}else 
									if([elementName isEqualToString:WAS_EATEN])
									{
										if(contentOfString)
										{
											[currDict setObject:contentOfString forKey:WAS_EATEN_KEY];
											[contentOfString release];
											contentOfString = nil;										
										}
									}
                                    else 
                                        if([elementName isEqualToString:MEALS_PHOTO_URL])
                                        {
                                            if(contentOfString)
                                            {
                                                [currDict setObject:contentOfString forKey:MEALS_PHOTO_URL];
                                                [contentOfString release];
                                                contentOfString = nil;										
                                            }
                                        }
   else
       if([elementName isEqualToString:MEALS_PHOTO_DATE])
    {
        if(contentOfString)
        {
            [currDict setObject:contentOfString forKey:MEALS_PHOTO_DATE];
            [contentOfString release];
            contentOfString = nil;										
        }
    }


}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
	if(contentOfString)
		[contentOfString appendString:string];
}

- (void)parserDidEndDocument:(NSXMLParser *)parse
{
	
	//myerrorcodeOBJ = (errorcodeOBJ *)[errordetails objectAtIndex:0];
//	responseStatus.returnCode = [NSString stringWithFormat:@"0"];
//	responseStatus.errorText = [NSString stringWithFormat:@""];
	//NSLog(@"**********\n\n\n\n\n\n%@\n\nreturnCode %@\nerrorText %@", responseStatus.returnCode, responseStatus.errorText);
}	

- (BOOL)getBoolVal:(NSString*)val
{
	if([val isEqualToString:@"true"])
		return YES;
	else
		return NO;
}



- (NSString*)getMobilityXml:(NSString*)date
{
	NSMutableString *sRequest=[[NSMutableString alloc] init];
	
//	[sRequest appendString:@"<nuv:Date>"];
// 	[sRequest appendString:date];
//	[sRequest appendString:@"</nuv:Date>"];
//	
//	
//	[sRequest appendString:@"<nuv:ErrorMessage>"];
// 	[sRequest appendString:@""];
//	[sRequest appendString:@"</nuv:ErrorMessage>"];
//	
//	[sRequest appendString:@"<nuv:ErrorStatus>"];
// 	[sRequest appendString:@"false"];
//	[sRequest appendString:@"</nuv:ErrorStatus>"];
//	
//	//F
//	[sRequest appendString:@"<nuv:FridaySelect>"];
// 	[sRequest appendString:self.FridaySelect];
//	[sRequest appendString:@"</nuv:FridaySelect>"];
//	
//	[sRequest appendString:@"<nuv:FridayText>"];
// 	[sRequest appendString:@"Friday"];
//	[sRequest appendString:@"</nuv:FridayText>"];
//	
//	[sRequest appendString:@"<nuv:MemberId>"];
// 	[sRequest appendString:self.MemberId];
//	[sRequest appendString:@"</nuv:MemberId>"];
//	
//	//M
//	[sRequest appendString:@"<nuv:MondaySelect>"];
// 	[sRequest appendString:self.MondaySelect];
//	[sRequest appendString:@"</nuv:MondaySelect>"];
//	
//	[sRequest appendString:@"<nuv:MondayText>"];
// 	[sRequest appendString:@"Monday"];
//	[sRequest appendString:@"</nuv:MondayText>"];
//	
//	//Sat
//	[sRequest appendString:@"<nuv:SaturdaySelect>"];
// 	[sRequest appendString:self.SaturdaySelect];
//	[sRequest appendString:@"</nuv:SaturdaySelect>"];
//	
//	[sRequest appendString:@"<nuv:SaturdayText>"];
// 	[sRequest appendString:@"Saturday"];
//	[sRequest appendString:@"</nuv:SaturdayText>"];
//	
//	//Sunday
//	[sRequest appendString:@"<nuv:SundaySelect>"];
// 	[sRequest appendString:self.SundaySelect];
//	[sRequest appendString:@"</nuv:SundaySelect>"];
//	
//	[sRequest appendString:@"<nuv:SundayText>"];
// 	[sRequest appendString:@"Sunday"];
//	[sRequest appendString:@"</nuv:SundayText>"];
//	
//	//ThursdaySelect
//	[sRequest appendString:@"<nuv:ThursdaySelect>"];
// 	[sRequest appendString:self.ThursdaySelect];
//	[sRequest appendString:@"</nuv:ThursdaySelect>"];
//	
//	[sRequest appendString:@"<nuv:ThursdayText>"];
// 	[sRequest appendString:@"Thursday"];
//	[sRequest appendString:@"</nuv:ThursdayText>"];
//	
//		
//	//TuesdaySelect
//	[sRequest appendString:@"<nuv:TuesdaySelect>"];
// 	[sRequest appendString:self.TuesdaySelect];
//	[sRequest appendString:@"</nuv:TuesdaySelect>"];
//	
//	[sRequest appendString:@"<nuv:TuesdayText>"];
// 	[sRequest appendString:@"Tuesday"];
//	[sRequest appendString:@"</nuv:TuesdayText>"];
//	
//
//	//WednesdaySelect
//	[sRequest appendString:@"<nuv:WednesdaySelect>"];
// 	[sRequest appendString:self.WednesdaySelect];
//	[sRequest appendString:@"</nuv:WednesdaySelect>"];
//	
//	[sRequest appendString:@"<nuv:WednesdayText>"];
// 	[sRequest appendString:@"Wednesday"];
//	[sRequest appendString:@"</nuv:WednesdayText>"];
//	
//	[sRequest appendString:@"<nuv:WeekLabel>"];
// 	[sRequest appendString:self.WeekLabel];
//	[sRequest appendString:@"</nuv:WeekLabel>"];
	
	return sRequest;
}

@end


/*
 
 <GetNutritionDayResult xmlns:a="http://schemas.datacontract.org/2004/07/NuvitaMobileService" xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
 <a:AmSnack>
 <a:CaloriesConsumed>0</a:CaloriesConsumed>
 <a:Description>Protein Bars</a:Description>
 <a:HealthRanking>veryhealthy</a:HealthRanking>
 <a:TargetCalories>292</a:TargetCalories>
 <a:wasEaten>false</a:wasEaten>
 </a:AmSnack>
 <a:Breakfast>
 <a:CaloriesConsumed>0</a:CaloriesConsumed>
 <a:Description>Oatmeal</a:Description>
 <a:HealthRanking>veryhealthy</a:HealthRanking>
 <a:TargetCalories>487</a:TargetCalories>
 <a:wasEaten>false</a:wasEaten>
 </a:Breakfast>
 <a:Date>2012-01-25T00:00:00</a:Date>
 <a:Dinner>
 <a:CaloriesConsumed>0</a:CaloriesConsumed>
 <a:Description/>
 <a:HealthRanking>notset</a:HealthRanking>
 <a:TargetCalories>487</a:TargetCalories>
 <a:wasEaten>false</a:wasEaten>
 </a:Dinner>
 <a:ErrorMessage i:nil="true"/>
 <a:ErrorStatus>false</a:ErrorStatus>
 <a:Lunch>
 <a:CaloriesConsumed>0</a:CaloriesConsumed>
 <a:Description/>
 <a:HealthRanking>notset</a:HealthRanking>
 <a:TargetCalories>390</a:TargetCalories>
 <a:wasEaten>false</a:wasEaten>
 </a:Lunch>
 <a:MemberId>8b5ed874-f43d-44f4-a8eb-2884f79e2acd</a:MemberId>
 <a:PmSnack>
 <a:CaloriesConsumed>0</a:CaloriesConsumed>
 <a:Description>Jerky</a:Description>
 <a:HealthRanking>veryhealthy</a:HealthRanking>
 <a:TargetCalories>292</a:TargetCalories>
 <a:wasEaten>false</a:wasEaten>
 </a:PmSnack>
 <a:Vitamin>false</a:Vitamin>
 <a:Water>false</a:Water>
 <a:WeekLabel>Week of 1/23</a:WeekLabel>
 </GetNutritionDayResult>
 
 
 */
