//
//  CustomImageForSegmentControl.m
//  Yaabbee
//
//  Created by Sarfaraj Biswas on 7/22/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "CustomImageForSegmentControl.h"
#import "constant.h"

#define IMAGE_WIDTH 74
#define IMAGE_HEIGHT 29

@implementation CustomImageForSegmentControl

- (void)customImageForSegmentedControl:(NSInteger)selectedOption SearchBarView:(NSArray *)searchBarArray lbl1Text:(NSString *)lbl1Text lbl2Text:(NSString *)lbl2Text lbl3Text:(NSString *)lbl3Text
{
	// for segmentedControl color..
	NSMutableArray *segRefA = nil;
	UIView *view[3];
	UILabel *lbl[3];
	
	UIImageView *imgView[3];
	UIImage *image3=[UIImage imageNamed:@"both_orange.png"]; 
	UIImage *image2=[UIImage imageNamed:@"arabic_orange.png"]; 
	UIImage *image1=[UIImage imageNamed:@"english_orange.png"]; 
	int i=0;
		
//	segRefA = [NSMutableArray arrayWithArray:searchBarArray];
	segRefA = [[NSMutableArray alloc] initWithArray:searchBarArray];
	
	for(UISegmentedControl *segRef in segRefA)
	{
		
		for(UIView *v in [segRef subviews])
			[v removeFromSuperview];
		
		if(i==0)
		{
			lbl[i]=[[UILabel alloc] initWithFrame:CGRectMake(0, 0,IMAGE_WIDTH, IMAGE_HEIGHT)];
			lbl[i].text = lbl1Text; 
			view[i]=[[UIView alloc] initWithFrame:CGRectMake(0, 0, IMAGE_WIDTH, IMAGE_HEIGHT)];
			imgView[i] =[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, IMAGE_WIDTH, IMAGE_HEIGHT)];
			imgView[i].image=image1;
		}
		else if(i==1)
		{
			lbl[i]=[[UILabel alloc] initWithFrame:CGRectMake(0, 0,102, IMAGE_HEIGHT)];
			lbl[i].text = lbl2Text;
			view[i]=[[UIView alloc] initWithFrame:CGRectMake(0, 0, 102, IMAGE_HEIGHT)];
			imgView[i] =[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 102, IMAGE_HEIGHT)];
			imgView[i].image=image2;
			
		}
		else
		{
			lbl[i]=[[UILabel alloc] initWithFrame:CGRectMake(0, 0,IMAGE_WIDTH, IMAGE_HEIGHT)];
			lbl[i].text = lbl3Text;	
			view[i]=[[UIView alloc] initWithFrame:CGRectMake(0, 0, IMAGE_WIDTH, IMAGE_HEIGHT)];
			imgView[i] =[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, IMAGE_WIDTH, IMAGE_HEIGHT)];
			imgView[i].image=image3;
			
		}
		
		lbl[i].font =[UIFont boldSystemFontOfSize:12];
		lbl[i].textAlignment=NSTextAlignmentCenter;
		view[i].backgroundColor=[UIColor clearColor];
		lbl[i].backgroundColor=[UIColor clearColor];
		if(selectedOption == i)
		{	
			
			imgView[i].alpha=1.0;
			lbl[i].textColor=[UIColor whiteColor];
		}
		else 
		{
			
			imgView[i].alpha=0.5;
			lbl[i].textColor=[UIColor whiteColor];//[CustomColor getColor:1];
		}
		
		[view[i] addSubview:imgView[i]];
		[view[i] addSubview:lbl[i]];
		[segRef addSubview:view[i]];
		
		[imgView[i] release];
		[lbl[i] release];
		[view[i] release];

		imgView[i] = nil;
		lbl[i] =nil;
		view[i] =nil;
		
		i++;
	}
	image3=nil;
	image1 =nil;
	image2=nil;
	
	[segRefA release], segRefA = nil;
	//searchBarView=nil;
}

@end
