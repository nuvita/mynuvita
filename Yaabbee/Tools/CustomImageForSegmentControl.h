//
//  CustomImageForSegmentControl.h
//  Yaabbee
//
//  Created by Sarfaraj Biswas on 7/22/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface CustomImageForSegmentControl : NSObject {

}

- (void)customImageForSegmentedControl:(NSInteger)selectedOption SearchBarView:(NSArray *)searchBarView lbl1Text:(NSString *)lbl1Text lbl2Text:(NSString *)lbl2Text lbl3Text:(NSString *)lbl3Text;

@end
