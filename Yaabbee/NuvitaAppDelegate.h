//
//  NuvitaAppDelegate.h
//  Yaabbee
//
//  Created by Srabati on 7/22/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//
//aab946d88bec290e429d60283d6657e875123db6 

//c087a7ef97e2d219c39b76a02b96a702f46dac1a

#import <UIKit/UIKit.h>
#import "constant.h"
#import "ForceNavigationViewController.h"
#import "ForceTabBarViewController.h"

@interface NuvitaAppDelegate : UIResponder <UIApplicationDelegate, UITabBarControllerDelegate>

@property (nonatomic, assign) BOOL isidlecheck;
@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) UITabBarController *forceTabBarViewController;
@property (nonatomic, retain) UINavigationController *forceNavigationViewController;

- (void)reloadAppDelegateRootViewController;
- (void)reloadAppDelegateRootViewControllerLandscape;

+ (void)updateStatus;
+ (BOOL)isNetworkAvailable;
- (void)setupNetworkCheck;

- (void)Checktouch;
- (void)Check;
+ (BOOL)isPhone5;
+ (BOOL)isIOS7;

+ (UIImage *)captureView:(UIView *)view;

@end
