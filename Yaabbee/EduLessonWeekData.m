//
//  claimActivityOBJ.m
//  Acclaris
//
//  Created by Sarfaraj Biswas on 09/11/10.
//  Copyright 2010 ObjectSol Technologies. All rights reserved.
//

#import "EduLessonWeekData.h"

#define ERROR_STATUS @"a:ErrorStatus"
#define ERROR_MSG @"a:ErrorMessage"

#define MEMBER_ID @"a:MemberId"
#define WEEK_LABEL @"a:WeekLabel"
#define TITLE @"a:Title"
#define LESSON_NUMER @"a:LessonNumber"
#define TEXT @"a:Text"

//question
#define QUESTION @"a:Question"
#define CORRECT_ANSWER @"a:CorrectAnswer"


//anws
#define ANSWER @"a:Answer"
#define POINTS @"a:Points"
#define ISCORRECT @"a:isCorrect"



@implementation EduLessonWeekData

@synthesize MemberId,WeekLabel,LessonNumber,Title,Text;
//@synthesize TeamName;

@synthesize ItemsArray;

@synthesize responseStatus;


#pragma mark Parse Xml Data

- (void)parseXMLFileAtData:(NSMutableData *)data parseError:(NSError **)error
{
	NSXMLParser *parser;
	parser = [[NSXMLParser alloc] initWithData:data];
	[parser setDelegate:self];
	[parser setShouldProcessNamespaces:NO];
	[parser setShouldReportNamespacePrefixes:NO];
	[parser setShouldResolveExternalEntities:NO];
	[parser parse];
	
	NSError *parseError = [parser parserError];
	
	if (parseError && error) 
	{
		*error = parseError;
	}
	
	[parser release];
}

- (void)parserDidStartDocument:(NSXMLParser *)parser
{
	responseStatus = [[ResponseStatus alloc]init];
	self.ItemsArray = [[NSMutableArray alloc]init];
}		

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
	if (qName)
	{
		elementName = qName;
	}
	else 
		if([elementName isEqualToString:ERROR_STATUS])
		{
			contentOfString=[NSMutableString string];
			[contentOfString retain];
			return;		
				
		}
	else 
		if([elementName isEqualToString:ERROR_MSG])
		{
			contentOfString=[NSMutableString string];
			[contentOfString retain];
			return;		
		}
	else 
		if([elementName isEqualToString:MEMBER_ID])
		{
			contentOfString=[NSMutableString string];
			[contentOfString retain];
			return;		
			
		}
		else
			if([elementName isEqualToString:WEEK_LABEL])
			{
				contentOfString=[NSMutableString string];
				[contentOfString retain];
				return;		
				
			}
			else
				if([elementName isEqualToString:TITLE])
				{
					contentOfString=[NSMutableString string];
					[contentOfString retain];
					return;		
					
				}
				else
					if([elementName isEqualToString:LESSON_NUMER])
					{
						contentOfString=[NSMutableString string];
						[contentOfString retain];
						return;		
						
					}
					else
						if([elementName isEqualToString:TEXT])
						{
							contentOfString=[NSMutableString string];
							[contentOfString retain];
							return;		
							
						}
		else //Question
			if([elementName isEqualToString:QUESTION])
			{
				questionItem = [[QuestionItem alloc]init];
				questionItem.Answers = [[NSMutableArray alloc]init]; 
				[questionItem retain];
				return;		
			}
			else 
				if([elementName isEqualToString:CORRECT_ANSWER])
				{
					contentOfString=[NSMutableString string];
					[contentOfString retain];
					return;		
					
				}
				else
					if([elementName isEqualToString:ANSWER])
					{
						answerItem = [[AnswerItem alloc]init];
						[answerItem retain];
						return;		
						
					}
					else
						if([elementName isEqualToString:POINTS])
						{
							contentOfString=[NSMutableString string];
							[contentOfString retain];
							return;		
							
						}
						else
							if([elementName isEqualToString:ISCORRECT])
							{
								contentOfString=[NSMutableString string];
								[contentOfString retain];
								return;		
								
							}
							
		
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName     
{	
	if (qName)
	{
		elementName = qName;
	}
	else 
			if([elementName isEqualToString:ERROR_STATUS])
			{
				if(contentOfString)
				{
					responseStatus.returnCode = contentOfString;
					[contentOfString release];
					contentOfString = nil;
				}				
			}
			else 
				if([elementName isEqualToString:ERROR_MSG])
				{
					if(contentOfString)
					{
						responseStatus.errorText = contentOfString;
						[contentOfString release];
						contentOfString = nil;
					}		
				}
	else 
		if([elementName isEqualToString:MEMBER_ID])
		{
			if(contentOfString)
			{
				self.MemberId = contentOfString;
				[contentOfString release];
				contentOfString = nil;
			}
		}
		else 
			if([elementName isEqualToString:WEEK_LABEL])
			{
				if(contentOfString)
				{
					self.WeekLabel = contentOfString;
					[contentOfString release];
					contentOfString = nil;
				}							
			}
			else 
				if([elementName isEqualToString:TITLE])
				{
					if(contentOfString)
					{
						self.Title = contentOfString;
						[contentOfString release];
						contentOfString = nil;
					}							
				}
				else 
					if([elementName isEqualToString:LESSON_NUMER])
					{
						if(contentOfString)
						{
							self.LessonNumber = contentOfString;
							[contentOfString release];
							contentOfString = nil;
						}							
					}
					else 
						if([elementName isEqualToString:TEXT])
						{
							if(contentOfString)
							{
								if(answerItem != nil)
									answerItem.Text = contentOfString;
								else if(questionItem !=nil)
									questionItem.Text = contentOfString;
								else
									self.Text = contentOfString;
								//self.Text = contentOfString;
								[contentOfString release];
								contentOfString = nil;
							}							
						}
		else //question
			if([elementName isEqualToString:QUESTION])
			{
				[self.ItemsArray addObject:questionItem];//
				
				questionItem = nil;
			}
		else 
			if([elementName isEqualToString:CORRECT_ANSWER])
				{
					if(contentOfString)
					{
						questionItem.CorrectAnswer = contentOfString;
						[contentOfString release];
						contentOfString = nil;
					}		
				}	
		else 
			if([elementName isEqualToString:ANSWER])
			{
				[questionItem.Answers addObject:answerItem];//
				
				answerItem = nil;				
			}
			else 
				if([elementName isEqualToString:POINTS])
				{
					if(contentOfString)
					{
						answerItem.Points = contentOfString;
						[contentOfString release];
						contentOfString = nil;
					}						
				}
				else 
					if([elementName isEqualToString:ISCORRECT])
					{
						if(contentOfString)
						{
							answerItem.isCorrect = contentOfString;
							[contentOfString release];
							contentOfString = nil;
						}							
					}
					
	
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
	if(contentOfString)
		[contentOfString appendString:string];
}

- (void)parserDidEndDocument:(NSXMLParser *)parse
{
	
	//myerrorcodeOBJ = (errorcodeOBJ *)[errordetails objectAtIndex:0];
//	responseStatus.returnCode = [NSString stringWithFormat:@"0"];
//	responseStatus.errorText = [NSString stringWithFormat:@""];
	//NSLog(@"**********\n\n\n\n\n\n%@\n\nreturnCode %@\nerrorText %@", responseStatus.returnCode, responseStatus.errorText);
}	

- (int)getNumCorrect
{
	int ansCount = 0;
	int count = [ItemsArray count];
	
	for(int ii = 0; ii < count; ii++)
	{
		QuestionItem* item =(QuestionItem*)[ItemsArray objectAtIndex:ii];
		if([item isAnsweredCorrect])
			ansCount++;
	}
	return ansCount;
}

- (float)getScore
{
	return ((float)[self getNumCorrect]/[self.ItemsArray count])*100;
}


@end



/*

 
 
 <a:LessonNumber>160</a:LessonNumber>
 <a:MemberId>8b5ed874-f43d-44f4-a8eb-2884f79e2acd</a:MemberId>
 <a:Quiz>
 <a:Questions>
 <a:Question>
 <a:Answers>
 <a:Answer>
 <a:Points>0</a:Points>
 <a:Text>True</a:Text>
 <a:isCorrect>false</a:isCorrect>
 </a:Answer>
 <a:Answer>
 <a:Points>0</a:Points>
 <a:Text>False</a:Text>
 <a:isCorrect>true</a:isCorrect>
 </a:Answer>
 </a:Answers>
 <a:CorrectAnswer>2</a:CorrectAnswer>
 <a:Text>It is useless to bother setting goals at any time.</a:Text>
 </a:Question>
 <a:Question>
 <a:Answers>
 <a:Answer>
 <a:Points>0</a:Points>
 <a:Text>Visualize it</a:Text>
 <a:isCorrect>true</a:isCorrect>
 </a:Answer>
 <a:Answer>
 <a:Points>0</a:Points>
 <a:Text>Think about it once and then forget it</a:Text>
 <a:isCorrect>false</a:isCorrect>
 </a:Answer>
 <a:Answer>
 <a:Points>0</a:Points>
 <a:Text>Write down the goal over and over again so it becomes real</a:Text>
 <a:isCorrect>false</a:isCorrect>
 </a:Answer>
 </a:Answers>
 <a:CorrectAnswer>1</a:CorrectAnswer>
 <a:Text>One way to help me reach my goals is to:</a:Text>
 </a:Question>
 <a:Question>
 <a:Answers>
 <a:Answer>
 <a:Points>0</a:Points>
 <a:Text>True</a:Text>
 <a:isCorrect>false</a:isCorrect>
 </a:Answer>
 <a:Answer>
 <a:Points>0</a:Points>
 <a:Text>False</a:Text>
 <a:isCorrect>true</a:isCorrect>
 </a:Answer>
 </a:Answers>
 <a:CorrectAnswer>2</a:CorrectAnswer>
 <a:Text>How I feel about achieving a particular goal has no influence on my success in reaching that goal?</a:Text>
 </a:Question>
 <a:Question>
 <a:Answers>
 <a:Answer>
 <a:Points>0</a:Points>
 <a:Text>False</a:Text>
 <a:isCorrect>false</a:isCorrect>
 </a:Answer>
 <a:Answer>
 <a:Points>0</a:Points>
 <a:Text>True</a:Text>
 <a:isCorrect>true</a:isCorrect>
 </a:Answer>
 </a:Answers>
 <a:CorrectAnswer>2</a:CorrectAnswer>
 <a:Text>I will read the next lesson which has the rest of the goal setting information.</a:Text>
 </a:Question>
 </a:Questions>
 <a:Text i:nil="true"/>
 </a:Quiz>
 
 <a:Text></a:Text>
 <a:Title>Reset your Resolutions – A 3-Step Plan Part I</a:Title>
 <a:WeekLabel>Week of 1/2</a:WeekLabel>
 
 */

