//
//  HealthMenuData.h
//  mynuvita
//
//  Created by John on 3/26/14.
//
//

#import <Foundation/Foundation.h>
#import "ResponseStatus.h"

@interface HealthMenuData : NSObject

@property (nonatomic, retain) ResponseStatus            *responseStatus;
@property (nonatomic, retain) NSMutableArray            *items;

- (void)parseXMLFileAtData:(NSMutableData *)data parseError:(NSError **)error;

@end
