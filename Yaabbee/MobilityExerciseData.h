//
//  claimActivityOBJ.h
//  Acclaris
//
//  Created by Sarfaraj Biswas on 09/11/10.
//  Copyright 2010 ObjectSol Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "ResponseStatus.h"

@interface MobilityExerciseData : NSObject<NSXMLParserDelegate> {
		
	NSMutableArray *itemsArray;
	NSMutableDictionary *dictItem;
	
	NSMutableString *contentOfString;
	
	ResponseStatus *responseStatus;
}


@property(nonatomic,retain)NSMutableDictionary *dictMobilityExercise;

@property(nonatomic,retain)NSString *WorkoutName, *WorkoutId, *WorkoutCode;

//
@property(nonatomic,retain)ResponseStatus *responseStatus;

- (void)parseXMLFileAtData:(NSMutableData *)data parseError:(NSError **)error;

- (BOOL)getBoolVal:(NSString*)val;

- (NSString*)getMobilityXml:(NSString*)date;

@end

/*
 
 */
