

#import <Foundation/Foundation.h>


@interface WellnessWallItem : NSObject  {

}


@property(nonatomic,retain)NSString *Desc;
@property(nonatomic,retain)NSString *LessonNumber;
@property(nonatomic,retain)NSString *Passed;
@property(nonatomic,retain)NSString *Title;
//@property(nonatomic,retain)NSString *WeekLabel;


@end


//<a:Desc>Most likley, you belong to one of two camps: You set resolutions every New Year or you don’t.</a:Desc>
//<a:LessonNumber>160</a:LessonNumber>
//<a:Passed>false</a:Passed>
//<a:Title>Reset your Resolutions – A 3-Step Plan Part I</a:Title>