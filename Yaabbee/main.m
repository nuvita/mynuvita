
//
//  main.m
//  Yaabbee
//
//  Created by Srabati on 7/22/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

int main(int argc, char *argv[])
{
    //return UIApplicationMain(argc, argv, nil, NSStringFromClass([OBIAppDelegate class]));
    
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    int retVal = UIApplicationMain(argc, argv, nil, NSStringFromClass([NuvitaAppDelegate class]));
    [pool release];
    return retVal;
}
