//
//  VO2MaxData.m
//  mynuvita
//
//  Created by John on 3/26/14.
//
//

#import "VO2MaxData.h"
#import "ResponseStatus.h"

@interface VO2MaxData () <NSXMLParserDelegate>

@property (nonatomic,retain) ResponseStatus *responseStatus;
@property (nonatomic,retain) NSMutableString *contentOfString;

@end

@implementation VO2MaxData

- (void)parseXMLFileAtData:(NSMutableData *)data parseError:(NSError **)error {
    NSXMLParser *parser;
    parser = [[NSXMLParser alloc] initWithData:data];
    [parser setDelegate:self];
    [parser setShouldProcessNamespaces:NO];
    [parser setShouldReportNamespacePrefixes:NO];
    [parser setShouldResolveExternalEntities:NO];
    [parser parse];

    NSError *parseError = [parser parserError];
    if (parseError && error) {
        *error = parseError;
    }

    [parser release];
}

- (void)parserDidStartDocument:(NSXMLParser *)parser {
	_responseStatus = [[ResponseStatus alloc]init];
//	TeamMembers = [[NSMutableArray alloc]init];
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
	
    if (qName) {
		elementName = qName;
	}
    
    NSLog(@"elementName @ didStartElement: %@", elementName);
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
	
    if (qName) {
		elementName = qName;
	}
    
    NSLog(@"elementName @ didEndElement: %@", elementName);
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
	if(_contentOfString)
		[_contentOfString appendString:string];
}

- (void)parserDidEndDocument:(NSXMLParser *)parse {

}

@end
