    //
//  MobiSuitHomeScreen.m
//  syncClient
//
//  Created by Sarfaraj Biswas on 29/10/11.
//  Copyright 2011 ObjectSol. All rights reserved.
//

#import "MobiSuitUploadQueueScreen.h"
#import "customization.h"
#import "SettingsViewController.h"
#import "constants.h"
#import "AccountViewController.h"
#import "MobiMenuCell.h"

#import "MSULoginViewController.h"
#import "MSUSignupViewController.h"
#import "MSUCaptchaViewController.h"
#import "MainSingleSourceViewController.h"

@implementation MobiSuitUploadQueueScreen

- (void)viewDidLoad 
{
    [super viewDidLoad];
    self.title=@APPNAME;
    
   self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg.png"]];		
   	
    UIBarButtonItem *settingsButton = [[UIBarButtonItem alloc] initWithTitle:@"Exit"
                                                      style:UIBarButtonItemStylePlain
                                                     target:self
                                                     action:@selector(showSettings:)];
    
    //self.navigationItem.rightBarButtonItem = settingsButton;
    
    UIBarButtonItem* signoutButton = [[UIBarButtonItem alloc] initWithTitle:@"SignOut"
													 style:UIBarButtonItemStylePlain
													target:self
													action:@selector(startSyncAll:)];
    
    //self.navigationItem.leftBarButtonItem = signoutButton;
	

    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}


- (void)viewWillAppear:(BOOL)animated 
{
    [super viewWillAppear:animated];
    //syncElementList->read();
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
   }

/*
 - (void)viewWillDisappear:(BOOL)animated {
 [super viewWillDisappear:animated];
 }
 */


/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations.
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */


#pragma mark -
#pragma mark Table view data source

// Customize the number of sections in the table view.
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    //int ret = CustomList::getInstance([NSBundle mainBundle])->count();
    return [[MyController.sharedInstance getUploadImageQ] countItem];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return 118.0;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath 
{
    static NSString *CellIdentifier = @"StatusQueueCell";
    
    PhotoShareQueueCell* cell = (PhotoShareQueueCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        NSArray *topLevelObjects = [[NSBundle mainBundle]
                                    loadNibNamed:@"StatusQueueCell"
                                    owner:nil 
                                    options:nil];
        for (id currentObject in topLevelObjects) {
            if ([currentObject isKindOfClass:[UITableViewCell class]]) {
                cell = (PhotoShareQueueCell*)currentObject;
                break;
            }
        }
    }
	
	
	//@synthesize itemImage;
//	@synthesize dateLb;
//	@synthesize statusLb;
	
	UploadImageInformtion* info = [[MyController.sharedInstance getUploadImageQ] getItem:indexPath.row];
	
	
	
	UIImage* hImage = [UIImage imageWithData: info.imageData];
	 
	[cell.itemImage setImage:hImage forState:UIControlStateNormal ];
	[cell.dateLb setText:info.date];
	
	NSMutableString *satusText=[[NSMutableString alloc] init];
	[satusText appendString:@"Uploading Status -> "];
	
	if(info.isUploading)
		[satusText appendString:@"uploading..."];
	else if(info.isFailed)
		[satusText appendString:@"failed..."];
	else
		[satusText appendString:@"inqueue..."];
	
	[cell.statusLb setText:satusText];
	
	cell.itemImage.tag = indexPath.row;
	
	//[cell.menuItemImage  addTarget:self action:@selector(doMenuItemAction:) forControlEvents:UIControlEventTouchUpInside];
        [cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
       
	if(!info.isUploading && !info.isFailed)
	{
		//remove
		UIImage* image = [UIImage imageNamed:@"icon_tasks.png"];
		
		UIButton *myButton = [UIButton buttonWithType:UIButtonTypeCustom];
		myButton.frame = CGRectMake(self.view.bounds.size.width - image.size.width - 4,  (118 - image.size.height)/2 , image.size.width, image.size.height); // position in the parent view and set the size of the button
		
		[myButton  setBackgroundImage:image forState:UIControlStateNormal];//.backgroundColor = [UIC]
		//[myButton setTitle:@"Click Me!" forState:UIControlStateNormal];
		// add targets and actions
		[myButton addTarget:self action:@selector(buttonClickedRemove:) forControlEvents:UIControlEventTouchUpInside];
		myButton.tag = indexPath.row;
		[cell addSubview:myButton];
		
	}
	else if(!info.isUploading && info.isFailed)
	{
		//remove
		UIImage* image = [UIImage imageNamed:@"icon_tasks.png"];
		
		UIButton *myButton = [UIButton buttonWithType:UIButtonTypeCustom];
		myButton.frame = CGRectMake(self.view.bounds.size.width - image.size.width - 4,  (118 - image.size.height)/2 , image.size.width, image.size.height); // position in the parent view and set the size of the button
		
		[myButton  setBackgroundImage:image forState:UIControlStateNormal];//.backgroundColor = [UIC]
		//[myButton setTitle:@"Click Me!" forState:UIControlStateNormal];
		// add targets and actions
		[myButton addTarget:self action:@selector(buttonClickedRemove:) forControlEvents:UIControlEventTouchUpInside];
		myButton.tag = indexPath.row;
		[cell addSubview:myButton];
		
		//retry
		UIImage* image1 = [UIImage imageNamed:@"done.png"];
		
		myButton = [UIButton buttonWithType:UIButtonTypeCustom];
		myButton.frame = CGRectMake(self.view.bounds.size.width - image.size.width - image1.size.width - 4*2,  (118 - image1.size.height)/2 , image1.size.width, image1.size.height); // position in the parent view and set the size of the button
		
		[myButton  setBackgroundImage:image1 forState:UIControlStateNormal];//.backgroundColor = [UIC]
		//[myButton setTitle:@"Click Me!" forState:UIControlStateNormal];
		// add targets and actions
		[myButton addTarget:self action:@selector(buttonClickedRetry:) forControlEvents:UIControlEventTouchUpInside];
		myButton.tag = indexPath.row;
		[cell addSubview:myButton];
		
	}
	
    [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];	
	
    return cell;
}

#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath 
{
	//[self setToolbarTitle : @"hi sarfaraj"];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
	cell.backgroundColor = [UIColor clearColor];  
}


-(void)buttonClickedRemove:(id)pid
{
	UIButton *myButton = (UIButton*)pid;
	
	[[MyController.sharedInstance getUploadImageQ] removeItemAtIndex:myButton.tag];
	
	[self reloadTable];
}

-(void)buttonClickedRetry:(id)pid
{
	UIButton *myButton = (UIButton*)pid;
	
	[[MyController.sharedInstance getUploadImageQ] retry:myButton.tag];
	
	[self reloadTable];	
}

#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}


- (void)dealloc 
{
	/*
    if (locManager) {
        [locManager stopUpdatingLocation];
        [locManager release];
    }*/
    [super dealloc];
    
}

- (void)reloadTable;//: (id) pid
{
	if([[MyController.sharedInstance getUploadImageQ] countItem] > 0)
	{
		[(UITableView*)self.view reloadData];	
	}	
	else
	{
		[self showAlert:@"Empty status queue"];
		
		[self.navigationController popViewControllerAnimated:YES];
	}	
}

//IBActions

- (IBAction) showSettings: (id) pId
{
    SettingsViewController* vc = [[[SettingsViewController alloc] initWithNibName:@"SettingsViewController" bundle:nil] autorelease];
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)doMenuItemAction:(id)sender
{
	UIButton *btn = (UIButton*)sender;
	
	switch (btn.tag) {
		case 0:
			//SettingsViewController* vc = [[[SettingsViewController alloc] initWithNibName:@"SettingsViewController" bundle:nil] autorelease];
			//[self.navigationController pushViewController:vc animated:YES];	
			break;
		case 1:
			if (CustomList::getInstance(nil)->count() <= 1) {
				MainSingleSourceViewController* vc = [[[MainSingleSourceViewController alloc] initWithNibName:@"MainSingleSourceViewController" bundle:nil] autorelease];
				//NSArray* vcs = [[NSArray alloc] initWithObjects:vc, NULL];
				[self.navigationController pushViewController:vc animated:YES];
				//[vcs release];
			}
			//RootViewController* vc1 = [[[RootViewController alloc] initWithNibName:@"RootViewController" bundle:nil] autorelease];
			//[self.navigationController pushViewController:vc1 animated:YES];	
			break;	
	}
}

- (void) setToolbarTitle: (NSString*)string{
    if(YES){
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 280, 300, 20)];
        label.textAlignment = UITextAlignmentCenter;
        label.backgroundColor = [UIColor clearColor];
        label.text = string;
        label.font = [UIFont boldSystemFontOfSize:12.0];
        UIBarButtonItem *toolBarTitle = [[UIBarButtonItem alloc] initWithCustomView:label];
        
        label.shadowColor = [UIColor blackColor];
        label.shadowOffset = CGSizeMake(0, 1);
        label.textColor = [UIColor whiteColor];
        
        [label release];
        
        NSMutableArray* toolbarItems = [[NSMutableArray arrayWithArray:toolbar.items] retain];
        if ([toolbarItems count] == 0) {
            return;
        }
        [toolbarItems replaceObjectAtIndex:0 withObject:toolBarTitle];
        toolbar.items = toolbarItems;
        
    }
}

- (void)showAlert:(NSString *)alterStr
{
    UIAlertView *myAlertView =  [[UIAlertView alloc] 
                                 initWithTitle:@"Information" 
                                 message:alterStr//@"" 
                                 delegate:nil 
                                 cancelButtonTitle:@"OK" 
                                 otherButtonTitles:nil, nil];
    [myAlertView show];
    [myAlertView release];
}


@end
