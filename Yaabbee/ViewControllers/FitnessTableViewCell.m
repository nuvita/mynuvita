//
//  FitnessTableViewCell.m
//  mynuvita
//
//  Created by John on 8/6/14.
//
//

#import "FitnessTableViewCell.h"

@interface FitnessTableViewCell ()

//...current score
@property (retain, nonatomic) IBOutlet UIImageView *yourTeamImageView;
@property (retain, nonatomic) IBOutlet UIImageView *opponentTeamImageView;
@property (retain, nonatomic) IBOutlet UILabel *scoreLabel;
@property (retain, nonatomic) IBOutlet UILabel *yourTeamLabel;
@property (retain, nonatomic) IBOutlet UILabel *opponentTeamLabel;

//...standing
@property (retain, nonatomic) IBOutlet UIImageView *standingImageView;
@property (retain, nonatomic) IBOutlet UILabel *nameLabel;
@property (retain, nonatomic) IBOutlet UILabel *standingLabel;
@property (retain, nonatomic) IBOutlet UILabel *percentLabel;
@property (retain, nonatomic) IBOutlet UILabel *averageLabel;

//...schedule
@property (retain, nonatomic) IBOutlet UIImageView *scheduleImageView;
@property (retain, nonatomic) IBOutlet UILabel *opponentLabel;
@property (retain, nonatomic) IBOutlet UILabel *resultLabel;
@property (retain, nonatomic) IBOutlet UILabel *recordLabel;

//...pre week
@property (retain, nonatomic) IBOutlet UIImageView *introImageView;
@property (retain, nonatomic) IBOutlet UILabel *introNameLabel;
@property (retain, nonatomic) IBOutlet UILabel *introMinutesLabel;

@end

@implementation FitnessTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)initImage:(NSDictionary *)data {

    UIImageView *imageview = [data valueForKey:@"outlet"];
    imageview.contentMode = UIViewContentModeScaleAspectFit;
    imageview.backgroundColor = [UIColor colorWithWhite:0.5 alpha:0.05];

    imageview.layer.cornerRadius = imageview.frame.size.height / 2;
    imageview.layer.borderWidth = 1.0;
    imageview.layer.borderColor = [UIColor colorWithRed:0.2 green:0.4 blue:0.6 alpha:1.0].CGColor;
    imageview.clipsToBounds = YES;

    UIActivityIndicatorView	*progressIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(imageview.frame.size.width/2 - 10, imageview.frame.size.height/2 - 10, 40, 40)];
    progressIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
    progressIndicator.autoresizingMask = (UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin);
    [progressIndicator sizeToFit];

    [imageview addSubview:progressIndicator];
    [progressIndicator startAnimating];

    NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                       imageview, @"outlet",
                                       progressIndicator, @"indicator",
                                       [data valueForKey:@"icon"], @"icon",
                                       nil];

    [self performSelectorInBackground:@selector(loadImage:) withObject:dictionary];
}

- (void)loadImage:(NSDictionary *)data {

    NSString *urlString = [data valueForKey:@"icon"];
    NSURL *url = [[NSURL alloc] initWithString:urlString];
    NSData *myData = [[NSData alloc] initWithContentsOfURL:url];

    if ([myData length] > 0) {
        UIImage *image = [[UIImage alloc] initWithData:[[NSData alloc] initWithContentsOfURL:url]];
        if (image) {
            UIImageView *imageview = [data valueForKey:@"outlet"];
            [imageview setImage:image];
        }
    }

    UIActivityIndicatorView	*progressIndicator = [data valueForKey:@"indicator"];
    [progressIndicator stopAnimating];
}

- (void)loadCurrentScoreData:(NSDictionary *)data {

    UIImageView *imageview = _opponentTeamImageView;
    [self initImage:[NSDictionary dictionaryWithObjectsAndKeys:
                     [data valueForKey:@"a:OpponentIcon"], @"icon",
                     imageview, @"outlet",
                     nil]];
    [_opponentTeamLabel setText:[data valueForKey:@"a:OpponentName"]];

    imageview = _yourTeamImageView;
    [self initImage:[NSDictionary dictionaryWithObjectsAndKeys:
                     [data valueForKey:@"a:TeamIcon"], @"icon",
                     imageview, @"outlet",
                     nil]];

    [_yourTeamLabel setText:[data valueForKey:@"a:TeamName"]];
    [_scoreLabel setText:[NSString stringWithFormat:@"%@ - %@", [data valueForKey:@"a:TeamScore"], [data valueForKey:@"a:OpponentScore"]]];
}

- (void)loadStandingsData:(NSDictionary *)data {

    UIImageView *imageview = _standingImageView;
    [self initImage:[NSDictionary dictionaryWithObjectsAndKeys:
                     [data valueForKey:@"a:AvatarUrl"], @"icon",
                     imageview, @"outlet",
                     nil]];

    [_nameLabel setText:[data valueForKey:@"a:TeamName"]];
    [_standingLabel setText:[data valueForKey:@"a:WinLossTie"]];
    [_percentLabel setText:[NSString stringWithFormat:@"%0.2f", [[data valueForKey:@"a:WinPercent"] floatValue]]];
    [_averageLabel setText:[data valueForKey:@"a:WinAverage"]];
}

- (void)loadScheduleData:(NSDictionary *)data {

    UIImageView *imageview = _scheduleImageView;
    [self initImage:[NSDictionary dictionaryWithObjectsAndKeys:
                     [data valueForKey:@"a:OpponentAvatarUrl"], @"icon",
                     imageview, @"outlet",
                     nil]];

    [_opponentLabel setText:[data valueForKey:@"a:OpponentName"]];
    [_resultLabel setText:[data valueForKey:@"a:Results"]];
    [_recordLabel setText:[data valueForKey:@"a:Record"]];
}

- (void)loadPreWeekData:(NSDictionary *)data {
    UIImageView *imageview = _introImageView;
    [self initImage:[NSDictionary dictionaryWithObjectsAndKeys:
                     [data valueForKey:@"a:AvatarUrl"], @"icon",
                     imageview, @"outlet",
                     nil]];

    [_introNameLabel setText:[data valueForKey:@"a:Name"]];
    [_introMinutesLabel setText:[data valueForKey:@"a:Minutes"]];
}

- (void)loadCardioWeekData:(NSDictionary *)data {

    CGRect frame = _introImageView.frame;
    frame.origin.x = 15;
    frame.origin.y = 2;
    frame.size.width = 40;
    frame.size.height = 40;
    _introImageView.frame = frame;

    UIImageView *imageview = _introImageView;
    [self initImage:[NSDictionary dictionaryWithObjectsAndKeys:
                     [data valueForKey:@"a:AvatarUrl"], @"icon",
                     imageview, @"outlet",
                     nil]];

    [_introNameLabel setText:[data valueForKey:@"a:Name"]];
    [_introMinutesLabel setText:[data valueForKey:@"a:Minutes"]];
}

@end
