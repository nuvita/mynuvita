//
//  FitnessLeagueViewController.m
//  mynuvita
//
//  Created by John on 7/24/14.
//
//

#import "FitnessLeagueViewController.h"
#import "FitnessDetailViewController.h"
#import "TeamEmailViewController.h"

@interface FitnessLeagueViewController ()

@property (retain, nonatomic) IBOutlet UITableView *leftTableView;
@property (retain, nonatomic) IBOutlet UITableView *rightTableView;
@property (retain, nonatomic) IBOutlet UILabel *weeklbl;
@property (retain, nonatomic) IBOutlet UIImageView *yourTeamImageView;
@property (retain, nonatomic) IBOutlet UIImageView *opponentTeamImageView;
@property (retain, nonatomic) IBOutlet UILabel *yourTeamLabel;
@property (retain, nonatomic) IBOutlet UILabel *opponentTeamLabel;
@property (retain, nonatomic) IBOutlet UILabel *yourTeamScore;
@property (retain, nonatomic) IBOutlet UILabel *opponentTeamScore;
@property (retain, nonatomic) IBOutlet UILabel *programlbl;

@property (retain, nonatomic) NSDate *dateSelected;

@property (retain, nonatomic) NSMutableArray *leftTeamList;
@property (retain, nonatomic) NSMutableArray *rightTeamList;
@property (retain, nonatomic) NSMutableDictionary *data;

@property (retain, nonatomic) JBLoadingView *loadingView;

//... intro week
@property (retain, nonatomic) IBOutlet UITableView *introTableView;
@property (retain, nonatomic) NSMutableArray *introWeekList;

//... fitness bowl
@property (retain, nonatomic) IBOutlet UITableView *bowlTableView;
@property (retain, nonatomic) NSMutableArray *bowlWeekList;

//... cardio bowl
@property (retain, nonatomic) IBOutlet UITableView *cardioTableView;
@property (retain, nonatomic) NSMutableArray *cardioWeekList;

@property (retain, nonatomic) JBCustomButton *buttonPrev;
@property (retain, nonatomic) JBCustomButton *buttonNext;

@end

@implementation FitnessLeagueViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.tabBarItem.title = @"Home";
        self.tabBarItem.image = [UIImage imageNamed:@"home.png"];

        _leftTeamList = [[NSMutableArray alloc] init];
        _rightTeamList = [[NSMutableArray alloc] init];
        _introWeekList = [[NSMutableArray alloc] init];
        _bowlWeekList = [[NSMutableArray alloc] init];
        _cardioWeekList = [[NSMutableArray alloc] init];
    }

    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.]
    _data = [[NSMutableDictionary alloc] init];
    _dateSelected = [NSDate date];
    _loadingView = [[JBLoadingView alloc] init];

    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Logout"
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:self
                                                                            action:@selector(logoutClicked)];
    [self addCustomBarButtonArrow];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];

    _programlbl.text = [[LoginPerser getLoginResponse] programName];
    [self getNFLWeekType];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Public Methods

- (void)logoutClicked {
    //	daysNextPrev = 0;
    //	self.view.hidden = YES;
    //
    //    isLoaded = NO;
    //	self.today = nil;

    [[NSNotificationCenter defaultCenter] postNotificationName:@"LOGOUT" object:nil];
}

- (void)initImage:(NSDictionary *)data {

    UIImageView *imageview = [data valueForKey:@"outlet"];

    CGRect frame = [imageview frame];

    if (frame.size.height != frame.size.width) {
        int diff = (frame.size.width - frame.size.height) / 2;
        frame.origin.x += diff;
    }

    frame.size.width = frame.size.height;
    imageview.frame = frame;


    imageview.contentMode = UIViewContentModeScaleAspectFit;
    imageview.backgroundColor = [UIColor colorWithWhite:0.5 alpha:0.05];
    imageview.layer.cornerRadius = imageview.frame.size.height / 2;
    imageview.layer.borderWidth = 1.0;
    imageview.layer.borderColor = [UIColor colorWithRed:0.2 green:0.4 blue:0.6 alpha:1.0].CGColor;
    imageview.clipsToBounds = YES;

    UIActivityIndicatorView	*progressIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(imageview.frame.size.width/2 - 10, imageview.frame.size.height/2 - 10, 40, 40)];
    progressIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
    progressIndicator.autoresizingMask = (UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin);
    [progressIndicator sizeToFit];

    [imageview addSubview:progressIndicator];
    [progressIndicator startAnimating];

    NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                       imageview, @"outlet",
                                       progressIndicator, @"indicator",
                                       [data valueForKey:@"icon"], @"icon",
                                       nil];

    [self performSelectorInBackground:@selector(loadImage:) withObject:dictionary];
}

- (void)loadImage:(NSDictionary *)data {

    NSString *urlString = [data valueForKey:@"icon"];
    NSURL *url = [[NSURL alloc] initWithString:urlString];
    NSData *myData = [[NSData alloc] initWithContentsOfURL:url];

    if ([myData length] > 0) {
        UIImage *image = [[UIImage alloc] initWithData:[[NSData alloc] initWithContentsOfURL:url]];
        if (image) {
            UIImageView *imageview = [data valueForKey:@"outlet"];
            [imageview setImage:image];
        }
    }

    UIActivityIndicatorView	*progressIndicator = [data valueForKey:@"indicator"];
    [progressIndicator stopAnimating];
}

- (void)addCustomBarButtonArrow {

    _buttonPrev =  [[JBCustomButton alloc] initWithFrame:CGRectMake(0, 20, 30, 30)];
    [_buttonPrev setButtonStyle:JBCustomButtonArrowLeftStyle];
	[_buttonPrev addTarget:self action:@selector(didTapPrevWeek) forControlEvents:UIControlEventTouchUpInside];

    _buttonNext =  [[JBCustomButton alloc] initWithFrame:CGRectMake(0, 20, 30, 30)];
    [_buttonNext setButtonStyle:JBCustomButtonArrowRightStyle];
	[_buttonNext addTarget:self action:@selector(didTapNextWeek) forControlEvents:UIControlEventTouchUpInside];

	self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:
                                               [[UIBarButtonItem alloc] initWithCustomView:_buttonNext],
                                               [[UIBarButtonItem alloc] initWithCustomView:_buttonPrev], nil];
}

- (void)loadInfo:(NSDictionary *)info {
    [_data setDictionary:info];

    [_leftTeamList removeAllObjects];
    [_rightTeamList removeAllObjects];
    [_weeklbl setText:[info valueForKey:@"a:WeekLabel"]];
    _programlbl.text = [[LoginPerser getLoginResponse] programName];

    [self initImage:[NSDictionary dictionaryWithObjectsAndKeys:
                     _yourTeamImageView, @"outlet",
                     [info valueForKey:@"a:TeamIcon"], @"icon",
                     nil]];
    [_yourTeamLabel setText:[info valueForKey:@"a:TeamName"]];
    [_yourTeamScore setText:[info valueForKey:@"a:TeamScore"]];

    [self initImage:[NSDictionary dictionaryWithObjectsAndKeys:
                     _opponentTeamImageView, @"outlet",
                     [info valueForKey:@"a:OpponentIcon"], @"icon",
                     nil]];
    [_opponentTeamLabel setText:[info valueForKey:@"a:OpponentName"]];
    [_opponentTeamScore setText:[info valueForKey:@"a:OpponentScore"]];

    [_leftTeamList addObjectsFromArray:[info valueForKey:@"a:TeamMembers"]];
    [_rightTeamList addObjectsFromArray:[info valueForKey:@"a:OpponentTeamMembers"]];
    [_leftTableView reloadData];
    [_rightTableView reloadData];
}

- (void)loadPreWeekData:(NSDictionary *)info {

    _programlbl.text = [[LoginPerser getLoginResponse] programName];
    _weeklbl.text = [info objectForKey:@"a:WeekLabel"];

    [_introWeekList removeAllObjects];
    [_introWeekList addObjectsFromArray:[info objectForKey:@"a:NFLMembers"]];

    [_introTableView reloadData];
}

- (void)loadFitnessBowlData:(NSDictionary *)info {

    _programlbl.text = [[LoginPerser getLoginResponse] programName];
    _weeklbl.text = [info objectForKey:@"a:WeekLabel"];

    [_bowlWeekList removeAllObjects];
    [_bowlWeekList addObjectsFromArray:[info objectForKey:@"a:NFLMatches"]];

    [_bowlTableView reloadData];
}

- (void)loadCardioBowlWeekData:(NSDictionary *)info {

    _programlbl.text = [[LoginPerser getLoginResponse] programName];
    _weeklbl.text = [info objectForKey:@"a:WeekLabel"];

    [_cardioWeekList removeAllObjects];
    [_cardioWeekList addObjectsFromArray:[info objectForKey:@"a:NFLCBTeams"]];

    [_cardioTableView reloadData];
}

- (NSDate *)getDateFromString:(NSString *)stringDate {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM-dd-yyyy"];

    return [dateFormatter dateFromString:stringDate];
}

- (NSString *)getDateFromDate:(NSDate *)date {

    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM-dd-yyyy"];

    return [dateFormatter stringFromDate:date];
}

- (void)getWeekDateByAdding:(BOOL)add {
    //    _adding = add;
    NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
    dayComponent.day = (add) ? 7 : -7;

    NSCalendar *theCalendar = [NSCalendar currentCalendar];
    NSDate *nextDate = [theCalendar dateByAddingComponents:dayComponent toDate:_dateSelected options:0];
    _dateSelected = nextDate;
    [self getNFLWeekType];
}

- (void)didTapPrevWeek {
    [self getWeekDateByAdding:NO];

}

- (void)didTapNextWeek {
    [self getWeekDateByAdding:YES];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if ([tableView isEqual:_cardioTableView])
        return [_cardioWeekList count];

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([tableView isEqual:_introTableView])
        return [_introWeekList count];

    if ([tableView isEqual:_bowlTableView])
        return [_bowlWeekList count];

    if ([tableView isEqual:_cardioTableView])
        return [[[_cardioWeekList objectAtIndex:section] objectForKey:@"a:TeamMembers"] count];

    return [tableView tag] == 0 ? [_leftTeamList count] : [_rightTeamList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];

    if ([tableView isEqual:_introTableView]) {

        FitnessTableViewCell *cell = (FitnessTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"Cell"];
        if (cell == nil) {

            NSArray *nibs = [[NSBundle mainBundle] loadNibNamed:@"FitnessTableViewCell" owner:self options:nil];
            cell = [nibs objectAtIndex:3];
        }

        [cell loadPreWeekData:[_introWeekList objectAtIndex:indexPath.row]];
        return cell;
    }

    if ([tableView isEqual:_bowlTableView]) {

        FitnessTableViewCell *cell = (FitnessTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"Cell"];
        if (cell == nil) {

            NSArray *nibs = [[NSBundle mainBundle] loadNibNamed:@"FitnessTableViewCell" owner:self options:nil];
            cell = [nibs objectAtIndex:0];
        }

        [cell loadCurrentScoreData:[_bowlWeekList objectAtIndex:indexPath.row]];
        return cell;
    }

    if ([tableView isEqual:_cardioTableView]) {

        FitnessTableViewCell *cell = (FitnessTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"Cell"];
        if (cell == nil) {

            NSArray *nibs = [[NSBundle mainBundle] loadNibNamed:@"FitnessTableViewCell" owner:self options:nil];
            cell = [nibs objectAtIndex:3];
            cell.backgroundColor = [UIColor clearColor];
        }

        [cell loadCardioWeekData:[[[_cardioWeekList objectAtIndex:indexPath.section] objectForKey:@"a:TeamMembers"] objectAtIndex:indexPath.row]];
        return cell;
    }

    cell.textLabel.font = [UIFont boldSystemFontOfSize:12.0];
    cell.detailTextLabel.font = [UIFont boldSystemFontOfSize:12.0];
    cell.detailTextLabel.textColor = [UIColor blackColor];

    if ([tableView tag] == 0) {
        NSArray *array = [[[_leftTeamList objectAtIndex:indexPath.row] valueForKey:@"a:Name"] componentsSeparatedByString:@" "];
        cell.textLabel.text = [array firstObject];
        cell.detailTextLabel.text = [array lastObject];

        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 40, 22)];
        label.backgroundColor = [UIColor clearColor];
        label.textAlignment = NSTextAlignmentRight;
        label.font = [UIFont boldSystemFontOfSize:18];
        label.text = [[_leftTeamList objectAtIndex:indexPath.row] objectForKey:@"a:Minutes"];
        cell.accessoryView = label;

    } else {
        NSArray *array = [[[_rightTeamList objectAtIndex:indexPath.row] valueForKey:@"a:Name"] componentsSeparatedByString:@" "];
        cell.textLabel.text = [array firstObject];
        cell.detailTextLabel.text = [array lastObject];

        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 40, 22)];
        label.backgroundColor = [UIColor clearColor];
        label.textAlignment = NSTextAlignmentRight;
        label.font = [UIFont boldSystemFontOfSize:18];
        label.text = [[_rightTeamList objectAtIndex:indexPath.row] objectForKey:@"a:Minutes"];
        cell.accessoryView = label;
    }

    return cell;
}

#pragma mark - UITableViewDelegate

- (UIView *)loadPreWeekHeader {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 30)];
    view.backgroundColor = [UIColor colorWithWhite:0.5 alpha:0.05];

    UIFont *font = [UIFont systemFontOfSize:14.0];

    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, 175, 21)];
    label1.font = font;
    label1.backgroundColor = [UIColor clearColor];
    label1.text = @"Participants";

    UILabel *label3 = [[UILabel alloc] initWithFrame:CGRectMake(222, 5, 100, 21)];
    label3.textAlignment = NSTextAlignmentCenter;
    label3.font = font;
    label3.backgroundColor = [UIColor clearColor];
    label3.text = @"IAZ Min";

    [view addSubview:label1];
    [view addSubview:label3];

    return view;
}

- (UIView *)loadCardioBowlWeekHeader:(NSDictionary *)info {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
    view.backgroundColor = [UIColor clearColor];

    UIImageView *teamIconView = [[UIImageView alloc] initWithFrame:CGRectMake(5, 5, 40, 40)];
    [self initImage:[NSDictionary dictionaryWithObjectsAndKeys:
                     teamIconView, @"outlet",
                     [NSString stringWithFormat:@"http://live.mynuvita.com%@", [info valueForKey:@"a:TeamIcon"]], @"icon",
                     nil]];

    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(60, 15, 175, 20)];
    label1.font = [UIFont boldSystemFontOfSize:18.0];
    label1.backgroundColor = [UIColor clearColor];
    label1.text = [info objectForKey:@"a:TeamName"];

    UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(260, 15, 50, 25)];
    label2.font = [UIFont boldSystemFontOfSize:25];
    label2.backgroundColor = [UIColor clearColor];
    label2.textAlignment = NSTextAlignmentCenter;
    label2.text = [info objectForKey:@"a:TeamScore"];

    [view addSubview:teamIconView];
    [view addSubview:label1];
    [view addSubview:label2];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if ([tableView isEqual:_introTableView])
        return 30;

    if ([tableView isEqual:_cardioTableView])
        return 50;

    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if ([tableView isEqual:_introTableView])
        return [self loadPreWeekHeader];

    if ([tableView isEqual:_cardioTableView])
        return [self loadCardioBowlWeekHeader:[_cardioWeekList objectAtIndex:section]];

    return nil;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {

    if ([cell respondsToSelector:@selector(tintColor)]) {

        if (tableView == _cardioTableView) {
            CGFloat cornerRadius = 5.f;
            cell.backgroundColor = UIColor.clearColor;
            CAShapeLayer *layer = [[CAShapeLayer alloc] init];
            CGMutablePathRef pathRef = CGPathCreateMutable();
            CGRect bounds = CGRectInset(cell.bounds, 10, 0);

            BOOL addLine = NO;
            if (indexPath.row == 0 && indexPath.row == [tableView numberOfRowsInSection:indexPath.section]-1) {
                CGPathAddRoundedRect(pathRef, nil, bounds, cornerRadius, cornerRadius);
            } else if (indexPath.row == 0) {
                CGPathMoveToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMaxY(bounds));
                CGPathAddArcToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMinY(bounds), CGRectGetMidX(bounds), CGRectGetMinY(bounds), cornerRadius);
                CGPathAddArcToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMinY(bounds), CGRectGetMaxX(bounds), CGRectGetMidY(bounds), cornerRadius);
                CGPathAddLineToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMaxY(bounds));
                addLine = YES;
            } else if (indexPath.row == [tableView numberOfRowsInSection:indexPath.section]-1) {
                CGPathMoveToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMinY(bounds));
                CGPathAddArcToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMaxY(bounds), CGRectGetMidX(bounds), CGRectGetMaxY(bounds), cornerRadius);
                CGPathAddArcToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMaxY(bounds), CGRectGetMaxX(bounds), CGRectGetMidY(bounds), cornerRadius);
                CGPathAddLineToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMinY(bounds));
            } else {
                CGPathAddRect(pathRef, nil, bounds);
                addLine = YES;
            }

            layer.path = pathRef;
            CFRelease(pathRef);
            layer.fillColor = [UIColor colorWithWhite:1.0f alpha:0.8f].CGColor;

            if (addLine == YES) {
                CALayer *lineLayer = [[CALayer alloc] init];
                CGFloat lineHeight = (1.f / [UIScreen mainScreen].scale);
                lineLayer.frame = CGRectMake(CGRectGetMinX(bounds), bounds.size.height-lineHeight, bounds.size.width, lineHeight);
                lineLayer.backgroundColor = tableView.separatorColor.CGColor;
                [layer addSublayer:lineLayer];
            }

            UIView *testView = [[UIView alloc] initWithFrame:bounds];
            [testView.layer insertSublayer:layer atIndex:0];
            testView.backgroundColor = [UIColor clearColor];
            cell.backgroundView = testView;

            UIView* selectedBackgroundView = [[UIView alloc] initWithFrame:bounds];
            UIView* visibleSelectedBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(10, 0.0f, bounds.size.width, bounds.size.height)];
            visibleSelectedBackgroundView.backgroundColor = tableView.separatorColor;
            [selectedBackgroundView addSubview:visibleSelectedBackgroundView];
            cell.selectedBackgroundView = selectedBackgroundView;
        }
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    //    [self showFitnessLeagueDetails];
}

#pragma mark - Web Service

- (UIImage *)captureView:(UIView *)view {

    CALayer *layer = view.layer;
    UIGraphicsBeginImageContext(view.bounds.size);
    CGContextClipToRect(UIGraphicsGetCurrentContext(), view.bounds);
    [layer renderInContext:UIGraphicsGetCurrentContext()];

    UIImage *screenImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return screenImage;
}

- (void)getNFLWeekType {
    [_loadingView loadingViewStartAnimating:self.view
                            withLoadingView:[_loadingView createLoadingView:self.view]
                                       text:@"Loading data. Please wait..."];
    //...limit
    _buttonNext.enabled = ![[self getDateFormat:_dateSelected] isEqualToString:[self getDateFormat:[NSDate date]]];

    NSString *dateString = [self getDateFromDate:_dateSelected];
    LoginResponseData *loginData = [LoginPerser getLoginResponse];

    NSDictionary *info = [NSDictionary dictionaryWithObjectsAndKeys:
                          dateString, @"Date",
                          [loginData memberID], @"memberId",
                          nil];

    request *r = [[request alloc] initWithTarget:self
                                   SuccessAction:@selector(onSuccessfulFetchNFLWeekType:resData:)
                                   FailureAction:@selector(onFailedFetch:)];
    r.isGenericRequest = YES;
    [r getNFLData:info withService:@"GetNFLWeekType"];
}

- (NSString *)getDateFormat:(NSDate *)date {
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM-dd-yyyy"];

    return [dateFormat stringFromDate:date];
}

- (void)getNFLTeam {

    NSString *dateString = [self getDateFromDate:_dateSelected];
    LoginResponseData *loginData = [LoginPerser getLoginResponse];

    NSDictionary *data = [NSDictionary dictionaryWithObjectsAndKeys:
                          dateString, @"Date",
                          [loginData memberID], @"memberId",
                          nil];

    request *r = [[request alloc] initWithTarget:self
                                   SuccessAction:@selector(onSuccessfulFetchNFLTeam:resData:)
                                   FailureAction:@selector(onFailedFetch:)];
    [r getNFLTeam:data];
}

- (void)getNFLIntroTeam {

    NSString *dateString = [self getDateFromDate:_dateSelected];
    LoginResponseData *loginData = [LoginPerser getLoginResponse];

    NSDictionary *info = [NSDictionary dictionaryWithObjectsAndKeys:
                          dateString, @"Date",
                          [loginData memberID], @"memberId",
                          nil];

    request *r = [[request alloc] initWithTarget:self
                                   SuccessAction:@selector(onSuccessfulFetchNFLPreWeek:resData:)
                                   FailureAction:@selector(onFailedFetch:)];
    r.isGenericRequest = YES;
    [r getNFLData:info withService:@"GetNFLPreWeek"];
}

- (void)getNFLFitnessBowl {

    NSString *dateString = [self getDateFromDate:_dateSelected];
    LoginResponseData *loginData = [LoginPerser getLoginResponse];

    NSDictionary *info = [NSDictionary dictionaryWithObjectsAndKeys:
                          dateString, @"Date",
                          [loginData memberID], @"memberId",
                          nil];

    request *r = [[request alloc] initWithTarget:self
                                   SuccessAction:@selector(onSuccessfulFetchNFLFitnessBowlWeek:resData:)
                                   FailureAction:@selector(onFailedFetch:)];
    r.isGenericRequest = YES;
    [r getNFLData:info withService:@"GetNFLFitnessBowlWeek"];
}

- (void)getNFLCardioBowl {

    NSString *dateString = [self getDateFromDate:_dateSelected];
    LoginResponseData *loginData = [LoginPerser getLoginResponse];

    NSDictionary *info = [NSDictionary dictionaryWithObjectsAndKeys:
                          dateString, @"Date",
                          [loginData memberID], @"memberId",
                          nil];

    request *r = [[request alloc] initWithTarget:self
                                   SuccessAction:@selector(onSuccessfulFetchNFLCardioBowlWeek:resData:)
                                   FailureAction:@selector(onFailedFetch:)];
    r.isGenericRequest = YES;
    [r getNFLData:info withService:@"GetNFLCardioBowlWeek"];
}

- (void)showAlertMessage:(NSString *)message {
    UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"Error"
                                                        message:message
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
    [alertview show];
}

- (void)onSuccessfulFetchNFLWeekType:(ResponseStatus *)response resData:(NSObject *)obj {

    if (response == nil && ![response isSuccess]) {
        [self showAlertMessage:@"No records found!"];
        return;
    }


    NSDictionary *data = [(NuvitaXMLParser *)obj info];
    NSString *weekType = [data objectForKey:@"GetNFLWeekTypeResult"];

    if ([weekType isEqualToString:@"None"]) {
        [[NSBundle mainBundle] loadNibNamed:@"FitnessDefaultViewController" owner:self options:nil];
        [_loadingView loadingViewStopAnimating:self.view
                                    withPrompt:@"No records found!"];

        return;
    }

    if ([weekType isEqualToString:@"Intro"]) {
        [self getNFLIntroTeam];
        return;
    }

    if ([weekType isEqualToString:@"Fitnes Bowl"]) {
        [self getNFLFitnessBowl];
        return;
    }

    if ([weekType isEqualToString:@"Cardio Bowl"]) {
        [self getNFLCardioBowl];
        return;
    }

    if ([weekType isEqualToString:@"Program"]) {
        [self getNFLTeam];
    }
}

- (void)onSuccessfulFetchNFLCardioBowlWeek:(ResponseStatus *)response resData:(NSObject *)obj {

    if (response == nil && ![response isSuccess]) {
        [self showAlertMessage:@"No records found!"];
        return;
    }

    [[NSBundle mainBundle] loadNibNamed:@"CardioBowlViewController" owner:self options:nil];
    NSDictionary *info = [(NuvitaXMLParser *)obj info];
    [self loadCardioBowlWeekData:info];
}

- (void)onSuccessfulFetchNFLPreWeek:(ResponseStatus *)response resData:(NSObject *)obj {

    if (response == nil && ![response isSuccess]) {
        [self showAlertMessage:@"No records found!"];
        return;
    }

    [[NSBundle mainBundle] loadNibNamed:@"IntroViewController" owner:self options:nil];
    [self loadPreWeekData:[(NuvitaXMLParser *)obj info]];
    [_loadingView loadingViewStopAnimating:self.view];
}

- (void)onSuccessfulFetchNFLFitnessBowlWeek:(ResponseStatus *)response resData:(NSObject *)obj {

    if (response == nil && ![response isSuccess]) {
        [self showAlertMessage:@"No records found!"];
        return;
    }

    [[NSBundle mainBundle] loadNibNamed:@"FitnessBowlViewController" owner:self options:nil];
    [self loadFitnessBowlData:[(NuvitaXMLParser *)obj info]];
    [_loadingView loadingViewStopAnimating:self.view];
}

- (void)onSuccessfulFetchNFLTeam:(ResponseStatus *)response resData:(NSObject *)obj {

    if (response == nil && ![response isSuccess]) {
        [self showAlertMessage:@"No records found!"];
        return;
    }

    [[NSBundle mainBundle] loadNibNamed:@"FitnessLeagueViewController" owner:self options:nil];
    NSDictionary *info = [(NuvitaXMLParser *)obj info];
    [self loadInfo:info];
}

- (void)onFailedFetch:(NSString *)error {

    if ([error isEqualToString:@"internal server error"]) {
        [[NSBundle mainBundle] loadNibNamed:@"FitnessDefaultViewController" owner:self options:nil];
        [_loadingView loadingViewStopAnimating:self.view];

        [self showAlertMessage:@"Ops! Internal server went wrong!"];
    }
}

#pragma mark - MFMailComposeViewControllerDelegate

- (IBAction)openMailForm {
    if (_data) {
        TeamEmailViewController *teamEmailViewController = [[TeamEmailViewController alloc] init];
        teamEmailViewController.data = _data;
        [self.navigationController pushViewController:teamEmailViewController animated:YES];
    }
}

@end