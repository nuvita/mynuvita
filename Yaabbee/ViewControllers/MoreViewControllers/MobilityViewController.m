//
//  MoreViewController.m
//  Yaabbee
//
//  Created by Sarfaraj Biswas on 7/22/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "MobilityViewController.h"

#import "Profile.h"
#import "constant.h"

@interface MobilityViewController ()

@property (nonatomic, retain) JBCustomButton *buttonPrev;
@property (nonatomic, retain) JBCustomButton *buttonNext;

@end

@implementation MobilityViewController
@synthesize tableView_, subheaderLb, subheaderLb2, today,btnSave;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

	isLoaded = NO;
	tools = [[MyTools alloc]init];
	loadingView = [tools createActivityIndicator1];	

    _buttonPrev =  [[JBCustomButton alloc] initWithFrame:CGRectMake(0, 20, 30, 30)];
    [_buttonPrev setButtonStyle:JBCustomButtonArrowLeftStyle];
	[_buttonPrev addTarget:self action:@selector(movePrevClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    _buttonNext =  [[JBCustomButton alloc] initWithFrame:CGRectMake(0, 20, 30, 30)];
    [_buttonNext setButtonStyle:JBCustomButtonArrowRightStyle];
	[_buttonNext addTarget:self action:@selector(moveNextClicked:) forControlEvents:UIControlEventTouchUpInside];
	
	self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:
                                               [[UIBarButtonItem alloc] initWithCustomView:_buttonNext],
                                               [[UIBarButtonItem alloc] initWithCustomView:_buttonPrev], nil];
	
	daysNextPrev = 0;
	self.view.hidden = YES;
	
	isLoaded= NO;	
	
	self.subheaderLb.font = [UIFont fontWithName:DEFAULT_BOLD_FONT_NAME size:defaultFontSize];
	self.subheaderLb2.font = [UIFont fontWithName:DEFAULT_BOLD_FONT_NAME size:defaultFontSize];
	
	self.btnSave.titleLabel.font = [UIFont fontWithName:DEFAULT_BOLD_FONT_NAME size:defaultFontSize];
	[self.btnSave setTitle:@"Save" forState:UIControlStateNormal];
    
//    if([NuvitaAppDelegate isPhone5]) {
//        self.btnSave.center = CGPointMake(self.btnSave.center.x, self.btnSave.center.y + 80);
//    }
//    
//    [self updateUI];
//    
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	if(!isLoaded)
		[self sendRequest:YES];
	
	[self manipulateNextPrev];
}

- (void)updateUI {
    if ([NuvitaAppDelegate isIOS7]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        
        CGRect frame = self.tableView_.frame;
        frame.origin.y -= 25;
        frame.size.height += 30;
        
        self.tableView_.frame = frame;
    }
}

- (void) backSelected:(BOOL)selected
{
	isLoaded = selected;
}

- (void)createRightBarItem
{
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Login" style:UIBarButtonItemStyleBordered target:self action:@selector(login)];
	self.navigationItem.rightBarButtonItem = barButtonItem;
}

- (void)login {
    
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    //self.tableView = nil;
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	if(mobilityWeekData == nil)
		return 0;
	else
		return 8;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([NuvitaAppDelegate isPhone5])
        return 44.0;
    
    return 36.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = nil;
    NSString *evenCell = CELL_EVEN;
    NSString *oddCell = CELL_ODD;
    
    if (fmod(indexPath.row, 2) == 0)
        CellIdentifier = evenCell;
    
    else
        CellIdentifier = oddCell;

    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
       		
    }
    
	//tableView.style=UITableViewCellStyle.
    [self configureCell:cell withIndex:indexPath];     	
    // Configure the cell...
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 39;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    CGSize size = CGSizeMake(211, 39);
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, size.height)];
    UIButton *saveBtn = [[UIButton alloc] initWithFrame:
                         CGRectMake((view.frame.size.width / 2) - (size.width *.9 / 2), size.height * .05, size.width * .9, size.height * .9)];
    [saveBtn setBackgroundImage:[UIImage imageNamed:@"wild_ones_redo.png"] forState:UIControlStateNormal];
    [saveBtn setTitleColor:[UIColor colorWithRed:0.2 green:0.4 blue:0.7 alpha:1.0] forState:UIControlStateNormal];
    [saveBtn setTitle:@"Save" forState:UIControlStateNormal];
    [saveBtn addTarget:self
                action:@selector(btnSaveClicked:)
      forControlEvents:UIControlEventTouchUpInside];


    [view addSubview:saveBtn];
    return view;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
   
	switch (indexPath.row) {
        case 0:
			if ([mobilityWeekData getBoolVal:mobilityWeekData.isMobilityX] ) 
			{
				if ([mobilityWeekData getBoolVal:mobilityWeekData.MondaySelect]) {
					[self nextScreen:0];
				}
				return;
			}
			
			if([mobilityWeekData getBoolVal:mobilityWeekData.MondaySelect])		
				mobilityWeekData.MondaySelect = @"false";
			else
				mobilityWeekData.MondaySelect = @"true";
            break;
        case 1:
			if ([mobilityWeekData getBoolVal:mobilityWeekData.isMobilityX] ) 
			{
				if ([mobilityWeekData getBoolVal:mobilityWeekData.TuesdaySelect]) {
					[self nextScreen:1];
				}
				return;
			}
			
			if([mobilityWeekData getBoolVal:mobilityWeekData.TuesdaySelect])		
				mobilityWeekData.TuesdaySelect = @"false";
			else
				mobilityWeekData.TuesdaySelect = @"true";
            break;
	case 2:
			if ([mobilityWeekData getBoolVal:mobilityWeekData.isMobilityX] ) 
			{
				if ([mobilityWeekData getBoolVal:mobilityWeekData.WednesdaySelect]) {
					[self nextScreen:2];
				}
				return;
			}
			
			if([mobilityWeekData getBoolVal:mobilityWeekData.WednesdaySelect])		
				mobilityWeekData.WednesdaySelect = @"false";
			else
				mobilityWeekData.WednesdaySelect = @"true";            
			break;
	case 3:
			if ([mobilityWeekData getBoolVal:mobilityWeekData.isMobilityX] ) 
			{
				if ([mobilityWeekData getBoolVal:mobilityWeekData.ThursdaySelect]) {
					[self nextScreen:3];
				}
				return;
			}
			
			if([mobilityWeekData getBoolVal:mobilityWeekData.ThursdaySelect])		
				mobilityWeekData.ThursdaySelect = @"false";
			else
				mobilityWeekData.ThursdaySelect = @"true";            
			break;
	case 4:
			if ([mobilityWeekData getBoolVal:mobilityWeekData.isMobilityX] ) 
			{
				if ([mobilityWeekData getBoolVal:mobilityWeekData.FridaySelect]) {
					[self nextScreen:4];
				}
				return;
			}
			
			if([mobilityWeekData getBoolVal:mobilityWeekData.FridaySelect])		
				mobilityWeekData.FridaySelect = @"false";
			else
				mobilityWeekData.FridaySelect = @"true";
            break;
	case 5:
			if ([mobilityWeekData getBoolVal:mobilityWeekData.isMobilityX] ) 
			{
				if ([mobilityWeekData getBoolVal:mobilityWeekData.SaturdaySelect]) {
					[self nextScreen:5];
				}
				return;
			}
			
			if([mobilityWeekData getBoolVal:mobilityWeekData.SaturdaySelect])		
				mobilityWeekData.SaturdaySelect = @"false";
			else
				mobilityWeekData.SaturdaySelect = @"true";
            break;
	case 6:
			if ([mobilityWeekData getBoolVal:mobilityWeekData.isMobilityX] ) 
			{
				if ([mobilityWeekData getBoolVal:mobilityWeekData.SundaySelect]) {
					[self nextScreen:6];
				}
				return;
			}
			
			if([mobilityWeekData getBoolVal:mobilityWeekData.SundaySelect])		
				mobilityWeekData.SundaySelect = @"false";
			else
				mobilityWeekData.SundaySelect = @"true";
            break;
        case 7:
            {
                WorkoutsViewController *vc = [[WorkoutsViewController alloc] initWithNibName:@"WorkoutsViewController" bundle:nil];	
            vc.today = self.today;//mobilityWeekData.Date;
            //vc.delegate=self;
                [self.navigationController pushViewController:vc animated:YES];
            }
            break;		
        default:
            break;
    }
	
	[tableView reloadData];
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
{
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	
	//MobilityWorkoutViewController *vc = [[MobilityWorkoutViewController alloc] initWithNibName:@"MobilityWorkoutViewController" bundle:nil];	
//	vc.dateString = mobilityWeekData.Date;
//	[self.navigationController pushViewController:vc animated:YES];
//	
//	[vc release];
}

- (void)configureCell:(UITableViewCell *)cell withIndex:(NSIndexPath *)indexPath{
    NSString *contentText = nil;
    NSString *dayText = @"";
    UIImage *cellImage = nil;
    BOOL flag = YES;
    switch (indexPath.row) {
        case 0:
            contentText = @"Monday";
			dayText = mobilityWeekData.MondayText;		
			if([mobilityWeekData getBoolVal:mobilityWeekData.MondaySelect])		
			   cellImage = [self getCellIconImage];
			else 
			   flag=NO;
            break;
        case 1:
            contentText = @"Tuesday";
			dayText = mobilityWeekData.TuesdayText;	
			if([mobilityWeekData getBoolVal:mobilityWeekData.TuesdaySelect])
			 cellImage = [self getCellIconImage];
			else 
				flag=NO;

            break;
	case 2:
            contentText = @"Wednesday";
			dayText = mobilityWeekData.WednesdayText;	
			if([mobilityWeekData getBoolVal:mobilityWeekData.WednesdaySelect])
			 cellImage = [self getCellIconImage];
			else 
				flag=NO;

            break;
	case 3:
            contentText = @"Thursday";
			dayText = mobilityWeekData.ThursdayText;	
	      if([mobilityWeekData getBoolVal:mobilityWeekData.ThursdaySelect])		
			  cellImage = [self getCellIconImage];
		  else 
			  flag=NO;

            break;
	case 4:
            contentText = @"Friday";
			dayText = mobilityWeekData.FridayText;	
	     if([mobilityWeekData getBoolVal:mobilityWeekData.FridaySelect])		
			 cellImage = [self getCellIconImage];
		 else 
			 flag=NO;

            break;
	case 5:
            contentText = @"Saturday";
			dayText = mobilityWeekData.SaturdayText;	
			if([mobilityWeekData getBoolVal:mobilityWeekData.SaturdaySelect])		
			 cellImage = [self getCellIconImage];
			else 
				flag=NO;

            break;
	case 6:
            contentText = @"Sunday";
			dayText = mobilityWeekData.SundayText;	
	     if([mobilityWeekData getBoolVal:mobilityWeekData.SundaySelect])		
			 cellImage = [self getCellIconImage];
		 else 
			 flag=NO;

            break;
        case 7:
            contentText = @"Mobility Video";
			flag=NO;
            break;		
        default:
            break;
    }

    if(cellImage == nil)
	cellImage = [UIImage imageNamed:@"ansuncheck.png"];		
	
     cell.detailTextLabel.text = dayText;
     cell.detailTextLabel.font = [UIFont fontWithName:DEFAULT_BOLD_FONT_NAME size:defaultFontSize];	
	cell.detailTextLabel.textColor= [UIColor blackColor]; 
	
    cell.textLabel.text = contentText;
    cell.textLabel.font = [UIFont fontWithName:DEFAULT_BOLD_FONT_NAME size:defaultFontSize];
    if (indexPath.row == 7) {
        cell.textLabel.textColor = [UIColor blackColor];
        
        cell.imageView.image =  [UIImage imageNamed:@"videoicon.png"];	;
    }
    else
    {
    cell.textLabel.textColor=[self getCellTitleColor:flag];//[UIColor blackColor];	
    cell.imageView.image = cellImage;
	}
    //cell.	
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {

    if ([cell respondsToSelector:@selector(tintColor)]) {

        if (tableView == self.tableView_) {
            CGFloat cornerRadius = 5.f;
            cell.backgroundColor = UIColor.clearColor;
            CAShapeLayer *layer = [[CAShapeLayer alloc] init];
            CGMutablePathRef pathRef = CGPathCreateMutable();
            CGRect bounds = CGRectInset(cell.bounds, 10, 0);

            BOOL addLine = NO;
            if (indexPath.row == 0 && indexPath.row == [tableView numberOfRowsInSection:indexPath.section]-1) {
                CGPathAddRoundedRect(pathRef, nil, bounds, cornerRadius, cornerRadius);
            } else if (indexPath.row == 0) {
                CGPathMoveToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMaxY(bounds));
                CGPathAddArcToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMinY(bounds), CGRectGetMidX(bounds), CGRectGetMinY(bounds), cornerRadius);
                CGPathAddArcToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMinY(bounds), CGRectGetMaxX(bounds), CGRectGetMidY(bounds), cornerRadius);
                CGPathAddLineToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMaxY(bounds));
                addLine = YES;
            } else if (indexPath.row == [tableView numberOfRowsInSection:indexPath.section]-1) {
                CGPathMoveToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMinY(bounds));
                CGPathAddArcToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMaxY(bounds), CGRectGetMidX(bounds), CGRectGetMaxY(bounds), cornerRadius);
                CGPathAddArcToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMaxY(bounds), CGRectGetMaxX(bounds), CGRectGetMidY(bounds), cornerRadius);
                CGPathAddLineToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMinY(bounds));
            } else {
                CGPathAddRect(pathRef, nil, bounds);
                addLine = YES;
            }

            layer.path = pathRef;
            CFRelease(pathRef);
            layer.fillColor = [UIColor colorWithWhite:1.0f alpha:0.8f].CGColor;

            if (addLine == YES) {
                CALayer *lineLayer = [[CALayer alloc] init];
                CGFloat lineHeight = (1.f / [UIScreen mainScreen].scale);
                lineLayer.frame = CGRectMake(CGRectGetMinX(bounds), bounds.size.height-lineHeight, bounds.size.width, lineHeight);
                lineLayer.backgroundColor = tableView.separatorColor.CGColor;
                [layer addSublayer:lineLayer];
            }

            UIView *testView = [[UIView alloc] initWithFrame:bounds];
            [testView.layer insertSublayer:layer atIndex:0];
            testView.backgroundColor = [UIColor clearColor];
            cell.backgroundView = testView;

            UIView* selectedBackgroundView = [[UIView alloc] initWithFrame:bounds];
            UIView* visibleSelectedBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(10, 0.0f, bounds.size.width, bounds.size.height)];
            visibleSelectedBackgroundView.backgroundColor = tableView.separatorColor;
            [selectedBackgroundView addSubview:visibleSelectedBackgroundView];
            cell.selectedBackgroundView = selectedBackgroundView;
        }
    }
}

- (UIImage*)getCellIconImage
{
	if([mobilityWeekData getBoolVal:mobilityWeekData.isMobilityX])
		return [UIImage imageNamed:@"yoga.png"];
	else
		return [UIImage imageNamed:@"anscheck.png"];
}

- (UIColor*)getCellTitleColor:(BOOL)flag
{
	if([mobilityWeekData getBoolVal:mobilityWeekData.isMobilityX])
	{
		if(flag) return [UIColor blackColor];
		
		return [UIColor grayColor];
	}
	else
		return [UIColor blackColor];
}

- (void)nextScreen:(int)dayIndex
{
	//
	NSCalendar *cal = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
	NSDateComponents *components = [cal components:( NSYearCalendarUnit | NSMonthCalendarUnit | NSWeekCalendarUnit | NSDayCalendarUnit ) fromDate:self.today];
	NSArray *listItems = [mobilityWeekData.WeekLabel componentsSeparatedByString:@"/"];

	int days = [[listItems objectAtIndex:[listItems count]-1] intValue];
	
	[components setDay:days + dayIndex];
	
	NSDate *dayDate = [cal dateFromComponents:components];
	
	NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
	[dateFormat setDateFormat:@"yyyy-MM-dd"];
	
	NSString *dateString = [dateFormat stringFromDate:dayDate];
	
	NSLog(@"Formated Date...%@", dateString);
	
	MobilityWorkoutViewController *vc = [[MobilityWorkoutViewController alloc] initWithNibName:@"MobilityWorkoutViewController" bundle:nil];	
	vc.dateString = dateString;//mobilityWeekData.Date;
	vc.delegate=self;
	[self.navigationController pushViewController:vc animated:YES];
}

//- (void)dealloc{
//    [tableView_ release];
//    [super dealloc];
//}


- (IBAction)checkButtonTapped:(id)sender
{
	//UIButton *btn = (UIButton*)sender;
	
	MobilityWorkoutViewController *vc = [[MobilityWorkoutViewController alloc] initWithNibName:@"MobilityWorkoutViewController" bundle:nil];	
	vc.dateString = mobilityWeekData.Date;
	vc.delegate=self;
	[self.navigationController pushViewController:vc animated:YES];
}

//----------------------

- (void)clearBodyUI
{
	[self manipulateNextPrev];
	
	//	if(self.memberUIView !=nil)
	//	{
	//		[[self.memberUIView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
	//	}
	self.view.hidden = YES;	
}


- (void)manipulateNextPrev
{	
}


- (void)buildUI
{
	LoginResponseData *loginResponseData = [LoginPerser getLoginResponse];
	
	self.subheaderLb.text = loginResponseData.programName;
	self.subheaderLb2.text = mobilityWeekData.WeekLabel;
	
	[self.tableView_ reloadData];
	
	self.view.hidden = NO;
	
	if([mobilityWeekData getBoolVal:mobilityWeekData.isMobilityX])
		self.btnSave.hidden=YES;
	else
		self.btnSave.hidden=NO;
}

- (IBAction)moveNextClicked:(id)sender
{
	[self getNextPrevDate:YES];
	
	[self sendRequest: YES];
}

- (IBAction)movePrevClicked:(id)sender
{
	[self getNextPrevDate:NO];
	
	[self sendRequest: NO];
}

- (IBAction)btnSaveClicked:(id)sender
{
	requestType = 1;
	
	NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
	[dateFormat setDateFormat:@"yyyy-MM-dd"];
	
	NSString *dateString = [dateFormat stringFromDate:self.today];
	
	NSLog(@"Formated Date...%@", dateString);
	
	[tools startLoading:self.navigationController.view childView:loadingView text:LOADING_TEXT];
	request *r=[[request alloc] initWithTarget:self
								 SuccessAction:@selector(onSucceffulLogin:resData:)
								 FailureAction:@selector(onFailureLogin)];
	
	[r SaveMobility:[mobilityWeekData getMobilityXml:dateString]];
}

//communication part

- (void)sendRequest:(BOOL)flag
{	
	if(self.today == nil)
		self.today = [NSDate date];

    //...limit
    _buttonNext.enabled = ![[self getDateFormat:self.today] isEqualToString:[self getDateFormat:[NSDate date]]];

	//reguestIndex = 0;
	[tools startLoading:self.navigationController.view childView:loadingView text:LOADING_TEXT];
	[self makeRequestProgress:flag];
}

- (NSString *)getDateFormat:(NSDate *)date {
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM-dd-yyyy"];

    return [dateFormat stringFromDate:date];
}

- (void)makeRequestProgress:(BOOL)flag
{
	requestType = 0;
	//NSCalendar
	//
	NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
	[dateFormat setDateFormat:@"MM-dd-yyyy"];
	
	NSString *dateString = [dateFormat stringFromDate:self.today];
	
	NSLog(@"Formated Date...%@", dateString);
	
	LoginResponseData *loginResponseData = [LoginPerser getLoginResponse];
	
	request *r=[[request alloc] initWithTarget:self
								 SuccessAction:@selector(onSucceffulLogin:resData:)
								 FailureAction:@selector(onFailureLogin)];
	[r getMobilityWeek:loginResponseData.memberID date:dateString];			
}


- (void)getNextPrevDate:(BOOL)flag
{	
	if(self.today == nil)
		self.today = [NSDate date];
	
	//
	NSCalendar *cal = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
	// required components for today
	NSDateComponents *components = [cal components:( NSYearCalendarUnit | NSMonthCalendarUnit | NSWeekCalendarUnit | NSDayCalendarUnit ) fromDate:self.today];
	if(flag)
		[components setDay:([components day] + 7)];
	//NSDate *lastWeek  = [cal dateFromComponents:components];
	else
		[components setDay:([components day] - 7)];
	//NSCalendar *theCalendar = [NSCalendar currentCalendar];
	self.today = [cal dateFromComponents:components];//[theCalendar dateByAddingComponents:dayComponent toDate:self.today options:0];
	
	//return dateString;
}



- (void)onSucceffulLogin:(ResponseStatus *)responseStatus resData:(NSObject *)obj
{
	[tools stopLoading:loadingView];
	
	if(requestType == 0)
	{
	if (responseStatus == nil || ![responseStatus isSuccess])
	{
		[self clearBodyUI];
		
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry" message:NO_RECORD_FOUND delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
		[alert show];
		
		return ;
	}
	
	mobilityWeekData = (MobilityWeekData *)obj;
	
	[self buildUI];
	}
	else {
		NSString* text = @"Mobility successfully saved."; 
		
		if (responseStatus == nil || [responseStatus isSuccess])
		{
			text = @"Couldnot save mobility week to server, please try again later.";
		}
		else {
			return;
		}
		
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Info" message:text delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
		[alert show];
		return ;
		
	}
}

- (void)onFailureLogin
{
	[tools stopLoading:loadingView];
	if(requestType == 0)
	[self clearBodyUI];
}


@end
