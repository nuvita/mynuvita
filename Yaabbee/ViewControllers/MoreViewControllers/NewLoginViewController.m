//
//  NewLoginViewController.m
//  Yaabbee
//
//  Created by Sarfaraj Biswas on 8/7/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "NewLoginViewController.h"


//#define TABLE_OFFSETY 0.0;//60

@interface NewLoginViewController ()

@property (assign)BOOL isKeyBoardVisible;

@end
 
@implementation NewLoginViewController

@synthesize tableView;
@synthesize isKeyBoardVisible;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (IBAction)didtaploginaccount:(id)sender {

    switch ([sender tag]) {
        case 1:
            userIDFld.text = @"2@2.COM";
            passwordFld.text = @"222222com";
            break;

        case 2:
            userIDFld.text = @"Biggs.jess@gmail.com";
            passwordFld.text = @"nuvita1";
            break;

        case 3:
            userIDFld.text = @"Luke.skywalker@starwars.com";
            passwordFld.text = @"force1";
            break;

        default:
            break;
    }

}

#pragma mark - View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
   
    tools = [[MyTools alloc]init];
    loadingView=[tools createActivityIndicator1];	
	
	[self.navigationItem setTitle:@"Login"];
    self.isKeyBoardVisible = NO;
    // Do any additional setup after loading the view from its nib.
    
    if([NuvitaAppDelegate isPhone5])
        TABLE_OFFSETY = 0.0;
    else
        TABLE_OFFSETY = 60.0;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    return (toInterfaceOrientation != UIInterfaceOrientationPortraitUpsideDown || toInterfaceOrientation != UIInterfaceOrientationPortrait);
}

- (BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}

#pragma mark -
#pragma mark Table Delegate and Data Source
#pragma mark -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 2;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)_tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
     
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    [self configureCell:cell withIndex:indexPath];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
	
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
	cell.backgroundColor = [UIColor clearColor];  
}

- (void)configureCell:(UITableViewCell *)cell withIndex:(NSIndexPath *)indexPath{
    
    UIView *cellBackView = cell.contentView;
    
    UIImage *img = [UIImage imageNamed:@"textarea.png"];
    	
    CustomTextField *textField = [[CustomTextField alloc] initWithFrame:CGRectMake((cellBackView.frame.size.width - img.size.width)/2, 0, img.size.width, img.size.height)];
    textField.textColor = [UIColor blackColor];
    textField.font = [UIFont fontWithName:DEFAULT_NORMAL_FONT_NAME size:headerFontSize];;	
    textField.autocorrectionType = UITextAutocorrectionTypeNo;
    textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    textField.backgroundColor = [UIColor colorWithPatternImage:img] ;
    if(indexPath.row == 0){
        userIDFld = textField;
        textField.placeholder = @"User ID";
        textField.keyboardType = UIKeyboardTypeEmailAddress;
        userIDFld.text = [[NSUserDefaults standardUserDefaults] stringForKey:@"USER_ID"];
        
#if TARGET_IPHONE_SIMULATOR
        if([userIDFld.text length] < 1)
            userIDFld.text = @"john.bariquit@ripeconcepts.com";
#endif

        } else {
            passwordFld = textField;
            textField.placeholder = @"Password";
            passwordFld.text = [[NSUserDefaults standardUserDefaults] stringForKey:@"PASSWORD"];//
            passwordFld.secureTextEntry = YES;
#if TARGET_IPHONE_SIMULATOR
            if([passwordFld.text length] < 1)
                passwordFld.text = @"jo2014ios";
#endif
        }
	
	[textField setValue:[UIColor darkGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
    textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;	
    textField.tag = indexPath.row;
    textField.delegate = self;
    [cellBackView addSubview:textField];
}

/*
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSLog(@"textField ");		
    //set color for text input
    textField.textColor = [UIColor redColor];
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    NSLog(@"textFieldShouldClear ");	
    //set color for placeholder text
    textField.textColor = [UIColor redColor];
    return YES;
}
*/

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
    UITableViewCell *cell = (UITableViewCell *) [[textField superview] superview];
    
    NSIndexPath *iPath = [self.tableView indexPathForCell:cell];
    
    if(!self.isKeyBoardVisible){
        
        self.isKeyBoardVisible = YES;
        
        CGRect tableFrame = self.tableView.frame;
        tableFrame.origin.y -= TABLE_OFFSETY;
        
        [UIView beginAnimations:nil context:(__bridge void *)(iPath)];
        [UIView setAnimationDuration:0.3];
        [UIView setAnimationDidStopSelector:@selector(animationDidStop:finished:context:)];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
        [UIView setAnimationDelegate:self];
        
        self.tableView.frame = tableFrame;
        
        [UIView commitAnimations];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
  
    [textField resignFirstResponder];
    
    if(self.isKeyBoardVisible){
        
        self.isKeyBoardVisible = NO;
        CGRect tableFrame = self.tableView.frame;
        tableFrame.origin.y += TABLE_OFFSETY;
        
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.3];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
        [UIView setAnimationDelegate:self];
        
        self.tableView.frame = tableFrame;
        
        [UIView commitAnimations];
    }

    return YES;
}

- (IBAction)loginClicked:(id)sender{
    [self sendRequest];
}

- (IBAction)cancelClicked:(id)sender{
    [self dismissViewControllerAnimated:YES completion:^{
        //...cancel
    }];
}

- (void)sendRequest {

	NSString *userID = userIDFld.text;
	NSString *password = passwordFld.text;
	if ([userID length] == 0) {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry"
                                                        message:@"Please enter a UserID"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles: nil];
		[alert show];
	} else if([password length] == 0) {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry"
                                                        message:@"Please enter a Password"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles: nil];
		[alert show];
	} else {
        //save data
        [[NSUserDefaults standardUserDefaults] setValue:userID forKey:@"USER_ID"];
        [[NSUserDefaults standardUserDefaults] setValue:password forKey:@"PASSWORD"];
        [[NSUserDefaults standardUserDefaults] synchronize];

		[tools startLoading:self./*navigationController.*/view childView:loadingView text:@"Signing in. Wait…."];
		request *r = [[request alloc] initWithTarget:self
									 SuccessAction:@selector(onSucceffulLogin:resData:)
									 FailureAction:@selector(onFailureLogin)];
		[r loginRequest:userID pass:password];
	}	
}


- (void)onSucceffulLogin:(ResponseStatus *)responseStatus resData:(NSMutableArray *)arr2 {
	[tools stopLoading:loadingView];
	if (responseStatus == nil || ![responseStatus isSuccess]) {
		NSString* errMsg=@"Wrong userid or passowrd." ;
		if ([responseStatus.errorText length]>0) {
			errMsg = responseStatus.errorText;
		}
        
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Info"
                                                        message:errMsg
                                                       delegate:self
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles: nil];
		[alert show];
		
		return ;
	}
	
	NuvitaAppDelegate* app = (NuvitaAppDelegate*) [[UIApplication sharedApplication] delegate];
	if ([LoginPerser isLifeStyle360]) {		
		((UITabBarItem*)[[app.forceTabBarViewController.tabBar items] objectAtIndex:1]).enabled=NO;
	} else {
		((UITabBarItem*)[[app.forceTabBarViewController.tabBar items] objectAtIndex:1]).enabled=YES;
	}
    
    if ([self.delegate respondsToSelector:@selector(newLoginViewController:didFinishLogin:)]) {
        [self.delegate newLoginViewController:self didFinishLogin:YES];
    }
}

- (void)onFailureLogin {
	[tools stopLoading:loadingView];	
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

@end
