//
//  VO2MaxChartViewController.m
//  mynuvita
//
//  Created by John on 4/29/14.
//
//

#import "VO2MaxChartViewController.h"

@interface VO2MaxChartViewController ()

@property (retain, nonatomic) IBOutlet UIView *lineGraphView;
@property (nonatomic, retain) NSMutableArray *graphDataList;
@property (retain, nonatomic) NSMutableArray *normCategoryList;

@property (retain, nonatomic) Vo2MaxChart *vo2MaxChart;



@end

@implementation VO2MaxChartViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    
    return self;
}

#pragma mark - View Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    _graphDataList = [[NSMutableArray alloc] init];
    _normCategoryList = [[NSMutableArray alloc] init];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    [self.tabBarController.tabBar setHidden:YES];
    [self formatToSuperscriptText];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
  
    if (_resultInfo) {
        
        //...set norm category
        NSArray *norms = [_resultInfo valueForKey:@"a:HealthTrendsNorms"];
        if ([norms isKindOfClass:[NSArray class]]) {
            [_normCategoryList removeAllObjects];
            [_normCategoryList addObjectsFromArray:[_resultInfo valueForKey:@"a:HealthTrendsNorms"]];
        }
        
        
        NSArray *healthTrendsSeriesSet = [_resultInfo valueForKey:@"a:HealthTrendsSeriesSet"];
        [healthTrendsSeriesSet enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            
//            if ([[obj valueForKey:@"a:MeasurementTypeId"] intValue] == 20 || [[obj valueForKey:@"a:MeasurementTypeId"] intValue] == 11) {
                [_graphDataList removeAllObjects];
                
                NSArray *healthTrends = [obj valueForKey:@"a:HealthTrends"];
                for (NSMutableDictionary *data in healthTrends) {

                    [data setObject:[obj valueForKey:@"a:MeasurementTypeId"] forKey:@"a:MeasurementTypeId"];
                    [_graphDataList addObject:data];
                }
//            }
            
        }];
        
        [self buildGraphUI];
    }
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:YES];
    NSArray *viewControllers = self.navigationController.viewControllers;
    if ([viewControllers indexOfObject:self] == NSNotFound) {
        // View is disappearing because it was popped from the stack
        [self.tabBarController.tabBar setHidden:NO];
        [(NuvitaAppDelegate *)[[UIApplication sharedApplication] delegate] performSelector:@selector(reloadAppDelegateRootViewController)
                                                                                withObject:nil
                                                                                afterDelay:1.0];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationLandscapeRight || interfaceOrientation == UIInterfaceOrientationLandscapeLeft);
}

- (BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationLandscapeLeft;
}

#pragma mark - Line Chart View

- (void)buildGraphUI {
    for (UIView *subview in [_lineGraphView subviews]) {
        [subview removeFromSuperview];
    }
    
    _vo2MaxChart = [[Vo2MaxChart alloc] initWithFrame:CGRectMake(0, 0, _lineGraphView.frame.size.width, _lineGraphView.frame.size.height)];
    _vo2MaxChart.backgroundColor = [UIColor whiteColor];
    
    [self setGraphPoints:_vo2MaxChart];
    [_lineGraphView addSubview:_vo2MaxChart];
}

- (void)setGraphPoints:(Vo2MaxChart *)chart {
    
    NSMutableArray *pointArr = [[NSMutableArray alloc] init];
    
    __block float max = 30.0;
    __block float min = 30.0;
    int ratio = ceil([_graphDataList count] / 5.0);
    [_graphDataList enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        
        NSDictionary *data = obj;
        if (data) {
            
            CGFloat value = [[data objectForKey:@"a:Value"] doubleValue];
            min = (value < min) ? min = value : min;
            max = (value > max) ? max = value : max;
            
            NSDictionary *info = [NSDictionary dictionaryWithObjectsAndKeys:
                                  [NSValue valueWithCGPoint:CGPointMake(idx, (double)value)], @"points",
                                  [data valueForKey:@"a:Date"], @"label",
                                  [data valueForKey:@"a:MeasurementTypeId"], @"type",
                                  nil];
            
            [pointArr addObject:info];
        }
        
    }];
    
    if ([_normCategoryList count] > 0 && max > [[[_normCategoryList lastObject] valueForKey:@"a:Value"] floatValue]) {
        [_normCategoryList addObject:[NSDictionary dictionaryWithObjectsAndKeys:
                                      [NSNumber numberWithInteger:max + 5], @"a:Value",
                                      @"", @"a:Label",
                                      nil]];
    }

    _vo2MaxChart.ratio = ratio;
    [_vo2MaxChart setMaximum:max + 5];
    [_vo2MaxChart setMinimum:min - 5];
    [_vo2MaxChart setNormCategoryList:_normCategoryList];
    [_vo2MaxChart setXDataList:pointArr];
    
}

#pragma mark - Public Method

- (void)formatToSuperscriptText {
    for (id subview in [self.view subviews]) {
        if ([subview isKindOfClass:[UILabel class]]) {
            
            UILabel *label = (UILabel *)subview;
            label.text = [[[label.text stringByReplacingOccurrencesOfString:@"vo2" withString:@"VO\u00B2"] stringByReplacingOccurrencesOfString:@"VO2" withString:@"VO\u00B2"] stringByReplacingOccurrencesOfString:@"Vo2" withString:@"VO\u00B2"];
        }
    }
}

@end
