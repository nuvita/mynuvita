//
//  OutBoxViewController.h
//  Yaabbee
//
//  Created by Sarfaraj Biswas on 8/22/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MobileCoreServices/MobileCoreServices.h>

#import "request.h"
#import "NuvitaAppDelegate.h"
#import "MyTools.h"
#import "LoginPerser.h"
#import "LoginResponseData.h"
#import  "MobilityExerciseData.h"
#import "UIImageView+AFNetworking.h"
#import "KTTextView.h"
#import "UIImage+Scale.h"

@protocol ShareStatusDelegate <NSObject>

@required
- (void) shouldUpdate:(BOOL)flag;

@end

@interface ShareStatusViewController : UIViewController<UIImagePickerControllerDelegate,UIActionSheetDelegate, UINavigationControllerDelegate>
{	
	MyTools *tools;
	UIView *loadingView;
    
//    TextView

	MobilityExerciseData *mobilityExerciseData;
	
	BOOL isLoaded;
	
	int daysNextPrev;
	NSDate *today;
	
	UIButton *buttonPrev, *buttonNext;
	int totalElement;
}
@property (nonatomic, retain)NSData*  imageContent;
@property (nonatomic, retain)NSString *WorkoutId;
@property (nonatomic, retain) IBOutlet UIImageView *profileIcon;
@property (nonatomic, retain) IBOutlet UIImageView *shareImage;
@property (nonatomic, retain) IBOutlet KTTextView *profileTextView;
@property (nonatomic, retain) IBOutlet UIButton *photoBtn;

@property (nonatomic, retain) IBOutlet UILabel *subheaderLb1;
@property (nonatomic, assign) id<ShareStatusDelegate> delegate;

- (void)cameraBtnClicked:(id)sender;

- (void)postBtnClicked:(id)sender;
// 
- (void)sendRequest:(BOOL)flag;

- (void)makeRequestStarProgress:(BOOL)flag;
- (void)resignKeyboard;
- (void)retrieveRawImageData:(UIImage *)image;

@end
