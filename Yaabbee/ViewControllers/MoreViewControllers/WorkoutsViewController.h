//
//  OutBoxViewController.h
//  Yaabbee
//
//  Created by Sarfaraj Biswas on 8/22/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "request.h"
#import "NuvitaAppDelegate.h"
#import "MyTools.h"
#import "LoginPerser.h"
#import "LoginResponseData.h"
#import  "MobilityWorkoutsData.h"
#import "ExerciseViewController.h"

@interface WorkoutsViewController : UIViewController {	
	MyTools *tools;
	UIView *loadingView;

	MobilityWorkoutsData *mobilityWorkoutsData;
	
	BOOL isLoaded;
	
	int daysNextPrev;
	NSDate *today;
	
	UIButton *buttonPrev, *buttonNext;
	int totalElement;
}

@property (nonatomic, retain)NSDate *today;
@property (nonatomic, retain) IBOutlet UITableView *tableView_;
@property (nonatomic, retain) IBOutlet UILabel *subheaderLb1;
@property (nonatomic, retain) IBOutlet UILabel *subheaderLb2;

//@property (nonatomic, retain) IBOutlet UISearchBar *searchBar;
//- (CGSize)calculateTextSizeWithText:(NSString *)text;

- (void)configureCell:(UITableViewCell *)cell withIndex:(NSIndexPath *)indexPath;

- (IBAction)moveNextClicked:(id)sender;
- (IBAction)movePrevClicked:(id)sender;
- (void)homeClicked:(id)sender;


// 
- (void)sendRequest:(BOOL)flag;

- (void)makeRequestStarProgress:(BOOL)flag;


- (void)getNextPrevDate:(BOOL)flag;

- (void)clearBodyUI;

- (void)manipulateNextPrev;

- (void)buildUI;
@end
