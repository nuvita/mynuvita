//
//  MoreViewController.h
//  Yaabbee
//
//  Created by Sarfaraj Biswas on 7/22/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "request.h"
#import "NuvitaAppDelegate.h"
#import "MyTools.h"
#import "LoginPerser.h"
#import "LoginResponseData.h"
#import  "MobilityWorkoutViewController.h"
#import "WorkoutsViewController.h"

@class MobilityWeekData;

@interface MobilityViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, MobilityDelegate>{
	
       MyTools *tools;
	UIView *loadingView;
	
	MobilityWeekData *mobilityWeekData;
	
	BOOL isLoaded;
	
	int daysNextPrev;
	//NSDate *today;

	int totalElement;
	
	int requestType;
} 

@property (nonatomic, retain) IBOutlet UIButton *btnSave;

@property (nonatomic, retain)NSDate *today;

@property (nonatomic,retain)IBOutlet UILabel *subheaderLb;
@property (nonatomic,retain)IBOutlet UILabel *subheaderLb2;

@property (nonatomic,retain)IBOutlet UITableView *tableView_;

- (void)createRightBarItem;
- (void)login;
- (void)configureCell:(UITableViewCell *)cell withIndex:(NSIndexPath *)indexPath;
- (IBAction)moveNextClicked:(id)sender;
- (IBAction)movePrevClicked:(id)sender;

- (IBAction)btnSaveClicked:(id)sender; 
- (IBAction)checkButtonTapped:(id)sender; 
- (UIImage*)getCellIconImage;
- (void)nextScreen:(int)dayIndex;
- (UIColor*)getCellTitleColor:(BOOL)flag;
//
//

- (void)sendRequest:(BOOL)flag;

- (void)makeRequestProgress:(BOOL)flag;


- (void)getNextPrevDate:(BOOL)flag;

- (void)clearBodyUI;

- (void)manipulateNextPrev;

- (void)buildUI;



@end
