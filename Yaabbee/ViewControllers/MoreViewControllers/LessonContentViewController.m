//
//  MyProfileViewController.m
//  Yaabbee
//
//  Created by Sarfaraj Biswas on 8/14/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "LessonContentViewController.h"
#import "LabelBackView.h"
#import <QuartzCore/QuartzCore.h>
#import "QuizViewController.h"
#import "constant.h"

#define CONTENT_LABEL 100

@implementation LessonContentViewController
@synthesize webView, activityIndicatorView,subheaderLb,today,lessonNumber;



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

   
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Quiz"
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:self
                                                                  action:@selector(editClicked:)];
	
	isLoaded = NO;
	tools = [[MyTools alloc]init];
	loadingView = [tools createActivityIndicator1];	
	
	daysNextPrev = 0;
	self.view.hidden = YES;
	
	isLoaded= NO;	
	    
//   self.subheaderLb.font = [UIFont fontWithName:DEFAULT_BOLD_FONT_NAME size:defaultFontSize];	
//    	
//    [self updateUI];
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	if(!isLoaded)
		[self sendRequest:YES];

}

- (void)updateUI {
    if ([NuvitaAppDelegate isIOS7]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)editClicked:(id)sender{
    QuizViewController *quizVC = [[QuizViewController alloc] initWithNibName:@"QuizViewController" bundle:nil];
    quizVC.eduLessonWeekData = eduLessonWeekData;
    [self.navigationController pushViewController:quizVC animated:YES];
}

- (void)loadWView
{
	activityIndicatorView = [[UIActivityIndicatorView alloc]initWithFrame: CGRectMake(145, 160, 25, 25)];
	[webView addSubview:activityIndicatorView];
	
	[webView setDelegate:self];
	//CGRect webFrame = CGRectMake(0.0, 0.0, 320.0, 460.0); 
	//UIWebView *webView = [[UIWebView alloc] initWithFrame:webFrame];
	[webView setBackgroundColor:[UIColor whiteColor]]; 
	NSString *urlAddress = @"http://www.mynuvita.com"; 
	//NSURL *url = [NSURL URLWithString:urlAddress]; 
	//NSURLRequest *requestObj = [NSURLRequest requestWithURL:url]; 
	//[webView loadRequest:requestObj]; 
	
	NSString *html = eduLessonWeekData.Text;//@"<html><head><title>The Meaning of Life</title></head><body><p>...really is <b>42</b>!</p></body></html>";  
	[webView loadHTMLString:html baseURL:[NSURL URLWithString:urlAddress]]; 
	
	//[self addSubview:webView];
	//[webView release];
}

 - (void)webViewDidStartLoad:(UIWebView *)webView {
	 NSLog(@"webViewDidStartLoad");
 [activityIndicatorView startAnimating];
// myLabel.hidden = FALSE;
 }
 
 - (void)webViewDidFinishLoad:(UIWebView *)webView {
	 NSLog(@"webViewDidFinishLoad");
 [activityIndicatorView stopAnimating];
 //myLabel.hidden = TRUE;
 }

- (void)clearBodyUI {
	self.view.hidden = YES;	
}


- (void)buildUI {
	totalElement = 0;
	
	if(eduLessonWeekData == nil || eduLessonWeekData.ItemsArray == nil) return ;
	
	totalElement = [eduLessonWeekData.ItemsArray count];
	
	if(totalElement == 0) {
		[self clearBodyUI ];
		
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry"
                                                        message:NO_RECORD_FOUND
                                                       delegate:self
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles: nil];
		[alert show];
		return ;		
	}
	
	self.subheaderLb.text = eduLessonWeekData.Title;
	//self.subheaderLb2.text = lessonWeekData.WeekLabel;
	[self loadWView ];
	//[self.dataTableView reloadData];
	
	self.view.hidden = NO;
	isLoaded = YES; 
}


//communication part

- (void)sendRequest:(BOOL)flag
{	
	if(self.today == nil)
		self.today = [NSDate date];
	
	//reguestIndex = 0;
	
	[tools startLoading:self.navigationController.view childView:loadingView text:LOADING_TEXT];
	
	[self makeRequestProgress:flag];
}


- (void)makeRequestProgress:(BOOL)flag {

	NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
	[dateFormat setDateFormat:@"MM-dd-yyyy"];
	
	NSString *dateString = [dateFormat stringFromDate:self.today];
	
	NSLog(@"Formated Date...%@", dateString);
	
	LoginResponseData *loginResponseData = [LoginPerser getLoginResponse];
	
	request *r=[[request alloc] initWithTarget:self
								 SuccessAction:@selector(onSucceffulLogin:resData:)
								 FailureAction:@selector(onFailureLogin)];
	[r getEducationLessonWeek:loginResponseData.memberID date:dateString lessonNumber:self.lessonNumber];
    
}


- (void)onSucceffulLogin:(ResponseStatus *)responseStatus resData:(NSObject *)obj {
	if (responseStatus == nil || ![responseStatus isSuccess]) {
        [tools stopLoading:loadingView];
		[self clearBodyUI];
		
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry"
                                                        message:NO_RECORD_FOUND
                                                       delegate:self
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles: nil];
		[alert show];
		return ;
	}
	
	eduLessonWeekData = (EduLessonWeekData *)obj;
	
	[self buildUI];
	
	[tools stopLoading:loadingView];		
}

- (void)onFailureLogin
{
	[tools stopLoading:loadingView];
	[self clearBodyUI];
}




@end
