//
//  MoreViewController.m
//  Yaabbee
//
//  Created by Sarfaraj Biswas on 7/22/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "LessonsViewController.h"
#import "LessonContentViewController.h"
#import "Profile.h"
#import "constant.h"

@interface LessonsViewController ()

@property (nonatomic, retain) JBCustomButton *buttonPrev;
@property (nonatomic, retain) JBCustomButton *buttonNext;

@end

@implementation LessonsViewController
@synthesize dataTableView, subheaderLb,today,subheaderLb2;

#pragma mark - View lifecycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

	isLoaded = NO;
	tools = [[MyTools alloc]init];
	loadingView = [tools createActivityIndicator1];

    _buttonPrev=  [[JBCustomButton alloc] initWithFrame:CGRectMake(0, 20, 30, 30)];
    [_buttonPrev setButtonStyle:JBCustomButtonArrowLeftStyle];
	[_buttonPrev addTarget:self action:@selector(movePrevClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    _buttonNext =  [[JBCustomButton alloc] initWithFrame:CGRectMake(0, 20, 30, 30)];
    [_buttonNext setButtonStyle:JBCustomButtonArrowRightStyle];
	[_buttonNext addTarget:self action:@selector(moveNextClicked:) forControlEvents:UIControlEventTouchUpInside];
	
	self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:
                                               [[UIBarButtonItem alloc] initWithCustomView:_buttonNext],
                                               [[UIBarButtonItem alloc] initWithCustomView:_buttonPrev], nil];
	
	daysNextPrev = 0;
	self.view.hidden = YES;
	
	isLoaded= NO;
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	if(!isLoaded)
		[self sendRequest:YES];
	
	[self manipulateNextPrev];
}

- (void)createRightBarItem {
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Login"
                                                                      style:UIBarButtonItemStyleBordered
                                                                     target:self
                                                                     action:@selector(login)];
}

- (void)login {
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return totalElement;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = nil;
    NSString *evenCell = CELL_EVEN;
    NSString *oddCell = CELL_ODD;
    
    if (fmod(indexPath.row, 2) == 0)
        CellIdentifier = evenCell;
    
    else
        CellIdentifier = oddCell;

    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
	cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    // Configure the cell...
    [self configureCell:cell withIndex:indexPath];     
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
	
	LessonItem *lessonItem =  (LessonItem*)[lessonWeekData.ItemsArray objectAtIndex:indexPath.row];
	if(lessonItem != nil) {
		LessonContentViewController *myprofileVC = [[LessonContentViewController alloc] initWithNibName:@"LessonContentViewController" bundle:nil];
		myprofileVC.lessonNumber = lessonItem.LessonNumber;
		myprofileVC.today = self.today;
        
		[self.navigationController pushViewController:myprofileVC animated:YES];
	}
}

- (void)configureCell:(UITableViewCell *)cell withIndex:(NSIndexPath *)indexPath{

   LessonItem *lessonItem =  (LessonItem*)[lessonWeekData.ItemsArray objectAtIndex:indexPath.row];
    cell.textLabel.text = lessonItem.Title;
    cell.textLabel.numberOfLines = 0;
    cell.textLabel.font = [UIFont fontWithName:DEFAULT_BOLD_FONT_NAME size:12.0];
    cell.imageView.image = [lessonItem.Passed isEqualToString:@"false"] ? [UIImage imageNamed:@"uncheck.png"] : [UIImage imageNamed:@"check.png"];
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath {
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	
	LessonItem *lessonItem =  (LessonItem*)[lessonWeekData.ItemsArray objectAtIndex:indexPath.row];
	if(lessonItem != nil) {
		LessonContentViewController *myprofileVC = [[LessonContentViewController alloc] initWithNibName:@"LessonContentViewController" bundle:nil];
		myprofileVC.lessonNumber = lessonItem.LessonNumber;
		myprofileVC.today = self.today;
        
		[self.navigationController pushViewController:myprofileVC animated:YES];
	}
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {

    if ([cell respondsToSelector:@selector(tintColor)]) {

        if (tableView == self.dataTableView) {
            CGFloat cornerRadius = 5.f;
            cell.backgroundColor = UIColor.clearColor;
            CAShapeLayer *layer = [[CAShapeLayer alloc] init];
            CGMutablePathRef pathRef = CGPathCreateMutable();
            CGRect bounds = CGRectInset(cell.bounds, 10, 0);

            BOOL addLine = NO;
            if (indexPath.row == 0 && indexPath.row == [tableView numberOfRowsInSection:indexPath.section]-1) {
                CGPathAddRoundedRect(pathRef, nil, bounds, cornerRadius, cornerRadius);
            } else if (indexPath.row == 0) {
                CGPathMoveToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMaxY(bounds));
                CGPathAddArcToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMinY(bounds), CGRectGetMidX(bounds), CGRectGetMinY(bounds), cornerRadius);
                CGPathAddArcToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMinY(bounds), CGRectGetMaxX(bounds), CGRectGetMidY(bounds), cornerRadius);
                CGPathAddLineToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMaxY(bounds));
                addLine = YES;
            } else if (indexPath.row == [tableView numberOfRowsInSection:indexPath.section]-1) {
                CGPathMoveToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMinY(bounds));
                CGPathAddArcToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMaxY(bounds), CGRectGetMidX(bounds), CGRectGetMaxY(bounds), cornerRadius);
                CGPathAddArcToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMaxY(bounds), CGRectGetMaxX(bounds), CGRectGetMidY(bounds), cornerRadius);
                CGPathAddLineToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMinY(bounds));
            } else {
                CGPathAddRect(pathRef, nil, bounds);
                addLine = YES;
            }

            layer.path = pathRef;
            CFRelease(pathRef);
            layer.fillColor = [UIColor colorWithWhite:1.0f alpha:0.8f].CGColor;

            if (addLine == YES) {
                CALayer *lineLayer = [[CALayer alloc] init];
                CGFloat lineHeight = (1.f / [UIScreen mainScreen].scale);
                lineLayer.frame = CGRectMake(CGRectGetMinX(bounds), bounds.size.height-lineHeight, bounds.size.width, lineHeight);
                lineLayer.backgroundColor = tableView.separatorColor.CGColor;
                [layer addSublayer:lineLayer];
            }

            UIView *testView = [[UIView alloc] initWithFrame:bounds];
            [testView.layer insertSublayer:layer atIndex:0];
            testView.backgroundColor = [UIColor clearColor];
            cell.backgroundView = testView;

            UIView* selectedBackgroundView = [[UIView alloc] initWithFrame:bounds];
            UIView* visibleSelectedBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(10, 0.0f, bounds.size.width, bounds.size.height)];
            visibleSelectedBackgroundView.backgroundColor = tableView.separatorColor;
            [selectedBackgroundView addSubview:visibleSelectedBackgroundView];
            cell.selectedBackgroundView = selectedBackgroundView;
        }
    }
}

- (void)clearBodyUI {
	[self manipulateNextPrev];
	self.view.hidden = YES;	
}


- (void)manipulateNextPrev {

}


- (void)buildUI {
	
	 totalElement = 0;
	 
	 if(lessonWeekData == nil || lessonWeekData.ItemsArray == nil) return ;
	 
	 totalElement = [lessonWeekData.ItemsArray count];
	 
	 if(totalElement == 0)
	 {
	 [self clearBodyUI ];
	 
	 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry"
                                                     message:NO_RECORD_FOUND
                                                    delegate:self
                                           cancelButtonTitle:@"Ok"
                                           otherButtonTitles: nil];
	 [alert show];
	 return ;		
	 }
	
	LoginResponseData *loginResponseData = [LoginPerser getLoginResponse];
	
	self.subheaderLb.text = loginResponseData.programName;
	self.subheaderLb2.text = lessonWeekData.WeekLabel;
	[self.dataTableView reloadData];
	self.view.hidden = NO;
}

- (IBAction)moveNextClicked:(id)sender {
	[self getNextPrevDate:YES];
	[self sendRequest: YES];
}

- (IBAction)movePrevClicked:(id)sender {
	[self getNextPrevDate:NO];
	[self sendRequest: NO];
}

- (void)sendRequest:(BOOL)flag {
	if(self.today == nil)
		self.today = [NSDate date];

    //...limit
    _buttonNext.enabled = ![[self getDateFormat:self.today] isEqualToString:[self getDateFormat:[NSDate date]]];
	
	[tools startLoading:self.navigationController.view childView:loadingView text:LOADING_TEXT];
	[self makeRequestProgress:flag];
}

- (NSString *)getDateFormat:(NSDate *)date {
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM-dd-yyyy"];

    return [dateFormat stringFromDate:date];
}

- (void)makeRequestProgress:(BOOL)flag {
	//NSCalendar
	NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
	[dateFormat setDateFormat:@"MM-dd-yyyy"];
	NSString *dateString = [dateFormat stringFromDate:self.today];

	LoginResponseData *loginResponseData = [LoginPerser getLoginResponse];
	loginResponseData.today =  dateString;
	
	request *r=[[request alloc] initWithTarget:self
								 SuccessAction:@selector(onSucceffulLogin:resData:)
								 FailureAction:@selector(onFailureLogin)];
	[r getEducationWeek:loginResponseData.memberID date:dateString];
}


- (void)getNextPrevDate:(BOOL)flag {
	if(self.today == nil)
		self.today = [NSDate date];
	
	
	NSCalendar *cal = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
	NSDateComponents *components = [cal components:( NSYearCalendarUnit | NSMonthCalendarUnit | NSWeekCalendarUnit | NSDayCalendarUnit ) fromDate:self.today];
	
	if(flag)
		[components setDay:([components day] + 7)];
	else
		[components setDay:([components day] - 7)];
	
	self.today = [cal dateFromComponents:components];

}

#pragma mark - Web Service Delegate

- (void)onSucceffulLogin:(ResponseStatus *)responseStatus resData:(NSObject *)obj {
	if (responseStatus == nil || ![responseStatus isSuccess]) {
		[tools stopLoading:loadingView];
		[self clearBodyUI];
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry"
                                                        message:NO_RECORD_FOUND
                                                       delegate:self
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles: nil];
		[alert show];
		return ;
	}
	
	lessonWeekData = (LessonWeekData *)obj;
	[self buildUI];
	[tools stopLoading:loadingView];		
}

- (void)onFailureLogin {
	[tools stopLoading:loadingView];
	[self clearBodyUI];
}

@end
