//
//  VideoPlayerViewController.h
//  mynuvita
//
//  Created by John on 3/17/14.
//
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <CoreMedia/CoreMedia.h>
#import <MediaPlayer/MediaPlayer.h>

@protocol VideoPlayerViewControllerDelegate <NSObject>

- (void)didFinishPlay;

@end

@interface VideoPlayerViewController : UIViewController

@property (strong, nonatomic) NSString *streamURL;

@end
