//
//  OutBoxViewController.m
//  Yaabbee
//
//  Created by Sarfaraj Biswas on 8/22/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "WorkoutsViewController.h"
#import "HomeViewController.h"
#import "VideoPlayerViewController.h"
#import "constant.h"

@interface WorkoutsViewController () <ExerciseViewControllerDelegate>

@end

@implementation WorkoutsViewController

@synthesize subheaderLb1, subheaderLb2, tableView_;

@synthesize today;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

	isLoaded = NO;
	tools = [[MyTools alloc]init];
	loadingView = [tools createActivityIndicator1];	
	
	//set font
	self.subheaderLb1.font = [UIFont fontWithName:DEFAULT_BOLD_FONT_NAME size:defaultFontSize];
	self.subheaderLb2.font = [UIFont fontWithName:DEFAULT_BOLD_FONT_NAME size:defaultFontSize];
	
	daysNextPrev = 0;
	self.view.hidden = YES;
	
	isLoaded= NO;

    self.navigationItem.leftBarButtonItem =[[UIBarButtonItem alloc] initWithTitle:@"Mobility"
                                                               style:UIBarButtonItemStylePlain
                                                              target:self action:@selector(homeClicked:)];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
    
	if(!isLoaded)
		[self sendRequest:YES];
	
	[self manipulateNextPrev];
}

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    return (toInterfaceOrientation != UIInterfaceOrientationPortraitUpsideDown || toInterfaceOrientation != UIInterfaceOrientationPortrait);
}

- (BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}

- (void)homeClicked:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
    /*
	HomeViewController *homeVC = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
   [self.navigationController pushViewController:homeVC animated:YES];
    [homeVC release];
	*/
}

#pragma mark TableView
#pragma mark -

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([NuvitaAppDelegate isPhone5])
        return 44.0;
    
    return 36.0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return totalElement;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = nil;
    
    CellIdentifier = CELL_EVEN;
    
    UITableViewCell *cell = nil;//[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
	
		if(indexPath.row%2)
		{
			cell.backgroundColor = [UIColor whiteColor];
		}
		else
		{
			cell.backgroundColor = [UIColor lightGrayColor];
		}	
    	
		[self configureCell:cell withIndex:indexPath];			
    }
	
		
    tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
    //2 weeks ago";
    // Configure the cell...
    return cell;
}

- (CGSize)calculateTextSizeWithText:(NSString *)text{
    CGSize size =  [text sizeWithFont:[UIFont systemFontOfSize:16] constrainedToSize: CGSizeMake(280.0, 480.0) lineBreakMode:NSLineBreakByWordWrapping];
    
    return size;
}

- (void)configureCell:(UITableViewCell *)cell withIndex:(NSIndexPath *)indexPath{
    
    NSMutableArray *arr = [mobilityWorkoutsData.dictMobilityWorkouts objectForKey:MOBILITY_EX_WORKOUTS];
    
	NSDictionary *dict = [arr objectAtIndex:indexPath.row];
    cell.textLabel.text = [dict objectForKey:MOBILITY_WORKOUT_NAME];
    cell.textLabel.font = [UIFont fontWithName:DEFAULT_BOLD_FONT_NAME size:defaultFontSize];
    cell.textLabel.textColor = [UIColor blackColor];    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSMutableArray *arr = [mobilityWorkoutsData.dictMobilityWorkouts objectForKey:MOBILITY_EX_WORKOUTS];
    
	NSDictionary *dict = [arr objectAtIndex:indexPath.row];
    
    ExerciseViewController *vc = [[ExerciseViewController alloc] initWithNibName:@"ExerciseViewController" bundle:nil];
    vc.WorkoutId = [dict objectForKey:MOBILITY_WORKOUT_ID];
    vc.delegate = self;

    [self.navigationController pushViewController:vc animated:YES];
    [(NuvitaAppDelegate *)[[UIApplication sharedApplication] delegate] performSelector:@selector(reloadAppDelegateRootViewControllerLandscape)
                                                                            withObject:nil
                                                                            afterDelay:0.5];
//    [self presentViewController:vc animated:YES completion:nil];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([cell respondsToSelector:@selector(tintColor)]) {
        
        if (tableView == self.tableView_) {
            CGFloat cornerRadius = 5.f;
            cell.backgroundColor = UIColor.clearColor;
            CAShapeLayer *layer = [[CAShapeLayer alloc] init];
            CGMutablePathRef pathRef = CGPathCreateMutable();
            CGRect bounds = CGRectInset(cell.bounds, 10, 0);
            BOOL addLine = NO;
            if (indexPath.row == 0 && indexPath.row == [tableView numberOfRowsInSection:indexPath.section]-1) {
                CGPathAddRoundedRect(pathRef, nil, bounds, cornerRadius, cornerRadius);
            } else if (indexPath.row == 0) {
                CGPathMoveToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMaxY(bounds));
                CGPathAddArcToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMinY(bounds), CGRectGetMidX(bounds), CGRectGetMinY(bounds), cornerRadius);
                CGPathAddArcToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMinY(bounds), CGRectGetMaxX(bounds), CGRectGetMidY(bounds), cornerRadius);
                CGPathAddLineToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMaxY(bounds));
                addLine = YES;
            } else if (indexPath.row == [tableView numberOfRowsInSection:indexPath.section]-1) {
                CGPathMoveToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMinY(bounds));
                CGPathAddArcToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMaxY(bounds), CGRectGetMidX(bounds), CGRectGetMaxY(bounds), cornerRadius);
                CGPathAddArcToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMaxY(bounds), CGRectGetMaxX(bounds), CGRectGetMidY(bounds), cornerRadius);
                CGPathAddLineToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMinY(bounds));
            } else {
                CGPathAddRect(pathRef, nil, bounds);
                addLine = YES;
            }
            
            layer.path = pathRef;
            CFRelease(pathRef);
            layer.fillColor = (indexPath.row % 2 == 0) ? tableView.separatorColor.CGColor : [UIColor colorWithWhite:1.f alpha:0.8f].CGColor;
            
            if (addLine == YES) {
                CALayer *lineLayer = [[CALayer alloc] init];
                CGFloat lineHeight = (1.f / [UIScreen mainScreen].scale);
                lineLayer.frame = CGRectMake(CGRectGetMinX(bounds), bounds.size.height-lineHeight, bounds.size.width, lineHeight);
                lineLayer.backgroundColor = tableView.separatorColor.CGColor;
                [layer addSublayer:lineLayer];
            }
            
            UIView *testView = [[UIView alloc] initWithFrame:bounds];
            [testView.layer insertSublayer:layer atIndex:0];
            testView.backgroundColor = [UIColor clearColor];
            cell.backgroundView = testView;
            
            UIView* selectedBackgroundView = [[UIView alloc] initWithFrame:bounds];
            UIView* visibleSelectedBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(10, 0.0f, bounds.size.width, bounds.size.height)];
            visibleSelectedBackgroundView.backgroundColor = tableView.separatorColor;
            [selectedBackgroundView addSubview:visibleSelectedBackgroundView];
            cell.selectedBackgroundView = selectedBackgroundView;
        }
    }
}

#pragma mark - ExerciseViewControllerDelegate

- (void)exerciseViewController:(ExerciseViewController *)viewController didFinishPickFile:(NSString *)file {
    VideoPlayerViewController *videoPlayerViewController = [[VideoPlayerViewController alloc] initWithNibName:@"VideoPlayerViewController" bundle:nil];
    videoPlayerViewController.streamURL = file;
//    [self presentViewController:videoPlayerViewController animated:YES completion:nil];
    [self.navigationController presentViewController:videoPlayerViewController animated:YES completion:^{

    }];
}

#pragma mark - ETC

- (void)clearBodyUI {
	[self manipulateNextPrev];

	self.view.hidden = YES;	
}


- (void)manipulateNextPrev {
}


- (void)buildUI
{
	totalElement = 0;
	
	[self.tableView_ reloadData];
	
  	if(mobilityWorkoutsData == nil || mobilityWorkoutsData.dictMobilityWorkouts == nil) return ;
    NSMutableArray *arr = [mobilityWorkoutsData.dictMobilityWorkouts objectForKey:MOBILITY_EX_WORKOUTS];
    
	totalElement = [arr count];	
    
    if(totalElement == 0) {
		[self clearBodyUI ];
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry"
                                                        message:NO_RECORD_FOUND
                                                       delegate:self
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles: nil];
		[alert show];
		return ;		
	}
    
    NSDictionary *dict = [arr objectAtIndex:0];
    if([((NSString*)[dict objectForKey:MOBILITY_WORKOUT_NAME]) isEqualToString:@"-- Select Workout --"])
        [arr removeObjectAtIndex:0];

    totalElement = [arr count];	
    
	LoginResponseData *loginResponseData = [LoginPerser getLoginResponse];
	
	self.subheaderLb2.text = mobilityWorkoutsData.WeekLabel;//@"13/17";
	self.subheaderLb1.text = loginResponseData.programName;
	
	[self.tableView_ reloadData];
	
	self.view.hidden = NO;
}

- (IBAction)moveNextClicked:(id)sender
{
	[self getNextPrevDate:YES];
	
	[self sendRequest: YES];
}

- (IBAction)movePrevClicked:(id)sender
{
	[self getNextPrevDate:NO];
	
	[self sendRequest: NO];
}

- (void)sendRequest:(BOOL)flag {
	if(self.today == nil)
		self.today = [NSDate date];
	
	//reguestIndex = 0;
	
	[tools startLoading:self.navigationController.view childView:loadingView text:LOADING_TEXT];
	
	[self makeRequestStarProgress:flag];
}


- (void)makeRequestStarProgress:(BOOL)flag {
	//
	NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
	[dateFormat setDateFormat:@"MM-dd-yyyy"];
	
	NSString *dateString = [dateFormat stringFromDate:self.today];
		
	LoginResponseData *loginResponseData = [LoginPerser getLoginResponse];
	
	request *r=[[request alloc] initWithTarget:self
								 SuccessAction:@selector(onSucceffulLogin:resData:)
								 FailureAction:@selector(onFailureLogin)];
	[r getMobilityWorkouts:loginResponseData.memberID date:dateString];
}


- (void)getNextPrevDate:(BOOL)flag
{	
	if(self.today == nil)
		self.today = [NSDate date];
	
	//
	NSCalendar *cal = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
	
	// required components for today
	NSDateComponents *components = [cal components:( NSYearCalendarUnit | NSMonthCalendarUnit | NSWeekCalendarUnit | NSDayCalendarUnit ) fromDate:self.today];
	
	if(flag)
		[components setDay:([components day] + 7)];
	//NSDate *lastWeek  = [cal dateFromComponents:components];
	else
		[components setDay:([components day] - 7)];
	
	//NSCalendar *theCalendar = [NSCalendar currentCalendar];
	
	self.today = [cal dateFromComponents:components];//[theCalendar dateByAddingComponents:dayComponent toDate:self.today options:0];
	
	//return dateString;
}

- (void)onSucceffulLogin:(ResponseStatus *)responseStatus resData:(NSObject *)obj
{
	if (responseStatus == nil || ![responseStatus isSuccess])
	{
		[tools stopLoading:loadingView];
		
		[self clearBodyUI];
		
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry"
                                                        message:NO_RECORD_FOUND
                                                       delegate:self
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
		[alert show];
		
		return ;
	}
    
	mobilityWorkoutsData = (MobilityWorkoutsData *)obj;
	//[starProgressData retain];
	isLoaded = YES;
	[self buildUI];
	
	[tools stopLoading:loadingView];		
}

- (void)onFailureLogin
{
	[tools stopLoading:loadingView];
	[self clearBodyUI];
}

@end
