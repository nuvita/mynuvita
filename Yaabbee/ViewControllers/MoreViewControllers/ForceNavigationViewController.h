//
//  ForceNavigationViewController.h
//  mynuvita
//
//  Created by John on 4/10/14.
//
//

#import <UIKit/UIKit.h>

@interface ForceNavigationViewController : UINavigationController

@property(nonatomic, assign) UIInterfaceOrientation orientation;
@property(nonatomic, assign) NSUInteger supportedInterfaceOrientatoin;

@end
		