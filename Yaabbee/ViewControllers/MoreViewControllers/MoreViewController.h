//
//  MoreViewController.h
//  Yaabbee
//
//  Created by Sarfaraj Biswas on 7/22/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WellnessWallViewController.h"
#import "JFNavigationTransitionDelegate.h"

@interface MoreViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>{
    UITableView *dataTableView;
	
	BOOL isMobilityHide;
}

@property (nonatomic,retain)IBOutlet UITableView *dataTableView;

@end
