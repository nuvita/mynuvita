//
//  MyProfileViewController.m
//  Yaabbee
//
//  Created by Sarfaraj Biswas on 8/14/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "WellnessWallViewController.h"
#import "LabelBackView.h"
#import <QuartzCore/QuartzCore.h>
#import "QuizViewController.h"
#import "constant.h"

#define CONTENT_LABEL 100

@implementation WellnessWallViewController
@synthesize subheaderLb,today,lessonNumber,contentScrollView,loadMoreFooterView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Share"
                                                                              style:UIBarButtonItemStylePlain
                                                                             target:self
                                                                             action:@selector(editClicked:)];

	isLoaded = NO;
	tools = [[MyTools alloc]init];
	loadingView = [tools createActivityIndicator1];	
	
	daysNextPrev = 0;
	self.view.hidden = YES;
	
	isLoaded= NO;	
	    
//   self.subheaderLb.font = [UIFont fontWithName:DEFAULT_BOLD_FONT_NAME size:defaultFontSize];	
//    	
//        
    self.contentScrollView.delegate = self;
    [self initContentView];
}

- (void)initContentView
{
    tagIndex = 1000;
    currPageNum = 1;
    yy = 0.0;
    
    self.loadMoreFooterView = nil;
    for (UIView *subview in [self.contentScrollView subviews]) {
            [subview removeFromSuperview];
    }
    
    //
    if (self.loadMoreFooterView == nil) {
        NSLog(@"height: %f", self.contentScrollView.contentSize.height);
		self.loadMoreFooterView = [[LoadMoreTableFooterView alloc] initWithFrame:CGRectMake(0.0f, self.contentScrollView.contentSize.height, self.contentScrollView.frame.size.width, self.contentScrollView.bounds.size.height)];
		self.loadMoreFooterView.delegate = self;
		[self.contentScrollView addSubview:self.loadMoreFooterView];
		//[view release];
	}
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	if(!isLoaded)
		[self sendRequest:YES];	
}

- (void)shouldUpdate:(BOOL)flag {
    [self initContentView];
    isLoaded = !flag;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)editClicked:(id)sender{
    ShareStatusViewController *vc = [[ShareStatusViewController alloc] initWithNibName:@"ShareStatusViewController" bundle:nil];
    vc.delegate=self;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)loadWView {
    NSString *urlAddress = @"http://www.mynuvita.com"; 
    
    float contentWidth = self.contentScrollView.frame.size.width;
    float xx = 0.0;
    
    float defaultWebViewHeight = 48;
    int size = [wellnessWallData.itemArr count];
    float gap = 8.0;
    
    if(tagIndex > 1000)
        yy = self.contentScrollView.contentSize.height;

    NSLog(@"yy: %f", yy);

    for (int ii = 0; ii < size; ii++) {
        CGRect frame = CGRectMake(xx, yy, contentWidth, defaultWebViewHeight);
        UIWebView *wv = [[UIWebView alloc] initWithFrame:frame];
        wv.delegate = self;
        wv.tag = tagIndex++;
        [wv loadHTMLString: [wellnessWallData.itemArr objectAtIndex:ii] baseURL:[NSURL URLWithString:urlAddress]];
       ((UIScrollView*)[wv.subviews objectAtIndex:0]).scrollEnabled  = NO;
        
        [self.contentScrollView addSubview:wv];
        yy += defaultWebViewHeight + gap;
    }
    
    self.contentScrollView.contentSize = CGSizeMake(contentWidth, yy);
    self.contentScrollView.contentOffset = CGPointMake(0, 0);
}

- (void)resizeContentView:(UIWebView*)wv {
    UIScrollView *scrollView = (UIScrollView*)[wv.subviews objectAtIndex:0];
    CGFloat height = scrollView.contentSize.height;
    NSLog(@"%f webViewDidFinishLoad %d", height, wv.tag);
    
    NSInteger index = (wv.tag - 1000) + 1; 
    NSInteger pageSize = tagIndex - 1000;
    CGFloat offset = height - wv.frame.size.height;
    
    if(offset <= 0) return;
    
    offset += 10;
    height += 10;
    
    CGRect  frame = wv.frame;
    [wv setFrame:CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, height)];
    
    for (; index < pageSize; index++) {
        UIWebView *webView = (UIWebView*)[self.contentScrollView viewWithTag:(1000 + index)];
        
        CGRect oldFrame= webView.frame;
        [webView setFrame:CGRectMake(oldFrame.origin.x, (oldFrame.origin.y + offset), oldFrame.size.width, oldFrame.size.height)];
        //[self.contentScrollView layoutSubviews];
    }
    
    self.contentScrollView.contentSize = CGSizeMake(self.contentScrollView.contentSize.width, self.contentScrollView.contentSize.height +offset);
    
    [self.loadMoreFooterView setFrame:CGRectMake(0.0f, self.contentScrollView.contentSize.height, self.contentScrollView.frame.size.width, self.contentScrollView.bounds.size.height)];
}

- (void)clearBodyUI {
	self.view.hidden = YES;	
}


- (void)buildUI
{	
	totalElement = 0;
	
	if(wellnessWallData == nil || wellnessWallData.itemArr == nil) return ;
	
	//totalElement = [eduLessonWeekData.ItemsArray count];
	
	self.subheaderLb.text = @"Wellness Wall";//eduLessonWeekData.Title;
	//self.subheaderLb2.text = lessonWeekData.WeekLabel;
	[self loadWView ];
	//[self.dataTableView reloadData];
	
	self.view.hidden = NO;
	isLoaded = YES; 
}

#pragma mark - Web Service Delegate

- (void)sendRequest:(BOOL)flag {
	if(self.today == nil)
		self.today = [NSDate date];
	
	[tools startLoading:self.navigationController.view childView:loadingView text:LOADING_TEXT];
	[self makeRequestProgress:flag];
}


- (void)makeRequestProgress:(BOOL)flag {
	//NSCalendar
	NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
	[dateFormat setDateFormat:@"MM-dd-yyyy"];
	LoginResponseData *loginResponseData = [LoginPerser getLoginResponse];
	
	request *r=[[request alloc] initWithTarget:self
								 SuccessAction:@selector(onSucceffulLogin:resData:)
								 FailureAction:@selector(onFailureLogin)];
	[r getWellnessWall:loginResponseData.memberID pageNumber:[NSString stringWithFormat:@"%d", currPageNum]];
}

- (void)onSucceffulLogin:(ResponseStatus *)responseStatus resData:(NSObject *)obj {
    if (_reloading) {
        [self doneLoadingMoreData];
    }
    
	if (responseStatus == nil || ![responseStatus isSuccess]) {
		[tools stopLoading:loadingView];
		
		[self clearBodyUI];
		
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry"
                                                        message:NO_RECORD_FOUND
                                                       delegate:self
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles: nil];
		[alert show];
		return ;
	}
	
	wellnessWallData = (WellnessWallData *)obj;
	
	[self buildUI];
	
	[tools stopLoading:loadingView];		
}

- (void)onFailureLogin {
    if (_reloading) {
        [self doneLoadingMoreData];
    }
    
	[tools stopLoading:loadingView];
	[self clearBodyUI];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    NSLog(@"webView...%@", request);

    if(navigationType == UIWebViewNavigationTypeLinkClicked) {
        [[UIApplication sharedApplication] openURL:[request URL]];    
        return NO;
    }
    
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
    
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [self resizeContentView:webView];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    
}

#pragma mark - Data Source Loading / Reloading Methods

- (void)loadMoreWalls:(NSInteger)type{
	
	_reloading = YES;
    
    currPageNum++; 
    [self sendRequest:YES];
}

- (void)doneLoadingMoreData{
	
	_reloading = NO;
	[self.loadMoreFooterView loadMoreScrollViewDataSourceDidFinishedLoading:self.contentScrollView];
}


#pragma mark -
#pragma mark UIScrollViewDelegate Methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
	    if ([self isNextPageAvailable]) 
        [self.loadMoreFooterView loadMoreScrollViewDidScroll:scrollView];		
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    if ([self isNextPageAvailable]) 
        [self.loadMoreFooterView loadMoreScrollViewDidEndDragging:scrollView];
 }

#pragma mark - LoadMoreTableFooterDelegate Methods

- (void)loadMoreTableFooterDidTriggerRefresh:(LoadMoreTableFooterView *)view {
	[self loadMoreWalls:1];
}

- (BOOL)loadMoreTableFooterDataSourceIsLoading:(LoadMoreTableFooterView *)view {
	return _reloading;
}

- (BOOL)isNextPageAvailable {
    if(wellnessWallData == nil || wellnessWallData.itemArr == nil) return NO;
	int size = [wellnessWallData.itemArr count];
    if(size % 10 == 0)
        return YES;
    
    return NO;
}


@end
