//
//  TCViewController.h
//  Yaabbee
//
//  Created by Sarfaraj Biswas on 7/23/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "request.h"
#import "NuvitaAppDelegate.h"
#import "MyTools.h"
#import "LoginPerser.h"
#import "LoginResponseData.h"
#import "EduLessonWeekData.h"

#import  "QuestionItem.h"
#import  "AnswerItem.h"

@class EduLessonWeekData;
@class QuestionItem;
@class AnswerItem;

@interface ResultViewController : UIViewController {
	MyTools *tools;
	UIView *loadingView;
	
	EduLessonWeekData *eduLessonWeekData;
}

@property (nonatomic, retain) EduLessonWeekData *eduLessonWeekData;

@property(nonatomic,retain) IBOutlet UILabel *subheaderLb;
@property(nonatomic,retain) IBOutlet UILabel *fldLb1;
@property(nonatomic,retain) IBOutlet UILabel *fldLb2;
@property(nonatomic,retain) IBOutlet UILabel *fldLb3;
@property(nonatomic,retain) IBOutlet UILabel *fldLb4;
@property(nonatomic,retain) IBOutlet UILabel *fldLb5;
@property(nonatomic,retain) IBOutlet UILabel *fldLb6;
@property(nonatomic,retain) IBOutlet UILabel *fldLb7;

- (void)buildUI;

- (void)saveClicked:(id)sender;

//- (void)sendRequest;

@end
