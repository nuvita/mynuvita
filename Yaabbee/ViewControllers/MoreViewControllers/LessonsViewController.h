//
//  MoreViewController.h
//  Yaabbee
//
//  Created by Sarfaraj Biswas on 7/22/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "request.h"
#import "NuvitaAppDelegate.h"
#import "MyTools.h"
#import "LoginPerser.h"
#import "LoginResponseData.h"
#import "LessonWeekData.h"
#import  "LessonItem.h"

@class LessonWeekData;


@interface LessonsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>{
    //UITableView *dataTableView;
	
	MyTools *tools;
	UIView *loadingView;
	
	LessonWeekData *lessonWeekData;
	
	BOOL isLoaded;
	
	int daysNextPrev;
	//NSDate *today;

	int totalElement;	
	
}

@property (nonatomic, retain)NSDate *today;


@property (nonatomic,retain)IBOutlet UILabel *subheaderLb;
@property (nonatomic,retain)IBOutlet UILabel *subheaderLb2;

@property (nonatomic,retain)IBOutlet UITableView *dataTableView;

- (void)createRightBarItem;
- (void)login;
- (void)configureCell:(UITableViewCell *)cell withIndex:(NSIndexPath *)indexPath;
- (IBAction)moveNextClicked:(id)sender;
- (IBAction)movePrevClicked:(id)sender;

//

- (void)sendRequest:(BOOL)flag;

- (void)makeRequestProgress:(BOOL)flag;


- (void)getNextPrevDate:(BOOL)flag;

- (void)clearBodyUI;

- (void)manipulateNextPrev;

- (void)buildUI;


@end
