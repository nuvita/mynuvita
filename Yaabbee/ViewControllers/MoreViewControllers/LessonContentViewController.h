//
//  MyProfileViewController.h
//  Yaabbee
//
//  Created by Sarfaraj Biswas on 8/14/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "request.h"
#import "NuvitaAppDelegate.h"
#import "MyTools.h"
#import "LoginPerser.h"
#import "LoginResponseData.h"
#import "EduLessonWeekData.h"

#import  "QuestionItem.h"
#import  "AnswerItem.h"

@class EduLessonWeekData;
@class QuestionItem;
@class AnswerItem;

@interface LessonContentViewController : UIViewController <UIWebViewDelegate>
{
	MyTools *tools;
	UIView *loadingView;
	
	EduLessonWeekData *eduLessonWeekData;
	
	BOOL isLoaded;
	
	int daysNextPrev;
	//NSDate *today;
	
	UIButton *buttonPrev, *buttonNext;
	int totalElement;		
}

//lessonNumber
@property (nonatomic, retain)NSDate *today;
@property(nonatomic, retain)NSString *lessonNumber;
@property (nonatomic,retain)IBOutlet UILabel *subheaderLb;
@property (nonatomic, retain) IBOutlet UIWebView *webView;
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *activityIndicatorView;


- (void)editClicked:(id)sender;

- (void)loadWView;

//
- (void)sendRequest:(BOOL)flag;

- (void)makeRequestProgress:(BOOL)flag;


//- (void)getNextPrevDate:(BOOL)flag;

- (void)clearBodyUI;

//- (void)manipulateNextPrev;

- (void)buildUI;


@end
