

#import "LineChartView.h"

#define H_VAL_ORIGIN 50

@interface LineChartView() {
    CALayer *linesLayer;
    UILabel *disLabel;

    NSInteger xxOffset;
    NSInteger xxRightOffset;

    NSInteger yyOffset;
}

@end

@implementation LineChartView

@synthesize array;

@synthesize hInterval,vInterval;

@synthesize hDesc,vDesc;
@synthesize HD, WD;
@synthesize multiple;


- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        
        hInterval = 12;
        vInterval = 10;
        
        linesLayer = [[CALayer alloc] init];
        linesLayer.masksToBounds = YES;
        linesLayer.backgroundColor = [UIColor whiteColor].CGColor;
        
        [self.layer addSublayer:linesLayer];
        
        self.multiple = 1;
 
        self.WD = self.frame.size.width;
        self.HD = self.frame.size.height;
        
        yyOffset = 20;
        xxOffset = 28;
        xxRightOffset= 8;

        self.hDesc = [[NSMutableArray alloc] init];
        
        int min = 0;
        int sec = 5;
        for (int i = 0; i < 10; i++) {
            sec = (sec + 30 == 35) ? 35 : 5;
            NSString *formattedSec = [NSString stringWithFormat:@"%d", sec];
            if (sec == 5) {
                min += 1;
                formattedSec = @"05";
            }
            
            [self.hDesc addObject:[NSString stringWithFormat:@"%d:%@", min, formattedSec]];
        }
        
    }
    
    return self;
}

- (NSString *)graphTimeLabel:(NSInteger)val {
    return [NSString stringWithFormat:@"%d",val * self.multiple];

}

- (void)drawRect:(CGRect)rect {
    [self setClearsContextBeforeDrawing: YES];
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGFloat backLineWidth = 1.0f;
    CGFloat backMiterLimit = 0.f;
    
    CGContextSetLineWidth(context, backLineWidth);
    CGContextSetMiterLimit(context, backMiterLimit);
    CGContextSetLineJoin(context, kCGLineJoinRound);
    CGContextSetLineCap(context, kCGLineCapRound);
    CGContextSetBlendMode(context, kCGBlendModeNormal);
    CGContextSetStrokeColorWithColor(context, [UIColor blackColor].CGColor);
    
    float vvUnit = (self.HD - yyOffset)/11;
    float hhUnit = (self.WD - (xxOffset + xxRightOffset))/10;//290

    int y = self.frame.size.height;//460 ;
    float endXX = (self.WD - xxRightOffset);//hhUnit*12;
   
    CGPoint bPoint = CGPointMake(xxOffset, y);
    CGPoint ePoint = CGPointMake(endXX, y);

    CGContextMoveToPoint(context, bPoint.x, bPoint.y-yyOffset);
    CGContextAddLineToPoint(context, ePoint.x, ePoint.y-yyOffset);

    //base line
    bPoint = CGPointMake(xxOffset, self.HD);//460);
    ePoint = CGPointMake(xxOffset, 0);

    
    CGContextMoveToPoint(context, bPoint.x, bPoint.y-yyOffset);
    CGContextAddLineToPoint(context, ePoint.x, ePoint.y + 10);//ePoint.y-yyOffset);

    //horizonatl points
    float fldWd = 20;
    float fldHd = 20;

    for (int i = 0; i < hDesc.count; i++) {
        int tag = (1000 + i);
        
        UILabel *label = nil;
        label = (UILabel *)[self viewWithTag:tag];
        
        if(label == nil)
        {
            label = [[UILabel alloc]initWithFrame:CGRectMake((xxOffset + (i + 1)*hhUnit - fldWd/2), self.HD - yyOffset, fldWd, fldHd)];
            [label setTextAlignment:NSTextAlignmentCenter];
            [label setBackgroundColor:[UIColor clearColor]];
            [label setTextColor:[UIColor colorWithWhite:0.059 alpha:1.000]];
            label.numberOfLines = 1;
            label.adjustsFontSizeToFitWidth = YES;
            [label setFont:[UIFont systemFontOfSize:12]];

            [label setText:[hDesc objectAtIndex:i]];
            label.tag = tag;
            
            [self addSubview:label];
        }
        else
        {
            [label setText:[hDesc objectAtIndex:i]];
        }
    }//end loop
    
    //
    CGContextStrokePath(context);

    CGFloat pointLineWidth = 2.0f;
    CGFloat pointMiterLimit = 5.0f;
    
    CGContextSetLineWidth(context, pointLineWidth);
    CGContextSetMiterLimit(context, pointMiterLimit);
    CGContextSetLineJoin(context, kCGLineJoinRound);
    CGContextSetLineCap(context, kCGLineCapRound );
    CGContextSetBlendMode(context, kCGBlendModeNormal);
    CGContextSetStrokeColorWithColor(context, [UIColor colorWithRed:0.0 green:0.0 blue:1 alpha:0.5].CGColor);

    
    float xxUnit = hhUnit / 6;
    if([array count] > 0) {
        
        CGPoint p1 = [[array objectAtIndex:0] CGPointValue];
        p1.y -= H_VAL_ORIGIN;
        int i = 1;
        CGContextMoveToPoint(context, (p1.x*xxUnit) + xxOffset + 1, (self.HD - yyOffset) - p1.y*vvUnit/10);
        for (; i<[array count]; i++) {
            p1 = [[array objectAtIndex:i] CGPointValue];
            p1.y -= H_VAL_ORIGIN;

            CGPoint goPoint = CGPointMake(p1.x*xxUnit + xxOffset, (self.HD - yyOffset) - p1.y*vvUnit/10);
            CGContextAddLineToPoint(context, goPoint.x, goPoint.y);
        }
    }
    
	CGContextStrokePath(context);

}

@end
