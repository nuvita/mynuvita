//
//  OutBoxViewController.m
//  Yaabbee
//
//  Created by Sarfaraj Biswas on 8/22/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "ExerciseViewController.h"
#import "HomeViewController.h"
#import "constant.h"

#import "VideoPlayerViewController.h"

@interface ExerciseViewController ()

@property (retain, nonatomic) IBOutlet UIView *videoTest;
@property (nonatomic,strong) MPMoviePlayerController* moviePlayer;

@end

@implementation ExerciseViewController

@synthesize subheaderLb1, subheaderLb2, tableView_;
@synthesize WorkoutId;
@synthesize txtFldExercise;
@synthesize txtFldRest;
@synthesize btnStart;
@synthesize btnStop;
@synthesize btnPause;
@synthesize lbStatus;
@synthesize lbWorkTime;
@synthesize lbRound;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        //[UIDevice currentDevice].orientation = UIInterfaceOrientationLandscapeRight;
    }
    return self;
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
	isLoaded = NO;
	tools = [[MyTools alloc]init];
	loadingView = [tools createActivityIndicator1];	

    //Toolbar
    [self setTxtFldTool:self.txtFldExercise tag:1000];
	self.txtFldExercise.text = @"30";
    [self setTxtFldTool:self.txtFldRest tag:1001];
    self.txtFldRest.text = @"30";
    
	//set font
	self.subheaderLb1.font = [UIFont fontWithName:DEFAULT_BOLD_FONT_NAME size:headerFontSize];
	
	
	daysNextPrev = 0;
	self.view.hidden = YES;
	isLoaded = NO;	
    
    //title image
    float frameWD = self.view.frame.size.width;
    
    if([NuvitaAppDelegate isPhone5])
        frameWD = 1136.0/2;

    self.imageTitle.center = CGPointMake(frameWD/2, self.imageTitle.center.y);
    self.subheaderLb1.center = CGPointMake((frameWD - self.subheaderLb1.frame.size.width - 10) + self.subheaderLb1.frame.size.width/2, self.subheaderLb1.center.y);



    [self initExerciseData];
//    [self updateUI];
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:YES];
//    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
    [self.tabBarController.tabBar setHidden:YES];

    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Mobility"
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:self
                                                                            action:@selector(backBtnClicked:)];
    [self deleteSaveVideo];

	if(!isLoaded)
		[self sendRequest:YES];

    [self.tableView_ reloadData];
}

- (void)viewWillDisappear:(BOOL)animated {
	[super viewWillDisappear:YES];
    [self.tabBarController.tabBar setHidden:NO];
//    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
}

- (NSString *)getFilePath {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    return [documentsDirectory stringByAppendingPathComponent:@"video.mp4"];
}

- (void)deleteSaveVideo {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    [fileManager removeItemAtPath:[self getFilePath] error:NULL];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationLandscapeRight || interfaceOrientation == UIInterfaceOrientationLandscapeLeft);
}

- (BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationLandscapeLeft;
}

#pragma mark - Public Methods

- (void)updateUI {
    if ([NuvitaAppDelegate isIOS7]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        
        CGRect frame = self.tableView_.frame;
        frame.origin.y -= 25;
        frame.size.height += 30;
        
        self.tableView_.frame = frame;
    }
}

- (IBAction)backBtnClicked:(id)sender{
//    [self dismissViewControllerAnimated:YES completion:^{
//        
//    }];
    [self.navigationController popViewControllerAnimated:YES];
    [(NuvitaAppDelegate *)[[UIApplication sharedApplication] delegate] performSelector:@selector(reloadAppDelegateRootViewController)
                                                                            withObject:nil
                                                                            afterDelay:1.0];
}


- (void)setTxtFldTool:(UITextField*)txtFld tag:(NSInteger)tag
{
    UIToolbar *toolbar = [[UIToolbar alloc] init];
	[toolbar setBarStyle:UIBarStyleBlackTranslucent];
	[toolbar sizeToFit];
	
	UIBarButtonItem *flexButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
	UIBarButtonItem *doneBtn =[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(resignKeyboard:)];
	doneBtn.tag = tag;
    
	NSArray *itemsArray = [NSArray arrayWithObjects:flexButton, doneBtn, nil];
	
	[toolbar setItems:itemsArray];
    
    [txtFld setInputAccessoryView:toolbar];
}

#pragma mark - UITableViewDataSource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 36;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return totalElement;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = nil;
    
    CellIdentifier = CELL_EVEN;
    UITableViewCell *cell = nil;//[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
//    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.backgroundColor = [UIColor whiteColor];
        
//		if(indexPath.row%2)
//		{
//			cell.backgroundColor = [UIColor whiteColor];
//		}
//		else
//		{
//			cell.backgroundColor = [UIColor lightGrayColor];
//		}	
    	
        if (isTimerRunning && exerciseCount == indexPath.row) {
            cell.backgroundColor =  [UIColor colorWithRed:186.0/255.0 green:216.0/255.0 blue:120.0/255.0 alpha:1];
        }
        
		[self configureCell:cell withIndex:indexPath];			
//    }

		
    tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
    //2 weeks ago";
    // Configure the cell...
    return cell;
}

- (CGSize)calculateTextSizeWithText:(NSString *)text{
    CGSize size =  [text sizeWithFont:[UIFont systemFontOfSize:16] constrainedToSize: CGSizeMake(280.0, 480.0) lineBreakMode:NSLineBreakByWordWrapping];
    
    return size;
}

- (void)configureCell:(UITableViewCell *)cell withIndex:(NSIndexPath *)indexPath{
    
    NSMutableArray *arr = [mobilityExerciseData.dictMobilityExercise objectForKey:MOBILITY_EXERCISES];
    
	NSDictionary *dict = [arr objectAtIndex:indexPath.row];
    cell.textLabel.text = [dict objectForKey:EXERCISE_NAME];
    cell.textLabel.font = [UIFont fontWithName:DEFAULT_BOLD_FONT_NAME size:defaultFontSize];
    cell.textLabel.textColor = [UIColor blackColor];
    
    NSString *thumbLink  =  [dict objectForKey:EXERCISE_IMAGE_URL];
    if (thumbLink) {
        NSString* avatarLink = [NSString stringWithFormat:@"%@%@", BASE_URL, thumbLink];
        //replace https with http. oh dear.
        avatarLink = [avatarLink stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        [cell.imageView setImageWithURL:[NSURL URLWithString:avatarLink] 
                    placeholderImage:[UIImage imageNamed:@"videoicon.png"]]; 
    } 

}

- (void)didFinishPickFile:(NSString *)file {
    VideoPlayerViewController *videoPlayerViewController = [[VideoPlayerViewController alloc] init];
    videoPlayerViewController.streamURL = file;
    [self presentViewController:videoPlayerViewController animated:YES completion:^{

    }];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSMutableArray *arr = [mobilityExerciseData.dictMobilityExercise objectForKey:MOBILITY_EXERCISES];
    
	NSDictionary *dict = [arr objectAtIndex:indexPath.row];
    NSString *videoLink  =  [dict objectForKey:EXERCISE_VIDEO_URL];
    videoLink = [NSString stringWithFormat:@"%@%@", BASE_URL, videoLink];
    videoLink = [videoLink stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];

    [self didFinishPickFile:videoLink];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([cell respondsToSelector:@selector(tintColor)]) {
        
        if (tableView == self.tableView_) {
            CGFloat cornerRadius = 5.f;
            cell.backgroundColor = UIColor.clearColor;
            CAShapeLayer *layer = [[CAShapeLayer alloc] init];
            CGMutablePathRef pathRef = CGPathCreateMutable();
            CGRect bounds = CGRectInset(cell.bounds, 10, 0);
            BOOL addLine = NO;
            if (indexPath.row == 0 && indexPath.row == [tableView numberOfRowsInSection:indexPath.section]-1) {
                CGPathAddRoundedRect(pathRef, nil, bounds, cornerRadius, cornerRadius);
            } else if (indexPath.row == 0) {
                CGPathMoveToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMaxY(bounds));
                CGPathAddArcToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMinY(bounds), CGRectGetMidX(bounds), CGRectGetMinY(bounds), cornerRadius);
                CGPathAddArcToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMinY(bounds), CGRectGetMaxX(bounds), CGRectGetMidY(bounds), cornerRadius);
                CGPathAddLineToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMaxY(bounds));
                addLine = YES;
            } else if (indexPath.row == [tableView numberOfRowsInSection:indexPath.section]-1) {
                CGPathMoveToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMinY(bounds));
                CGPathAddArcToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMaxY(bounds), CGRectGetMidX(bounds), CGRectGetMaxY(bounds), cornerRadius);
                CGPathAddArcToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMaxY(bounds), CGRectGetMaxX(bounds), CGRectGetMidY(bounds), cornerRadius);
                CGPathAddLineToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMinY(bounds));
            } else {
                CGPathAddRect(pathRef, nil, bounds);
                addLine = YES;
            }
            
            layer.path = pathRef;
            CFRelease(pathRef);
            layer.fillColor = [UIColor colorWithWhite:1.f alpha:0.8f].CGColor;
            
            if (addLine == YES) {
                CALayer *lineLayer = [[CALayer alloc] init];
                CGFloat lineHeight = (1.f / [UIScreen mainScreen].scale);
                lineLayer.frame = CGRectMake(CGRectGetMinX(bounds), bounds.size.height-lineHeight, bounds.size.width, lineHeight);
                lineLayer.backgroundColor = tableView.separatorColor.CGColor;
                [layer addSublayer:lineLayer];
            }
            
            UIView *testView = [[UIView alloc] initWithFrame:bounds];
//#warning disabled kay e convert sa apportable
            [testView.layer insertSublayer:layer atIndex:0];
            testView.backgroundColor = [UIColor clearColor];
            cell.backgroundView = testView;
            
            UIView* selectedBackgroundView = [[UIView alloc] initWithFrame:bounds];
            UIView* visibleSelectedBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(10, 0.0f, bounds.size.width, bounds.size.height)];
            visibleSelectedBackgroundView.backgroundColor = tableView.separatorColor;
            [selectedBackgroundView addSubview:visibleSelectedBackgroundView];
            cell.selectedBackgroundView = selectedBackgroundView;
            
        }
    }
}

#pragma mark - Helpers

//- (NSUInteger)supportedInterfaceOrientations{
//    //Modify for supported orientations
//    return UIInterfaceOrientationMaskLandscapeLeft|UIInterfaceOrientationMaskLandscapeRight;
//    //return UIInterfaceOrientationMaskPortrait|UIInterfaceOrientationMaskPortraitUpsideDown;
//    //UIInterfaceOrientationLandscapeLeft|UIInterfaceOrientationLandscapeRight|
//}


//- (NSUInteger)supportedInterfaceOrientations
//{
//    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
//        return (UIInterfaceOrientationMaskPortrait|
//                UIInterfaceOrientationMaskPortraitUpsideDown);
//    else
//        return UIInterfaceOrientationMaskAll;
//}


//

- (void)resignKeyboard:(UIBarButtonItem*)sender {
    int tag = sender.tag;
    switch (tag) {
        case 1000:
            [self.txtFldExercise resignFirstResponder];
            break;
        case 1001:
            [self.txtFldRest resignFirstResponder];
            break;
        default:
            break;
    }
}

//- (void)dealloc{
    //[mobilityExerciseData release];
    //[tools release];
//    if(nsTimer)
//        [nsTimer invalidate];
//    
//    self.subheaderLb1 = nil;
//    self.subheaderLb2 = nil;
//    self.tableView_ = nil;
//    self.WorkoutId = nil;
//    self.txtFldExercise = nil;
//    self.txtFldRest = nil;
//    self.btnStart = nil;
//    self.btnStop = nil;
//    self.btnPause  = nil;
//    self.lbStatus =nil;
//    self.lbWorkTime = nil;
//    self.lbRound = nil;
//    
//    [_imageTitle release];
//    [super dealloc];
//}


//----------------------

- (void)clearBodyUI
{
	//self.view.hidden = YES;
    
    [self backBtnClicked:nil];
}


- (void)buildUI
{
	totalElement = 0;
	
	[self.tableView_ reloadData];
	
  	if(mobilityExerciseData == nil || mobilityExerciseData.dictMobilityExercise == nil) return ;
    NSMutableArray *arr = [mobilityExerciseData.dictMobilityExercise objectForKey:MOBILITY_EXERCISES];
	totalElement = [arr count];	
    
    if(totalElement == 0)
	{
		[self clearBodyUI ];
		
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry" message:NO_RECORD_FOUND delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
		[alert show];
		
		return ;		
	}
    
	LoginResponseData *loginResponseData = [LoginPerser getLoginResponse];
	
	self.subheaderLb2.text = mobilityExerciseData.WorkoutName;//@"13/17";
	self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:[loginResponseData programName]
                                                                              style:UIBarButtonItemStylePlain
                                                                             target:nil
                                                                             action:nil];
	
	[self.tableView_ reloadData];
	
	self.view.hidden = NO;
}

- (void)initExerciseData
{
    exerciseCount = 0;
    self.btnStop.tag = 1000;
    isTimerRunning = NO;
    self.lbStatus.text = @"EXERCISE";
    self.lbWorkTime.text = @":00";
    self.lbRound.text = @"Rounds: 0";
    timerCount = 0;
    roundCount = 0;
    isExerciseMode = YES;
    self.txtFldExercise.enabled = YES;
    self.txtFldRest.enabled = YES;
    self.btnStart.tag = 1000;
    
    [self.tableView_ reloadData];
}

- (IBAction)startBtnClicked:(id)sender
{
    if (self.isValidInput && nsTimer == nil) {
        [self startExerciseTimer]; 
        self.txtFldExercise.enabled = NO;
        self.txtFldRest.enabled = NO;
        [self.tableView_ reloadData];
    }
}

- (IBAction)stopBtnClicked:(id)sender
{
    UIButton* btn = (UIButton*)sender;
    if(btn.tag == 1000)
    {
        [btn setBackgroundImage:[UIImage imageNamed:@"quit_btn.png"] forState:UIControlStateNormal];
        btn.tag = 1001;
        if(nsTimer)
            [nsTimer invalidate];
        nsTimer = nil;
        self.btnStart.tag = 1001;
    }
    else if(btn.tag == 1001)
    {
        [btn setBackgroundImage:[UIImage imageNamed:@"stop_btn.png"] forState:UIControlStateNormal];
        nsTimer = nil;
        [self initExerciseData];
    }
}

- (IBAction)pauseBtnClicked:(id)sender
{
    if(nsTimer)
    {
        self.btnStart.tag = 1001;
        [nsTimer invalidate];
    }
    nsTimer = nil;
}

- (void)startExerciseTimer
{
    isTimerRunning = YES;
    if(self.btnStart.tag == 1000)
    {
        timerCount = [self.txtFldExercise.text intValue];
        [self setStatusLabel: @"EXERCISE" val:timerCount round:roundCount];
    }
    
    if(self.btnStop.tag == 1001)
    {
        [self.btnStop setBackgroundImage:[UIImage imageNamed:@"stop_btn.png"] forState:UIControlStateNormal];
        self.btnStop.tag = 1000;
    }
    
    nsTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timerEvent:) userInfo:nil repeats:YES];
}

//
- (void)finishExercise
{
    if(nsTimer)
    {
        self.btnStart.tag = 1001;
        [nsTimer invalidate];
    }
    nsTimer = nil;
    
}

//NSTimer
- (void)timerEvent:(id)sender
{
    NSString *lb = @"EXERCISE";
    
    if (isExerciseMode) {
        timerCount --;
        if (timerCount < 0) {
            timerCount = [self.txtFldRest.text intValue];
            isExerciseMode = NO;
        }
    }
    else
    {
        timerCount --;
        if (timerCount < 0) {
            timerCount =  [self.txtFldExercise.text intValue];
            isExerciseMode = YES;
            exerciseCount++;
           // [self.tableView_ reloadData];
            [self highlightingTableRow];
        }
    }
    
    if(!isExerciseMode)
        lb = @"REST";
    
    [self setStatusLabel:lb val:timerCount round:roundCount];
}

- (void)highlightingTableRow
{
    NSMutableArray *arr = [mobilityExerciseData.dictMobilityExercise objectForKey:MOBILITY_EXERCISES];
    if(exerciseCount >= [arr count])
    {
        roundCount++;
        exerciseCount = 0;
    }
    
    NSIndexPath *path= [NSIndexPath indexPathForRow:exerciseCount inSection:0];
    [self.tableView_ scrollToRowAtIndexPath:path atScrollPosition:UITableViewScrollPositionTop animated:YES];
    [self.tableView_ reloadData];
}

- (void)setStatusLabel:(NSString*)lb val:(NSInteger)count round:(NSInteger)roundCnt
{
    self.lbStatus.text = lb;
    self.lbWorkTime.text = [NSString stringWithFormat:@":%d", count];
    self.lbRound.text = [NSString stringWithFormat:@"Rounds: %d", roundCnt];
}

- (BOOL)isValidInput
{
    if ([self.txtFldExercise.text intValue] >0 && [self.txtFldRest.text intValue] > 0) {
        return YES;
    }
    
    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please enter the valid input." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    
    [alertView show];
    
    return NO;
}

- (void)sendRequest:(BOOL)flag
{
    NuvitaAppDelegate *app = (NuvitaAppDelegate*)[[UIApplication sharedApplication] delegate];
    
	[tools startLoading:app.window childView:loadingView text:LOADING_TEXT];
	
	[self makeRequestStarProgress:flag];
}

- (void)makeRequestStarProgress:(BOOL)flag
{	
	request *r=[[request alloc] initWithTarget:self
								 SuccessAction:@selector(onSucceffulLogin:resData:)
								 FailureAction:@selector(onFailureLogin)];
    
    LoginResponseData *loginResponseData = [LoginPerser getLoginResponse];

	[r getMobilityExercises:loginResponseData.memberID  workoutId:self.WorkoutId];
}

- (void)onSucceffulLogin:(ResponseStatus *)responseStatus resData:(NSObject *)obj {
	if (responseStatus == nil || ![responseStatus isSuccess])
	{
		[tools stopLoading:loadingView];
		
		[self clearBodyUI];
		
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry" message:NO_RECORD_FOUND delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
		[alert show];
		
		return ;
	}
    mobilityExerciseData = (MobilityExerciseData*)obj;
    isLoaded = YES;
	[self buildUI];
	
	[tools stopLoading:loadingView];		
}

- (void)onFailureLogin {
	[tools stopLoading:loadingView];
	[self clearBodyUI];
}


////play video
//- (void) goplay:(NSString*)moviePath
//{
//    //NSString *moviePath = @”link”;
//    
//    NSLog(@"movie path: %@",moviePath);
//    NSURL *movieURL = [NSURL URLWithString:[moviePath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
////    NSURL *movieURL = [NSURL URLWithString:moviePath];
////    UIGraphicsBeginImageContext(CGSizeMake(1,1));
//    MPMoviePlayerViewController *movieViewController = [[MPMoviePlayerViewController alloc] initWithContentURL:movieURL];
//    movieViewController.moviePlayer.allowsAirPlay = NO;
////    UIGraphicsEndImageContext();
////    if ([self respondsToSelector:@selector(presentMoviePlayerViewControllerAnimated:)]) {
//        [self presentMoviePlayerViewControllerAnimated:movieViewController];
////        [movieViewController release], movieViewController = nil;
////    } else {
////        // Register to receive a notification when the movie has finished playing.
////        [[NSNotificationCenter defaultCenter] addObserver:self
////                                                 selector:@selector(moviePlayBackDidFinish)
////                                                     name:MPMoviePlayerPlaybackDidFinishNotification
////                                                   object:movieViewController];
////    }
//}
//
////- (void) moviePlayBackDidFinish:(NSNotification*)notification
////{
//////    MPMoviePlayerController *moviePlayer = [notification object];
//////    [[NSNotificationCenter defaultCenter] removeObserver:self
//////                                                    name:MPMoviePlayerPlaybackDidFinishNotification
//////                                                  object:moviePlayer];
//////    [moviePlayer release];
//////    moviePlayer = nil;
////}

@end
