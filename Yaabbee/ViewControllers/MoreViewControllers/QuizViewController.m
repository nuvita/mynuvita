//
//  EditMyProfileViewController.m
//  Yaabbee
//
//  Created by Sarfaraj Biswas on 8/15/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "QuizViewController.h"
#import "ResultViewController.h"

#define TABLE_OFFSETY 110;

@implementation QuizViewController

@synthesize profileTable;
@synthesize btnSubmit;
@synthesize questionLbl, subheaderLb,eduLessonWeekData,subheaderLb2;

#pragma mark - View lifecycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidLoad {
    [super viewDidLoad];
   
    app = (NuvitaAppDelegate*)[[UIApplication sharedApplication] delegate];
	
    // Do any additional setup after loading the view from its nib.
	isLoaded = NO;
	tools = [[MyTools alloc]init];
	loadingView = [tools createActivityIndicator1];	
	
	
    JBCustomButton *buttonPrev =  [[JBCustomButton alloc] initWithFrame:CGRectMake(0, 20, 30, 30)];
    [buttonPrev setButtonStyle:JBCustomButtonArrowLeftStyle];
	[buttonPrev addTarget:self action:@selector(movePrevClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self movePrevClicked:nil];
    
    JBCustomButton *buttonNext =  [[JBCustomButton alloc] initWithFrame:CGRectMake(0, 20, 30, 30)];
    [buttonNext setButtonStyle:JBCustomButtonArrowRightStyle];
	[buttonNext addTarget:self action:@selector(moveNextClicked:) forControlEvents:UIControlEventTouchUpInside];
	
	self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:
                                               [[UIBarButtonItem alloc] initWithCustomView:buttonNext],
                                               [[UIBarButtonItem alloc] initWithCustomView:buttonPrev], nil];
    [[self.navigationItem.rightBarButtonItems lastObject] setEnabled:NO];
	
	currQuestion = 0;
	totalElement = 0;
	self.view.hidden = YES;
	
	isLoaded= NO;	
	
	self.subheaderLb.font = [UIFont fontWithName:DEFAULT_NORMAL_FONT_NAME size:defaultFontSize];
	self.subheaderLb2.font = [UIFont fontWithName:DEFAULT_BOLD_FONT_NAME size:defaultFontSize];

	[self buildUI];
//    [self updateUI];
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - UITableViewDataSource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 54;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return totalElement;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    [self configureCell:cell withIndex:indexPath];
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell withIndex:(NSIndexPath *)indexPath{
    
	AnswerItem *answerItem = (AnswerItem*)[currQuestionItem.Answers objectAtIndex:indexPath.row];
	cell.textLabel.text = answerItem.Text;
	cell.textLabel.textColor = [UIColor grayColor];
	cell.textLabel.numberOfLines = 0;
	cell.textLabel.font = [UIFont fontWithName:DEFAULT_NORMAL_FONT_NAME size:smallFontSize];	
	
	if(answerItem.flag)
        cell.imageView.image = [UIImage imageNamed:@"anscheck.png"];
	else
		cell.imageView.image = [UIImage imageNamed:@"ansuncheck.png"]; 

}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
	AnswerItem *answerItem = nil;
    for(int ii = 0; ii < totalElement; ii ++) {
        answerItem = (AnswerItem*)[currQuestionItem.Answers objectAtIndex:ii];
        
        if(indexPath.row == ii) {
            currQuestionItem.isAnswered = YES;
            currQuestionItem.answeredIndex = ii;
            answerItem.flag = YES;
        } else {
            answerItem.flag = NO;
        }
    }		

    [tableView reloadData];
    [self setSubmitBtn];	
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {

    if ([cell respondsToSelector:@selector(tintColor)]) {

        if (tableView == self.profileTable) {
            CGFloat cornerRadius = 5.f;
            cell.backgroundColor = UIColor.clearColor;
            CAShapeLayer *layer = [[CAShapeLayer alloc] init];
            CGMutablePathRef pathRef = CGPathCreateMutable();
            CGRect bounds = CGRectInset(cell.bounds, 10, 0);

            BOOL addLine = NO;
            if (indexPath.row == 0 && indexPath.row == [tableView numberOfRowsInSection:indexPath.section]-1) {
                CGPathAddRoundedRect(pathRef, nil, bounds, cornerRadius, cornerRadius);
            } else if (indexPath.row == 0) {
                CGPathMoveToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMaxY(bounds));
                CGPathAddArcToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMinY(bounds), CGRectGetMidX(bounds), CGRectGetMinY(bounds), cornerRadius);
                CGPathAddArcToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMinY(bounds), CGRectGetMaxX(bounds), CGRectGetMidY(bounds), cornerRadius);
                CGPathAddLineToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMaxY(bounds));
                addLine = YES;
            } else if (indexPath.row == [tableView numberOfRowsInSection:indexPath.section]-1) {
                CGPathMoveToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMinY(bounds));
                CGPathAddArcToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMaxY(bounds), CGRectGetMidX(bounds), CGRectGetMaxY(bounds), cornerRadius);
                CGPathAddArcToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMaxY(bounds), CGRectGetMaxX(bounds), CGRectGetMidY(bounds), cornerRadius);
                CGPathAddLineToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMinY(bounds));
            } else {
                CGPathAddRect(pathRef, nil, bounds);
                addLine = YES;
            }

            layer.path = pathRef;
            CFRelease(pathRef);
            layer.fillColor = [UIColor colorWithWhite:0.9f alpha:0.8f].CGColor;

            if (addLine == YES) {
                CALayer *lineLayer = [[CALayer alloc] init];
                CGFloat lineHeight = (1.f / [UIScreen mainScreen].scale);
                lineLayer.frame = CGRectMake(CGRectGetMinX(bounds), bounds.size.height-lineHeight, bounds.size.width, lineHeight);
                lineLayer.backgroundColor = tableView.separatorColor.CGColor;
                [layer addSublayer:lineLayer];
            }

            UIView *testView = [[UIView alloc] initWithFrame:bounds];
            [testView.layer insertSublayer:layer atIndex:0];
            testView.backgroundColor = [UIColor clearColor];
            cell.backgroundView = testView;

            UIView* selectedBackgroundView = [[UIView alloc] initWithFrame:bounds];
            UIView* visibleSelectedBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(10, 0.0f, bounds.size.width, bounds.size.height)];
            visibleSelectedBackgroundView.backgroundColor = tableView.separatorColor;
            [selectedBackgroundView addSubview:visibleSelectedBackgroundView];
            cell.selectedBackgroundView = selectedBackgroundView;
        }
    }
}



- (void)updateUI {
    if ([NuvitaAppDelegate isIOS7]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        
        CGRect frame = self.profileTable.frame;
        frame.origin.y -= 25;
        frame.size.height += 30;
        
        self.profileTable.frame = frame;
    }
}


- (IBAction)moveNextClicked:(id)sender {
    
    UIButton *buttonPrev = [self.navigationItem.rightBarButtonItems lastObject];
    UIButton *buttonNext = [self.navigationItem.rightBarButtonItems firstObject];
    
	if(currQuestion  < totalQuestion - 1)
		currQuestion++;
	
	if(currQuestion == totalQuestion - 1) {
		buttonNext.enabled = NO;
	}
	
	if(currQuestion > 0)
		buttonPrev.enabled = YES;
	
	[self showNextPrevQuestion];
	
	[self setSubmitBtn];
}

- (IBAction)movePrevClicked:(id)sender {
    
    UIButton *buttonPrev = [self.navigationItem.rightBarButtonItems lastObject];
    UIButton *buttonNext = [self.navigationItem.rightBarButtonItems firstObject];
    
	if(currQuestion  > 0)
		currQuestion--;
	
	if(currQuestion == 0)
		buttonPrev.enabled = NO;
	
	if (currQuestion < totalQuestion - 1) {
		buttonNext.enabled = YES;
	}
	
	[self showNextPrevQuestion];
}

- (void)setSubmitBtn
{
	if(currQuestion == totalQuestion - 1 && [self isAnsweredAllQuestion])
	{
		UIBarButtonItem *signoutButton = [[UIBarButtonItem alloc] initWithTitle:@"Submit" style:UIBarButtonItemStylePlain target:self action:@selector(btnSubmitClicked:)];
		self.navigationItem.rightBarButtonItem=nil;
		[self.navigationItem setRightBarButtonItem:signoutButton];		
	}
}

- (IBAction)btnSubmitClicked:(id)sender
{
	 [self sendRequest];
}

- (BOOL)isAnsweredAllQuestion
{
	for (int ii = 0; ii < totalQuestion; ii++) {
		QuestionItem *item = (QuestionItem*)[self.eduLessonWeekData.ItemsArray objectAtIndex:ii];
		if (!item.isAnswered) {
			return NO;
		}
	}
	
	return YES;
}

- (void)buildUI {
	totalQuestion = 0;
	currQuestion = 0;
    
	if(self.eduLessonWeekData == nil || self.eduLessonWeekData.ItemsArray == nil) return ;
	
	totalQuestion = [self.eduLessonWeekData.ItemsArray count];
	if(totalQuestion == 0) {
		[self clearBodyUI ];
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry"
                                                        message:NO_RECORD_FOUND
                                                       delegate:self
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles: nil];
		[alert show];
		return ;		
	}
	
	[self showNextPrevQuestion];
	
	self.view.hidden = NO;
	isLoaded = YES;
}

- (void)showNextPrevQuestion {
	currQuestionItem = (QuestionItem*)[self.eduLessonWeekData.ItemsArray objectAtIndex:currQuestion];
	totalElement = [currQuestionItem.Answers count];
	self.subheaderLb.text = eduLessonWeekData.Title;
	
	self.questionLbl.text = currQuestionItem.Text;

	[self.profileTable reloadData];
	self.subheaderLb.text = [NSString stringWithFormat:@"Question %i of %i", (currQuestion + 1), totalQuestion];
}


- (void)clearBodyUI {

	self.view.hidden = YES;	
}

- (void)sendRequest {
	if (![self isAnsweredAllQuestion]) {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry"
                                                        message:@"Please answer the all question and try again"
                                                       delegate:self
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles: nil];
		[alert show];
		return ;				
	}
	
	LoginResponseData *loginResponseData = [LoginPerser getLoginResponse];
	if(self.eduLessonWeekData == nil || self.eduLessonWeekData.ItemsArray == nil) return ;
    
	int ltotalQuestion = [self.eduLessonWeekData.ItemsArray count];
	NSString *correctAnswers = [NSString stringWithFormat:@"%d", [self.eduLessonWeekData getNumCorrect]];
	NSString *totalQuestions = [NSString stringWithFormat:@"%d", ltotalQuestion];
	
	[tools startLoading:self.navigationController.view childView:loadingView text:LOADING_TEXT];
	request *r=[[request alloc] initWithTarget:self
								 SuccessAction:@selector(onSucceffulLogin:resData:)
								 FailureAction:@selector(onFailureLogin)];
	[r saveQuiz:loginResponseData.memberID date:loginResponseData.today lessonNumber:self.eduLessonWeekData.LessonNumber correctAnswers:correctAnswers totalQuestions:totalQuestions];
}


- (void)onSucceffulLogin:(ResponseStatus *)responseStatus resData:(NSObject *)obj {
	[tools stopLoading:loadingView];
	
	if (responseStatus == nil || [responseStatus isSuccess]) {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry"
                                                        message:@"Couldnot save member quiz to server, please try again later."
                                                       delegate:self
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles: nil];
		[alert show];
		return ;
        
	} else {
	
		ResultViewController *resultVC = [[ResultViewController alloc] initWithNibName:@"ResultViewController" bundle:nil];
		resultVC.eduLessonWeekData = self.eduLessonWeekData;
		[self.navigationController pushViewController:resultVC animated:YES];
	}
}

- (void)onFailureLogin {
	[tools stopLoading:loadingView];
}

@end
