//
//  VideoPlayerViewController.m
//  mynuvita
//
//  Created by John on 3/17/14.
//
//

#import "VideoPlayerViewController.h"

@interface VideoPlayerViewController ()

@property (retain, nonatomic) IBOutlet UIView *videoThumbnailView;

@property (nonatomic,strong) MPMoviePlayerController* moviePlayerController;

@end

@implementation VideoPlayerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    NSURL *url = [NSURL URLWithString:_streamURL];
    _moviePlayerController = [[MPMoviePlayerController alloc] initWithContentURL:url];
    CGRect frame = _videoThumbnailView.frame;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayBackDidFinish:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:_moviePlayerController];


    [_moviePlayerController.view setFrame:frame];
    _moviePlayerController.controlStyle = MPMovieControlStyleFullscreen;
    _moviePlayerController.shouldAutoplay = NO;

    [self.view addSubview:_moviePlayerController.view];
    [_moviePlayerController setFullscreen:YES animated:YES];
}

- (void)moviePlayBackDidFinish:(NSNotification*)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:MPMoviePlayerPlaybackDidFinishNotification
                                                  object:nil];

    [self dismissViewControllerAnimated:YES completion:^{
        NSLog(@"finish playing movie");
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}

- (BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationLandscapeRight;
}

@end
