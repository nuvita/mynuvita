//
//  Vo2MaxChart.m
//  mynuvita
//
//  Created by John on 4/29/14.
//
//

#import "Vo2MaxChart.h"

@interface Vo2MaxChart ()

//@property (nonatomic, retain) CALayer *linesLayer;
@property (nonatomic, retain) NSArray *rateList;

@property (assign) NSInteger xxOffset;
@property (assign) NSInteger yyOffset;
@property (assign) NSInteger xxRightOffset;

@end

@implementation Vo2MaxChart

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        _ratio = 0;
        
        _xxOffset = 20;
        _yyOffset = 10;
        _xxRightOffset = 10;

    }
    
    return self;
}

- (NSString *)getLabelAtIndex:(NSInteger)index withCount:(NSInteger)count {
    
    if (index == count) {
        return [[_xDataList lastObject] valueForKey:@"label"];
    }

    return [[_xDataList objectAtIndex:index * _ratio] valueForKey:@"label"];
}

- (void)drawRect:(CGRect)rect {
    [self setClearsContextBeforeDrawing: YES];
    
    CGContextRef context = UIGraphicsGetCurrentContext();

    int height = self.frame.size.height;
    int width = self.frame.size.width;

    CGPoint bPoint = CGPointMake(_xxOffset, _yyOffset);
    CGPoint ePoint = CGPointMake(_xxOffset, height - _yyOffset);
    
    CGContextSetLineWidth(context, 1.0f);
    CGContextSetMiterLimit(context, 0.f);
    CGContextSetLineJoin(context, kCGLineJoinRound);
    CGContextSetLineCap(context, kCGLineCapRound);
    CGContextSetBlendMode(context, kCGBlendModeNormal);
    CGContextSetStrokeColorWithColor(context, [UIColor blackColor].CGColor);
    CGContextSaveGState(context);
    
    CGContextMoveToPoint(context, bPoint.x, bPoint.y);
    CGContextAddLineToPoint(context, ePoint.x, ePoint.y);
    CGContextAddLineToPoint(context, ePoint.x + (width - _xxOffset - _xxRightOffset), ePoint.y);
    CGContextStrokePath(context);

    NSInteger zeroOffsetRatio = height * .20; //.. set 20% of the height space as offset
    NSInteger yHeight = height - _yyOffset - zeroOffsetRatio;
    
    NSLog(@"norm: %@", _normCategoryList);
    
    if ([_normCategoryList count] > 0) {
        
        NSLog(@"fuck???");
        _minimum = [[[_normCategoryList objectAtIndex:1] valueForKey:@"a:Value"] integerValue];
        _maximum = [[[_normCategoryList lastObject] valueForKey:@"a:Value"] integerValue] - _minimum;
        [_normCategoryList enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            
            if (idx > 0) {
                NSDictionary *data = (NSDictionary *)obj;
                
                NSInteger yPoint = [[data valueForKey:@"a:Value"] integerValue] - [[[_normCategoryList objectAtIndex:1] valueForKey:@"a:Value"] integerValue];
                CGPoint goPoint = CGPointMake( _xxOffset, yHeight - (yPoint * yHeight / _maximum));
                goPoint.y += _yyOffset;
                
                CGPoint bPoint = CGPointMake(_xxOffset, goPoint.y);
                CGPoint ePoint = CGPointMake(_xxOffset + (width - _xxOffset - _xxRightOffset), goPoint.y);
                
                CGContextRestoreGState(context);
                
                CGContextMoveToPoint(context, bPoint.x - 5, bPoint.y);
                CGContextAddLineToPoint(context, bPoint.x, bPoint.y);
                CGContextStrokePath(context);
                CGContextSaveGState(context);
                
                CGFloat dashes[] = {2,2};
                CGContextSetLineDash(context, 2.0, dashes, 2);
                CGContextSetStrokeColorWithColor(context, [UIColor colorWithWhite:0.7 alpha:1.0].CGColor);
                CGContextMoveToPoint(context, bPoint.x, bPoint.y);
                CGContextAddLineToPoint(context, ePoint.x, ePoint.y);
                CGContextStrokePath(context);
                
                
                UILabel *leftlbl = [[UILabel alloc] initWithFrame:CGRectMake(bPoint.x - 30, bPoint.y  - 13 , 25, 15)];
                leftlbl.backgroundColor = [UIColor clearColor];
                leftlbl.font = [UIFont systemFontOfSize:8.0];
                leftlbl.textAlignment = NSTextAlignmentRight;
                leftlbl.text = [NSString stringWithFormat:@"%@", [data valueForKey:@"a:Value"]];
                
                [self addSubview:leftlbl];
                
                UILabel *normlbl = [[UILabel alloc] initWithFrame:CGRectMake(bPoint.x + 2, bPoint.y - 10, 50, 10)];
                normlbl.backgroundColor = [UIColor clearColor];
                normlbl.textColor = [UIColor grayColor];
                normlbl.font = [UIFont systemFontOfSize:8.0];
                normlbl.textAlignment = NSTextAlignmentLeft;
                normlbl.text = [NSString stringWithFormat:@"%@", [data valueForKey:@"a:Label"]];
                
                [self addSubview:normlbl];
            }
           
        }];
    
    } else {
    
        NSLog(@"max: %d", _maximum);
        NSLog(@"min: %d", _minimum);
        float topGap = (height - _yyOffset - 10) / 6.0;
        float topOffset = (_maximum - _minimum) / 7.0;
        int topValueHolder = topOffset;
        
        bPoint = CGPointMake(_xxOffset, height - _yyOffset);
        
        for (int i = 1; i < 7; i ++) {
            
            bPoint.y -= topGap;
            CGContextRestoreGState(context);
            
            CGContextMoveToPoint(context, bPoint.x, bPoint.y);
            CGContextAddLineToPoint(context, bPoint.x - 5, bPoint.y);
            CGContextStrokePath(context);
            
            CGContextSaveGState(context);
            
            CGFloat dashes[] = {2,2};
            CGContextSetLineDash(context, 2.0, dashes, 2);
            CGContextSetStrokeColorWithColor(context, [UIColor colorWithWhite:0.7 alpha:1.0].CGColor);
            CGContextMoveToPoint(context, bPoint.x, bPoint.y);
            CGContextAddLineToPoint(context, width, bPoint.y);
            CGContextStrokePath(context);
            
            UILabel *label = (UILabel *)[self viewWithTag:i + 100];
            if (label == nil) {
                label = [[UILabel alloc] initWithFrame:CGRectMake(-5, bPoint.y - 5, 20, 10)];
                label.tag = i + 100;
                label.backgroundColor = [UIColor clearColor];
                label.textAlignment = NSTextAlignmentRight;
                label.font = [UIFont systemFontOfSize:8.0];
                
                [self addSubview:label];
            }
            
            
            label.text = [NSString stringWithFormat:@"%d", (int)topOffset * i];
//            topValueHolder += (int)topOffset;
        }
        
    }
    
    NSLog(@"shittt???");
 
    CGFloat normal[] = {1};
    CGContextSetLineDash(context, 0, normal, 0);
    CGContextSetStrokeColorWithColor(context, [UIColor blackColor].CGColor);
 
    NSInteger count = ([_xDataList count] > 5) ? ([_xDataList count] / _ratio) + 1 : [_xDataList count];
    float xxWidhtGap = (width - _xxOffset - _xxRightOffset) / count;
    float space = ([_xDataList count] > 5) ? _xxOffset : xxWidhtGap / 2;
    
    for (int i = 0; i < count; i++) {

        CGPoint point = CGPointMake((xxWidhtGap * (i + 1)) + _xxOffset, height - _yyOffset);
        point.x -= space;
        
        CGContextMoveToPoint(context, point.x, point.y);
        CGContextAddLineToPoint(context, point.x, point.y + 5);
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(point.x - 25, point.y + 5, 50, 11)];
        label.backgroundColor = [UIColor clearColor];
        label.font = [UIFont systemFontOfSize:8.0];
        label.textAlignment = NSTextAlignmentCenter;
        label.text = [self getLabelAtIndex:i withCount:count - 1];

        [self addSubview:label];
    }
    
    CGContextStrokePath(context);
    
    CGContextSetLineWidth(context, 3.0f);
    CGContextSetMiterLimit(context, 5.0f);
    CGContextSetLineJoin(context, kCGLineJoinMiter);
    CGContextSetLineCap(context, kCGLineCapRound);
    CGContextSetBlendMode(context, kCGBlendModeNormal);
    
    float xxRatio = (width - _xxOffset - _xxRightOffset) / [_xDataList count];
    if ([_xDataList count] > 0) {
    
//        if ([_xDataList count] > 5)
//            space = 0;
        
        __block float previous = 0.0;
        [_xDataList enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
           
            NSDictionary *data = (NSDictionary *)obj;
            CGPoint temp;
            CGPoint p = [[data valueForKey:@"points"] CGPointValue];
            p.y -= _minimum;
            
            CGContextSetStrokeColorWithColor(context, [UIColor colorWithRed:0.9 green:0.6 blue:0.2 alpha:1.0].CGColor);

            if (idx == 0) {
                
                temp = CGPointMake(((p.x + 1) * xxRatio) + _xxOffset, yHeight - (p.y * yHeight / _maximum));
                temp.x -= space;
                temp.y += _yyOffset;
                CGContextMoveToPoint(context, temp.x, temp.y);
            
            } else {
                
                CGPoint goPoint = CGPointMake(((p.x + 1) * xxRatio) + _xxOffset, yHeight - (p.y * yHeight / _maximum));
                goPoint.x -= space;
                goPoint.y += _yyOffset;
                CGContextAddLineToPoint(context, goPoint.x, goPoint.y);
                temp = goPoint;
            }
            
            
            
            UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(temp.x - 4, temp.y - 4, 8, 8)];
            button.backgroundColor = /*([[data valueForKey:@"type"] intValue] == 20) ? */[UIColor colorWithRed:0.9 green:0.6 blue:0.2 alpha:1.0];// : [UIColor colorWithRed:0.2 green:0.4 blue:0.7 alpha:1.0];
            button.layer.cornerRadius = 4.0f;
            button.clipsToBounds = YES;
            
            int ylabel = (previous > p.y) ? temp.y + 5: temp.y - 15;
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(temp.x - 15, ylabel, 30, 10)];
            label.backgroundColor = [UIColor clearColor];
            label.font = [UIFont systemFontOfSize:10.0];
            label.text = [NSString stringWithFormat:@"%.1f", [[data valueForKey:@"points"] CGPointValue].y];
            label.textAlignment = NSTextAlignmentCenter;
            
            [self addSubview:button];
            [self addSubview:label];
            
            previous = p.y;
        }];
    }
    
    CGContextStrokePath(context);
}

- (IBAction)onTouchUp:(id)sender {
    
    UIButton *testButton = (UIButton *)sender;
    CGRect oldBtnRect = testButton.frame;
    testButton.titleLabel.font = [UIFont systemFontOfSize:18.0];
    [testButton sizeToFit];
    testButton.frame = CGRectMake(testButton.frame.origin.x - ((testButton.frame.size.width - oldBtnRect.size.width)/2), testButton.frame.origin.y - ((testButton.frame.size.height - oldBtnRect.size.height)/2), testButton.frame.size.width, testButton.frame.size.height);
}

@end
