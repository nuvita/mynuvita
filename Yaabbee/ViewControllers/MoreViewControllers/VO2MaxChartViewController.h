//
//  VO2MaxChartViewController.h
//  mynuvita
//
//  Created by John on 4/29/14.
//
//

#import <UIKit/UIKit.h>
#include <math.h>

#import "Vo2MaxChart.h"

@interface VO2MaxChartViewController : UIViewController

@property (nonatomic, retain) NSDictionary *resultInfo;

@end
