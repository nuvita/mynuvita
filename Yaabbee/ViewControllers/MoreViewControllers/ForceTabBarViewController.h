//
//  ForceTabBarViewController.h
//  mynuvita
//
//  Created by John on 4/11/14.
//
//

#import <UIKit/UIKit.h>

@interface ForceTabBarViewController : UITabBarController

@property(nonatomic, assign) UIInterfaceOrientation orientation;
@property(nonatomic, assign) NSUInteger supportedInterfaceOrientatoin;

@end
