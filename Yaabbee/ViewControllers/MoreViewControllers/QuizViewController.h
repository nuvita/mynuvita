//
//  EditMyProfileViewController.h
//  Yaabbee
//
//  Created by Sarfaraj Biswas on 8/15/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

#import "EduLessonWeekData.h"
#import "QuestionItem.h"
#import "AnswerItem.h"
#import "LabelBackView.h"
#import "constant.h"

@class EduLessonWeekData;
@class QuestionItem;
@class AnswerItem;


@interface QuizViewController : UIViewController <UITextFieldDelegate>
{
	NuvitaAppDelegate *app;
	
	MyTools *tools;
	UIView *loadingView;
	
	EduLessonWeekData *eduLessonWeekData;
	
	QuestionItem *currQuestionItem;
	
	BOOL isLoaded;
	
	int currQuestion;
	//NSDate *today;

	int totalElement;
	int totalQuestion;	
}



@property (nonatomic, retain) IBOutlet UIButton *btnSubmit;

@property (nonatomic, retain) EduLessonWeekData *eduLessonWeekData;

@property (nonatomic, retain) IBOutlet UITableView *profileTable;

//@property (nonatomic, retain) Profile *profile;

@property(nonatomic,retain)IBOutlet UILabel *subheaderLb, *subheaderLb2;
//@property(nonatomic,retain)IBOutlet UILabel *questionLb;

@property (nonatomic, retain) IBOutlet UILabel *questionLbl;

- (void)configureCell:(UITableViewCell *)cell withIndex:(NSIndexPath *)indexPath;
//- (NSString *)titleForCellWithIndex:(NSIndexPath *)indexPath;
- (void)showNextPrevQuestion;

- (BOOL)isAnsweredAllQuestion;

- (IBAction)moveNextClicked:(id)sender;
- (IBAction)movePrevClicked:(id)sender;
- (void)setSubmitBtn;
- (IBAction)btnSubmitClicked:(id)sender;

- (void)buildUI;
- (void)clearBodyUI;

@end
