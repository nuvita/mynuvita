//
//  TCViewController.m
//  Yaabbee
//
//  Created by Sarfaraj
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ResultViewController.h"
#import "LabelBackView.h"
#import <QuartzCore/QuartzCore.h>
#import "constant.h"

@implementation ResultViewController
@synthesize subheaderLb, fldLb1,fldLb2,fldLb3,fldLb4,fldLb5,fldLb6,fldLb7,eduLessonWeekData;

#pragma mark - View lifecycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.	
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)viewDidLoad {
    [super viewDidLoad];

	tools = [[MyTools alloc]init];
	loadingView = [tools createActivityIndicator1];	
	
	self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back"
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:self
                                                                            action:@selector(saveClicked:)];
   	
    self.subheaderLb.font = [UIFont fontWithName:DEFAULT_BOLD_FONT_NAME size:defaultFontSize];
	self.fldLb1.font = [UIFont fontWithName:DEFAULT_BOLD_FONT_NAME size:defaultFontSize];	
	self.fldLb2.font = [UIFont fontWithName:DEFAULT_BOLD_FONT_NAME size:bigFontSize];	
	self.fldLb3.font = [UIFont fontWithName:DEFAULT_BOLD_FONT_NAME size:defaultFontSize];	
	self.fldLb4.font = [UIFont fontWithName:DEFAULT_BOLD_FONT_NAME size:bigFontSize];	
	self.fldLb5.font = [UIFont fontWithName:DEFAULT_BOLD_FONT_NAME size:defaultFontSize];	
	self.fldLb6.font = [UIFont fontWithName:DEFAULT_BOLD_FONT_NAME size:defaultFontSize];	
	self.fldLb7.font = [UIFont fontWithName:DEFAULT_BOLD_FONT_NAME size:bigFontSize];	
	
	[self buildUI];
}


- (void)saveClicked:(id)sender{
   
	UINavigationController *navController = [self.tabBarController.viewControllers objectAtIndex:4];
	int index=2;
	NSInteger totalQuestion = [self.eduLessonWeekData.ItemsArray count];
	if(totalQuestion == [self.eduLessonWeekData getNumCorrect])
		index=1;
	[navController popToViewController:[navController.viewControllers objectAtIndex:index] animated:YES];
}

- (void)buildUI {
    
	int totalQuestion = 0;
	if(self.eduLessonWeekData == nil || self.eduLessonWeekData.ItemsArray == nil) return ;
	
	totalQuestion = [self.eduLessonWeekData.ItemsArray count];

	fldLb2.text = [NSString stringWithFormat:@"%d", [self.eduLessonWeekData getNumCorrect]];
	fldLb4.text = [NSString stringWithFormat:@"%d", totalQuestion];
	fldLb7.text = [NSString stringWithFormat:@"%0.f%@", [self.eduLessonWeekData getScore], @"%"];
}

@end
