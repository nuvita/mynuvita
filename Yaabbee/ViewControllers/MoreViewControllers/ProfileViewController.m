//
//  ProfileViewController.m
//  mynuvita
//
//  Created by John on 3/20/14.
//
//

#import "ProfileViewController.h"
#import "VO2MaxChartViewController.h"

@interface ProfileViewController ()

@property (nonatomic, retain) MyTools *tools;
@property (nonatomic, retain) UIView *loadingView;
@property (retain, nonatomic) UIActivityIndicatorView *progressIndicator;

@property (retain, nonatomic) IBOutlet UITableView *tableview;
@property (nonatomic, retain) NSMutableArray *menuList;

@end

@implementation ProfileViewController

#pragma mark - View Life Cycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
//        self.title = @"Profile";
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    
    _tools = [[MyTools alloc] init];
    _loadingView = [_tools createActivityIndicatorFromView:self.tabBarController.view];
    _menuList = [[NSMutableArray alloc] init];
    

    [self initProgressIndicator];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    [self.tabBarController.tabBar setHidden:NO];
    if ([_menuList count] == 0) {
        [self getHealthMeasurementMenu];
        return;
    }
    
    [_tableview reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    return (toInterfaceOrientation != UIInterfaceOrientationPortraitUpsideDown || toInterfaceOrientation != UIInterfaceOrientationPortrait);
}

- (BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}

#pragma mark - Public Method

- (void)initProgressIndicator {
    _progressIndicator = [[UIActivityIndicatorView alloc] init];
    _progressIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
    _progressIndicator.autoresizingMask = (UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin);
    [_progressIndicator sizeToFit];
}

- (void)loadVo2ChartView:(NSDictionary *)result {
    
    VO2MaxChartViewController *vo2MaxChartViewController = [[VO2MaxChartViewController alloc] init];
    vo2MaxChartViewController.resultInfo = result;
    
    [self.navigationController pushViewController:vo2MaxChartViewController animated:YES];
    [(NuvitaAppDelegate *)[[UIApplication sharedApplication] delegate] performSelector:@selector(reloadAppDelegateRootViewControllerLandscape)
                                                                            withObject:nil
                                                                            afterDelay:0.5];
}

#pragma mark - Web Service Request

- (void)getHealthMeasurementCurrentTrends:(NSInteger)groupId {
    
    LoginResponseData *loginResponseData = [LoginPerser getLoginResponse];
    NSDictionary *data = [NSDictionary dictionaryWithObjectsAndKeys:
                          [NSNumber numberWithInt:groupId], @"groupId",
                          [loginResponseData memberID], @"memberId",
                          nil];
    
    request *r = [[request alloc] initWithTarget:self
                                   SuccessAction:@selector(onSuccessfulFetchRequest:resData:)
                                   FailureAction:@selector(onFailedFetch)];
    
    [r getHealthMeasurementCurrentTrends:data];
}

- (void)getHealthMeasurementMenu {
    
    [_tools startLoading:self.navigationController.view childView:_loadingView text:LOADING_TEXT];
    request *r = [[request alloc] initWithTarget:self
                                   SuccessAction:@selector(onSucceffulFetchMeasurementMenu:resData:)
                                   FailureAction:@selector(onFailedFetch)];
    
    [r getHealthMeasurementMenu];
}

#pragma mark - Web Service Delegate

- (void)onSuccessfulFetchRequest:(ResponseStatus *)responseStatus resData:(NSObject *)obj {
    
    NSLog(@"DATA: %@", [(NuvitaXMLParser *)obj info]);
    if ([responseStatus isSuccess]) {
        [self loadVo2ChartView:[(NuvitaXMLParser *)obj info]];
        return;
    }
    
    UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"Error"
                                                        message:[responseStatus errorText]
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
    [alertview show];
    [_tableview reloadData];
}

- (void)onSucceffulFetchMeasurementMenu:(ResponseStatus *)responseStatus resData:(NSObject *)obj {
	[_tools stopLoading:_loadingView];
    
	if ([responseStatus isSuccess]) {
        [self loadData:[(NuvitaXMLParser *)obj info]];
    }
}

- (void)onFailedFetch {
	[_tools stopLoading:_loadingView];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                    message:@"Something went wrong in your network connection."
                                                   delegate:self
                                          cancelButtonTitle:@"Ok"
                                          otherButtonTitles: nil];
    [alert show];
}

#pragma mark - Public Method 

- (void)loadData:(NSDictionary *)info {
    [_menuList removeAllObjects];
    [_menuList addObjectsFromArray:[info objectForKey:@"a:HealthMeasurementMenuItems"]];
    [_tableview reloadData];
}

- (NSString *)cleanHTMLString:(NSString *)html {

    NSRange range;
    NSString *string = [html copy];
    while ((range = [string rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
        string = [string stringByReplacingCharactersInRange:range withString:@""];
    
    return [string stringByReplacingOccurrencesOfString:@"2" withString:@"\u00B2"];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_menuList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];

    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.textLabel.font = [UIFont fontWithName:DEFAULT_BOLD_FONT_NAME size:defaultFontSize];
    cell.textLabel.text = [self cleanHTMLString:[[_menuList objectAtIndex:indexPath.row] valueForKey:@"a:Label"]];
    cell.imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"groupid-%@.png",[[_menuList objectAtIndex:indexPath.row] valueForKey:@"a:MeasurementGroupId"]]];
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:YES];
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    NSInteger groupid = [[[_menuList objectAtIndex:indexPath.row] valueForKey:@"a:MeasurementGroupId"] integerValue];
    switch (groupid) {
            
        case 6: {
        
            cell.userInteractionEnabled = NO;
            cell.accessoryView = _progressIndicator;
            [_progressIndicator startAnimating];
            
            [self getHealthMeasurementCurrentTrends:groupid];
            
        }break;
            
        default: {
            cell.userInteractionEnabled = NO;
            cell.accessoryView = _progressIndicator;
            [_progressIndicator startAnimating];
            
            [self getHealthMeasurementCurrentTrends:groupid];
        }
            break;
    }
}

@end
