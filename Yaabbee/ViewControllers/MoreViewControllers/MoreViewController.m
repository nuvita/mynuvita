//
//  MoreViewController.m
//  Yaabbee
//
//  Created by Sarfaraj Biswas on 7/22/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "MoreViewController.h"
#import "LoginPerser.h"
#import "Profile.h"
#import "MobilityViewController.h"
#import "LessonsViewController.h"
#import "constant.h"

@interface MoreViewController ()

@property (strong, nonatomic) JFNavigationTransitionDelegate *navigationTransitionDelegate;
@property (retain, nonatomic) NSMutableArray *morelist;
@property (retain, nonatomic) NSMutableArray *moreImageList;

@end

@implementation MoreViewController
@synthesize dataTableView;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.tabBarItem.title = @"More";
        self.tabBarItem.image = [UIImage imageNamed:@"more.png"];
    }
    return self;
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //[self createRightBarItem];
    
	isMobilityHide = [LoginPerser isLifeStyle360];
    _morelist = [[NSMutableArray alloc] initWithObjects:@"Mobility", @"Weekly Lessons", @"Wellness Wall", nil];
    _moreImageList = [[NSMutableArray alloc] initWithObjects:[UIImage imageNamed:@"mobility-icon.png"], [UIImage imageNamed:@"weekly-lesson-icon.png"], [UIImage imageNamed:@"wellness-wall-icon.png"], nil];
    if ([LoginPerser isLifeStyle360] || [[[LoginPerser getLoginResponse] programName] isEqualToString:@"Fitness League"]) {
        [_morelist removeObject:[_morelist firstObject]];
        [_moreImageList removeObject:[_moreImageList firstObject]];
    }

}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
    [self.tabBarController.tabBar setHidden:NO];

    [self.dataTableView reloadData];
    
    _navigationTransitionDelegate = [[JFNavigationTransitionDelegate alloc] init];
    self.navigationController.delegate = _navigationTransitionDelegate;
}

#pragma mark - UpdateUI

- (void)createRightBarItem {
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Login"
                                                                      style:UIBarButtonItemStyleBordered
                                                                     target:self
                                                                     action:@selector(login)];
}

- (void)login {
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark -
#pragma mark Table Delegate and UITableViewDataSource
#pragma mark -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_morelist count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
	cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

    cell.textLabel.font = [UIFont fontWithName:DEFAULT_BOLD_FONT_NAME size:defaultFontSize];
    cell.textLabel.text = [_morelist objectAtIndex:indexPath.row];
    cell.imageView.image = [_moreImageList objectAtIndex:indexPath.row];
    cell.imageView.alpha = 0.7f;
    return cell;
}

- (void)loadSelectedCategory:(NSString *)category {
    
    if ([category isEqualToString:@"Mobility"]) {
        MobilityViewController *myprofileVC = [[MobilityViewController alloc] initWithNibName:@"MobilityViewController" bundle:nil];
        [self.navigationController pushViewController:myprofileVC animated:YES];
    } else if ([category isEqualToString:@"Weekly Lessons"]) {
        LessonsViewController *lessonsVC = [[LessonsViewController alloc] initWithNibName:@"LessonsViewController" bundle:nil];
        [self.navigationController pushViewController:lessonsVC animated:YES];
    } else {
        WellnessWallViewController *wellnessWall = [[WellnessWallViewController alloc] initWithNibName:@"WellnessWallViewController" bundle:nil];
        [self.navigationController pushViewController:wellnessWall animated:YES];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self loadSelectedCategory:[_morelist objectAtIndex:indexPath.row]];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([cell respondsToSelector:@selector(tintColor)]) {
        
        if (tableView == self.dataTableView) {
            CGFloat cornerRadius = 5.f;
            cell.backgroundColor = UIColor.clearColor;
            CAShapeLayer *layer = [[CAShapeLayer alloc] init];
            CGMutablePathRef pathRef = CGPathCreateMutable();
            CGRect bounds = CGRectInset(cell.bounds, 10, 0);

            BOOL addLine = NO;
            if (indexPath.row == 0 && indexPath.row == [tableView numberOfRowsInSection:indexPath.section]-1) {
                CGPathAddRoundedRect(pathRef, nil, bounds, cornerRadius, cornerRadius);
            } else if (indexPath.row == 0) {
                CGPathMoveToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMaxY(bounds));
                CGPathAddArcToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMinY(bounds), CGRectGetMidX(bounds), CGRectGetMinY(bounds), cornerRadius);
                CGPathAddArcToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMinY(bounds), CGRectGetMaxX(bounds), CGRectGetMidY(bounds), cornerRadius);
                CGPathAddLineToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMaxY(bounds));
                addLine = YES;
            } else if (indexPath.row == [tableView numberOfRowsInSection:indexPath.section]-1) {
                CGPathMoveToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMinY(bounds));
                CGPathAddArcToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMaxY(bounds), CGRectGetMidX(bounds), CGRectGetMaxY(bounds), cornerRadius);
                CGPathAddArcToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMaxY(bounds), CGRectGetMaxX(bounds), CGRectGetMidY(bounds), cornerRadius);
                CGPathAddLineToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMinY(bounds));
            } else {
                CGPathAddRect(pathRef, nil, bounds);
                addLine = YES;
            }
            
            layer.path = pathRef;
            CFRelease(pathRef);
            layer.fillColor = [UIColor colorWithWhite:1.0f alpha:0.8f].CGColor;
            
            if (addLine == YES) {
                CALayer *lineLayer = [[CALayer alloc] init];
                CGFloat lineHeight = (1.f / [UIScreen mainScreen].scale);
                lineLayer.frame = CGRectMake(CGRectGetMinX(bounds), bounds.size.height-lineHeight, bounds.size.width, lineHeight);
                lineLayer.backgroundColor = tableView.separatorColor.CGColor;
                [layer addSublayer:lineLayer];
            }
            
            UIView *testView = [[UIView alloc] initWithFrame:bounds];
            [testView.layer insertSublayer:layer atIndex:0];
            testView.backgroundColor = [UIColor clearColor];
            cell.backgroundView = testView;
            
            UIView* selectedBackgroundView = [[UIView alloc] initWithFrame:bounds];
            UIView* visibleSelectedBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(10, 0.0f, bounds.size.width, bounds.size.height)];
            visibleSelectedBackgroundView.backgroundColor = tableView.separatorColor;
            [selectedBackgroundView addSubview:visibleSelectedBackgroundView];
            cell.selectedBackgroundView = selectedBackgroundView;
        }
    }
}

@end
