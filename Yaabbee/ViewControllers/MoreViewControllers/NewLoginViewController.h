//
//  NewLoginViewController.h
//  Yaabbee
//
//  Created by Sarfaraj Biswas on 8/7/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "request.h"
#import "MyTools.h"
#import "CustomTextField.h"
#import "constant.h"
#import "ResponseStatus.h"

@class NewLoginViewController;
@protocol NewLoginViewControllerDelegate <NSObject>

- (void)newLoginViewController:(NewLoginViewController *)viewController didFinishLogin:(BOOL)flag;

@end

@interface NewLoginViewController : UIViewController <UITextFieldDelegate> {
	MyTools *tools;
	UIView *loadingView;
	
	CustomTextField *userIDFld;
	CustomTextField *passwordFld;
    float TABLE_OFFSETY;//60

}

@property (nonatomic, retain) id <NewLoginViewControllerDelegate> delegate;
@property (nonatomic,retain) IBOutlet UITableView *tableView;

- (void)configureCell:(UITableViewCell *)cell withIndex:(NSIndexPath *)indexPath;
- (IBAction)loginClicked:(id)sender;
- (IBAction)cancelClicked:(id)sender;


- (void)sendRequest;


@end