//
//  OutBoxViewController.m
//  Yaabbee
//
//  Created by Sarfaraj Biswas on 8/22/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "ShareStatusViewController.h"
#import "HomeViewController.h"
#import "constant.h"
#import "KTTextView.h"

@implementation ShareStatusViewController

@synthesize subheaderLb1;
@synthesize profileIcon;
@synthesize shareImage;
@synthesize profileTextView;
@synthesize photoBtn;
@synthesize WorkoutId;
@synthesize imageContent;
@synthesize delegate;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

	isLoaded = NO;
	tools = [[MyTools alloc]init];
	loadingView = [tools createActivityIndicator1];	

    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Post"
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:self
                                                                  action:@selector(postBtnClicked:)];

	//set font
	self.subheaderLb1.font = [UIFont fontWithName:DEFAULT_BOLD_FONT_NAME size:defaultFontSize];
    self.subheaderLb1.text = @"Share Status";
    
    //accessory view
	UIToolbar *toolbar = [[UIToolbar alloc] init];
	[toolbar setBarStyle:UIBarStyleBlackTranslucent];
	[toolbar sizeToFit];
	
	UIBarButtonItem *flexButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
	UIBarButtonItem *doneButton =[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(resignKeyboard)];
	
	NSArray *itemsArray = [NSArray arrayWithObjects:flexButton, doneButton, nil];
	[toolbar setItems:itemsArray];
	[self.profileTextView setInputAccessoryView:toolbar];
    [self.profileTextView setPlaceholderText:@"Share your thoughts..."];
    [self.profileTextView setFont:[UIFont fontWithName:DEFAULT_NORMAL_FONT_NAME size:defaultFontSize]];
    self.profileTextView.contentInset = UIEdgeInsetsMake(0.0,0.0,0.0,0.0);
    
    LoginResponseData *loginResponseData = [LoginPerser getLoginResponse];
    [self.profileIcon.layer setBorderWidth:1.0];
    [self.profileIcon.layer setBorderColor:[[UIColor darkGrayColor] CGColor]];
    
    NSString *avatarLink = loginResponseData.avator;
    UIImageView *iconImgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 40, 40)];
    [iconImgView setImage:[UIImage imageNamed:@"avator.png"]];
    [iconImgView.layer setBorderWidth:1.0];
    [iconImgView.layer setBorderColor:[[UIColor darkGrayColor] CGColor]];
    
    [self.profileIcon addSubview:iconImgView];
    if (avatarLink) {
        [iconImgView setImageWithURL:[NSURL URLWithString:avatarLink] 
                    placeholderImage:[UIImage imageNamed:@"avator.png"]]; 
    } 
 
    [self.photoBtn addTarget:self action:@selector(cameraBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
}

- (void)postBtnClicked:(id)sender{
    [self resignKeyboard];
	[self sendRequest:YES];
}

- (void)resignKeyboard {
	[self.profileTextView resignFirstResponder];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

//Photo
- (void)cameraBtnClicked:(id)sender {
    UIActionSheet *cameraMenuSheet = [[UIActionSheet alloc] initWithTitle:@"Add Photo" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Camera", @"Choose Photo", nil];
    [cameraMenuSheet showFromTabBar:self.tabBarController.tabBar];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSString *title = [actionSheet buttonTitleAtIndex:buttonIndex];
    
    // Initialize UIImagePickerController
    if ([title isEqualToString:@"Camera"]) {
        // Camera
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePicker.delegate = self;
        imagePicker.allowsEditing = YES;
        imagePicker.showsCameraControls = YES;
        imagePicker.mediaTypes = [NSArray arrayWithObjects:(NSString *)kUTTypeImage, nil];
        
        [self presentViewController:imagePicker animated:YES completion:^{
            
        }];
        //newMedia = YES;
    } else if ([title isEqualToString:@"Choose Photo"]) {
        // Photo library
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        imagePicker.delegate = self;
        imagePicker.allowsEditing = YES;
        imagePicker.mediaTypes = [NSArray arrayWithObjects:(NSString *)kUTTypeImage, nil];
        [self presentViewController:imagePicker animated:YES completion:^{
            
        }];
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
    [self.view setNeedsDisplay];
}

// Method for saving image to photo album
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    
    if ([mediaType isEqualToString:(NSString *)kUTTypeImage]) {
        
        // Access the uncropped image from info dictionary
        UIImage *image = [info objectForKey:@"UIImagePickerControllerEditedImage"];
  
        [self retrieveRawImageData:image];
        [self.shareImage setImage:image]; 
     }
   
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo {
    
    [photoBtn setImage:image forState:UIControlStateNormal];
    
    [[picker parentViewController] dismissModalViewControllerAnimated:YES];
}

- (void)retrieveRawImageData:(UIImage *)image
{
    if(image!=nil)
    {   
        float imgW = image.size.width;
        float imgH = image.size.height;
        float expectedW = 200.0;
        float expectedH = 200.0;

        if(imgW > imgH)
            expectedW = 300.0;
        
        expectedH = imgH/(imgW/expectedW);
        
        image = [image scaleToSize:CGSizeMake(expectedW, expectedH)];
        NSData*  image_content = UIImagePNGRepresentation(image);
        self.imageContent = [NSData dataWithData:image_content];
  
        NSUInteger len1 = [self.imageContent length];
        NSLog(@"%d...length of imageData....%f",len1, expectedH);
   }
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (void)sendRequest:(BOOL)flag {
    NSString *text = self.profileTextView.text;
	
	if ([text length]==0) {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry"
                                                        message:@"Please enter the status text."
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles: nil];
		[alert show];
        return;
	}

	[tools startLoading:self.navigationController.view childView:loadingView text:@"Sharing..."];
	
	[self makeRequestStarProgress:flag];
}

- (void)makeRequestStarProgress:(BOOL)flag {
    NSString* text = self.profileTextView.text;
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
	[dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];//MM-dd-yyyy HH:mm:ss
	NSString *dateString = [dateFormat stringFromDate:[NSDate date]];
	LoginResponseData *loginResponseData = [LoginPerser getLoginResponse];

    dateString = [dateString stringByReplacingOccurrencesOfString:@" " withString:@"T"];
	request *r=[[request alloc] initWithTarget:self
								 SuccessAction:@selector(onSucceffulLogin:resData:)
								 FailureAction:@selector(onFailureLogin)];
	if(self.imageContent) {
        [r saveWellnessWallPhoto:loginResponseData.memberID date:dateString text:text photo:self.imageContent];
    } else {
        [r saveWellnessWall:loginResponseData.memberID date:dateString text:text photo:@""];
    }
}

- (void)onSucceffulLogin:(ResponseStatus *)responseStatus resData:(NSObject *)obj {
    [tools stopLoading:loadingView];
    NSString* text = @"Sent successfully."; 
	
	if (responseStatus == nil || [responseStatus isSuccess]) {
		text = @"Could not your share status, please try again later.";
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Info"
                                                        message:text
                                                       delegate:self
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles: nil];
        [alert show];
        return;
	}
    else
        [self.delegate shouldUpdate:YES];
	    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)onFailureLogin {
	[tools stopLoading:loadingView];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	if (buttonIndex == 0) {
		if (alertView.tag == 2) {
			
		}
	}
}

@end
