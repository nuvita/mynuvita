//
//  MyProfileViewController.h
//  Yaabbee
//
//  Created by Sarfaraj Biswas on 8/14/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "request.h"
#import "NuvitaAppDelegate.h"
#import "MyTools.h"
#import "LoginPerser.h"
#import "LoginResponseData.h"
#import "WellnessWallData.h"
#import "ShareStatusViewController.h"
#import "LoadMoreTableFooterView.h"

@interface WellnessWallViewController : UIViewController <UIWebViewDelegate, LoadMoreTableFooterDelegate, UIScrollViewDelegate, ShareStatusDelegate>
{
	MyTools *tools;
	UIView *loadingView;
	
	WellnessWallData *wellnessWallData;
	
	BOOL isLoaded;
	
	int daysNextPrev;
	//NSDate *today;
	
	UIButton *buttonPrev, *buttonNext;
	int totalElement;
    NSInteger currPageNum;
    
    float yy;
    float contentHeight;
    NSInteger tagIndex;
    BOOL _reloading;
}

@property (nonatomic, retain)LoadMoreTableFooterView *loadMoreFooterView;
@property (nonatomic, retain)NSDate *today;
@property(nonatomic, retain)NSString *lessonNumber;
@property (nonatomic,retain)IBOutlet UILabel *subheaderLb;
//@property (nonatomic, retain) IBOutlet UIWebView *webView;
@property (nonatomic, retain) IBOutlet UIScrollView *contentScrollView;


- (void)editClicked:(id)sender;

- (void)loadWView;

//
- (void)sendRequest:(BOOL)flag;

- (void)makeRequestProgress:(BOOL)flag;


//- (void)getNextPrevDate:(BOOL)flag;
- (void)clearBodyUI;
- (void)buildUI;

- (void)resizeContentView:(UIWebView*)wv;
- (void)loadMoreWalls:(NSInteger)type;
- (BOOL)isNextPageAvailable;
- (void)doneLoadingMoreData;
- (void)initContentView;

@end
