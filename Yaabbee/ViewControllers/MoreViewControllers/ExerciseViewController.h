//
//  OutBoxViewController.h
//  Yaabbee
//
//  Created by Sarfaraj Biswas on 8/22/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "request.h"
#import "NuvitaAppDelegate.h"
#import "MyTools.h"
#import "LoginPerser.h"
#import "LoginResponseData.h"
#import  "MobilityExerciseData.h"
#import "UIImageView+AFNetworking.h"
#import "NuvitaAppDelegate.h"

#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>
#import <CoreMedia/CoreMedia.h>

@class ExerciseViewController;
@protocol ExerciseViewControllerDelegate <NSObject>

- (void)exerciseViewController:(ExerciseViewController *)viewController didFinishPickFile:(NSString *)file;

@end

@interface ExerciseViewController : UIViewController {
	MyTools *tools;
	UIView *loadingView;

	MobilityExerciseData *mobilityExerciseData;
	
	BOOL isLoaded;
	
	int daysNextPrev;
	NSDate *today;
	
	UIButton *buttonPrev, *buttonNext;
	int totalElement;
    NSTimer *nsTimer;
    NSInteger timerCount;
    NSInteger roundCount;
    NSInteger exerciseCount;
    BOOL isExerciseMode;
    
    BOOL isTimerRunning;
}

@property (strong, nonatomic) id <ExerciseViewControllerDelegate> delegate;

@property (retain, nonatomic) IBOutlet UIImageView *imageTitle;


@property (nonatomic, retain)NSString *WorkoutId;
@property (nonatomic, retain) IBOutlet UITableView *tableView_;
@property (nonatomic, retain) IBOutlet UILabel *subheaderLb1;
@property (nonatomic, retain) IBOutlet UILabel *subheaderLb2;

//
@property(nonatomic, retain)IBOutlet UITextField *txtFldExercise;
@property(nonatomic, retain)IBOutlet UITextField *txtFldRest;

@property(nonatomic, retain)IBOutlet UIButton *btnStart;
@property(nonatomic, retain)IBOutlet UIButton *btnStop;
@property(nonatomic, retain)IBOutlet UIButton *btnPause;

@property(nonatomic, retain)IBOutlet UILabel   *lbStatus;
@property(nonatomic, retain)IBOutlet UILabel   *lbWorkTime;
@property(nonatomic, retain)IBOutlet UILabel   *lbRound;

@property (strong, nonatomic) MPMoviePlayerController *moviePlayerViewController;

//@property (nonatomic, retain) IBOutlet UISearchBar *searchBar;
//- (CGSize)calculateTextSizeWithText:(NSString *)text;
- (void)setStatusLabel:(NSString*)lb val:(NSInteger)count round:(NSInteger)roundCnt;
- (void)configureCell:(UITableViewCell *)cell withIndex:(NSIndexPath *)indexPath;
- (void)initExerciseData;
- (IBAction)startBtnClicked:(id)sender;
- (IBAction)stopBtnClicked:(id)sender;
- (IBAction)pauseBtnClicked:(id)sender;
- (void)startExerciseTimer;
- (void)timerEvent:(id)sender;
- (BOOL)isValidInput;
//

- (void)resignKeyboard:(UIBarButtonItem*)sender;
- (void)setTxtFldTool:(UITextField*)txtFld tag:(NSInteger)tag;
- (IBAction)backBtnClicked:(id)sender;

// 
- (void)sendRequest:(BOOL)flag;

- (void)makeRequestStarProgress:(BOOL)flag;


//- (void)getNextPrevDate:(BOOL)flag;

- (void)clearBodyUI;
- (void)buildUI;

//- (void) goplay:(NSString*)moviePath;
//- (void) moviePlayBackDidFinish:(NSNotification*)notification;
- (void)highlightingTableRow;

@end
