//
//  ForceNavigationViewController.m
//  mynuvita
//
//  Created by John on 4/10/14.
//
//

#import "ForceNavigationViewController.h"

@interface ForceNavigationViewController ()

@end

@implementation ForceNavigationViewController

@synthesize supportedInterfaceOrientatoin = _supportedInterfaceOrientatoin;
@synthesize orientation = _orientation;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        _supportedInterfaceOrientatoin = UIInterfaceOrientationMaskPortrait;
        _orientation = UIInterfaceOrientationPortrait;
        
        self.view.backgroundColor = [UIColor whiteColor];
        self.navigationBar.tintColor = [UIColor colorWithRed:0.2 green:0.4 blue:0.7 alpha:1.0];
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return _supportedInterfaceOrientatoin;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return self.orientation;
}

@end
