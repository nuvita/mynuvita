//
//  JBLoadingView.h
//  mynuvita
//
//  Created by John on 9/4/14.
//
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>

@interface JBLoadingView : NSObject

- (UIImageView *)createLoadingView:(UIView *)view;
- (void)loadingViewStartAnimating:(UIView *)parentView withLoadingView:(UIView *)loadingView text:(NSString *)loadingText;
- (void)loadingViewStopAnimating:(UIView *)parentView;
- (void)loadingViewStopAnimating:(UIView *)parentView withPrompt:(NSString *)prompt;

@end
