//
//  FitnessTableViewCell.h
//  mynuvita
//
//  Created by John on 8/6/14.
//
//

#import <UIKit/UIKit.h>

@interface FitnessTableViewCell : UITableViewCell

- (void)loadCurrentScoreData:(NSDictionary *)data;
- (void)loadStandingsData:(NSDictionary *)data;
- (void)loadScheduleData:(NSDictionary *)data;

- (void)loadPreWeekData:(NSDictionary *)data;
- (void)loadCardioWeekData:(NSDictionary *)data;

@end
