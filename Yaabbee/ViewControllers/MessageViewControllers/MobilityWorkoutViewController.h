//
//  MessagesViewController.h
//  Yaabbee
//
//  Created by Sarfaraj Biswas on 7/22/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "request.h"
#import "NuvitaAppDelegate.h"
#import "MyTools.h"
#import "LoginPerser.h"
#import "LoginResponseData.h"
#import  "HomeViewController.h"

#import "MobilityXData.h"
//#import "MobilityViewController.h"

@protocol MobilityDelegate <NSObject>

@required
- (void) backSelected:(BOOL)selected;
@end

@interface MobilityWorkoutViewController : UIViewController<UIPickerViewDelegate, UITextFieldDelegate>
{
	NSMutableArray *columnsTitle;
	
	MyTools *tools;
	UIView *loadingView;
	
	MobilityXData *mobilityXData;
	
	BOOL isLoaded;
	
	int daysNextPrev;
	NSDate *today;
	
	UIButton *buttonPrev, *buttonNext;
	int totalElement;
	
	UIButton *doneButton ;
       UIPickerView *myPickerView;
	
	BOOL isKeyBoardVisible;
	
	NSMutableDictionary *dictItem;
	
	int requestType;
	
	UIButton *workoutBtn;
	UITextField* roundsTxtFld;
}

@property (nonatomic, retain) id <MobilityDelegate> delegate;

@property(nonatomic, retain)UIView *headerView, *bodyView;
@property (nonatomic, retain)NSDate *today;
@property (nonatomic, retain) IBOutlet NSString *dateString;
//@property (nonatomic, retain) IBOutlet UILabel *subheaderLb2;
@property (nonatomic, retain) IBOutlet UITableView *tableView_;

//
- (IBAction)pickerBtnClicked:(id)sender;
- (IBAction)saveBtnClicked:(id)sender;
- (IBAction)hideKeyB:(id)sender;
- (void)hideKeys;
- (IBAction)pickerDoneBtClicked:(id)sender;
- (void)showPickerView:(NSInteger)sender;
//- (void)buildBodyUI;
- (void)buildSubHeaderUI;

- (void)backBtnClicked;

//
- (void)sendRequest:(BOOL)flag;

- (void)makeRequestProgress:(BOOL)flag;


- (void)getNextPrevDate:(BOOL)flag;

- (void)clearBodyUI;

- (void)manipulateNextPrev;

- (void)buildUI;

- (void)setRoundsVal;
@end
