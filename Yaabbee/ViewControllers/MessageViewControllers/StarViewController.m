//
//  OutBoxViewController.m
//  Yaabbee
//
//  Created by Sarfaraj Biswas on 8/22/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "StarViewController.h"
#import "HomeViewController.h"
#import "constant.h"

#define STAR_BUTTON_TITLE @"My Org"
#define GSTAR_BUTTON_TITLE @"Global"

@interface StarViewController ()

@property (nonatomic, retain) JBCustomButton *buttonNext;
@property (nonatomic, retain) JBCustomButton *buttonPrev;

@end

@implementation StarViewController

@synthesize subheaderLb1, subheaderLb2, tableView_,loadMoreFooterView;
@synthesize today;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.tabBarItem.title = @"Star";
        self.tabBarItem.image = [UIImage imageNamed:@"star.png"];
    }
    return self;
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    UIBarButtonItem *btn = [[UIBarButtonItem alloc] initWithTitle:GSTAR_BUTTON_TITLE
                                                            style:UIBarButtonItemStylePlain
                                                           target:self
                                                           action:@selector(starBtnClicked:)];
    btn.tag = 0;
    [self.navigationItem setLeftBarButtonItem:btn];
    
	isLoaded = NO;
	
	tools = [[MyTools alloc]init];
	loadingView = [tools createActivityIndicator1];	
	
	_buttonPrev =  [[JBCustomButton alloc] initWithFrame:CGRectMake(0, 20, 30, 30)];
    [_buttonPrev setButtonStyle:JBCustomButtonArrowLeftStyle];
	[_buttonPrev addTarget:self action:@selector(movePrevClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    _buttonNext =  [[JBCustomButton alloc] initWithFrame:CGRectMake(0, 20, 30, 30)];
    [_buttonNext setButtonStyle:JBCustomButtonArrowRightStyle];
	[_buttonNext addTarget:self action:@selector(moveNextClicked:) forControlEvents:UIControlEventTouchUpInside];
	
	self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:
                                               [[UIBarButtonItem alloc] initWithCustomView:_buttonNext],
                                               [[UIBarButtonItem alloc] initWithCustomView:_buttonPrev], nil];

	daysNextPrev = 0;
	self.view.hidden = YES;
	
	isLoaded= NO;	
    
    currStar = 0;//0 for Star 1- Gstar.
    currPage = 1;
    //
    if (self.loadMoreFooterView == nil) {		
		self.loadMoreFooterView = [[LoadMoreTableFooterView alloc] initWithFrame:CGRectMake(0.0f, self.tableView_.contentSize.height, self.tableView_.frame.size.width, self.tableView_.bounds.size.height)];
		self.loadMoreFooterView.delegate = self;
		[self.tableView_ addSubview:self.loadMoreFooterView];
	}

    self.loadMoreFooterView.hidden = YES;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    //self.refreshHeaderView =nil;
    self.loadMoreFooterView = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	if(!isLoaded)
		[self sendRequest:YES];

	NSLog(@"viewWillAppear...");
}

- (void)starBtnClicked:(id)sender{
	
    UIBarButtonItem *btn = (UIBarButtonItem*)sender;
    switch(btn.tag)
    {
        case 0:
            currStar = 1;
            currPage = 1;
            self.today = nil;
            [self sendRequest:YES];
            
            [btn setTitle:STAR_BUTTON_TITLE];
            btn.tag = 1;
            self.loadMoreFooterView.hidden = NO;
            //self.refreshHeaderView.hidden = NO;
            break;
        case 1:
            currPage = 1;
            currStar = 0;
            self.today = nil;
            [self sendRequest:YES];
            
            [btn setTitle:GSTAR_BUTTON_TITLE];
            btn.tag = 0;
            self.loadMoreFooterView.hidden = YES;
            //self.refreshHeaderView.hidden = YES;
            break;
    }
}



#pragma mark - UITableViewDataSource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (currStar == 1) {
        return 48;
    }
    return 36;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return totalElement;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = nil;
    
    CellIdentifier = CELL_EVEN;

    UITableViewCell *cell = nil;//[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
		[self configureCell:cell withIndex:indexPath];			
    }

    return cell;
}

- (CGSize)calculateTextSizeWithText:(NSString *)text{
    CGSize size =  [text sizeWithFont:[UIFont systemFontOfSize:16] constrainedToSize: CGSizeMake(280.0, 480.0) lineBreakMode:NSLineBreakByWordWrapping];
    
    return size;
}

- (void)configureCell:(UITableViewCell *)cell withIndex:(NSIndexPath *)indexPath{
    UIView *cellBackView = cell.contentView;
    
	StarItem *starItem = nil;
		
	starItem = (StarItem *)[starProgressData.StarItemsArray objectAtIndex:indexPath.row];
	
	NSString *rank =  starItem.Rank;
	NSString *name =  starItem.Name;
	NSString *pval =  starItem.ProgressValue;
	NSString *pcent =  starItem.ProgressPerecent;
	
	if ([LoginPerser isNuvitaX]) {
		pval =  @"";
		pcent =  starItem.ProgressValue;
	}
	
	CGFloat frameWidth = 300.0;
    CGFloat offsetX = 10.0;
    
    //index....
    //NSString *indexText = [rank stringByAppendingString:[NSString stringWithFormat:@"%@", ""]];
    CGSize titleTextSize = [self calculateTextSizeWithText:rank];
    CGRect frame = cellBackView.frame;
    frame.origin.x  = 10;
    frame.origin.y = (currStar == 0 ? 8 : cellBackView.frame.size.height/2 - defaultFontSize/2);
    frame.size.width =  20;//+ offsetX;
    frame.size.height = titleTextSize.height;
    
    UILabel *lbIndex = [[UILabel alloc] initWithFrame:frame];
    lbIndex.font = [UIFont fontWithName:DEFAULT_BOLD_FONT_NAME size:defaultFontSize];
    lbIndex.text = rank;
    lbIndex.textColor = [UIColor blackColor];
    lbIndex.backgroundColor = [UIColor clearColor];	
    [cellBackView addSubview:lbIndex];

    //title
    //NSString *titleText = [rank stringByAppendingString:[NSString stringWithFormat:@"  %@", name]];
    titleTextSize = [self calculateTextSizeWithText:name];
    //CGRect frame = cellBackView.frame;
    frame.origin.x  = 32;
    frame.origin.y = (currStar == 1 ? 4 : 8);
    frame.size.width = frameWidth/2 ;//+ offsetX;
    frame.size.height = titleTextSize.height;
    
    UILabel *mainLabel = [[UILabel alloc] initWithFrame:frame];
    mainLabel.font = [UIFont fontWithName:DEFAULT_BOLD_FONT_NAME size:defaultFontSize];
    mainLabel.text = name;
    mainLabel.textColor = [UIColor blackColor];
    mainLabel.backgroundColor = [UIColor clearColor];	
    [cellBackView addSubview:mainLabel];
    
    //organization
    if (currStar == 1) {
    CGRect frameOrg = cellBackView.frame;
    frameOrg.origin.x  = 32;
    frameOrg.origin.y = cellBackView.frame.size.height/2 + 4;
    frameOrg.size.width = frameWidth  ;//+ offsetX;
    frameOrg.size.height = titleTextSize.height;
    
    UILabel *lbOrg = [[UILabel alloc] initWithFrame:frameOrg];
    lbOrg.font = [UIFont fontWithName:DEFAULT_NORMAL_FONT_NAME size:defaultFontSize];
    lbOrg.text = starItem.Organization;
    lbOrg.textColor = [UIColor darkGrayColor];
    lbOrg.backgroundColor = [UIColor clearColor];
    [cell.contentView addSubview:lbOrg];
    }//endif
	//
    NSString *timeText = pval;//@"102 min";
    titleTextSize =[self calculateTextSizeWithText:timeText];
    frame.origin.x = frameWidth/2 + offsetX + 40;// - titleTextSize.width - offsetX;
    frame.size.width = frameWidth/4 - offsetX;//titleTextSize.width + offsetX;
    UILabel *timeLabel = [[UILabel alloc] initWithFrame:frame];
    timeLabel.textAlignment = NSTextAlignmentRight;
    timeLabel.font = [UIFont fontWithName:DEFAULT_BOLD_FONT_NAME size:defaultFontSize];
    timeLabel.text = timeText;
    timeLabel.textColor = [UIColor blackColor];//colorWithRed:0.0 green:77.0 /255.0 blue:74.0/255.0 alpha:1];
    timeLabel.backgroundColor = [UIColor clearColor];
	
    [cellBackView addSubview:timeLabel];
    
    //
    NSString *perc = pcent;//@"%45";
    //titleTextSize =[self calculateTextSizeWithText:perc];
    frame.origin.x = (frameWidth/2)  + (frameWidth/4) + 20;//- offsetX;//frame.origin.x + frame.size.width + offsetX;//
    //NSLog(@"Frame size %f %f", cellBackView.frame.size.width, titleTextSize.width)	;
    frame.size.width = (frameWidth/4) - offsetX;//
    UILabel *percLabel = [[UILabel alloc] initWithFrame:frame];
    percLabel.textAlignment = NSTextAlignmentRight;
    percLabel.font = [UIFont fontWithName:DEFAULT_BOLD_FONT_NAME size:defaultFontSize];
    percLabel.text = perc;
    percLabel.textColor = [UIColor blackColor];//olorWithRed:0.0 green:77.0 /255.0 blue:74.0/255.0 alpha:1];
    percLabel.backgroundColor = [UIColor clearColor];
	
    [cellBackView addSubview:percLabel];
    
}

#pragma mark - UITableViewDelegate

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 24)];
    label.textAlignment = NSTextAlignmentRight;
    label.backgroundColor = [UIColor whiteColor];
    label.font = [UIFont boldSystemFontOfSize:14];
    label.text = @"Minutes\t%\t";
    return label;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
	tableView.backgroundColor = [UIColor clearColor];  
}


#pragma mark -
#pragma mark Data Source Loading / Reloading Methods

- (void)reloadTableViewDataSource:(NSInteger)type{
	//  should be calling your tableviews data source model to reload
	//  put here just for demo
	_reloading = YES;
    if (type == 0) {
        currPage--;
    }
    else
	currPage++; 
    [self sendRequest:YES];
}

- (void)doneLoadingTableViewData{
	
	//  model should call this when its done loading
	_reloading = NO;
    
    //[self.refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:self.tableView_];
	[self.loadMoreFooterView loadMoreScrollViewDataSourceDidFinishedLoading:self.tableView_];
}


#pragma mark -
#pragma mark UIScrollViewDelegate Methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
	if (currStar == 0) {
        return;
    }
    
    if ([self isNextPageAvailable]) 
        [self.loadMoreFooterView loadMoreScrollViewDidScroll:scrollView];		
	
//    if([self isPrevPageAvailable])
//        [self.refreshHeaderView egoRefreshScrollViewDidScroll:scrollView];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    if (currStar == 0) {
        return;
    }
	if ([self isNextPageAvailable]) 
        [self.loadMoreFooterView loadMoreScrollViewDidEndDragging:scrollView];
    
//    if([self isPrevPageAvailable])
//        [self.refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
}

#pragma mark -
#pragma mark LoadMoreTableFooterDelegate Methods

- (void)loadMoreTableFooterDidTriggerRefresh:(LoadMoreTableFooterView *)view {
    
	[self reloadTableViewDataSource:1];
	//[self performSelector:@selector(doneLoadingTableViewData) withObject:nil afterDelay:3.0];
}

- (BOOL)loadMoreTableFooterDataSourceIsLoading:(LoadMoreTableFooterView *)view {
	return _reloading;
}

- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView*)view
{
    [self reloadTableViewDataSource:0];
}
- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView*)view
{
    return _reloading;
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)clearBodyUI {
	self.view.hidden = YES;	
}

- (void)buildUI {
	totalElement = 0;
	
    if(currStar == 1)
        [self doneLoadingTableViewData];
    
	//[self.tableView_ reloadData];
	
	if(starProgressData == nil || starProgressData.StarItemsArray == nil) return ;
	
	totalElement = [starProgressData.StarItemsArray count];
    
	if(totalElement == 0)
	{
		[self clearBodyUI ];
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry" message:NO_RECORD_FOUND delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
		[alert show];
		return ;		
	}
	
//	LoginResponseData *loginResponseData = [LoginPerser getLoginResponse];
	self.subheaderLb2.text = starProgressData.StarsTitle;//@"13/17";
	self.subheaderLb1.text = starProgressData.WeekLabel;
	
	[self.tableView_ reloadData];
    
    if (totalElement > (currPage - 1)*10) {
    NSIndexPath *toPath = [NSIndexPath indexPathForRow:(currPage - 1)*10 inSection:0];
    [self.tableView_ scrollToRowAtIndexPath:toPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    }
	self.view.hidden = NO;
}

- (IBAction)moveNextClicked:(id)sender
{
    [self initGStarVar];
	[self getNextPrevDate:YES];
	[self sendRequest: YES];
}

- (IBAction)movePrevClicked:(id)sender
{
    [self initGStarVar];
	[self getNextPrevDate:NO];	
	[self sendRequest: NO];
}

- (void)initGStarVar
{
    currPage = 1;
}

- (BOOL)isNextPageAvailable
{
    if(starProgressData == nil || starProgressData.StarItemsArray == nil) return NO;
	
	int size = [starProgressData.StarItemsArray count];
    
    if(currStar == 1 && (size % 10) == 0)
        return YES;
    
    return NO;
}

- (BOOL)isPrevPageAvailable
{
    if(currStar == 1 && currPage > 1)
        return YES;
    return NO;
}


//communication part

- (void)sendRequest:(BOOL)flag {	
	if(self.today == nil)
		self.today = [NSDate date];

    //...limit
    _buttonNext.enabled = ![[self getDateFormat:self.today] isEqualToString:[self getDateFormat:[NSDate date]]];

	[tools startLoading:self.navigationController.view childView:loadingView text:LOADING_TEXT];
	[self makeRequestStarProgress:flag];
}

- (NSString *)getDateFormat:(NSDate *)date {
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM-dd-yyyy"];

    return [dateFormat stringFromDate:date];
}

- (void)makeRequestStarProgress:(BOOL)flag {

	NSString *dateString = [self getDateFormat:self.today];
	LoginResponseData *loginResponseData = [LoginPerser getLoginResponse];
	
	request *r=[[request alloc] initWithTarget:self
								 SuccessAction:@selector(onSucceffulLogin:resData:)
								 FailureAction:@selector(onFailureLogin)];
    
    if (currStar == 0) {
        [r getStars:loginResponseData.memberID date:dateString];
    }else if(currStar == 1) {
        [r getGlobalStars:[NSString stringWithFormat:@"%d", currPage] date:dateString];
    }
}


- (void)getNextPrevDate:(BOOL)flag {
	if(self.today == nil)
		self.today = [NSDate date];

	NSCalendar *cal = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
	NSDateComponents *components = [cal components:( NSYearCalendarUnit | NSMonthCalendarUnit | NSWeekCalendarUnit | NSDayCalendarUnit ) fromDate:self.today];
	
	if(flag)
		[components setDay:([components day] + 7)];
	else
		[components setDay:([components day] - 7)];
	
	self.today = [cal dateFromComponents:components];//[theCalendar dateByAddingComponents:dayComponent toDate:self.today options:0];
}

- (void)onSucceffulLogin:(ResponseStatus *)responseStatus resData:(NSObject *)obj
{
	if (responseStatus == nil || ![responseStatus isSuccess])
	{
		[tools stopLoading:loadingView];
		if(!_reloading)
		[self clearBodyUI];
		
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry" message:NO_RECORD_FOUND delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
		[alert show];
		
		return ;
	}
	
    if(currStar == 1 && _reloading)
    {
        StarProgressData* itemObj = (StarProgressData *)obj;
        if(itemObj != nil && itemObj.StarItemsArray != nil)
        {
            [starProgressData.StarItemsArray addObjectsFromArray:itemObj.StarItemsArray];
        }
    }
    else
    {
        starProgressData = (StarProgressData *)obj;
    }
    
	[self buildUI];
	[tools stopLoading:loadingView];		
}

- (void)onFailureLogin
{
    [tools stopLoading:loadingView];
    switch (currStar) {
        case 0:
            [self clearBodyUI];
            break;
        case 1:
             [self doneLoadingTableViewData];
            if (!_reloading) {
                [self clearBodyUI];
            }
            break;
        default:
            break;
    }
}

@end
