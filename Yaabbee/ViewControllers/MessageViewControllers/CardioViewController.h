//
//  MessagesViewController.h
//  Yaabbee
//
//  Created by Sarfaraj Biswas on 7/22/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "request.h"
#import "NuvitaAppDelegate.h"
#import "MyTools.h"
#import "LoginPerser.h"
#import "LoginResponseData.h"
#import  "HomeViewController.h"

@class CardioWeekItem;
@class CardioWeekData;


@interface CardioViewController : UIViewController
{
	NSMutableArray *columnsTitle1;
	NSMutableArray *columnsTitle2;
	
	MyTools *tools;
	UIView *loadingView;
	
	CardioWeekData *cardioWeekData;
	
	BOOL isLoaded;
	
	int daysNextPrev;
	NSDate *today;
	
	int totalElement;
}


@property(nonatomic, retain)UIView *headerView, *bodyView;

@property (nonatomic, retain)NSDate *today;

@property (nonatomic, retain) NSMutableArray *columnsTitle1;
@property (nonatomic, retain) NSMutableArray *columnsTitle2;

//@property (nonatomic, retain) IBOutlet UILabel *subheaderLb1;
//@property (nonatomic, retain) IBOutlet UILabel *subheaderLb2;

@property (nonatomic, retain) IBOutlet UITableView *tableView_;


- (IBAction)moveNextClicked:(id)sender;
- (IBAction)movePrevClicked:(id)sender;
- (void)homeClicked:(id)sender;


- (void)buildBodyUI;
- (void)buildSubHeaderUI;

//
- (void)sendRequest:(BOOL)flag;

- (void)makeRequestProgress:(BOOL)flag;


- (void)getNextPrevDate:(BOOL)flag;

- (void)clearBodyUI;

- (void)manipulateNextPrev;

- (void)buildUI;


@end
