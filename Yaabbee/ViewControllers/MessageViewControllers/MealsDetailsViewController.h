//
//  InBoxViewController.h
//  Yaabbee
//
//  Created by Sarfaraj Biswas on 8/21/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NSData+Base64.h"
#import "NutritionDayData.h"
#import "request.h"
#import "NuvitaAppDelegate.h"
#import "MyTools.h"
#import "LoginPerser.h"
#import "LoginResponseData.h"
#import "UIImage+Scale.h"
#import "UIImageView+AFNetworking.h"
#import <MobileCoreServices/UTCoreTypes.h>

@protocol MealsDetailsDelegate <NSObject>

@required
- (void) backSelected:(BOOL)selected;
@end

@interface MealsDetailsViewController : UIViewController<UITextViewDelegate, UIPickerViewDelegate, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>
{
	MyTools *tools;
	UIView *loadingView;

    NSMutableArray *rankingArray;
    UIButton *doneButton ;
    UIPickerView *myPickerView;
	
    UILabel *rankLabel;
	
    UITextField *targetTxtFld;
    UITextField *consumedTxtFld;

    NSInteger saveRequestType;
    BOOL isTCamera ;
}

@property(nonatomic, retain)NSData*  imageContent;

@property (nonatomic, retain) IBOutlet UIView *cameraView;
@property (nonatomic, retain) IBOutlet UIImageView *cameraImageView;
@property (nonatomic, retain) IBOutlet UILabel *cameraLb;

@property (nonatomic, retain) IBOutlet UIButton *btnSave;
@property (nonatomic, retain) NutritionDayData *nutritionDayData;
@property (nonatomic, retain) NSString *dayKey,*enumMeal;
@property (nonatomic, retain) NSString *rankingText, *consumedCal;
@property (nonatomic, retain) NSString *subTitleText;

@property (nonatomic, retain)IBOutlet UITableView *messageTable;
@property (nonatomic, retain) IBOutlet UITextView *descTextView;

@property (nonatomic, retain) id <MealsDetailsDelegate> delegate;

- (CGSize)calculateTextSizeWithText:(NSString *)text;
- (IBAction)doneNote:(id)sender;
//- (IBAction)aMethod:(id)sender;

- (void)backBtnClicked;

//- (void)showPickerView:(NSInteger)sender;

- (IBAction)btnSaveClicked:(id)sender;   
//
//- (void)sendRequest:(BOOL)flag;

//- (void)clearBodyUI;

//- (void)manipulateNextPrev;

- (int)getHealthRank:(NSString*)rank;
- (void)resignKeyboard;
- (void)imageWasTapped :(id) sender;
- (void)saveMealsData;
- (void)saveMealsCameraData;
- (void)retrieveRawImageData:(UIImage *)image;

@end
