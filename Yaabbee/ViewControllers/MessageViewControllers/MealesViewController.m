//
//  MessagesViewController.m
//  Yaabbee
//
//  Created by Sarfaraj Biswas on 7/22/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "MealesViewController.h"
#import "MealsDetailsViewController.h"
//#import "OutBoxViewController.h"
#import "constant.h"

@interface MealesViewController ()

@property (retain, nonatomic) JBCustomButton *prev;
@property (retain, nonatomic) JBCustomButton *next;

@property (retain, nonatomic) NSArray *meals;

@end

@implementation MealesViewController
@synthesize messageTable, subhedaerLb,today,subheaderLb2,btnSave;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
//    NSString *xibName = @"MealesViewControllerI4";
//    if ([NuvitaAppDelegate isPhone5]) {
//        xibName = @"MealesViewController";
//    }
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.tabBarItem.title = @"Meals";
        self.tabBarItem.image = [UIImage imageNamed:@"meals.png"];
    }

    return self;
}

- (id) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        return [self initWithNibName:@"" bundle:nil];
    }

    return self;
}


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _prev =  [[JBCustomButton alloc] initWithFrame:CGRectMake(0, 20, 55, 30)];
    [_prev setButtonStyle:JBCustomButtonArrowTextLeftStyle];
    [_prev setTitle:@"Fri" forState:UIControlStateNormal];
	[_prev addTarget:self action:@selector(movePrevClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:_prev];

    _next =  [[JBCustomButton alloc] initWithFrame:CGRectMake(0, 20, 55, 30)];
    [_next setButtonStyle:JBCustomButtonArrowTextRightStyle];
    [_next setTitle:@"Wed" forState:UIControlStateNormal];
	[_next addTarget:self action:@selector(moveNextClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:_next];


	daysNextPrev = 0;
	self.view.hidden = YES;
	isLoaded= NO;	
	
	tools = [[MyTools alloc]init];
	loadingView = [tools createActivityIndicator1];


    _meals = @[@"Breakfast", @"Am Snack", @"Lunch", @"Pm Snack", @"Dinner", @"Water", @"Vitamin"];
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
    [self sendRequest:YES];
}

#pragma mark - BuildUI

- (void)buildIOSUI {
    
    if (![NuvitaAppDelegate isIOS7]) {
        
        for (UIView *subview in [self.view subviews]) {
            
            CGRect frame = subview.frame;
            
            switch ([subview tag]) {
                case 1111:
                    frame.origin.y -= 64;
                    break;
                    
                case 3333:
                    if (![NuvitaAppDelegate isPhone5]) frame.origin.y -= 64;
                    else frame.origin.y += 25;
                    break;
                    
                default:
                    break;
            }
            
            subview.frame = frame;
        }
        
        return;
    }
    
    
    UIButton *button = (UIButton *)[self.view viewWithTag:3333];
    CGRect frame = button.frame;
    frame.origin.y += (![NuvitaAppDelegate isPhone5]) ? 0 : 87;
    
    button.frame = frame;
}

- (void) backSelected:(BOOL)selected
{
	isLoaded = selected;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return 7;
}

- (BOOL)isHealthyRank:(NSString *)rank {

    if ([rank isEqualToString:@"Very Healthy"] || [rank isEqualToString:@"Healthy"])
        return YES;

    return NO;
}

- (NSString *)filterKey:(NSString *)text {
    
    NSArray *array = [text componentsSeparatedByString:@" "];
    if ([array count] == 2) {
        return [[[array firstObject] capitalizedString] stringByAppendingString:[[array lastObject] capitalizedString]];
    }
    
    return text;
}

- (void)didTapCellImage:(id)sender {
    
    UIButton *button = (UIButton *)sender;
//    UIView *cellContentView = (UIView *)button.superview;
//    UITableViewCell *cell = (UITableViewCell *)cellContentView.superview;

    NSLog(@"index: %d", [button tag]);
    NSString *key = [self filterKey:[_meals objectAtIndex:[sender tag]]];
    NSMutableDictionary *cellData = [[nutritionDayData nutritionDayDict] objectForKey:key];

    if ([cellData isKindOfClass:[NSDictionary class]]) {
        [cellData setValue:[self verifyMealTracking:[cellData valueForKey:WAS_EATEN_KEY]] forKey:WAS_EATEN_KEY];
    } else {
        [[nutritionDayData nutritionDayDict] setValue:[self verifyMealTracking:(NSString *)cellData] forKey:key];
    }

//    NSLog(@"celldata: %@", cellData);
//    NSLog(@"data: %@", [nutritionDayData nutritionDayDict]);
    [self.messageTable reloadData];
}

- (NSString *)verifyMealTracking:(NSString *)track {
    return [track boolValue] ? @"false" : @"true";
}

- (UIImage *)trackMealImage:(id)data {

    if ([data isKindOfClass:[NSString class]]) {
        return [data isEqualToString:@"true"] ? [UIImage imageNamed:@"orange_box"] : [UIImage imageNamed:@"empty_box"];
    }

    if ([[data valueForKey:WAS_EATEN_KEY] boolValue]) {
        if ([self isHealthyRank:[data valueForKey:HEALTH_RANKING_KEY]])
            return [UIImage imageNamed:@"orange_box"];
        return [UIImage imageNamed:@"gray_box"];
    }

    return [UIImage imageNamed:@"empty_box"];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"CellIdentifier";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    cell.textLabel.font = [UIFont fontWithName:DEFAULT_BOLD_FONT_NAME size:defaultFontSize];
    cell.detailTextLabel.font = [UIFont fontWithName:DEFAULT_NORMAL_FONT_NAME size:smallFontSize];

    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
    [button setTag:indexPath.row];
    if (indexPath.row != 2 && indexPath.row != 4)
        [button addTarget:self
                   action:@selector(didTapCellImage:)
         forControlEvents:UIControlEventTouchUpInside];
    [cell addSubview:button];

    NSString *key = [_meals objectAtIndex:indexPath.row];
    NSDictionary *data = [[nutritionDayData nutritionDayDict] valueForKey:[self filterKey:key]];

    cell.textLabel.text = key;

    if (indexPath.row < 5) {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.detailTextLabel.text = [data valueForKey:DESCRIPTION_KEY];
    }

    cell.imageView.layer.cornerRadius = 2.0f;
    cell.imageView.clipsToBounds = YES;

    cell.imageView.image = [self trackMealImage:data];

    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 39;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    CGSize size = CGSizeMake(211, 39);
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, size.height)];
    UIButton *saveBtn = [[UIButton alloc] initWithFrame:
                         CGRectMake((view.frame.size.width / 2) - (size.width *.9 / 2), size.height * .05, size.width * .9, size.height * .9)];
    [saveBtn setBackgroundImage:[UIImage imageNamed:@"wild_ones_redo.png"] forState:UIControlStateNormal];
    [saveBtn setTitleColor:[UIColor colorWithRed:0.2 green:0.4 blue:0.7 alpha:1.0] forState:UIControlStateNormal];
    [saveBtn setTitle:@"Save" forState:UIControlStateNormal];
    [saveBtn addTarget:self
                action:@selector(btnSaveClicked:)
      forControlEvents:UIControlEventTouchUpInside];


    [view addSubview:saveBtn];
    return view;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	[tableView deselectRowAtIndexPath:indexPath animated:YES];

    NSString *dayKey = @"";
	NSString *enumMeal = @"";
	switch (indexPath.row) {
        case 0:
            dayKey = BREAKFAST_KEY;
            enumMeal = @"1";
            break;
        case 1:
            dayKey = AM_SNACK_KEY;
            enumMeal = @"4";
            break;
        case 2:
            dayKey = LUNCH_KEY;
            enumMeal = @"2";
            break;
        case 3:
            dayKey = PM_SNACK_KEY;
            enumMeal = @"5";
            break;
        case 4:
            enumMeal = @"3";
            dayKey = DINNER_KEY;
            break;
        default:
            break;
    }

	if(indexPath.row < 5){

        [self saveNutritionDayData:1];

		MealsDetailsViewController *inBoxVC = [[MealsDetailsViewController alloc] initWithNibName:@"MealsDetailsViewController" bundle:nil];
		inBoxVC.dayKey = dayKey;
		inBoxVC.enumMeal = enumMeal;
		inBoxVC.subTitleText = [NSString stringWithFormat:@"%@ %@", dayKey, self.subhedaerLb.text];
		inBoxVC.nutritionDayData = nutritionDayData;
		inBoxVC.delegate=self;
		[self.navigationController pushViewController:inBoxVC animated:YES];
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
	//tableView.backgroundColor = [UIColor clearColor];  
}

#pragma mark - Public Method

//communiation part
- (void)setNextPrevBtnImage {

	NSCalendar *cal = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
	NSDateComponents *components = [cal components:(NSWeekdayCalendarUnit| NSYearCalendarUnit | NSMonthCalendarUnit | NSWeekCalendarUnit | NSDayCalendarUnit ) fromDate:self.today];
	NSInteger weekDay = [components weekday];
	if (weekDay == 7) {
		weekDay = 1;
	}
	else 
		weekDay++;

    [_next setTitle:[self getDaysString:weekDay] forState:UIControlStateNormal];
    [_next setNeedsDisplay];

	weekDay = [components weekday];
	if (weekDay == 1) {
		weekDay = 7;
	}
	else 
		weekDay--;

    [_prev setTitle:[self getDaysString:weekDay] forState:UIControlStateNormal];
    [_prev setNeedsDisplay];
}

- (NSString *)getDaysString:(NSInteger)weekDay {
	NSString* imgName = @"";
	switch (weekDay) {
		case 1:
			imgName = @"Sun";
			break;
		case 2:
			imgName = @"Mon";
			break;
		case 3:
			imgName = @"Tue";
			break;
		case 4:
			imgName = @"Wed";
			break;
		case 5:
			imgName = @"Thu";
			break;
		case 6:
			imgName = @"Fri";
			break;
		case 7:
			imgName = @"Sat";
			break;
		default:
			break;
	}

	return imgName;
}

- (void)clearBodyUI
{
	;//[self manipulateNextPrev];
	
	self.view.hidden = YES;	
}


- (void)manipulateNextPrev
{	
	if([self.today compare:[NSDate date]] == NSOrderedSame || [self.today compare:[NSDate date]] == NSOrderedDescending )
	{
		[buttonNext setEnabled:NO];
	}
	else
	{
		[buttonNext setEnabled:YES];
	}
}

- (void)buildUI
{
	LoginResponseData *loginResponseData = [LoginPerser getLoginResponse];
	
	self.subheaderLb2.text = loginResponseData.programName;

	self.subhedaerLb.text = nutritionDayData.DayLabel;
	
	[self.messageTable reloadData];
	
	self.view.hidden = NO;
}

- (IBAction)moveNextClicked:(id)sender
{
    [self saveNutritionDayData:2];
	[self getNextPrevDate:YES];
		//[self sendRequest: YES];
}

- (IBAction)movePrevClicked:(id)sender
{
    [self saveNutritionDayData:3];

	[self getNextPrevDate:NO];
	
	//[self sendRequest: NO];
}

//communication part
- (IBAction)btnSaveClicked:(id)sender
{
    [self saveNutritionDayData:4];
}

#pragma mark - Web Service Request

- (void)saveNutritionDayData:(NSInteger)saveRequestType {
	requestType = saveRequestType;

	NSString *memberID = [LoginPerser getLoginResponse].memberID;
	NSString *date = nutritionDayData.Date;
		
	NSDictionary *dict = [nutritionDayData.nutritionDayDict objectForKey:BREAKFAST_KEY];
	NSString *bf = [dict objectForKey:WAS_EATEN_KEY];

	dict = [nutritionDayData.nutritionDayDict objectForKey:AM_SNACK_KEY];
	NSString *amsk=  [dict objectForKey:WAS_EATEN_KEY];

	dict = [nutritionDayData.nutritionDayDict objectForKey:PM_SNACK_KEY];
	NSString *pmsk=  [dict objectForKey:WAS_EATEN_KEY];
	
	NSString *water =  [nutritionDayData.nutritionDayDict objectForKey:WATER_KEY];
	NSString *vitamin =   [nutritionDayData.nutritionDayDict objectForKey:VITAMIN_KEY];

    [tools startLoading:self.navigationController.view childView:loadingView text:LOADING_TEXT];
    request *r = [[request alloc] initWithTarget:self
								 SuccessAction:@selector(onSucceffulLogin:resData:)
								 FailureAction:@selector(onFailureLogin)];

	[r saveNutritionDay:memberID
                   date:date
              Breakfast:bf
                AMSnack:amsk
                PMSnack:pmsk
                  Water:water
                Vitamin:vitamin];
}

- (void)sendRequest:(BOOL)flag {
	requestType=0;
	if(self.today == nil)
		self.today = [NSDate date];

    //...limit
    _next.enabled = ![[self getDateFormat:self.today] isEqualToString:[self getDateFormat:[NSDate date]]];

	[tools startLoading:self.navigationController.view childView:loadingView text:LOADING_TEXT];
	[self makeRequestProgress:flag];
	[self setNextPrevBtnImage];
}

- (NSString *)getDateFormat:(NSDate *)date {
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM-dd-yyyy"];

    return [dateFormat stringFromDate:date];
}

- (void)makeRequestProgress:(BOOL)flag {
	//NSCalendar
	NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
	[dateFormat setDateFormat:@"MM-dd-yyyy"];
	
	NSString *dateString = [dateFormat stringFromDate:self.today];
	
	NSLog(@"Formated Date...%@", dateString);
	
	LoginResponseData *loginResponseData = [LoginPerser getLoginResponse];
	
	request *r=[[request alloc] initWithTarget:self
								 SuccessAction:@selector(onSucceffulLogin:resData:)
								 FailureAction:@selector(onFailureLogin)];
	[r getNutritionDay:loginResponseData.memberID date:dateString];
}


- (void)getNextPrevDate:(BOOL)flag
{	
	if(self.today == nil)
		self.today = [NSDate date];
	
	//
	NSCalendar *cal = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
	NSDateComponents *components = [cal components:(NSWeekdayCalendarUnit| NSYearCalendarUnit | NSMonthCalendarUnit | NSWeekCalendarUnit | NSDayCalendarUnit ) fromDate:self.today];
	
	if(flag)
		[components setDay:([components day] + 1)];
	else
		[components setDay:([components day] - 1)];
	
	self.today = [cal dateFromComponents:components];//[theCalendar dateByAddingComponents:dayComponent toDate:self.today options:0];
}

#pragma mark - Web Service Delegate

- (void)onSucceffulLogin:(ResponseStatus *)responseStatus resData:(NSObject *)obj {
	[tools stopLoading:loadingView];
	
	if (requestType==0) {
		if (responseStatus == nil || ![responseStatus isSuccess])
		{
			//[tools stopLoading:loadingView];		
			[self clearBodyUI];
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry" message:NO_RECORD_FOUND delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
			[alert show];
			return ;
		}

		nutritionDayData = (NutritionDayData*)obj;
        NSLog(@"data: %@", nutritionDayData.nutritionDayDict);

		[self buildUI];
        [self.messageTable reloadData];
	}
	else {
		NSString* text = @"Nutrition day successfully saved."; 
		
		if (responseStatus == nil || [responseStatus isSuccess])
		{
			text = @"Couldnot save nutrition day to server, please try again later.";
		}
		else {
//            if(requestType == 2)
//                [self sendRequest: YES];
//            else if(requestType == 3)
//                [self sendRequest: NO];
//            else if(requestType ==4)
//                return;
            [self sendRequest:YES];
			return;
		}
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Info" message:text delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
		[alert show];
		//alert.tag=2;
		return ;	
	}
}

- (void)onFailureLogin
{
	[tools stopLoading:loadingView];
	[self clearBodyUI];
}

- (UIImage*)getDaysRightImg:(int)weekDay
{
	NSString* imgName = @"";
	switch (weekDay) {
		case 1:
			imgName = @"sn1.png";
			break;
		case 2:
			imgName = @"mon1.png";
			break;
		case 3:
			imgName = @"tue1.png";
			break;
		case 4:
			imgName = @"wed1.png";
			break;
		case 5:
			imgName = @"thu1.png";
			break;
		case 6:
			imgName = @"fri1.png";
			break;
		case 7:
			imgName = @"sat1.png";
			break;
		default:
			break;
	}
	
	return [UIImage imageNamed:imgName];
}

- (UIImage*)getDaysLeftImg:(int)weekDay
{
	NSString* imgName = @"";
	switch (weekDay) {
		case 1:
			imgName = @"sn11.png";
			break;
		case 2:
			imgName = @"mon11.png";
			break;
		case 3:
			imgName = @"tue11.png";
			break;
		case 4:
			imgName = @"wed11.png";
			break;
		case 5:
			imgName = @"thu11.png";
			break;
		case 6:
			imgName = @"fri11.png";
			break;
		case 7:
			imgName = @"sat11.png";
			break;
		
		default:
			break;
	}
	
	return [UIImage imageNamed:imgName];
}

@end
