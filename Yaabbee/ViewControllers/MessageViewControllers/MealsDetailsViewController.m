//
//  InBoxViewController.m
//  Yaabbee
//
//  Created by Sarfaraj Biswas on 8/21/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "MealsDetailsViewController.h"
#import "constant.h"
#import <QuartzCore/QuartzCore.h>
//#import "NutritionDayData.h"

#define TEXTFLD_OFFSETY 100

@interface MealsDetailsViewController ()

@property (retain, nonatomic) IBOutlet UILabel *programLabel;
@property (assign) BOOL isKeyBoardVisible;
@property (assign) BOOL isPickerVisible;
@property (retain, nonatomic) NSMutableArray *items;

@end

@implementation MealsDetailsViewController

@synthesize isKeyBoardVisible;
@synthesize descTextView,messageTable;
@synthesize nutritionDayData;
@synthesize dayKey,subTitleText,btnSave,enumMeal,rankingText,consumedCal;
@synthesize delegate;
@synthesize cameraView,cameraImageView,cameraLb, imageContent;

#pragma mark - View lifecycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        _items = [[NSMutableArray alloc] init];
    }

    return self;
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)initData {

    _programLabel.text = [[LoginPerser getLoginResponse] programName];
    NSDictionary *dict = [self.nutritionDayData.nutritionDayDict objectForKey:self.dayKey];

	[self.descTextView setFont:[UIFont systemFontOfSize:defaultFontSize]];
	[self.descTextView.layer setBorderColor:[[UIColor darkGrayColor] CGColor]];
    [self.descTextView.layer setBorderWidth:1.0];
    [self.descTextView.layer setCornerRadius:4.0];
    self.descTextView.clipsToBounds = YES;
	self.descTextView.text = [dict objectForKey:DESCRIPTION_KEY];

    //accessory view
	UIToolbar *toolbar = [[UIToolbar alloc] init];
	[toolbar sizeToFit];

	UIBarButtonItem *flexButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                                target:self
                                                                                action:nil];

	UIBarButtonItem *doneBtn =[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                                            target:self
                                                                            action:@selector(resignKeyboard)];

	NSArray *itemsArray = [NSArray arrayWithObjects:flexButton, doneBtn, nil];
	[toolbar setItems:itemsArray];
	[self.descTextView setInputAccessoryView:toolbar];

    self.messageTable.layer.cornerRadius = 4.0;
    self.messageTable.clipsToBounds = YES;
	[self.messageTable.layer setBorderWidth: 1.0];
	[self.messageTable.layer setBorderColor: [[UIColor grayColor] CGColor]];

    _isPickerVisible = NO;
    self.rankingText = [dict objectForKey:HEALTH_RANKING_KEY];//@"healthy";
	self.consumedCal = [NSString stringWithFormat:@"%@", [dict objectForKey:CALORIES_CONSUMED_KEY]];

    //camera
    self.cameraLb.hidden = YES;

    isTCamera = YES;
    LoginResponseData *loginResponseData = [LoginPerser getLoginResponse];
    if ([loginResponseData.useCameraMeals isEqualToString:@"false"]) {
        isTCamera = NO;
    }

    if(!isTCamera) {
        self.cameraView.hidden = YES;
        self.descTextView.frame = CGRectMake(self.descTextView.frame.origin.x, self.descTextView.frame.origin.y, 300, self.descTextView.frame.size.height);
    } else {

        UITapGestureRecognizer *singleTapRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(imageWasTapped:)];
        [self.cameraImageView addGestureRecognizer:singleTapRecognizer];

        NSString* imgLink = [dict objectForKey:MEALS_PHOTO_URL];
        if (imgLink && [imgLink length] > 0) {
            self.cameraImageView.tag = 10099;
            [self.cameraImageView setImageWithURL:[NSURL URLWithString:imgLink]
                                 placeholderImage:[UIImage imageNamed:@"camera.png"]];


            self.cameraLb.hidden = NO;
            NSString* dateStr = [dict objectForKey:MEALS_PHOTO_DATE];
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
            NSDate *mealDate = [dateFormat dateFromString:dateStr];
            [dateFormat setDateFormat:@"MM/dd @ hh:mmaa"];

            NSString *dateString = [dateFormat stringFromDate:mealDate];
            self.cameraLb.text = dateString;
        }
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    tools = [[MyTools alloc]init];
	loadingView = [tools createActivityIndicator1];

	rankingArray = [[NSMutableArray alloc]initWithObjects:@"Missed", @"Very Unhealthy",@"Unhealthy",@"Moderate",@"Healthy", @"Very Healthy",nil];

	self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Meals"
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:self
                                                                  action:@selector(backBtnClicked)];

    [self initData];
    [self loadStaticItems];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Actions

- (void)backBtnClicked {
    [self saveMealsData];
}

- (void)imageWasTapped:(id)sender {
    
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
#if TARGET_IPHONE_SIMULATOR
    imagePicker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
#else
    imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera ;
#endif    
    imagePicker.delegate = self;
    imagePicker.allowsEditing = YES;
    imagePicker.mediaTypes = [NSArray arrayWithObjects:(NSString *)kUTTypeImage, nil];
   
    [self presentViewController:imagePicker animated:YES completion:^{
        
    }];
}

#pragma mark - UIImagePickerControllerDelegate

/// Method for saving image to photo album
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    
    if ([mediaType isEqualToString:(NSString *)kUTTypeImage]) {
        
        // Access the uncropped image from info dictionary
        UIImage *image = [info objectForKey:@"UIImagePickerControllerEditedImage"];
        if(image) {
            self.cameraImageView.image = image;
            [self retrieveRawImageData:image];
        }
        
        [picker dismissViewControllerAnimated:YES completion:^{
            
        }];
        
        self.cameraLb.hidden = NO;
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"MM/dd @ hh:mmaa"];
        self.cameraLb.text = [dateFormat stringFromDate:[NSDate date]];
    }

    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (void)retrieveRawImageData:(UIImage *)image {
    if(image!=nil)
    {   
        float imgW = image.size.width;
        float imgH = image.size.height;
        float expectedW = 150.0;
        float expectedH = 200.0;
        
        if(imgW > imgH)
            expectedW = 200.0;
        
        expectedH = imgH/(imgW/expectedW);
        
        image = [image scaleToSize:CGSizeMake(expectedW, expectedH)];
        self.imageContent = UIImagePNGRepresentation(image);

//        NSUInteger len1 = [self.imageContent length];
    }
}

#pragma mark - UITableViewDataSource

- (CGFloat)tableView:(UITableView *)_tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (_isPickerVisible && indexPath.row == 1)
        return 162;
    return 44;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_items count];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"CellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
        cell.textLabel.font = [UIFont systemFontOfSize:defaultFontSize];
        cell.detailTextLabel.font = [UIFont systemFontOfSize:defaultFontSize];
    }


    if (indexPath.row == 0) {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.detailTextLabel.textColor = [[self.rankingText lowercaseString] isEqualToString:@"healthy"] || [[self.rankingText lowercaseString] isEqualToString:@"very healthy"] ? [UIColor orangeColor] : [UIColor lightGrayColor];
    }

    if (_isPickerVisible && indexPath.row == 1) {

        UIPickerView *pickerView = (UIPickerView *)[cell viewWithTag:1111];
        if (!pickerView) {
            NSLog(@"no pickerview found!");
            pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0, 300, 162)];
            pickerView.tag = 1111;
            pickerView.delegate = self;
            pickerView.showsSelectionIndicator = YES;
        }

        [pickerView selectRow:[self getHealthRank:rankingText] inComponent:0 animated:YES];
        [cell addSubview:pickerView];
        return cell;
    }

    NSDictionary *item = [_items objectAtIndex:indexPath.row];
    cell.textLabel.text = [item objectForKey:@"label"];
    cell.detailTextLabel.text = [item objectForKey:@"value"];
    return cell;
}

- (CGSize)calculateTextSizeWithText:(NSString *)text{
    CGSize size =  [text sizeWithFont:[UIFont systemFontOfSize:15]
                    constrainedToSize: CGSizeMake(320.0, 480.0)
                        lineBreakMode:NSLineBreakByWordWrapping];
    return size;
}

- (void)loadStaticItems {
    [_items removeAllObjects];
    [_items addObject:[NSDictionary dictionaryWithObjectsAndKeys:
                       @"Ranking", @"label",
                       self.rankingText, @"value",
                       nil]];

    if ([LoginPerser isNuvitaX]) {
        NSDictionary *data = [self.nutritionDayData.nutritionDayDict objectForKey:self.dayKey];
        [_items addObject:[NSDictionary dictionaryWithObjectsAndKeys:
                           @"Target", @"label",
                           [data objectForKey:TARGET_CALORIES_KEY], @"value",
                           nil]];

        [_items addObject:[NSDictionary dictionaryWithObjectsAndKeys:
                           @"Consumed", @"label",
                           self.consumedCal, @"value",
                           nil]];
    }
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 39;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectZero];
    view.backgroundColor = [UIColor whiteColor];

    CGSize size = CGSizeMake(211, 39);
    UIButton *saveBtn = [[UIButton alloc] initWithFrame:
                          CGRectMake((tableView.frame.size.width / 2) - (size.width *.9 / 2), size.height * .05, size.width * .9, size.height * .9)];
    [saveBtn setBackgroundImage:[UIImage imageNamed:@"wild_ones_redo.png"] forState:UIControlStateNormal];
    [saveBtn setTitleColor:[UIColor colorWithRed:0.2 green:0.4 blue:0.7 alpha:1.0] forState:UIControlStateNormal];
    [saveBtn setTitle:@"Save" forState:UIControlStateNormal];
    [saveBtn addTarget:self
                action:@selector(btnSaveClicked:)
      forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:saveBtn];

    return view;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    if (indexPath.row == 0) {
        _isPickerVisible = !_isPickerVisible;

        if (!_isPickerVisible) {
            [_items removeObjectAtIndex:1];
            NSIndexPath *newIndexPath = [NSIndexPath indexPathForRow:1 inSection:0];
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
        } else {

            [_items insertObject:[NSDictionary dictionaryWithObjectsAndKeys:
                                  @"", @"label",
                                  @"", @"value",
                                  nil]
                         atIndex:1];

            NSIndexPath *newIndexPath = [NSIndexPath indexPathForRow:1 inSection:0];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
        }
    }

}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    // NSIndexPath *iPath = [self.tableView indexPathForCell:cell];
    if(self.navigationItem.rightBarButtonItem!=nil)
		self.navigationItem.rightBarButtonItem.tag=textField.tag;
	
    if(!self.isKeyBoardVisible){
	self.navigationItem.rightBarButtonItem =nil;
	UIBarButtonItem *barBtnDone = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneNote:)];
	barBtnDone.title = @"Done";
	barBtnDone.tag=textField.tag;
	self.navigationItem.rightBarButtonItem = barBtnDone;
		
        self.isKeyBoardVisible = YES;
        
        CGRect tableFrame = self.view.frame;
        tableFrame.origin.y -= TEXTFLD_OFFSETY;
        
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.3];
        [UIView setAnimationDidStopSelector:@selector(animationDidStop:finished:context:)];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
        [UIView setAnimationDelegate:self];
        
        self.view.frame = tableFrame;
        
        [UIView commitAnimations];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{

    self.consumedCal = textField.text;	
    [textField resignFirstResponder];
    self.navigationItem.rightBarButtonItem = nil;
    if(self.isKeyBoardVisible){
        
        self.isKeyBoardVisible = NO;
        CGRect tableFrame = self.view.frame;
        tableFrame.origin.y += TEXTFLD_OFFSETY;
        
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.3];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
        [UIView setAnimationDelegate:self];
        
        self.view.frame = tableFrame;
        
        [UIView commitAnimations];
    }
	
    return YES;
}

- (IBAction)doneNote:(id)sender {
    switch (((UIBarButtonItem*)sender).tag) {
		case 1:
			[self.descTextView resignFirstResponder];
			break;
		case 2:
			[targetTxtFld resignFirstResponder];
			
			break;
		case 3:
			self.consumedCal = consumedTxtFld.text;
			[consumedTxtFld resignFirstResponder];
			break;
			
		default:
			break;
	}	
    	
	if(self.isKeyBoardVisible){
		
		self.isKeyBoardVisible = NO;
		CGRect tableFrame = self.view.frame;
		tableFrame.origin.y += TEXTFLD_OFFSETY;
		
		[UIView beginAnimations:nil context:NULL];
		[UIView setAnimationDuration:0.3];
		[UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
		[UIView setAnimationDelegate:self];
		
		self.view.frame = tableFrame;
		
		[UIView commitAnimations];
	}
    self.navigationItem.rightBarButtonItem = nil;
    //self.navigationItem.rightBarButtonItem = rightButton;	
}

- (int)getHealthRank:(NSString*)rank {
	NSInteger len = [rankingArray count];
	
	for (int ii=0; ii < len;ii++) {
		
		NSString* data = [rankingArray objectAtIndex:ii];
		
		if([[data lowercaseString] isEqualToString:[rank lowercaseString]])
		    return ii;
	}
	
	return 0;
}

#pragma mark - UIPickerView

- (void)resignKeyboard {
	[self.descTextView resignFirstResponder];
}

#pragma mark - UIPickerViewDelegate

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    NSUInteger numRows = pickerView.tag == 1 ? [rankingArray count] : [rankingArray count];
    
    return numRows;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    self.rankingText = [rankingArray objectAtIndex:row];
    [_items replaceObjectAtIndex:0
                      withObject:[NSDictionary dictionaryWithObjectsAndKeys:
                                  @"Ranking", @"label",
                                  self.rankingText, @"value",
                                  nil]];

    [self.messageTable reloadData];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [rankingArray objectAtIndex:row];
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    CGFloat componentWidth = 180.0;
	return componentWidth;
}

#pragma mark - Web Service Request

- (IBAction)btnSaveClicked:(id)sender {
    if(self.imageContent == nil || [self.imageContent length]  == 0) {
        [self saveMealsData];
        return;
    }

    [self saveMealsCameraData];
}

- (void)saveMealsData {
    saveRequestType = 0;
	NSDictionary* dict = [self.nutritionDayData.nutritionDayDict objectForKey:self.dayKey];
	
	NSString *memberID = [LoginPerser getLoginResponse].memberID;
	NSString *date = self.nutritionDayData.Date;
	NSString *desc = self.descTextView.text;
	NSString *targetCal = [NSString stringWithFormat:@"%@", [dict objectForKey:TARGET_CALORIES_KEY]];
	NSString *consumedCalText = [NSString stringWithFormat:@"%@", [dict objectForKey:CALORIES_CONSUMED_KEY]];
	
	if([LoginPerser isNuvitaX])
	{
		targetCal= targetTxtFld.text;
		consumedCalText= consumedTxtFld.text;
	}
	
	NSString *numMeal = self.enumMeal;
	NSString *healthRank =  [NSString stringWithFormat:@"%lu", (unsigned long)[rankingArray indexOfObject:rankingText]];

	[tools startLoading:self.navigationController.view childView:loadingView text:LOADING_TEXT];
	request *r=[[request alloc] initWithTarget:self
								 SuccessAction:@selector(onSucceffulLogin:resData:)
								 FailureAction:@selector(onFailureLogin)];

	[r saveNutritionMeal:memberID
                    date:date
                mealEnum:numMeal
             description:desc
        caloriesConsumed:consumedCalText
           healthRanking:healthRank
          targetCalories:targetCal];
}

- (void)saveMealsCameraData {
    saveRequestType = 1;

	NSString *memberID = [LoginPerser getLoginResponse].memberID;
	NSString *date = nil;//self.nutritionDayData.Date;

	NSString *numMeal = self.enumMeal;
	
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
	[dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
	NSString *dateString = [dateFormat stringFromDate:[NSDate date]];
    
    date = [dateString stringByReplacingOccurrencesOfString:@" " withString:@"T"];

	
	[tools startLoading:self.navigationController.view childView:loadingView text:@"Uploading..."];
	request *r=[[request alloc] initWithTarget:self
								 SuccessAction:@selector(onSucceffulLogin:resData:)
								 FailureAction:@selector(onFailureLogin)];
	
	[r saveMealsCamera:memberID
                  date:date
              mealEnum:numMeal
                 photo:self.imageContent];
}

#pragma mark - Web Service Delegate

- (void)onSucceffulLogin:(ResponseStatus *)responseStatus resData:(NSObject *)obj {
	[tools stopLoading:loadingView];
	
	NSString* text = @"Nutrition meal successfully saved.";
	if (responseStatus == nil || [responseStatus isSuccess]) {
		text = @"Couldnot save nutrition meal to server, please try again later.";
	} else {

        if (saveRequestType ==1) {
            [self saveMealsData];
            return;
        }

		[self.navigationController popViewControllerAnimated:YES];
		return;
	}

	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Info"
                                                    message:text
                                                   delegate:self
                                          cancelButtonTitle:@"Ok"
                                          otherButtonTitles: nil];
	[alert setTag:2];
    [alert show];
}

- (void)onFailureLogin {
	[tools stopLoading:loadingView];
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	if (buttonIndex == 0) {
		if (alertView.tag == 2) {
			[self.navigationController popViewControllerAnimated:YES];
		}
	}
}

@end
