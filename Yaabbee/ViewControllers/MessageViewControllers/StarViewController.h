//
//  OutBoxViewController.h
//  Yaabbee
//
//  Created by Sarfaraj Biswas on 8/22/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "request.h"
#import "NuvitaAppDelegate.h"
#import "MyTools.h"
#import "LoginPerser.h"
#import "LoginResponseData.h"
#import  "LoadMoreTableFooterView.h"
#import "EGORefreshTableHeaderView.h"

@class StarItem;
@class StarProgressData;

@interface StarViewController : UIViewController <LoadMoreTableFooterDelegate, EGORefreshTableHeaderDelegate, UITableViewDelegate>
{	
	MyTools *tools;
	UIView *loadingView;

	StarProgressData *starProgressData;
	
	BOOL isLoaded;
	
	int daysNextPrev;
	NSDate *today;
	
//	UIButton *buttonPrev, *buttonNext;
	int totalElement;
    NSInteger serverRequestID;
    NSInteger currPage;
    NSInteger currStar;
    BOOL _reloading;
    
    LoadMoreTableFooterView *loadMoreFooterView;
    //EGORefreshTableHeaderView *refreshHeaderView;
}

//@property (nonatomic, retain)EGORefreshTableHeaderView *refreshHeaderView;
@property (nonatomic, retain)LoadMoreTableFooterView *loadMoreFooterView;
@property (nonatomic, retain)NSDate *today;
@property (nonatomic, retain) IBOutlet UITableView *tableView_;
@property (nonatomic, retain) IBOutlet UILabel *subheaderLb1;
@property (nonatomic, retain) IBOutlet UILabel *subheaderLb2;

//@property (nonatomic, retain) IBOutlet UISearchBar *searchBar;
- (CGSize)calculateTextSizeWithText:(NSString *)text;

- (void)configureCell:(UITableViewCell *)cell withIndex:(NSIndexPath *)indexPath;

- (IBAction)moveNextClicked:(id)sender;
- (IBAction)movePrevClicked:(id)sender;
- (void)starBtnClicked:(id)sender;

// 
- (void)sendRequest:(BOOL)flag;

- (void)makeRequestStarProgress:(BOOL)flag;


- (void)getNextPrevDate:(BOOL)flag;

- (void)clearBodyUI;

- (void)buildUI;
- (void)reloadTableViewDataSource:(NSInteger)type;
- (void)doneLoadingTableViewData;
- (void)initGStarVar;
- (BOOL)isNextPageAvailable;
- (BOOL)isPrevPageAvailable;

@end
