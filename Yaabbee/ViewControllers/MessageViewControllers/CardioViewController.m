//
//  MessagesViewController.m
//  Yaabbee
//
//  Created by Sarfaraj Biswas on 7/22/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "CardioViewController.h"
//#import "InBoxViewController.h"
#import "CustTableCell.h"
#import "constant.h"
#import "CustTableHeaderView.h"
#import <QuartzCore/QuartzCore.h>

@interface CardioViewController ()

@property (retain, nonatomic) IBOutlet UILabel *programName;
@property (retain, nonatomic) IBOutlet UILabel *weekLabel;

@property (nonatomic, retain) JBCustomButton *buttonNext;
@property (nonatomic, retain) JBCustomButton *buttonPrev;

@end

@implementation CardioViewController

@synthesize columnsTitle1, columnsTitle2;
@synthesize tableView_, headerView, bodyView;

@synthesize today;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.tabBarItem.title = @"Cardio";
        self.tabBarItem.image = [UIImage imageNamed:@"cardio.png"];
    }
    return self;
}

- (id) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        return [self initWithNibName:@"" bundle:nil];
    }
    return self;
}


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];

    isLoaded = NO;
	
	tools = [[MyTools alloc]init];
	loadingView = [tools createActivityIndicator1];
	
    _buttonPrev =  [[JBCustomButton alloc] initWithFrame:CGRectMake(0, 20, 30, 30)];
    [_buttonPrev setButtonStyle:JBCustomButtonArrowLeftStyle];
	[_buttonPrev addTarget:self action:@selector(movePrevClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    _buttonNext =  [[JBCustomButton alloc] initWithFrame:CGRectMake(0, 20, 30, 30)];
    [_buttonNext setButtonStyle:JBCustomButtonArrowRightStyle];
	[_buttonNext addTarget:self action:@selector(moveNextClicked:) forControlEvents:UIControlEventTouchUpInside];
	
	self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:
                                               [[UIBarButtonItem alloc] initWithCustomView:_buttonNext],
                                               [[UIBarButtonItem alloc] initWithCustomView:_buttonPrev], nil];
		
	daysNextPrev = 0;
	self.view.hidden = YES;
	isLoaded= NO;	
	
       //if(columnsTitle1) 
       self.columnsTitle1 = [NSMutableArray arrayWithObjects:@"Target", @"Below", @"In zone", @"Above", nil];
       self.columnsTitle2 = [NSMutableArray arrayWithObjects:@"Date", @"Cals", @"Below", @"In zone", @"Above", @"Total", nil];

}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	if(!isLoaded)
		[self sendRequest:YES];
	
	[self manipulateNextPrev];
    
	NSLog(@"viewWillAppear...");
}

- (void)homeClicked:(id)sender {
	NuvitaAppDelegate* app = (NuvitaAppDelegate*)[[UIApplication sharedApplication] delegate];
	HomeViewController *quizVC = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
	[app.forceNavigationViewController pushViewController:quizVC animated:YES ];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - section build ui

- (void)buildSubHeaderUI {
	LoginResponseData *loginResponseData = [LoginPerser getLoginResponse];
	_programName.text = [loginResponseData programName];
    _weekLabel.text = [cardioWeekData WeekLabel];
}

- (void)buildBodyUI
{
	UIImage* image = [UIImage imageNamed:@"smallbox.png"];
	
	if(self.bodyView != nil)
	{
		[self.bodyView removeFromSuperview];
	}
	
    //
    float hrmBodyY = 30.0;//for 3.5 inch iphone, 44 for 4 inch iphone.
    
    if([NuvitaAppDelegate isPhone5])
        hrmBodyY = 44.0;
    
	self.bodyView = [[UIView alloc]initWithFrame:CGRectMake(10, 100, self.view.frame.size.width - 20, image.size.height)];
    self.bodyView.layer.cornerRadius = 5.0;
    self.bodyView.clipsToBounds = YES;
	
	//UILabel* label = nil;
	
	//grid
	float gridW= 220;
	float gridH = 81;
	
	UIView *gridView = [[UIView alloc]initWithFrame:CGRectMake(10, 20, gridW, gridH)];
    [gridView setTag:1111];
	[gridView.layer setBorderWidth:2.0];
	[gridView.layer setBorderColor:[[UIColor colorWithWhite:0.5 alpha:1.0] CGColor]];
	
	//grid heder
	CustTableHeaderView *gridHView = [[CustTableHeaderView alloc]initWithFrame:CGRectMake(0, 0, gridW, gridH/3)];
	[gridHView numberOfCol:4 width:  gridW];
    gridHView.backgroundColor = [UIColor whiteColor];

	UILabel *label = nil;
	
	float gap = (gridW/4 );
	float pos = 0;
	for (int ii = 0; ii < 4; ii++) {
        pos = ii*gap + 1;
		label = [[UILabel	alloc] initWithFrame:CGRectMake(pos, 0, gap - 1, gridH/3)];
		
		label.backgroundColor = [UIColor clearColor];
		label.font = [UIFont fontWithName:DEFAULT_BOLD_FONT_NAME size:smallFontSize];
		label.text = [self.columnsTitle1 objectAtIndex:ii];
		label.textAlignment = NSTextAlignmentCenter;
        label.textColor = [UIColor colorWithRed:0.2 green:0.4 blue:0.7 alpha:1.0];

		[gridHView addSubview:label];
	}
	
	[gridView addSubview:gridHView];
	//[bodyView addSubview:gridView];
	
	//grid body
	//HR
	gridHView = [[CustTableHeaderView alloc]initWithFrame:CGRectMake(0, gridH/3, gridW, gridH/3)];
	gridHView.backgroundColor = [UIColor clearColor];
	[gridHView numberOfCol:4 width:  gridW];

//    pos = gap + 1;
	for (int ii = 0; ii < 4; ii++) {
        pos = ii * gap + 1;

        label = [[UILabel	alloc] initWithFrame:CGRectMake(pos, 0, 53, gridH/3)];
        label.font = [UIFont systemFontOfSize:tooSmallFontSize];
        label.textAlignment = NSTextAlignmentCenter;
        label.textColor = [UIColor blackColor];
        label.backgroundColor = [UIColor clearColor];
		
		switch (ii) {
			case 0:
				label.backgroundColor = CARDIO_GRID_BODY_BLACK_COLOR;
                label.textColor = [UIColor colorWithRed:0.2 green:0.4 blue:0.7 alpha:1.0];
				label.text = @"HR";
				break;
			case 1:
				label.backgroundColor = CARDIO_GRID_BODY_YELLOW_COLOR;
				label.text = cardioWeekData.BelowRangePct;
				break;
			case 2:
				label.backgroundColor = CARDIO_GRID_BODY_GREEN_COLOR;
				label.text = cardioWeekData.InZoneRangePct;
				break;
			case 3:
				label.backgroundColor = CARDIO_GRID_BODY_ORANGE_COLOR;
				label.text = cardioWeekData.AboveRangePct;
				break;	
			default:
				break;
		}

		[gridHView addSubview:label]; 
	}		
	[gridView addSubview:gridHView];
	
	//Zones
	gridHView = [[CustTableHeaderView alloc]initWithFrame:CGRectMake(0, 2*gridH/3, gridW, gridH/3)];
	gridHView.backgroundColor = [UIColor clearColor];
	[gridHView numberOfCol:4 width:gridW];
	
	for(int ii = 0; ii < 4; ii++) {
        pos = ii*gap + 1;

        label = [[UILabel	alloc] initWithFrame:CGRectMake(pos, 0, 53, gridH/3)];
        label.backgroundColor = [UIColor clearColor];
        label.font = [UIFont fontWithName:DEFAULT_NORMAL_FONT_NAME size:tooSmallFontSize];
        label.textAlignment = NSTextAlignmentCenter;
        label.textColor = [UIColor blackColor];
        label.lineBreakMode = YES;
        label.numberOfLines = 2;

		switch (ii) {
			case 0:
				label.backgroundColor = CARDIO_GRID_BODY_BLACK_COLOR;
                label.textColor = [UIColor colorWithRed:0.2 green:0.4 blue:0.7 alpha:1.0];
				label.text = @"Zones";
				break;
			case 1:
				label.backgroundColor = CARDIO_GRID_BODY_YELLOW_COLOR;
				label.text = [cardioWeekData.BelowRangeHR stringByReplacingOccurrencesOfString:@" " withString:@"\n"];//[uid stringByReplacingOccurrencesOfString:@"-" withString:@""];
				break;
			case 2:
				label.backgroundColor = CARDIO_GRID_BODY_GREEN_COLOR;
				label.text = [cardioWeekData.InZoneRangeHR stringByReplacingOccurrencesOfString:@" " withString:@"\n"];;
				break;
			case 3:
				label.backgroundColor = CARDIO_GRID_BODY_ORANGE_COLOR;
				label.text = [cardioWeekData.AboveRangeHR stringByReplacingOccurrencesOfString:@" " withString:@"\n"];;
				break;	
			default:
				break;
		}

		[gridHView addSubview:label]; 
	}		
	[gridView addSubview:gridHView];

	//
	[self.bodyView addSubview:gridView];
	
	//[self.view addSubview: bodyView];
	
	//percentage
	gridView = [[UIView alloc]initWithFrame:CGRectMake((10*2 + gridW) - 1, 20 - 1 , 50 + 2,  gridH + 2)];
	[gridView.layer setBorderWidth:2.0];
	[gridView.layer setBorderColor:[[UIColor colorWithWhite:0.5 alpha:1.0] CGColor]];

	[self.bodyView addSubview:gridView];

	int progressH = gridH*[cardioWeekData.ProgressPerecent intValue]/100;
	if(progressH > gridH) progressH = gridH;
	
	gridView = [[UIView alloc]initWithFrame:CGRectMake((10*2 + gridW) + 1, 19 + (gridH - progressH), 50 - 2, progressH)];
	if([cardioWeekData.ProgressPerecent intValue] < 70)
		gridView.backgroundColor = CARDIO_PERC_GRAY_COLOR;
	else if([cardioWeekData.ProgressPerecent intValue] > 69 && [cardioWeekData.ProgressPerecent intValue] < 90)
		gridView.backgroundColor = CARDIO_PERC_ORANGE_COLOR;
	else
		gridView.backgroundColor = CARDIO_PERC_GREEN_COLOR;
		
	[self.bodyView addSubview:gridView];
	
	//val
	gridView = [[UIView alloc]initWithFrame:CGRectMake((10*2 + gridW), 20, 50, gridH)];
	gridView.backgroundColor =[UIColor clearColor];;
	label = [[UILabel	alloc] initWithFrame:CGRectMake(0, 0, 50, gridH)];
	
	label.backgroundColor = [UIColor clearColor];
	label.font =[UIFont systemFontOfSize:smallFontSize];
	label.text = [cardioWeekData.ProgressPerecent stringByAppendingString:@"%"];//@"65%";
	label.textAlignment = NSTextAlignmentCenter;
	label.textColor = [UIColor colorWithRed:0.2 green:0.4 blue:0.7 alpha:1.0];
	[gridView addSubview:label]; 
	[self.bodyView addSubview:gridView];
	
	
	//label Field one
	gridView = [[UIView alloc]initWithFrame:CGRectMake(10, 26 + gridH, self.bodyView.frame.size.width - 20, gridH/3)];
	gridView.backgroundColor = [UIColor clearColor];
	
	//text
	label = [[UILabel	alloc] initWithFrame:CGRectMake(0, 0, gridW, gridH/3)];
	
	label.backgroundColor = [UIColor clearColor];
	label.font = [UIFont systemFontOfSize:smallFontSize];
	label.text = @"This weeks goal:";//[self.columnsTitle2 objectAtIndex:ii];// [NSString stringWithFormat:@"(%d, %d)", section, ii];
	label.textAlignment = NSTextAlignmentRight;
	label.textColor = [UIColor blackColor];
	//label.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleHeight;
	[gridView addSubview:label]; 
	
	//val
	label = [[UILabel	alloc] initWithFrame:CGRectMake(gridW + 10, 0, 50, gridH/3)];
	
	label.backgroundColor = [UIColor clearColor];
	label.font =[UIFont fontWithName:DEFAULT_NORMAL_FONT_NAME size:smallFontSize];
	label.text = cardioWeekData.Goal;
	label.textAlignment = NSTextAlignmentRight;
	label.textColor = [UIColor blackColor];
	//label.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleHeight;
	[gridView addSubview:label]; 

	[self.bodyView addSubview:gridView];
	
	//field two----	
	gridView = [[UIView alloc]initWithFrame:CGRectMake(10, 20 + gridH + gridH/3, self.bodyView.frame.size.width - 20, gridH/3)];
	gridView.backgroundColor = [UIColor clearColor];
	
	//text
	label = [[UILabel	alloc] initWithFrame:CGRectMake(0, 0, gridW, gridH/3)];
	
	label.backgroundColor = [UIColor clearColor];
	label.font = [UIFont fontWithName:DEFAULT_NORMAL_FONT_NAME size:smallFontSize];
	label.text = @"Your progress:";
	label.textAlignment = NSTextAlignmentRight;
	label.textColor = [UIColor blackColor];
	//label.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleHeight;
	[gridView addSubview:label]; 
	
	//val
	label = [[UILabel	alloc] initWithFrame:CGRectMake(gridW + 10, 0, 50, gridH/3)];
	
	label.backgroundColor = [UIColor clearColor];
	label.font = [UIFont fontWithName:DEFAULT_NORMAL_FONT_NAME size:smallFontSize];
	label.text = cardioWeekData.Progress;//
	label.textAlignment = NSTextAlignmentRight;
	label.textColor = [UIColor blackColor];
	//label.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleHeight;
	[gridView addSubview:label]; 
	
	[self.bodyView addSubview:gridView];
	
	//
	[self.view addSubview: self.bodyView];
	//[bodyView release];
}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [cardioWeekData.ItemsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	NSString *MyIdentifier = [NSString stringWithFormat:@"MyIdentifier %i", indexPath.row];
	
	CustTableCell *cell = (CustTableCell *)[tableView dequeueReusableCellWithIdentifier:MyIdentifier];
	
//	if (cell == nil) {
        cell = [[CustTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MyIdentifier];
		
		UILabel *label = nil;//[[[UILabel	alloc] initWithFrame:CGRectMake(0.0, 0, 30.0,
																//	tableView.rowHeight)] autorelease];
		[cell numberOfCol:6 width: tableView.frame.size.width];
		
		float gap = (tableView.frame.size.width/6 );
		float pos = 0;
		
		int size = 6;//[cardioWeekData.ItemsArray count];
		
		for (int ii = 0; ii < size; ii++) {
			pos = ii * gap + 1;
			label = [[UILabel	alloc] initWithFrame:CGRectMake(pos, 0, 48, 30)];
//            label.layer.borderWidth = 2.0;
//            label.layer.borderColor = [[UIColor redColor] CGColor];

			CardioWeekItem* cardioWeekItem = (CardioWeekItem*)[cardioWeekData.ItemsArray objectAtIndex:indexPath.row];
			
			switch (ii) {
				case 0:
					label.text = [cardioWeekItem getDate];
					label.backgroundColor = (indexPath.row%2 == 0)? CARDIO_GRID_BODY_GRAY_COLOR : CARDIO_GRID_BODY_WHITE_COLOR;
					break;
				case 1:
					label.text = cardioWeekItem.Calories;
					label.backgroundColor = (indexPath.row%2 == 0)? CARDIO_GRID_BODY_GRAY_COLOR : CARDIO_GRID_BODY_WHITE_COLOR;
					break;
				case 5:	
					label.text = cardioWeekItem.Total;
					label.backgroundColor = (indexPath.row%2 == 0)? CARDIO_GRID_BODY_GRAY_COLOR : CARDIO_GRID_BODY_WHITE_COLOR;
					break;
				case 2:
					label.text = cardioWeekItem.Below;
					label.backgroundColor = CARDIO_GRID_BODY_YELLOW_COLOR;
					break;
				case 3:
					label.text = cardioWeekItem.InZone;
					label.backgroundColor = CARDIO_GRID_BODY_GREEN_COLOR;
					break;
				case 4:
					label.text = cardioWeekItem.Above;
					label.backgroundColor = CARDIO_GRID_BODY_ORANGE_COLOR;
					break;
				default:
					break;
			}
			//label.backgroundColor = [UIColor redColor];
			label.font = [UIFont fontWithName:DEFAULT_NORMAL_FONT_NAME size:tooSmallFontSize];
			label.lineBreakMode = YES;
			label.numberOfLines = 2; 
			label.textAlignment = NSTextAlignmentCenter;
			label.textColor = [UIColor blackColor];
			//label.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleHeight;
			[cell.contentView addSubview:label]; 
			
		}

    cell.selectionStyle = UITableViewCellEditingStyleNone;
    cell.backgroundColor = [UIColor colorWithWhite:0.5 alpha:1.0];

    CGSize mainViewSize = cell.bounds.size;
    CGFloat borderWidth = 2;
    UIColor *borderColor = [UIColor colorWithWhite:0.5 alpha:1.0];
    UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, borderWidth, mainViewSize.height)];
    UIView *rightView = [[UIView alloc] initWithFrame:CGRectMake(mainViewSize.width - borderWidth, 0, borderWidth, mainViewSize.height)];
    leftView.opaque = YES;
    rightView.opaque = YES;
    leftView.backgroundColor = borderColor;
    rightView.backgroundColor = borderColor;

    // for bonus points, set the views' autoresizing mask so they'll stay with the edges:
    leftView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleRightMargin;
    rightView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleLeftMargin;

    [cell addSubview:leftView];
    [cell addSubview:rightView];


	return cell;
}


- (CGFloat)tableView:(UITableView *)messageTable heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 30;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 23;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 23;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
	CustTableHeaderView *headerView1 = [[CustTableHeaderView alloc] initWithFrame:CGRectMake(0, 0, 320, 26)];
	[headerView1 numberOfCol:6 width:tableView.frame.size.width];
    headerView1.backgroundColor = [UIColor whiteColor];
    headerView1.layer.borderColor = [[UIColor colorWithWhite:0.5 alpha:1.0] CGColor];
    headerView1.layer.borderWidth = 2.0;

	UILabel *label = nil;
	
	float gap = (tableView.frame.size.width/6 );
	float pos = 0;
	for(int ii = 0; ii < 6; ii++)
	{
		pos = ii*gap + 1;				
		label = [[UILabel	alloc] initWithFrame:CGRectMake(pos, 0, gap - 1, 23)];

		label.backgroundColor = [UIColor clearColor];
		label.font = [UIFont fontWithName:DEFAULT_BOLD_FONT_NAME size:smallFontSize];
		label.text = [self.columnsTitle2 objectAtIndex:ii];
		label.textAlignment = NSTextAlignmentCenter;
		label.textColor = [UIColor colorWithRed:0.2 green:0.4 blue:0.7 alpha:1.0];
		[headerView1 addSubview:label]; 
		
	}
	
	return headerView1;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
	CustTableHeaderView *headerView1 = [[CustTableHeaderView alloc]initWithFrame:CGRectMake(0, 0, 320, 23)];
	[headerView1 numberOfCol:6 width: tableView.frame.size.width];
	headerView1.backgroundColor = [UIColor whiteColor];
    headerView1.layer.borderColor = [[UIColor colorWithWhite:0.5 alpha:1.0] CGColor];
    headerView1.layer.borderWidth = 2.0;
	
	UILabel *label = nil;
	
	float gap = (tableView.frame.size.width/6  );
	float pos = 0;
	for(int ii = 0; ii < 6; ii++)
	{
		pos = ii*gap + 1;				
		label = [[UILabel	alloc] initWithFrame:CGRectMake(pos, 0, gap - 2, 23)];
		switch (ii) {
			case 0:
				label.text = @"Total";
				label.backgroundColor = [UIColor clearColor];
				break;
			case 1:
				label.text = [cardioWeekData getTotalCals];
				label.backgroundColor = [UIColor clearColor];
				break;
			case 5:	
				label.text = [cardioWeekData getTotalVal];
				label.backgroundColor = [UIColor clearColor];
				break;
			case 2:
				label.text = [cardioWeekData getTotalBelow];
				label.backgroundColor = CARDIO_GRID_BODY_YELLOW_COLOR;
				break;
			case 3:
				label.text = [cardioWeekData getTotalInZone];
				label.backgroundColor = CARDIO_GRID_BODY_GREEN_COLOR;
				break;
			case 4:
				label.text = [cardioWeekData getTotalAbove];
				label.backgroundColor = CARDIO_GRID_BODY_ORANGE_COLOR;
				break;
			default:
				break;
		}
		
		
		label.font = [UIFont fontWithName:DEFAULT_NORMAL_FONT_NAME size:tooSmallFontSize];
		label.textAlignment = NSTextAlignmentCenter;
		label.textColor = [UIColor blackColor];
		//label.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleHeight;
		[headerView1 addSubview:label]; 
		
	}

	return headerView1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

- (void)clearBodyUI
{
	[self manipulateNextPrev];
	
	//	if(self.memberUIView !=nil)
	//	{
	//		[[self.memberUIView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
	//	}
	self.view.hidden = YES;	
}


- (void)manipulateNextPrev
{	
//  if([self.today compare:[NSDate date]] == NSOrderedSame || [self.today compare:[NSDate date]] == NSOrderedDescending )
//	{
//		[buttonNext setEnabled:NO];
//	}
//	else
//	{
//		[buttonNext setEnabled:YES];
//	}
}


- (void)buildUI
{
	totalElement = 0;
	
	if(cardioWeekData == nil || cardioWeekData.ItemsArray == nil) return ;
	
	totalElement = [cardioWeekData.ItemsArray count];
	
//	if(totalElement == 0)
//	{
//		[self clearBodyUI ];
//		
//		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry" message:NO_RECORD_FOUND delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
//		[alert show];
//		[alert release];
//		
//		return ;		
//	}
	
	[self buildSubHeaderUI];	
	[self buildBodyUI];
	
	[self.tableView_ reloadData];
	
	self.view.hidden = NO;
}

- (IBAction)moveNextClicked:(id)sender
{
	[self getNextPrevDate:YES];
	
	[self sendRequest: YES];
}

- (IBAction)movePrevClicked:(id)sender
{
	[self getNextPrevDate:NO];
	
	[self sendRequest: NO];
}



//communication part

- (void)sendRequest:(BOOL)flag
{	
	if(self.today == nil)
		self.today = [NSDate date];

    //...limit
    _buttonNext.enabled = ![[self getDateFormat:self.today] isEqualToString:[self getDateFormat:[NSDate date]]];
	
	//reguestIndex = 0;
	[tools startLoading:self.navigationController.view childView:loadingView text:LOADING_TEXT];
	[self makeRequestProgress:flag];
}

- (NSString *)getDateFormat:(NSDate *)date {
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM-dd-yyyy"];

    return [dateFormat stringFromDate:date];
}

- (void)makeRequestProgress:(BOOL)flag{

	NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
//    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
//    [dateFormat setTimeZone:timeZone];
    [dateFormat setDateFormat:@"MM-dd-yyyy"];

	NSString *dateString = [dateFormat stringFromDate:self.today];

	LoginResponseData *loginResponseData = [LoginPerser getLoginResponse];
	request *r=[[request alloc] initWithTarget:self
								 SuccessAction:@selector(onSucceffulLogin:resData:)
								 FailureAction:@selector(onFailureLogin)];
	[r getCardioWeek:loginResponseData.memberID date:dateString];
			
}


- (void)getNextPrevDate:(BOOL)flag {
	if(self.today == nil)
		self.today = [NSDate date];

	NSCalendar *cal = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
	NSDateComponents *components = [cal components:( NSYearCalendarUnit | NSMonthCalendarUnit | NSWeekCalendarUnit | NSDayCalendarUnit ) fromDate:self.today];
	
	if(flag)
		[components setDay:([components day] + 7)];
	else
		[components setDay:([components day] - 7)];

	self.today = [cal dateFromComponents:components];//[theCalendar dateByAddingComponents:dayComponent toDate:self.today options:0];
}



- (void)onSucceffulLogin:(ResponseStatus *)responseStatus resData:(NSObject *)obj {
	if (responseStatus == nil || ![responseStatus isSuccess]) {
		[tools stopLoading:loadingView];
		
		[self clearBodyUI];
		
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry"
                                                        message:NO_RECORD_FOUND
                                                       delegate:self
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles: nil];
		[alert show];
		return ;
	}

	cardioWeekData = (CardioWeekData *)obj;
	[self buildUI];
	[tools stopLoading:loadingView];		
}

- (void)onFailureLogin {
	[tools stopLoading:loadingView];
	[self clearBodyUI];
}

@end
