//
//  MessagesViewController.h
//  Yaabbee
//
//  Created by Sarfaraj Biswas on 7/22/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "NutritionDayData.h"
#import "request.h"
#import "NuvitaAppDelegate.h"
#import "MyTools.h"
#import "LoginPerser.h"
#import "LoginResponseData.h"
#import "MealsDetailsViewController.h"

#import "JBCustomButton.h"

@interface MealesViewController : UIViewController<MealsDetailsDelegate> {
	MyTools *tools;
	UIView *loadingView;
	
	NutritionDayData *nutritionDayData;
	
	BOOL isLoaded;
	
	NSInteger requestType;
	
	int daysNextPrev;
	NSDate *today;
	
	UIButton *buttonPrev, *buttonNext;
}
@property (nonatomic, retain) IBOutlet UIButton *btnSave;
//@property (nonatomic, retain) IBOutlet UIButton *btnSave;

@property (nonatomic, retain)NSDate *today;

@property (nonatomic,retain)IBOutlet UILabel *subhedaerLb;
@property (nonatomic,retain)IBOutlet UILabel *subheaderLb2;

@property (nonatomic, retain)IBOutlet UITableView *messageTable;

- (IBAction)moveNextClicked:(id)sender;
- (IBAction)movePrevClicked:(id)sender;

- (IBAction)btnSaveClicked:(id)sender;  

//
- (void)sendRequest:(BOOL)flag;

- (void)makeRequestProgress:(BOOL)flag;


- (void)getNextPrevDate:(BOOL)flag;

- (void)clearBodyUI;

- (void)manipulateNextPrev;

- (void)buildUI;

- (void)setNextPrevBtnImage;
- (UIImage*)getDaysLeftImg:(int)weekDay;
- (UIImage*)getDaysRightImg:(int)weekDay;
- (void)saveNutritionDayData:(NSInteger)saveRequestType;

@end
