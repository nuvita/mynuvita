//
//  MessagesViewController.m
//  Yaabbee
//
//  Created by Sarfaraj Biswas on 7/22/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>
#import "MobilityWorkoutViewController.h"

#import "CustTableCell.h"
#import "constant.h"
#import "CustTableHeaderView.h"

#define TEXTFLD_OFFSETY 120

@interface MobilityWorkoutViewController ()

@property (assign)BOOL isKeyBoardVisible;

@end

@implementation MobilityWorkoutViewController

//@synthesize columnsTitle;
@synthesize tableView_, headerView, bodyView,dateString,isKeyBoardVisible,delegate;

@synthesize today;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }

    return self;
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];

	isLoaded = NO;
	tools = [[MyTools alloc]init];
	loadingView = [tools createActivityIndicator1];	
	
	daysNextPrev = 0;
	self.view.hidden = YES;
	isLoaded = NO;
	
    columnsTitle = [NSMutableArray arrayWithObjects:@"Date", @"Cals", @"Below", @"In zone", @"Above", @"Total", nil];
	[self.tableView_.layer setBorderWidth: 1.0];
	[self.tableView_.layer setBorderColor: [[UIColor grayColor] CGColor]];
	
	dictItem = [NSMutableDictionary dictionary];
	requestType = -1;
	
	UIBarButtonItem *editButton = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:self action:@selector(backBtnClicked)];
	[self.navigationItem setLeftBarButtonItem:editButton];
    [self updateUI];
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	if(!isLoaded)
		[self sendRequest:YES];
}

#pragma mark - UpdateUI

- (void)updateUI {
    if ([NuvitaAppDelegate isIOS7])
        self.edgesForExtendedLayout = UIRectEdgeNone;
}

- (void)backBtnClicked {
	[self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - section build ui

- (void)buildSubHeaderUI {
	if(self.headerView != nil) {
		[self.headerView removeFromSuperview];
	}
	
	self.headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 27)];
	self.headerView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"header2.png"]];
    
	UILabel* label = nil;
	label = [[UILabel	alloc] initWithFrame:CGRectMake(10, 0, self.headerView.frame.size.width/2 - 10, self.headerView.frame.size.height)];
	label.backgroundColor = [UIColor clearColor];
	label.font = [UIFont fontWithName:DEFAULT_BOLD_FONT_NAME size:defaultFontSize];
	label.text = mobilityXData.WeekLabel;//@"Week of 10/27";
	label.textAlignment = NSTextAlignmentLeft;
	label.textColor = [UIColor blackColor];
	[self.headerView addSubview:label];
	[self.view addSubview: self.headerView];
	self.isKeyBoardVisible =NO;
}


#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	if (mobilityXData && mobilityXData.dictMobilityXHRs) {
		NSMutableArray *arr = [mobilityXData.dictMobilityXHRs objectForKey:MOBILITY_XHRS];
		return [arr count];
	}
	return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	NSString *MyIdentifier = [NSString stringWithFormat:@"MyIdentifier %i", indexPath.row];
	UITableViewCell *cell = nil;
	if (cell == nil) {
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MyIdentifier];
	
		UILabel *label = nil;
		CustTableHeaderView *headerView1 = [[CustTableHeaderView alloc]initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 30)];
		[headerView1 numberOfCol:6 width:  tableView.frame.size.width];
		headerView1.backgroundColor = [UIColor clearColor];
		
		float gap = (tableView.frame.size.width/6 );
		float pos = 0;
		NSMutableArray *arr = [mobilityXData.dictMobilityXHRs objectForKey:MOBILITY_XHRS];
		NSDictionary *itemXHR = [arr objectAtIndex:indexPath.row];
		int size = 6;
		
		for(int ii = 0; ii < size; ii++) {
			pos = ii*gap + 1;				
			label = [[UILabel	alloc] initWithFrame:CGRectMake(pos, 0, gap - 1, 30 - 1)];
				
			switch (ii) {
				case 0:
					label.text = [LoginPerser getDate: [itemXHR objectForKey:MOBILITY_HRDATEID]];
					label.backgroundColor = (indexPath.row%2 == 0)? CARDIO_GRID_BODY_GRAY_COLOR : CARDIO_GRID_BODY_WHITE_COLOR;
					break;
				case 1:
					label.text = [itemXHR objectForKey:MOBILITY_CALORIES];
					label.backgroundColor = (indexPath.row%2 == 0)? CARDIO_GRID_BODY_GRAY_COLOR : CARDIO_GRID_BODY_WHITE_COLOR;
					break;
				case 5:	
					label.text = [itemXHR objectForKey:MOBILITY_TOTAL];
					label.backgroundColor = (indexPath.row%2 == 0)? CARDIO_GRID_BODY_GRAY_COLOR : CARDIO_GRID_BODY_WHITE_COLOR;
					break;
				case 2:
					label.text = [itemXHR objectForKey:MOBILITY_BELOW];
					label.backgroundColor = CARDIO_GRID_BODY_YELLOW_COLOR;
					break;
				case 3:
					label.text = [itemXHR objectForKey:MOBILITY_INZONE];
					label.backgroundColor = CARDIO_GRID_BODY_GREEN_COLOR;
					break;
				case 4:
					label.text = [itemXHR objectForKey:MOBILITY_ABOVE];
					label.backgroundColor = CARDIO_GRID_BODY_ORANGE_COLOR;
					break;
				default:
					break;
			}
            
			label.font = [UIFont fontWithName:DEFAULT_NORMAL_FONT_NAME size:tooSmallFontSize];
			label.lineBreakMode = YES;
			label.numberOfLines = 2; 
			label.textAlignment = NSTextAlignmentCenter;
			label.textColor = [UIColor blackColor];
			[headerView1 addSubview:label]; 
		}
		
		[cell.contentView addSubview:headerView1]; 
		
		
		//workout part
		UIView *gridView = [[UIView alloc]initWithFrame:CGRectMake(0, 30, tableView.frame.size.width, 30)];
		gridView.backgroundColor = [UIColor whiteColor];
		
		//frame
		CGRect frame = gridView.frame;
		UIImage* img =nil;
		UIButton *btn = nil;
		
		//spinner btn
		img=[UIImage imageNamed:@"box1.png"];
		
		frame.origin.x  = 4;
		frame.origin.y = 3;//(cell.contentView.frame.size.height - defaultFontSize)/2;
		frame.size.width = img.size.width;
		frame.size.height = img.size.height;
		
		btn =  [UIButton buttonWithType:UIButtonTypeCustom];
		[btn setBackgroundImage:img forState:UIControlStateNormal];
		[btn addTarget:self action:@selector(pickerBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
		[btn setFrame:frame];
		[btn setTitleColor:[UIColor blackColor] forState: UIControlStateNormal];
		btn.titleLabel.font = [UIFont fontWithName:DEFAULT_NORMAL_FONT_NAME size:defaultFontSize];
		
		NSString* workoutName = @"-- Select Workout --";
		if (![((NSString*)[itemXHR objectForKey:MOBILITY_WORKOUT_CODE] ) isEqualToString:@"NA"]) {
			workoutName = [itemXHR objectForKey:MOBILITY_WORKOUT_NAME];
		}
		[btn setTitle:workoutName forState:UIControlStateNormal];
		btn.tag=indexPath.row;
		[gridView addSubview:btn]; 
		
		//text field
		img=[UIImage imageNamed:@"box2.png"];
		
		frame.origin.x  = frame.origin.x + frame.size.width +4;
		frame.origin.y = 3;//(cell.contentView.frame.size.height - defaultFontSize)/2;
		frame.size.width = img.size.width;
		frame.size.height = img.size.height;
		
		UITextField *targetTxtFld = [[UITextField alloc] initWithFrame:frame];
		targetTxtFld.textColor = [UIColor blackColor];
		[targetTxtFld setBackground:img];
		targetTxtFld.font = [UIFont fontWithName:DEFAULT_NORMAL_FONT_NAME size:defaultFontSize];
		targetTxtFld.text=[itemXHR objectForKey:MOBILITY_WORKOUT_ROUNDS];
		targetTxtFld.keyboardType=UIKeyboardTypeNumberPad;
		targetTxtFld.delegate=self;
		targetTxtFld.tag=2;
		targetTxtFld.textAlignment = NSTextAlignmentCenter;
		targetTxtFld.borderStyle=UITextBorderStyleNone;
		targetTxtFld.tag=indexPath.row;
		[gridView addSubview:targetTxtFld];
		
		//save btn
		img=[UIImage imageNamed:@"wild_ones.png"];
		
		frame.origin.x  = frame.origin.x + frame.size.width +4;
		frame.origin.y = 3;//(cell.contentView.frame.size.height - defaultFontSize)/2;
		frame.size.width = 56;//img.size.width;
		frame.size.height = img.size.height;
		
		btn =  [UIButton buttonWithType:UIButtonTypeCustom];
		[btn setBackgroundImage:img forState:UIControlStateNormal];
		[btn addTarget:self action:@selector(saveBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
		[btn setFrame:frame];
		[btn setTitleColor:[UIColor blackColor] forState: UIControlStateNormal];
		[btn setTitle:@"Save" forState:UIControlStateNormal];
		btn.titleLabel.font = [UIFont fontWithName:DEFAULT_NORMAL_FONT_NAME size:defaultFontSize];
		btn.tag=indexPath.row;
		[gridView addSubview:btn]; 
		
		[cell.contentView addSubview:gridView];
	}
    
	cell.selectionStyle = UITableViewCellEditingStyleNone;
	return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
	return 27;
}


- (CGFloat)tableView:(UITableView *)messageTable heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
	CustTableHeaderView *headerView1 = [[CustTableHeaderView alloc]initWithFrame:CGRectMake(0, 0, 320, 27)];
	[headerView1 numberOfCol:6 width:  tableView.frame.size.width];
	headerView1.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"header2.png"]];
	
	UILabel *label = nil;
	
	float gap = (tableView.frame.size.width/6 );
	float pos = 0;
	for(int ii = 0; ii < 6; ii++) {
		pos = ii*gap + 1;				
		label = [[UILabel	alloc] initWithFrame:CGRectMake(pos, 0, gap - 1, 27)];

		label.backgroundColor = [UIColor clearColor];
		label.font = [UIFont fontWithName:DEFAULT_BOLD_FONT_NAME size:smallFontSize];
		label.text = [columnsTitle objectAtIndex:ii];// [NSString stringWithFormat:@"(%d, %d)", section, ii];
		label.textAlignment = NSTextAlignmentCenter;
		label.textColor = [UIColor whiteColor];
		[headerView1 addSubview:label]; 
		
	}
	return headerView1;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

- (IBAction)pickerBtnClicked:(id)sender {
	workoutBtn = (UIButton*)sender;
	[self showPickerView: workoutBtn.tag];
}

- (IBAction)saveBtnClicked:(id)sender {
	if (roundsTxtFld) {
		 [roundsTxtFld resignFirstResponder];
	}
    
	[self hideKeys];
	requestType=1;
	int currIndx=((UIButton*)sender).tag;
    
	LoginResponseData *loginResponseData = [LoginPerser getLoginResponse];
	NSMutableArray *arr1 = [mobilityXData.dictMobilityXHRs objectForKey:MOBILITY_XHRS];
	NSDictionary *itemXHR = [arr1 objectAtIndex:currIndx];

	[tools startLoading:self.navigationController.view childView:loadingView text:LOADING_TEXT];
	request *r=[[request alloc] initWithTarget:self
								 SuccessAction:@selector(onSucceffulLogin:resData:)
								 FailureAction:@selector(onFailureLogin)];
	
	[r saveMobilityXWeek:loginResponseData.memberID date:[itemXHR objectForKey:MOBILITY_HRDATEID] WorkoutId:[itemXHR objectForKey:MOBILITY_WORKOUT_ID] Rounds:[itemXHR objectForKey:MOBILITY_WORKOUT_ROUNDS]];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
	roundsTxtFld=textField;
	
	if(!self.isKeyBoardVisible){
		UIBarButtonItem *barBtnDone = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(hideKeyB:)];
		barBtnDone.title = @"Done";
		barBtnDone.tag=textField.tag;
		self.navigationItem.rightBarButtonItem = barBtnDone;
		
		self.isKeyBoardVisible = YES;
        
		CGRect tableFrame = self.view.frame;
		
		tableFrame.origin.y -= 60*roundsTxtFld.tag;
        
		[UIView beginAnimations:nil context:NULL];
		[UIView setAnimationDuration:0.3];
		[UIView setAnimationDidStopSelector:@selector(animationDidStop:finished:context:)];
		[UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
		[UIView setAnimationDelegate:self];
        
		self.view.frame = tableFrame;
        
		[UIView commitAnimations];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
	
    [textField resignFirstResponder];
   
	[self hideKeys];
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    UIBarButtonItem *barBtnDone = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(hideKeyB:)];
    barBtnDone.title = @"Done";
    barBtnDone.tag=1;
    self.navigationItem.rightBarButtonItem = barBtnDone;
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    
}

- (IBAction)hideKeyB:(id)sender {
    [roundsTxtFld resignFirstResponder];
	[self hideKeys];	
}

- (void)hideKeys {
	if(self.isKeyBoardVisible){
		
		self.isKeyBoardVisible = NO;
		CGRect tableFrame = self.view.frame;
		tableFrame.origin.y += 60*roundsTxtFld.tag;;
		
		[UIView beginAnimations:nil context:NULL];
		[UIView setAnimationDuration:0.3];
		[UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
		[UIView setAnimationDelegate:self];
		
		self.view.frame = tableFrame;
		
		[UIView commitAnimations];
		
		[self setRoundsVal];	
	}
	
    self.navigationItem.rightBarButtonItem = nil;
}

- (void)setRoundsVal {
	NSMutableArray *arr1 = [mobilityXData.dictMobilityXHRs objectForKey:MOBILITY_XHRS];
	NSDictionary *itemXHR = [arr1 objectAtIndex:roundsTxtFld.tag];
	
	[itemXHR setValue:roundsTxtFld.text forKey:MOBILITY_WORKOUT_ROUNDS];
}

#pragma mark - UIPickerViewDelegate

- (void)showPickerView:(NSInteger)sender {
    
    myPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 200, 320, 200)];
    [self.view addSubview:myPickerView];
    
    myPickerView.delegate = self;
    myPickerView.showsSelectionIndicator = YES;
    myPickerView.tag = sender;

    doneButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [doneButton addTarget:self 
                   action:@selector(pickerDoneBtClicked:)
         forControlEvents:UIControlEventTouchDown];
    doneButton.frame = CGRectMake(265.0, 4.0,  52.0, 30.0);
    [doneButton setImage:[UIImage imageNamed:@"done.png"] forState:UIControlStateNormal];
  
    [myPickerView addSubview:doneButton];
}

// tell the picker how many rows are available for a given component
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
	if (mobilityXData.dictMobilityWorkouts) {
		NSMutableArray* arr = [mobilityXData.dictMobilityWorkouts objectForKey:MOBILITY_WORKOUTS];
		return [arr count];
	}
	
    return 0;
}

// tell the picker how many components it will have
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}


- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {

	if (mobilityXData.dictMobilityWorkouts) {
        
		NSMutableArray* arr = [mobilityXData.dictMobilityWorkouts objectForKey:MOBILITY_WORKOUTS];
		NSDictionary* dict=[arr objectAtIndex:row];
		[workoutBtn setTitle:[dict objectForKey:MOBILITY_WORKOUT_NAME] forState:UIControlStateNormal];
		
		
		NSMutableArray *arr1 = [mobilityXData.dictMobilityXHRs objectForKey:MOBILITY_XHRS];
		NSDictionary *itemXHR = [arr1 objectAtIndex:workoutBtn.tag];
		[itemXHR setValue:[dict objectForKey:MOBILITY_WORKOUT_ID] forKey:MOBILITY_WORKOUT_ID];
	}
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
	if (mobilityXData.dictMobilityWorkouts) {
        
		NSMutableArray* arr = [mobilityXData.dictMobilityWorkouts objectForKey:MOBILITY_WORKOUTS];
		NSDictionary* dict=[arr objectAtIndex:row];
		NSString* wName = [dict objectForKey:MOBILITY_WORKOUT_NAME];
		
		if([wName isEqualToString:[workoutBtn titleForState:UIControlStateNormal]])
			[myPickerView selectRow:row inComponent:0 animated:YES]; 
			
		return [dict objectForKey:MOBILITY_WORKOUT_NAME];
	}
    
	return @"";
}


// tell the picker the width of each row for a given component
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    CGFloat componentWidth = 0.0;
	componentWidth = 135.0;	
	
	return componentWidth;
}


- (IBAction)pickerDoneBtClicked:(id)sender {
    [doneButton removeFromSuperview];
    [myPickerView removeFromSuperview];
    
    myPickerView = nil;	
}

- (void)clearBodyUI {
	[self manipulateNextPrev];
	self.view.hidden = YES;	
}


- (void)manipulateNextPrev {
	if([self.today compare:[NSDate date]] == NSOrderedSame || [self.today compare:[NSDate date]] == NSOrderedDescending ) {
		[buttonNext setEnabled:NO];
	} else {
		[buttonNext setEnabled:YES];
	}
}


- (void)buildUI {
	totalElement = 0;
	
	if(mobilityXData == nil || mobilityXData.dictMobilityXHRs == nil) return ;
	
	NSMutableArray *arr = [mobilityXData.dictMobilityXHRs objectForKey:MOBILITY_XHRS];
	
	totalElement = [arr count];
	
	if(totalElement == 0) {
        
		[self clearBodyUI ];
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry"
                                                        message:NO_RECORD_FOUND
                                                       delegate:self
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles: nil];
		[alert show];
		return ;		
	}
	
	[self buildSubHeaderUI];
	[self.tableView_ reloadData];
	self.view.hidden = NO;
}

#pragma mark - Web Service Delegate

- (void)sendRequest:(BOOL)flag {
	requestType=0;
	[tools startLoading:self.navigationController.view childView:loadingView text:LOADING_TEXT];
	
	[self makeRequestProgress:flag];
}

- (void)makeRequestProgress:(BOOL)flag {
	
	LoginResponseData *loginResponseData = [LoginPerser getLoginResponse];
	
	request *r=[[request alloc] initWithTarget:self
								 SuccessAction:@selector(onSucceffulLogin:resData:)
								 FailureAction:@selector(onFailureLogin)];
	[r getMobilityXWeek:loginResponseData.memberID date:self.dateString];
}


- (void)getNextPrevDate:(BOOL)flag {
	if(self.today == nil)
		self.today = [NSDate date];
	
	NSCalendar *cal = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
	NSDateComponents *components = [cal components:( NSYearCalendarUnit | NSMonthCalendarUnit | NSWeekCalendarUnit | NSDayCalendarUnit ) fromDate:self.today];
	
	if(flag)
		[components setDay:([components day] + 7)];
	else
		[components setDay:([components day] - 7)];
	
	self.today = [cal dateFromComponents:components];
}

- (void)onSucceffulLogin:(ResponseStatus *)responseStatus resData:(NSObject *)obj {
	[tools stopLoading:loadingView];
	
	if (requestType==0) {
		if (responseStatus == nil || ![responseStatus isSuccess]) {
			[tools stopLoading:loadingView];
			[self clearBodyUI];
			
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry"
                                                            message:@"No heart rate files found!"
                                                           delegate:self
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles: nil];
			[alert show];
			return ;
		}
		
		mobilityXData = (MobilityXData *)obj;
		[self buildUI];
	} else if(requestType==1) {
		if (responseStatus == nil || [responseStatus isSuccess]) {
			[tools stopLoading:loadingView];
			
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry"
                                                            message:responseStatus.errorText
                                                           delegate:self
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles: nil];
			[alert show];
			return ;
		}
	}
}

- (void)onFailureLogin {
	[tools stopLoading:loadingView];
	if (requestType==0) 
        [self clearBodyUI];
}

@end













