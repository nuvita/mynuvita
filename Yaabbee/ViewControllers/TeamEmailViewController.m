//
//  TeamEmailViewController.m
//  mynuvita
//
//  Created by John on 8/29/14.
//
//

#import "TeamEmailViewController.h"

@interface TeamEmailViewController () <MFMailComposeViewControllerDelegate>

@property (retain, nonatomic) IBOutlet UITableView *tableview;
@property (retain, nonatomic) NSMutableArray *leftTeamList;
@property (retain, nonatomic) NSMutableArray *rightTeamList;

@end

@implementation TeamEmailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _rightTeamList = [[NSMutableArray alloc] init];
    _leftTeamList = [[NSMutableArray alloc] init];
    NSLog(@"data: %@", _data);

//    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Continue"
                                                                              style:UIBarButtonItemStylePlain
                                                                             target:self
                                                                             action:@selector(didOpenEmail)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Public Methods

- (NSArray *)getRecipients {
    NSMutableArray *recipients = [[NSMutableArray alloc] init];

    for (NSDictionary *teams in [_data objectForKey:@"a:TeamMembers"]) {
        if (![teams objectForKey:@"a:Selected"]) {
            [recipients addObject:[teams objectForKey:@"a:EmailAddress"]];
        }
    }

    for (NSDictionary *teams in [_data objectForKey:@"a:OpponentTeamMembers"]) {
        if (![teams objectForKey:@"a:Selected"]) {
            [recipients addObject:[teams objectForKey:@"a:EmailAddress"]];
        }
    }

    return recipients;
}

- (void)didOpenEmail {
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    picker.mailComposeDelegate = self;
    Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
    if ([mailClass canSendMail]) {
        //..
        [picker setToRecipients:[self getRecipients]];

        [picker setSubject:@"Fitness League"];
        if (![_data objectForKey:@"a:IncludeScores"]) {
            [picker setMessageBody:[NSString stringWithFormat:@"The score for the week %@ are: <br/> %@: %@ <br/> %@: %@", [_data objectForKey:@"a:WeekLabel"], [_data objectForKey:@"a:TeamName"], [_data objectForKey:@"a:TeamScore"], [_data objectForKey:@"a:OpponentName"], [_data objectForKey:@"a:OpponentScore"]] isHTML:YES];
        }


        [self presentViewController:picker animated:YES completion:^{
            NSLog(@".....loaded mail view");
        }];

    } else {

        UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"Error"
                                                            message:@"Unable to load mail. Please set-up an email account in Settings > Mail,Contacts,Calendar."
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
        [alertview show];
    }

}

#pragma mark - MFMailComposeViewControllerDelegate

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    if (result == MFMailComposeResultSent)
        NSLog(@"Message has been sent");

    [self dismissViewControllerAnimated:YES completion:^{
        [self.navigationController popViewControllerAnimated:YES];
    }];
}

#pragma mark - UITableViewDataSource

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return [NSString stringWithFormat:@"Who do you want to include in your email? \n\n%@",[_data objectForKey:@"a:TeamName"]];
            break;

        case 1:
            return [NSString stringWithFormat:@"%@", [_data objectForKey:@"a:OpponentName"]];

        case 2:
            return @"Do you want to include current scores?";
            break;

        default:
            break;
    }

    return nil;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    if (section == 0)
        return [[_data objectForKey:@"a:TeamMembers"] count];
    else if (section == 1)
        return [[_data objectForKey:@"a:OpponentTeamMembers"] count];
    else
        return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    static NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];

    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }

    switch ([indexPath section]) {
        case 0:
            cell.imageView.image = [[[[_data objectForKey:@"a:TeamMembers"] objectAtIndex:indexPath.row] objectForKey:@"a:Selected"] boolValue] ? [UIImage imageNamed:@"uncheck-flat.png"] : [UIImage imageNamed:@"check-flat.png"];
            cell.textLabel.text = [[[_data objectForKey:@"a:TeamMembers"] objectAtIndex:indexPath.row] objectForKey:@"a:Name"];
            break;

        case 1:
            cell.imageView.image = [[[[_data objectForKey:@"a:OpponentTeamMembers"] objectAtIndex:indexPath.row] objectForKey:@"a:Selected"] boolValue] ? [UIImage imageNamed:@"uncheck-flat.png"] : [UIImage imageNamed:@"check-flat.png"];
            cell.textLabel.text = [[[_data objectForKey:@"a:OpponentTeamMembers"] objectAtIndex:indexPath.row] objectForKey:@"a:Name"];
            break;

        default:
            cell.imageView.image = [[_data objectForKey:@"a:IncludeScores"] boolValue] ? [UIImage imageNamed:@"uncheck-flat.png"] : [UIImage imageNamed:@"check-flat.png"];
            cell.textLabel.text = @"Current Scores";
            break;
            break;
    }

    return cell;
}

#pragma mark - UITableViewDelegate


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    BOOL selection;
    switch ([indexPath section]) {
        case 0:
            selection = [[[[_data objectForKey:@"a:TeamMembers"] objectAtIndex:indexPath.row] objectForKey:@"a:Selected"] boolValue];
            [[[_data objectForKey:@"a:TeamMembers"] objectAtIndex:indexPath.row] setObject:[NSNumber numberWithBool:!selection]
                                                                                  forKey:@"a:Selected"];
            break;

        case 1:
            selection = [[[[_data objectForKey:@"a:OpponentTeamMembers"] objectAtIndex:indexPath.row] objectForKey:@"a:Selected"] boolValue];
            [[[_data objectForKey:@"a:OpponentTeamMembers"] objectAtIndex:indexPath.row] setObject:[NSNumber numberWithBool:!selection]
                                                                                    forKey:@"a:Selected"];
            break;

        default:
            selection = [[_data objectForKey:@"a:IncludeScores"] boolValue];
            [_data setObject:[NSNumber numberWithBool:!selection] forKey:@"a:IncludeScores"];
            break;
    }

    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [_tableview performSelector:@selector(reloadData) withObject:nil afterDelay:0.3];
}

@end
