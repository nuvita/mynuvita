//
//  TeamEmailViewController.h
//  mynuvita
//
//  Created by John on 8/29/14.
//
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface TeamEmailViewController : UIViewController

@property (retain, nonatomic) NSMutableDictionary *data;

@end
