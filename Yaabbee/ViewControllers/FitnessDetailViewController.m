//
//  FitnessDetailViewController.m
//  mynuvita
//
//  Created by John on 8/6/14.
//
//

#import "FitnessDetailViewController.h"

@interface FitnessDetailViewController ()

@property (retain, nonatomic) IBOutlet UITableView *tableview;
@property (retain, nonatomic) UISegmentedControl *segmentedControl;
@property (retain, nonatomic) NSMutableArray *categoryInfoList;
@property (retain, nonatomic) IBOutlet UILabel *programLabel;
@property (retain, nonatomic) IBOutlet UILabel *weekLabel;

@property (retain, nonatomic) NSDate *dateSelected;

@property (retain, nonatomic) JBLoadingView *loadingView;
@property (retain, nonatomic) JBCustomButton *buttonPrev;
@property (retain, nonatomic) JBCustomButton *buttonNext;

@end

@implementation FitnessDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.tabBarItem.title = @"Fitness League";
        self.tabBarItem.image = [UIImage imageNamed:@"score-icon.png"];

        _categoryInfoList = [[NSMutableArray alloc] init];
    }

    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _loadingView = [[JBLoadingView alloc] init];
    _dateSelected = [NSDate date];
    _programLabel.text = [[LoginPerser getLoginResponse] programName];

    [self addCustomBarButtonArrow];
    [self didSelectOption:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Public Methods

- (void)addCustomBarButtonArrow {

    _buttonPrev=  [[JBCustomButton alloc] initWithFrame:CGRectMake(0, 20, 30, 30)];
    [_buttonPrev setButtonStyle:JBCustomButtonArrowLeftStyle];
	[_buttonPrev addTarget:self action:@selector(didTapPrevWeek) forControlEvents:UIControlEventTouchUpInside];

    _buttonNext =  [[JBCustomButton alloc] initWithFrame:CGRectMake(0, 20, 30, 30)];
    [_buttonNext setButtonStyle:JBCustomButtonArrowRightStyle];
	[_buttonNext addTarget:self action:@selector(didTapNextWeek) forControlEvents:UIControlEventTouchUpInside];

	self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:
                                               [[UIBarButtonItem alloc] initWithCustomView:_buttonNext],
                                               [[UIBarButtonItem alloc] initWithCustomView:_buttonPrev], nil];
}

- (IBAction)didSelectOption:(id)sender {
    [_categoryInfoList removeAllObjects];
    [self getNFLWeek];
}

- (void)loadNFLData:(NSDictionary *)info {

    if ([_segmentedControl selectedSegmentIndex] == 1) {
        if ([[info valueForKey:@"a:Standings"] isKindOfClass:[NSArray class]])
            [_categoryInfoList addObjectsFromArray:[info valueForKey:@"a:Standings"]];
    } else if ([_segmentedControl selectedSegmentIndex] == 2) {
        if ([[info valueForKey:@"a:Schedule"] isKindOfClass:[NSArray class]])
            [_categoryInfoList addObjectsFromArray:[info valueForKey:@"a:Schedule"]];
    } else {
        if ([[info objectForKey:@"a:NFLMatches"] isKindOfClass:[NSArray class]])
            [_categoryInfoList addObjectsFromArray:[info valueForKey:@"a:NFLMatches"]];
    }

    [_tableview reloadData];
}

- (NSString *)getDateFromDate:(NSDate *)date {

    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
//    [dateFormatter setTimeZone:timeZone];
    [dateFormatter setDateFormat:@"MM-dd-yyyy"];

    return [dateFormatter stringFromDate:date];
}

- (void)getWeekDateByAdding:(BOOL)add {

    NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
    dayComponent.day = (add) ? 7 : -7;

    NSCalendar *theCalendar = [NSCalendar currentCalendar];
    NSDate *nextDate = [theCalendar dateByAddingComponents:dayComponent
                                                    toDate:_dateSelected
                                                   options:0];
    _dateSelected = nextDate;
    [self didSelectOption:nil];
}

- (void)didTapPrevWeek {
    [self getWeekDateByAdding:NO];
}

- (void)didTapNextWeek {
    [self getWeekDateByAdding:YES];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_categoryInfoList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    FitnessTableViewCell *cell = (FitnessTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil) {
        NSArray *nibs = [[NSBundle mainBundle] loadNibNamed:@"FitnessTableViewCell" owner:self options:nil];
        cell = [nibs objectAtIndex:[_segmentedControl selectedSegmentIndex]];
    }

    //    cell.backgroundColor = indexPath.row % 2 == 0 ? [UIColor whiteColor] : [UIColor colorWithWhite:0.5 alpha:0.05];

    if ([_segmentedControl selectedSegmentIndex] == 0) {
        [cell loadCurrentScoreData:[_categoryInfoList objectAtIndex:indexPath.row]];
    }

    if ([_segmentedControl selectedSegmentIndex] == 1) {
        [cell loadStandingsData:[_categoryInfoList objectAtIndex:indexPath.row]];
    }

    if ([_segmentedControl selectedSegmentIndex] == 2) {
        [cell loadScheduleData:[_categoryInfoList objectAtIndex:indexPath.row]];
    }

    return cell;
}

#pragma mark - UITableViewDelegate

- (UIView *)loadStandingHeader {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 40, 320, 32)];
    view.backgroundColor = [UIColor colorWithWhite:0.5 alpha:0.05];

    UIFont *font = [UIFont systemFontOfSize:14.0];

    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, 175, 21)];
    label1.font = font;
    label1.text = @"Team";

    UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(182, 5, 45, 21)];
    label2.textAlignment = NSTextAlignmentCenter;
    label2.font = font;
    label2.text = @"W-L-T";

    UILabel *label3 = [[UILabel alloc] initWithFrame:CGRectMake(227, 5, 45, 21)];
    label3.textAlignment = NSTextAlignmentCenter;
    label3.font = font;
    label3.text = @"PCT";

    UILabel *label4 = [[UILabel alloc] initWithFrame:CGRectMake(272, 5, 45, 21)];
    label4.textAlignment = NSTextAlignmentCenter;
    label4.font = font;
    label4.text = @"AVG";

    [view addSubview:label1];
    [view addSubview:label2];
    [view addSubview:label3];
    [view addSubview:label4];

    return view;
}

- (UIView *)loadScheduleHeader {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 40, 320, 32)];
    view.backgroundColor = [UIColor colorWithWhite:0.5 alpha:0.05];

    UIFont *font = [UIFont systemFontOfSize:14.0];

    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, 175, 21)];
    label1.font = font;
    label1.backgroundColor = [UIColor clearColor];
    label1.text = @"Opponent";

    UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(182, 5, 90, 21)];
    label2.textAlignment = NSTextAlignmentCenter;
    label2.font = font;
    label2.backgroundColor = [UIColor clearColor];
    label2.text = @"Result";

    UILabel *label3 = [[UILabel alloc] initWithFrame:CGRectMake(272, 5, 45, 21)];
    label3.textAlignment = NSTextAlignmentCenter;
    label3.font = font;
    label3.backgroundColor = [UIColor clearColor];
    label3.text = @"Rec";

    [view addSubview:label1];
    [view addSubview:label2];
    [view addSubview:label3];

    return view;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        UIView *view = [[UIView alloc] initWithFrame:CGRectZero];
        view.backgroundColor = [UIColor whiteColor];

        NSArray *itemArray = [NSArray arrayWithObjects: @"Current Scores", @"Standings", @"Schedule", nil];
        UISegmentedControl *segmentedControl = [[UISegmentedControl alloc] initWithItems:itemArray];
        segmentedControl.backgroundColor = [UIColor whiteColor];
        segmentedControl.frame = CGRectMake(5, 5, 310, 30);
        segmentedControl.tintColor = [UIColor colorWithRed:0.2 green:0.4 blue:0.7 alpha:1.0];
        segmentedControl.segmentedControlStyle = UISegmentedControlStylePlain;
        segmentedControl.selectedSegmentIndex = [_segmentedControl selectedSegmentIndex];
        segmentedControl.enabled = YES;
        segmentedControl.momentary = NO;
        [segmentedControl addTarget:self
                             action:@selector(didSelectOption:)
                   forControlEvents:UIControlEventValueChanged];


        _segmentedControl = segmentedControl;
        [view addSubview:segmentedControl];

        if ([_segmentedControl selectedSegmentIndex] == 1) {
            UIView *header = [self loadStandingHeader];
            [view addSubview:header];
        }else if ([_segmentedControl selectedSegmentIndex] == 2) {
            UIView *header = [self loadScheduleHeader];
            [view addSubview:header];
        }

        return view;
    }

    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if ([_segmentedControl selectedSegmentIndex] == 0)
        return 40;
    return 72;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {

    NSArray *nibs = [[NSBundle mainBundle] loadNibNamed:@"FitnessTableViewCell" owner:self options:nil];
    FitnessTableViewCell *cell = [nibs objectAtIndex:[_segmentedControl selectedSegmentIndex]];
    return cell.contentView.frame.size.height;
}

#pragma mark - Web Service

- (void)showAlertMessage:(NSString *)message {
    [_categoryInfoList removeAllObjects];
    [_tableview reloadData];

    if ([message length] > 0) {
        UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"Error"
                                                            message:message
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
        [alertview show];
    }
}

- (void)getNFLWeek {
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];

    //...limit
    _buttonNext.enabled = ![[self getDateFormat:_dateSelected] isEqualToString:[self getDateFormat:[NSDate date]]];

    LoginResponseData *loginData = [LoginPerser getLoginResponse];
    NSDictionary *data = [NSDictionary dictionaryWithObjectsAndKeys:
                          [self getDateFromDate:_dateSelected], @"Date",
                          [loginData memberID], @"memberId",
                          nil];

    request *r = [[request alloc] initWithTarget:self
                                   SuccessAction:@selector(onSuccessfulFetchNFLWeek:resData:)
                                   FailureAction:@selector(onFailedFetch:)];
    [r getNFLWeek:data];
}

- (NSString *)getDateFormat:(NSDate *)date {
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM-dd-yyyy"];

    return [dateFormat stringFromDate:date];
}

- (void)getNFLCategoryType {
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    [_loadingView loadingViewStartAnimating:self.view
                            withLoadingView:[_loadingView createLoadingView:self.view]
                                       text:@"Loading data. Please wait..."];

    LoginResponseData *loginData = [LoginPerser getLoginResponse];
    NSDictionary *data = [NSDictionary dictionaryWithObjectsAndKeys:
                          [self getDateFromDate:_dateSelected], @"Date",
                          [loginData memberID], @"memberId",
                          nil];

    request *r = [[request alloc] initWithTarget:self
                                   SuccessAction:@selector(onSuccessfulFetchNFLCategory:resData:)
                                   FailureAction:@selector(onFailedFetch:)];
    r.isGenericRequest = YES;
    switch ([_segmentedControl selectedSegmentIndex]) {
        case 0:
            [r getNFLData:data withService:@"GetNFLWeek"];
            break;

        case 1:
            [r getNFLData:data withService:@"GetNFLStandings"];
            break;

        case 2:
            [r getNFLData:data withService:@"GetNFLSchedule"];
            break;

        default:
            break;
    }
}

- (void)onSuccessfulFetchNFLCategory:(ResponseStatus *)response resData:(NSObject *)obj {
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    if (response == nil && ![response isSuccess]) {
        [self showAlertMessage:@"No records found!"];
        return;
    }

    NSDictionary *info = [(NuvitaXMLParser *)obj info];
    [self loadNFLData:info];
    [_loadingView loadingViewStopAnimating:self.view];
}

- (void)onSuccessfulFetchNFLWeek:(ResponseStatus *)response resData:(NSObject *)obj {
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    
    if (response == nil && ![response isSuccess]) {
        [self showAlertMessage:@"No records found!"];
        return;
    }

    _weekLabel.text = [[(NuvitaXMLParser *)obj info] valueForKey:@"a:WeekLabel"];
    [self performSelector:@selector(getNFLCategoryType) withObject:nil afterDelay:0.3];
    [_loadingView loadingViewStopAnimating:self.view];
}

- (void)onFailedFetch:(NSString *)error {
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    if ([error isEqualToString:@"internal server error"]) {
        [self showAlertMessage:@"Ops! Internal server went wrong!"];
    }

    [_loadingView loadingViewStopAnimating:self.view];
}

@end