//
//  HomeViewController.h
//  Yaabbee
//
//  Created by Sarfaraj Biswas on 7/22/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "request.h"
#import "NuvitaAppDelegate.h"
#import "MyTools.h"
#import "LoginPerser.h"
#import "LoginResponseData.h"
#import "UIImageView+AFNetworking.h"

#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

#import "JBCustomButton.h"

@class MemberProgressData;
@class TeamProgressItem;
@class TeamProgressData;

@interface HomeViewController : UIViewController <MFMailComposeViewControllerDelegate> {
	MyTools *tools;
	UIView *loadingView;
	
	int reguestIndex;
	
	MemberProgressData *memberProgressData;	
	TeamProgressData *teamProgressData;
	
	BOOL isLoaded;
	
	int daysNextPrev;
	NSDate *today;
	
//	UIButton *buttonPrev, *buttonNext;
}

@property (nonatomic, retain)NSDate *today;

@property (nonatomic, retain)IBOutlet UIImageView *cardioProgBarUIImage, *memberUIView;
@property (nonatomic, retain)IBOutlet UIImageView *mobilityProgBarUIImage;
@property (nonatomic, retain)IBOutlet UIImageView *nutritionProgBarUIImage;

@property (nonatomic, retain)IBOutlet UIImageView *cardioProgCurveUIImage;
@property (nonatomic, retain)IBOutlet UIImageView *mobilityProgCurveUIImage;
@property (nonatomic, retain)IBOutlet UIImageView *nutritionProgCurveUIImage;

@property (nonatomic, retain)IBOutlet UILabel *cardioPercLb;
@property (nonatomic, retain)IBOutlet UILabel *mobilityPercLb;
@property (nonatomic, retain)IBOutlet UILabel *nutritionPercLb;

@property (nonatomic, retain)IBOutlet UILabel *cardioLb;
@property (nonatomic, retain)IBOutlet UILabel *mobilityLb;
@property (nonatomic, retain)IBOutlet UILabel *nutritionLb;

@property (nonatomic, retain)IBOutlet UILabel *teamProgressLb;

@property (nonatomic, retain)IBOutlet UILabel *progressWeekLb;
@property (nonatomic, retain)IBOutlet UILabel *programNameLb;


- (IBAction)moveNextClicked:(id)sender;
- (IBAction)movePrevClicked:(id)sender;
- (void)logoutClicked;

- (IBAction)memberBtnClicked:(id)sender;
- (void)memberIconClicked:(id)sender;
- (void)buildMemberProgressUI;

- (void)sendRequest:(BOOL)flag;

- (int)getIntVal:(NSString *)str;

- (int)getBarColorID:(NSString *)val;

//- (int)getBarWidth:(NSString *)val;
- (int)getBarWidth:(NSString *)val padding:(NSInteger)padding;

- (NSString *)getBarImgName:(NSString *)val flag:(BOOL)flag ;

- (void)makeRequestTeamProgress:(BOOL)flag;

- (void)makeRequestMemberProgress:(BOOL)flag;

- (void)getNextPrevDate:(BOOL)flag;

- (void)clearBodyUI;

- (void)manipulateNextPrev;

@end
