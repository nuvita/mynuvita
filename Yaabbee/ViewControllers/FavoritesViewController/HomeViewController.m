//
//  HomeViewController.m
//  Yaabbee
//
//  Created by Sarfaraj Biswas on 7/22/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "HomeViewController.h"
#import "NewLoginViewController.h"
#import <QuartzCore/QuartzCore.h>

#define AVATAR_PHOTO_W 40;
#define AVATAR_PHOTO_OFFSET 44;
#define AVATAR_PHOTO_H 40;

@interface HomeViewController ()

@property (retain, nonatomic) IBOutlet UIImageView *topUI;
@property (retain, nonatomic) IBOutlet UITableView *teamTableView;
@property (retain, nonatomic) IBOutlet UITableView *memberTableView;

@property (nonatomic, retain) NSMutableArray *teamList;
@property (nonatomic, retain) NSMutableArray *memberList;

@property (nonatomic, retain) JBCustomButton *buttonNext;
@property (nonatomic, retain) JBCustomButton *buttonPrev;

@end

@implementation HomeViewController

@synthesize memberUIView, cardioProgBarUIImage, mobilityProgBarUIImage, nutritionProgBarUIImage, cardioProgCurveUIImage, mobilityProgCurveUIImage, nutritionProgCurveUIImage;

@synthesize cardioPercLb, mobilityPercLb,  nutritionPercLb, teamProgressLb, progressWeekLb, programNameLb;

@synthesize today;

@synthesize cardioLb;
@synthesize mobilityLb;
@synthesize nutritionLb;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {

    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.tabBarItem.title = @"Home";
        self.tabBarItem.image = [UIImage imageNamed:@"home.png"];

        _teamList = [[NSMutableArray alloc] init];
        _memberList = [[NSMutableArray alloc] init];
    }

    return self;
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
}

#pragma mark - View lifecycle

- (void)viewDidLoad{
    [super viewDidLoad];
	
    isLoaded = NO;
	
    tools = [[MyTools alloc] init];
    loadingView = [tools createActivityIndicator1];
	

    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Logout"
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:self
                                                                            action:@selector(logoutClicked)];
    
	_buttonPrev =  [[JBCustomButton alloc] initWithFrame:CGRectMake(0, 20, 30, 30)];
    [_buttonPrev setButtonStyle:JBCustomButtonArrowLeftStyle];
	[_buttonPrev addTarget:self action:@selector(movePrevClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    _buttonNext =  [[JBCustomButton alloc] initWithFrame:CGRectMake(0, 20, 30, 30)];
    [_buttonNext setButtonStyle:JBCustomButtonArrowRightStyle];
	[_buttonNext addTarget:self action:@selector(moveNextClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:
                                               [[UIBarButtonItem alloc] initWithCustomView:_buttonNext],
                                               [[UIBarButtonItem alloc] initWithCustomView:_buttonPrev], nil];
    
	daysNextPrev = 0;
	self.view.hidden = YES;
	isLoaded= NO;
    
    _topUI.layer.borderColor = [[UIColor colorWithWhite:0.5 alpha:1.0] CGColor];
    _topUI.layer.borderWidth = 1.0f;

    [self buildUI];
}

#pragma mark - Build UI

- (void)buildUI {
    
    if (![NuvitaAppDelegate isIOS7]) {
        for (UIView *subview in [self.view subviews]) {
            if ([subview tag] != 1111) {
                CGRect frame = subview.frame;
                frame.origin.y -= 64;
                
                subview.frame = frame;
            }
        }
    }
}

- (int)getBarColorID:(NSString *)val
{
	int perc = [self getIntVal:val];
	
	if(perc < 70) return 0;
	
	else if(perc < 90) return 1;
	
	else
		return 2; 
}

- (int)getBarWidth:(NSString *)val padding:(NSInteger)padding
{
	int perc = [self getIntVal:val];
	
	UIImage *img = [UIImage imageNamed:@"progressbar_g.png"];
	
	int barWd = (img.size.width - 4 - padding)*perc/100;
	
	return barWd > (img.size.width - 4 - padding) ? (img.size.width - 4 - padding) : barWd;
}

- (NSString *)getBarImgName:(NSString *)val flag:(BOOL)flag
{
	int perc = [self getIntVal:val];
	NSString *progBarName = nil;
	NSString *progCurveName = nil;

	if(perc < 70)
	{
		progBarName = @"progressbar_b.png";
		progCurveName =  @"blackcurve.png";			
	}
	else if(perc > 69 && perc < 90)
	{
		progBarName = @"progressbar_o.png";
		progCurveName =  @"orangecurve.png";						
	}
	else if(perc >= 90)
	{
		progBarName = @"progressbar_g.png";
		progCurveName =  @"greencurve.png";						
	}
	
	if(flag) return progBarName;
	
	return progCurveName;
}

#pragma ---Member Progress 
#pragma mark
- (void)buildMemberProgressUI
{
	self.view.hidden = NO;
	UIImage *img = nil;
	UIImage *img1 = nil;
	UIButton *button = nil;
	
	NSString *labText = nil;
	NSString *percText = nil;
	NSString *teamName = nil;
	NSString *progBarName = nil;
	NSString *progCurveName = nil;
	
	//member information
	if(memberProgressData != nil)
	{
		int currIndx = -1;
		int progPerVal;
		NSString *itemTitle = @"";
		NSString *itemVal= @"";
		
		for(int ii= 0; ii < 3; ii++)
		{
			switch (ii) {
				case 0:
					progPerVal = [self getBarWidth:memberProgressData.CardioPercent padding:0];
					itemTitle = memberProgressData.CardioLabel ;//stringByAppendingString:@"%"];
					itemVal = [memberProgressData.CardioPercent stringByAppendingString:@"%"];
					
					progBarName = [self getBarImgName:memberProgressData.CardioPercent flag: YES];
					progCurveName =  [self getBarImgName:memberProgressData.CardioPercent flag: NO];		
					img = [UIImage imageNamed:progBarName];
					img1 = [UIImage imageNamed:progCurveName];

					if(itemTitle != nil && [itemTitle length] > 0)
						currIndx++;
					else {
						continue;
					}
					break;
				case 1:
					progPerVal =[self getBarWidth:memberProgressData.MobilityPercent padding:0];
					itemTitle = memberProgressData.MobilityLabel ;//stringByAppendingString:@"%"];
					itemVal = [memberProgressData.MobilityPercent stringByAppendingString:@"%"];
					
					progBarName = [self getBarImgName:memberProgressData.MobilityPercent flag: YES];
					progCurveName =  [self getBarImgName:memberProgressData.MobilityPercent flag: NO];		
					img = [UIImage imageNamed:progBarName];
					img1 = [UIImage imageNamed:progCurveName];
					
					if(itemTitle != nil && [itemTitle length] > 0)
						currIndx++;
					else {
						continue;
					}
					break;
				case 2:
					progPerVal =[self getBarWidth:memberProgressData.NutritionPercent padding:0];
					itemTitle = memberProgressData.NutritionLabel ;//stringByAppendingString:@"%"];
					itemVal = [memberProgressData.NutritionPercent stringByAppendingString:@"%"];
					
					progBarName = [self getBarImgName:memberProgressData.NutritionPercent flag: YES];
					progCurveName =  [self getBarImgName:memberProgressData.NutritionPercent flag: NO];		
					img = [UIImage imageNamed:progBarName];
					img1 = [UIImage imageNamed:progCurveName];

					if(itemTitle != nil && [itemTitle length] > 0)
						currIndx++;
					else {
						continue;
					}
					break;
				default:
					break;
			}//endswitch	
			
			switch (currIndx) {
				case 0:
					self.cardioPercLb.text = itemVal;
					self.cardioLb.text = itemTitle;

					[cardioProgBarUIImage setImage: img];
					[cardioProgCurveUIImage setImage: img1];
					
					[cardioProgCurveUIImage setFrame:CGRectMake(cardioProgCurveUIImage.frame.origin.x, cardioProgCurveUIImage.frame.origin.y,  progPerVal, cardioProgCurveUIImage.frame.size.height)];
					
					break;
				case 1:
					self.mobilityPercLb.text = itemVal;
					self.mobilityLb.text = itemTitle;
					
					[mobilityProgBarUIImage setImage: img];
					[mobilityProgCurveUIImage setImage: img1];
					[mobilityProgCurveUIImage setFrame:CGRectMake(mobilityProgCurveUIImage.frame.origin.x, mobilityProgCurveUIImage.frame.origin.y,  progPerVal ,mobilityProgCurveUIImage.frame.size.height)];
					break;
				case 2:
					self.nutritionPercLb.text =itemVal;
					self.nutritionLb.text = itemTitle;
                
					[nutritionProgBarUIImage setImage: img];
					[nutritionProgCurveUIImage setImage: img1];
					[nutritionProgCurveUIImage setFrame:CGRectMake(nutritionProgCurveUIImage.frame.origin.x, nutritionProgCurveUIImage.frame.origin.y,  progPerVal, nutritionProgCurveUIImage.frame.size.height)];
					break;
				default:
					break;
			}//endswitch
		}
		
		//for hidden
		
		switch (currIndx) {
			case -1:
				self.cardioPercLb.hidden = YES;
				self.cardioLb.hidden = YES;
				
				cardioProgBarUIImage.hidden = YES;
				cardioProgCurveUIImage.hidden = YES;
				//break;
			case 0:
				self.mobilityPercLb.hidden = YES;
				self.mobilityLb.hidden = YES;
				
				
				mobilityProgBarUIImage.hidden = YES;
				mobilityProgCurveUIImage.hidden = YES;
				
				//break;
			case 1:
				self.nutritionPercLb.hidden = YES;
				self.nutritionLb.hidden = YES;
				
				nutritionProgBarUIImage.hidden = YES;
				nutritionProgCurveUIImage.hidden = YES;
				
				break;
			default:
				break;
		}//endswitch
		
		//other
		LoginResponseData *loginResponseData = [LoginPerser getLoginResponse];
		
		self.progressWeekLb.text =  loginResponseData.programName;
		self.programNameLb.text = memberProgressData.WeekLabel;
		self.teamProgressLb.text = [[loginResponseData.firstName stringByAppendingString:@" "]stringByAppendingString:loginResponseData.lastName];
	}
    
    
	//teamProgressData
	//team information
	if(self.memberUIView !=nil)
	{
		[[self.memberUIView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
	}
	
	float frameW = self.memberUIView.frame.size.width - 20;
	float frameH = self.memberUIView.frame.size.height - 20;
	float xx = 10;
	float yy = 10;
    float photoIconWH = 40;
	float lrPadding = 4;
    
	 UIScrollView* containerView = [[UIScrollView alloc] initWithFrame:CGRectMake(xx, yy, frameW, frameH)];
	
	 containerView.backgroundColor = [UIColor clearColor];	 
	 containerView.scrollEnabled = YES;
	 containerView.pagingEnabled = YES;
	 containerView.bounces = NO;
	 //containerView.directionalLockEnabled = YES;
	 
	if(teamProgressData == nil || teamProgressData.TeamMembers == nil) return ;
	
	NSInteger size = [teamProgressData.TeamMembers count];

    //
    int showInPage = 2;//3 for iphone 3.5, 4 for iphone 4 inch
    if([NuvitaAppDelegate isPhone5])
        showInPage = 4;
    
    
	containerView.contentSize = CGSizeMake(0, size*frameH/showInPage);
	TeamProgressItem *teamProgressItem = nil;
	
	for (int ii = 0; ii < size; ii++) {
		
		teamProgressItem = (TeamProgressItem *)[teamProgressData.TeamMembers objectAtIndex:ii];
		
		//data
        NSString* avatarLink = teamProgressItem.Avatar;
		labText = teamProgressItem.FirstName;//@"Ran ";
		percText = [teamProgressItem.ProgressPercent stringByAppendingString:@"%"];
		teamName = teamProgressData.TeamName;
		
		progBarName = [self getBarImgName:teamProgressItem.ProgressPercent flag: YES];
		progCurveName =  [self getBarImgName:teamProgressItem.ProgressPercent flag: NO];		
		
		int progWidth = [self getBarWidth:teamProgressItem.ProgressPercent padding:photoIconWH + lrPadding];//];
		
		UIView *uiView = [[UIView alloc] initWithFrame:CGRectMake(0, ii *frameH/showInPage, frameW, frameH/showInPage)];
		
		if(ii == 0)
		{
			//Wild Btn	
			img = [UIImage imageNamed:@"gray-btn.png"];
			xx = frameW - img.size.width;
			yy = 0;
			
			button =  [UIButton buttonWithType:UIButtonTypeCustom];
//			[button setBackgroundImage:img forState:UIControlStateNormal];
//            [[button layer] setBorderColor:[UIColor grayColor].CGColor];
//            [[button layer] setBorderWidth:1.0f];
			[button addTarget:self action:@selector(memberBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
			button.tag = -1;
			[button setTitle:teamName forState:UIControlStateNormal];
			[button setTitleColor:[UIColor colorWithRed:0.2 green:0.4 blue:0.7 alpha:1.0] forState:UIControlStateNormal];
            [button titleLabel].adjustsFontSizeToFitWidth = YES;
			button.titleLabel.font = [UIFont fontWithName:DEFAULT_BOLD_FONT_NAME size:defaultFontSize];
			[button setFrame:CGRectMake(xx, yy, img.size.width, img.size.height)];
			button.titleEdgeInsets = UIEdgeInsetsMake(0, 6, 0, 6);
			
			[uiView addSubview:button];			
		}

        //avatar
        xx = 0;
        yy = (frameH/showInPage - 40)/2;
        
        UIImageView *iconImgView = [[UIImageView alloc]initWithFrame:CGRectMake(xx, yy, 40, 40)];
        [iconImgView setImage:[UIImage imageNamed:@"avator.png"]];
//		[iconImgView addTarget:self action:@selector(memberIconClicked:) forControlEvents:UIControlEventTouchUpInside];
        iconImgView.tag = ii;
        [iconImgView.layer setBorderWidth:1.0];
        [iconImgView.layer setBorderColor:[[UIColor darkGrayColor] CGColor]];

        [uiView addSubview:iconImgView];
        
        if (avatarLink) {
                 //replace https with http. oh dear.
            [iconImgView setImageWithURL:[NSURL URLWithString:avatarLink] 
                           placeholderImage:[UIImage imageNamed:@"avator.png"]]; 
        } 
        button =  [UIButton buttonWithType:UIButtonTypeCustom];
		[button addTarget:self action:@selector(memberBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
		button.tag = ii;
        [button setFrame:CGRectMake(xx, yy, 40, 40)];
        [uiView addSubview:button];

		//label 
		//img = [UIImage imageNamed:@"fran1_d.png"];
		xx = photoIconWH + lrPadding;
		yy = 4;
	
        UILabel *lb = [[UILabel alloc]initWithFrame:CGRectMake(xx, yy, img.size.width, img.size.height)];
		lb.text = labText;// forState:UIControlStateNormal];
		lb.backgroundColor = [UIColor clearColor];
		lb.textColor = [UIColor blackColor] ;
		lb.font = [UIFont fontWithName:DEFAULT_BOLD_FONT_NAME size:smallFontSize];
		lb.textAlignment = NSTextAlignmentLeft;
        
		[uiView addSubview: lb];
		//progress bar
		//xx = 0;
		yy = yy + img.size.height + 4;
		img = [UIImage imageNamed:progBarName];
		
		UIImageView *uiImgView1 = [[UIImageView alloc] initWithFrame:CGRectMake(xx, yy, img.size.width - photoIconWH - lrPadding, img.size.height)];		
		uiImgView1.image = img;		
		[uiView addSubview: uiImgView1];
		
		//xx = 0;
		//yy += smallFontSize;  
		img1 = [UIImage imageNamed:progCurveName ];
		uiImgView1 = [[UIImageView alloc] initWithFrame:CGRectMake(xx + 2, yy + (img.size.height - img1.size.height)/2, progWidth, img1.size.height) ];		
		uiImgView1.image = img1;		
		[uiView addSubview: uiImgView1];
		
		//text view
		xx = img.size.width ;
		//yy = yy + img.size.height;
		
		lb = [[UILabel alloc]initWithFrame:CGRectMake(xx, yy, frameW - img.size.width, img.size.height)];
		lb.text = percText;// forState:UIControlStateNormal];
		lb.backgroundColor = [UIColor clearColor];
		lb.textColor = [UIColor blackColor] ;
		lb.font = [UIFont fontWithName:DEFAULT_BOLD_FONT_NAME size:smallFontSize];
		lb.textAlignment = NSTextAlignmentRight;
		[uiView addSubview: lb];
		[containerView addSubview:uiView];
		
		teamProgressItem = nil;
	}
	
 	[self.memberUIView addSubview: containerView];
	
	isLoaded = YES;
}

- (void)clearBodyUI {
	[self manipulateNextPrev];
	
	if(self.memberUIView !=nil) {
		[[self.memberUIView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
	}
    
	self.view.hidden = YES;
}

- (void)manipulateNextPrev {
}

- (int)getIntVal:(NSString *)str {
	@try{
		return [str intValue];
	}
	@catch (NSException  *ex) {
		
	}
	
	return 0;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	self.today = [NSDate date];
	//if(!isLoaded)
	[self sendRequest:YES];
	[self manipulateNextPrev];

	NSLog(@"viewWillAppear...");
}

- (void)logoutClicked {
	daysNextPrev = 0;
	self.view.hidden = YES;
		
    isLoaded = NO;
	self.today = nil;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"LOGOUT" object:nil];
}

- (IBAction)memberBtnClicked:(id)sender {
	UIButton *btn = (UIButton*)sender;
	
	if(btn.tag < 0) {
		[self sendEmails:[teamProgressData getTeamsEmail] emailSub:@"" emailBody:@""];
		return ;
	}
			
	TeamProgressItem *teamProgressItem = (TeamProgressItem *)[teamProgressData.TeamMembers objectAtIndex:btn.tag];
	[self sendEmail:teamProgressItem.EmailAddress emailSub:@"" emailBody:@""];
}

- (void)memberIconClicked:(id)sender {
	UIImageView *btn = (UIImageView*)sender;
	
	if(btn.tag < 0) {
		[self sendEmails:[teamProgressData getTeamsEmail] emailSub:@"" emailBody:@""];
		return ;
	}
    
	TeamProgressItem *teamProgressItem = (TeamProgressItem *)[teamProgressData.TeamMembers objectAtIndex:btn.tag];
	[self sendEmail:teamProgressItem.EmailAddress emailSub:@"" emailBody:@""];
}

- (IBAction)moveNextClicked:(id)sender
{
	[self getNextPrevDate:YES];
	[self sendRequest: YES];
}

- (IBAction)movePrevClicked:(id)sender
{
	[self getNextPrevDate:NO];
	[self sendRequest: NO];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Web Service Call/Delegate

- (void)sendRequest:(BOOL)flag {
	if(self.today == nil)
		self.today = [NSDate date];

    //...limit
    _buttonNext.enabled = ![[self getDateFormat:self.today] isEqualToString:[self getDateFormat:[NSDate date]]];

	reguestIndex = 0;
	[tools startLoading:self.navigationController.view childView:loadingView text:LOADING_TEXT];
	
	[self makeRequestTeamProgress:flag];
}

- (NSString *)getDateFormat:(NSDate *)date {
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM-dd-yyyy"];

    return [dateFormat stringFromDate:date];
}


- (void)makeRequestTeamProgress:(BOOL)flag {
	reguestIndex = 0;
	//NSCalendar
	//
	NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
	[dateFormat setDateFormat:@"MM-dd-yyyy"];
	
	NSString *dateString = [dateFormat stringFromDate:self.today];
	
	NSLog(@"Formated Date...%@", dateString);
	
	//NSString *dateString = [self getNextPrevDate:flag];
	LoginResponseData *loginResponseData = [LoginPerser getLoginResponse];
	
	request *r=[[request alloc] initWithTarget:self
								 SuccessAction:@selector(onSuccessfulLogin:resData:)
								 FailureAction:@selector(onFailureLogin)];
	[r getTeamProgress:loginResponseData.memberID date:dateString];
}


- (void)makeRequestMemberProgress:(BOOL)flag {
	reguestIndex = 1;
	//NSCalendar	
	//
	NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
	[dateFormat setDateFormat:@"MM-dd-yyyy"];
	
	NSString *dateString = [dateFormat stringFromDate:self.today];
	
	NSLog(@"Formated Date...%@", dateString);
		
	LoginResponseData *loginResponseData = [LoginPerser getLoginResponse];
	
	request *r=[[request alloc] initWithTarget:self
								 SuccessAction:@selector(onSuccessfulLogin:resData:)
								 FailureAction:@selector(onFailureLogin)];
	[r getMemberProgress:loginResponseData.memberID date:dateString];
}

- (void)getNextPrevDate:(BOOL)flag {
	if(self.today == nil)
	   self.today = [NSDate date];
		
	//
	NSCalendar *cal = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
	
       // required components for today
       NSDateComponents *components = [cal components:( NSYearCalendarUnit | NSMonthCalendarUnit | NSWeekCalendarUnit | NSDayCalendarUnit ) fromDate:self.today];
	
	if(flag)
	     [components setDay:([components day] + 7)];
	//NSDate *lastWeek  = [cal dateFromComponents:components];
	else
	      [components setDay:([components day] - 7)];
	
	//NSCalendar *theCalendar = [NSCalendar currentCalendar];
	
	self.today = [cal dateFromComponents:components];//[theCalendar dateByAddingComponents:dayComponent toDate:self.today options:0];
}

- (void)onSuccessfulLogin:(ResponseStatus *)responseStatus resData:(NSObject *)obj {
	if (responseStatus == nil || ![responseStatus isSuccess]) {
		[tools stopLoading:loadingView];
		
		[self clearBodyUI];
		
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry"
                                                        message:NO_RECORD_FOUND
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
		[alert show];
		
		return ;
	}
    
	if (reguestIndex == 0) {
		teamProgressData = (TeamProgressData *)obj;
//        [self loadTeamProgressData];
		[self makeRequestMemberProgress:YES];
		return;
	} else {
		memberProgressData = (MemberProgressData *)obj;
		[self buildMemberProgressUI];
//        [self loadMemberProgressData];
	}
    
	[tools stopLoading:loadingView];		
}

//- (void)loadTeamProgressData {
//    [_teamList removeAllObjects];
//    [_teamList addObjectsFromArray:[teamProgressData TeamMembers]];
//
//    [_teamTableView reloadData];
//}
//
//- (void)loadMemberProgressData {
//    [_memberList removeAllObjects];
//    [_memberList addObjectsFromArray:[NSArray arrayWithObjects:
//                                    [NSDictionary dictionaryWithObjectsAndKeys:
//                                     memberProgressData.CardioLabel, @"label",
//                                     memberProgressData.CardioPercent, @"percent",
//                                     nil],
//                                    [NSDictionary dictionaryWithObjectsAndKeys:
//                                     memberProgressData.MobilityLabel, @"label",
//                                     memberProgressData.MobilityPercent, @"percent",
//                                     nil],
//                                    [NSDictionary dictionaryWithObjectsAndKeys:
//                                     memberProgressData.NutritionLabel, @"label",
//                                     memberProgressData.NutritionPercent, @"percent",
//                                     nil],
//                                    nil]];
//
//    [_memberTableView reloadData];
//}

- (void)onFailureLogin {
	[tools stopLoading:loadingView];
	[self clearBodyUI];
}

#pragma mark - Email Content

- (void)sendSupportEmail {
	[self sendEmail:@"support@appsea.net" emailSub:@"MobiSuit - Support" emailBody:@""];
}

- (void) referToFriend {
	NSString *emailBody = @"I'm using the MobiSuit for the Mobile Application, and so can you. Get access to sync, upload photos  etc.. on the go!\nRegister on to www.mobisuit.com and download on your IPhone to download the app now.";
	[self sendEmail:@"" emailSub:@"MobiSuit - " emailBody:emailBody];
}

- (void)sendEmails:(NSMutableArray*)toEmails emailSub:(NSString*)eSub emailBody:(NSString*)ebody {
    
	MFMailComposeViewController * controller = [[MFMailComposeViewController alloc] init];
	controller.mailComposeDelegate = self;
	
	Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
    if ([mailClass canSendMail]) {
        
        [controller setToRecipients:toEmails];
        [controller setSubject:eSub];
        [controller setMessageBody:ebody isHTML:YES];
        
        [self presentViewController:controller animated:YES completion:^{
        
        }];
    }
}

- (void)sendEmail:(NSString*)toEmail emailSub:(NSString*)eSub emailBody:(NSString*)ebody {
    
	MFMailComposeViewController * controller = [[MFMailComposeViewController alloc] init];
	controller.mailComposeDelegate = self;
	
	NSMutableArray *recipientArray = [[NSMutableArray alloc] init];
	[recipientArray addObject:toEmail];
	
    Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
	if ([mailClass canSendMail]) {
        
        [controller setToRecipients:recipientArray];
        [controller setSubject:eSub];
        [controller setMessageBody:ebody isHTML:YES];
        
        [self presentViewController:controller animated:YES completion:^{
            
        }];
    }
}

- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error {
	if (result == MFMailComposeResultSent) {
		NSLog(@"Message Sent");
	}

    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

//#pragma mark - UITableViewDataSource
//
//- (UIColor *)getProgressColorValue:(CGFloat)percent {
//    if(percent > 0 && percent < 70)
//        return [UIColor colorWithRed:255.0/255.0 green:182.0/255.0 blue:124.0/255.0 alpha:1];
//    else if(percent > 69 && percent < 90)
//        return [UIColor colorWithRed:217.0/255.0 green:123.0/255.0 blue:112.0/255.0 alpha:1];
//    else if(percent >= 90)
//        return [UIColor colorWithRed:184.0/255.0 green:205.0/255.0 blue:116.0/255.0 alpha:1];
//
//    return [UIColor colorWithWhite:0.8 alpha:0.8];
//}
//
//
//
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    return tableView == _memberTableView ? [_memberList count] : [_teamList count];
//}
//
//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//    static NSString *cellIdentifier = @"Cell";
//    UITableViewCell *cell = [tableView ]
//
//    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
//
//    if (tableView == _memberTableView) {
//
//        CGRect frame = tableView.frame;
//
//        NSDictionary *data = [_memberList objectAtIndex:indexPath.row];
//        CGFloat percent = [[data objectForKey:@"percent"] floatValue];
//        CGFloat percentProgress = (frame.size.width - 60) * (percent / 100);
//
//        cell.textLabel.font = [UIFont fontWithName:DEFAULT_BOLD_FONT_NAME size:smallFontSize];
//        cell.textLabel.text = [data objectForKey:@"label"];
//        cell.detailTextLabel.text = @"  ";
//
//        UIColor *progressColor = [self getProgressColorValue:percent];
//        UIView *baseProgressView = [[UIView alloc] initWithFrame:CGRectMake(15, 25, frame.size.width - 60, 14)];
//        baseProgressView.backgroundColor = [UIColor clearColor];
//        baseProgressView.layer.borderColor = [progressColor CGColor];
//        baseProgressView.layer.borderWidth = 1.0f;
//        baseProgressView.layer.cornerRadius = 2.0f;
//        baseProgressView.clipsToBounds = YES;
//
//        UIView *valueProgressView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, percentProgress, 14)];
//        valueProgressView.backgroundColor = [progressColor colorWithAlphaComponent:0.8];
//
//        [baseProgressView addSubview:valueProgressView];
//        [cell.contentView addSubview:baseProgressView];
//
//        UIView *container = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
//        container.backgroundColor = [UIColor clearColor];
//
//        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 25, 44, 14)];
//        label.backgroundColor = [UIColor clearColor];
//        label.font = [UIFont fontWithName:DEFAULT_BOLD_FONT_NAME size:14.0];
//        label.textAlignment = NSTextAlignmentRight;
//        label.text = [[data objectForKey:@"percent"] stringByAppendingString:@"%"];
//
//        [container addSubview:label];
//        cell.accessoryView = container;
//
//    } else if (tableView == _teamTableView) {
//
//        TeamProgressItem *item = [_teamList objectAtIndex:indexPath.row];
//
//        CGRect frame = tableView.frame;
//        CGFloat percent = 90;//[item.ProgressPercent floatValue];
//        CGFloat percentProgress = (frame.size.width - 104) * (percent / 100);
//
//        cell.textLabel.font = [UIFont fontWithName:DEFAULT_BOLD_FONT_NAME size:smallFontSize];
//        cell.textLabel.text = [item FirstName];
//        cell.detailTextLabel.text = @"  ";
//
//        UIColor *progressColor = [self getProgressColorValue:percent];
//        UIView *baseProgressView = [[UIView alloc] initWithFrame:CGRectMake(75, 25, frame.size.width - 120, 14)];
//        baseProgressView.backgroundColor = [UIColor clearColor];
//        baseProgressView.layer.borderColor = [progressColor CGColor];
//        baseProgressView.layer.borderWidth = 1.0f;
//        baseProgressView.layer.cornerRadius = 2.0f;
//        baseProgressView.clipsToBounds = YES;
//
//        UIView *valueProgressView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, percentProgress, 14)];
//        valueProgressView.backgroundColor = [progressColor colorWithAlphaComponent:0.8];
//
//        [baseProgressView addSubview:valueProgressView];
//        [cell.contentView addSubview:baseProgressView];
//
//        UIView *container = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
//        container.backgroundColor = [UIColor clearColor];
//
//        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 25, 44, 14)];
//        label.backgroundColor = [UIColor clearColor];
//        label.font = [UIFont fontWithName:DEFAULT_BOLD_FONT_NAME size:14.0];
//        label.textAlignment = NSTextAlignmentRight;
//        label.text = [[item ProgressPercent] stringByAppendingString:@"%"];
//
//        [container addSubview:label];
//        cell.accessoryView = container;
//
//        cell.imageView.image = [UIImage imageNamed:@"avator"];
//
//        [self performSelector:@selector(initImage:)
//                   withObject:[NSDictionary dictionaryWithObjectsAndKeys:
//                               [item Avatar], @"icon",
//                               cell.imageView, @"outlet",
//                               nil]];
//    }
//
//    return cell;
//}
//
//- (void)initImage:(NSDictionary *)data {
//
//    UIImageView *imageview = [data valueForKey:@"outlet"];
//    imageview.contentMode = UIViewContentModeScaleAspectFit;
//    imageview.backgroundColor = [UIColor colorWithWhite:0.5 alpha:0.05];
//
//    imageview.layer.cornerRadius = imageview.frame.size.height / 2;
//    imageview.layer.borderWidth = 1.0;
//    imageview.layer.borderColor = [UIColor colorWithRed:0.2 green:0.4 blue:0.6 alpha:1.0].CGColor;
//    imageview.clipsToBounds = YES;
//
//    UIActivityIndicatorView	*progressIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(imageview.frame.size.width/2 - 10, imageview.frame.size.height/2 - 10, 40, 40)];
//    progressIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
//    progressIndicator.autoresizingMask = (UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin);
//    [progressIndicator sizeToFit];
//
//    [imageview addSubview:progressIndicator];
//    [progressIndicator startAnimating];
//
//    NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:
//                                       imageview, @"outlet",
//                                       progressIndicator, @"indicator",
//                                       [data valueForKey:@"icon"], @"icon",
//                                       nil];
//
//    [self performSelector:@selector(loadImage:)
//               withObject:dictionary];
//}
//
//- (void)loadImage:(NSDictionary *)data {
//
//    NSString *urlString = [data valueForKey:@"icon"];
//    NSURL *url = [[NSURL alloc] initWithString:urlString];
//    NSData *myData = [[NSData alloc] initWithContentsOfURL:url];
//
//    if ([myData length] > 0) {
//        UIImage *image = [[UIImage alloc] initWithData:[[NSData alloc] initWithContentsOfURL:url]];
//        if (image) {
//            UIImageView *imageview = [data valueForKey:@"outlet"];
//            [imageview setImage:image];
//        }
//    }
//
//    UIActivityIndicatorView	*progressIndicator = [data valueForKey:@"indicator"];
//    [progressIndicator stopAnimating];
//}
//
//#pragma mark - UITableViewDelegate
//
//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
//    return 20.0;
//}
//
//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
//    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
//    label.textAlignment = NSTextAlignmentRight;
//    label.backgroundColor = [UIColor clearColor];
//    label.font = [UIFont fontWithName:DEFAULT_BOLD_FONT_NAME size:smallFontSize];
//
//    label.text = [NSString stringWithFormat:@"%@ %@", [[[LoginPerser getLoginResponse] firstName] capitalizedString], [[[LoginPerser getLoginResponse] lastName] capitalizedString]];
//    return label;
//}
//
////- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
@end
