//
//  claimActivityOBJ.h
//  Acclaris
//
//  Created by Sarfaraj Biswas on 09/11/10.
//  Copyright 2010 ObjectSol Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "ResponseStatus.h"

@interface MobilityXData : NSObject<NSXMLParserDelegate> {
		
	NSMutableArray *itemsArray;
	NSMutableDictionary *dictItem;
	
	NSMutableString *contentOfString;
	
	ResponseStatus *responseStatus;
}


@property(nonatomic,retain)NSMutableDictionary *dictMobilityWorkouts, *dictMobilityXHRs;

@property(nonatomic,retain)NSString *WeekLabel, *MemberId, *Date;

//
@property(nonatomic,retain)ResponseStatus *responseStatus;

- (void)parseXMLFileAtData:(NSMutableData *)data parseError:(NSError **)error;

- (BOOL)getBoolVal:(NSString*)val;

- (NSString*)getMobilityXml:(NSString*)date;

//- (NSString*)getTotalBelow;
//- (NSString*)getTotalInZone;
//- (NSString*)getTotalAbove;
//- (NSString*)getTotalVal;

@end

/*
 

 
 <a:MobilityWorkouts>
 <a:MobilityWorkout>
 <a:WorkoutCode>NA</a:WorkoutCode>
 <a:WorkoutId>0</a:WorkoutId>
 <a:WorkoutName>-- Select Workout --</a:WorkoutName>
 </a:MobilityWorkout>
 <a:MobilityWorkout>
 <a:WorkoutCode>W1</a:WorkoutCode>
 <a:WorkoutId>30</a:WorkoutId>
 <a:WorkoutName>Workout 1</a:WorkoutName>
 </a:MobilityWorkout>
 <a:MobilityWorkout>
 <a:WorkoutCode>W2</a:WorkoutCode>
 <a:WorkoutId>31</a:WorkoutId>
 <a:WorkoutName>Workout 2</a:WorkoutName>
 </a:MobilityWorkout>
 <a:MobilityWorkout>
 <a:WorkoutCode>W3</a:WorkoutCode>
 <a:WorkoutId>32</a:WorkoutId>
 <a:WorkoutName>Workout 3</a:WorkoutName>
 </a:MobilityWorkout>
 </a:MobilityWorkouts>
 <a:MobilityXHRs>
 <a:MobilityXHR>
 <a:Above>5</a:Above>
 <a:Below>45</a:Below>
 <a:Calories>453</a:Calories>
 <a:ErrorMessage i:nil="true"/>
 <a:ErrorStatus>false</a:ErrorStatus>
 <a:HRDateId>3/4/2012 4:46:01 PM</a:HRDateId>
 <a:InZone>19</a:InZone>
 <a:MemberId>8b5ed874-f43d-44f4-a8eb-2884f79e2acd</a:MemberId>
 <a:Total>69</a:Total>
 <a:WorkoutId>0</a:WorkoutId>
 <a:WorkoutName/>
 <a:WorkoutRounds>0</a:WorkoutRounds>
 </a:MobilityXHR>
 </a:MobilityXHRs>
 
 */
