#import "UAView.h"
#define TABLE_CELL_BACKGROUND {119.0/255,107.0/255,229.0/255,1.0,100.0/255,89.0/255,197.0/255,1.0}
#define kDefaultMargin 0

@implementation UAView

- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame])
	{
        
    }
    return self;
}

- (void)drawRect:(CGRect)arect
{
    int lineWidth = 1;
	CGRect rect = [self bounds];
    rect.size.width -= lineWidth;
    rect.size.height -= lineWidth;
    rect.origin.x += lineWidth / 2.0;
    rect.origin.y += lineWidth / 2.0;
	
    CGFloat minx = CGRectGetMinX(rect), midx = CGRectGetMidX(rect), maxx = CGRectGetMaxX(rect);
    CGFloat miny = CGRectGetMinY(rect),/* midy = CGRectGetMidY(rect),*/ maxy = CGRectGetMaxY(rect);
    maxy += 1;
	
    CGFloat locations[2] = { 0.0, 1.0 };
	
    CGContextRef c = UIGraphicsGetCurrentContext();
    CGColorSpaceRef myColorspace = CGColorSpaceCreateDeviceRGB();
    CGGradientRef myGradient = nil;
    CGFloat components[8] = TABLE_CELL_BACKGROUND;
    CGContextSetStrokeColorWithColor(c, [[UIColor whiteColor] CGColor]);
    CGContextSetLineWidth(c, lineWidth);
    CGContextSetAllowsAntialiasing(c, YES);
    CGContextSetShouldAntialias(c, YES);
	
	CGMutablePathRef path = CGPathCreateMutable();
	CGPathMoveToPoint(path, NULL, minx, maxy);
	CGPathAddArcToPoint(path, NULL, minx, miny, midx, miny, kDefaultMargin);
	CGPathAddArcToPoint(path, NULL, minx, miny, midx, miny, 100);
	CGPathAddArcToPoint(path, NULL, maxx, miny, maxx, maxy, kDefaultMargin);
	CGPathAddLineToPoint(path, NULL, maxx, maxy);
	//CGPathAddLineToPoint(path, NULL, maxx, 10);
	CGPathAddLineToPoint(path, NULL, minx, maxy);
	CGPathCloseSubpath(path);
	
	CGContextSaveGState(c);
	CGContextAddPath(c, path);
	CGContextClip(c);
	
	myGradient = CGGradientCreateWithColorComponents(myColorspace, components, locations, 2);
	CGContextDrawLinearGradient(c, myGradient, CGPointMake(minx,miny), CGPointMake(minx,maxy), 0);
	
	CGContextAddPath(c, path);
	CGContextStrokePath(c);
	CGContextRestoreGState(c);
	
	CGColorSpaceRelease(myColorspace);
    CGGradientRelease(myGradient);
    return;	
}

- (void)dealloc
{
    [super dealloc];
}


@end
