//
//  claimActivityOBJ.m
//  Acclaris
//
//  Created by Sarfaraj Biswas on 09/11/10.
//  Copyright 2010 ObjectSol Technologies. All rights reserved.
//

#import "CardioWeekItem.h"

@implementation CardioWeekItem

@synthesize Above;
@synthesize Below;
@synthesize Calories;
@synthesize Date;

@synthesize InAbove;
@synthesize InZone;
@synthesize MemberId;
@synthesize Total;

- (NSString*)getDate
{
	@try {
		return [Date substringWithRange:NSMakeRange(0, 10)];				
	}
	@catch (NSException * e) {
		
	}
	@finally {
		
	}
	
	return Date;
	//NSDateFormatter *dateFormater = [[NSDateFormatter alloc] init];
//	[dateFormater setDateFormat:@"yyyy-MM-ddThh:mm:aa"];
//	
//	NSDate *date = [dateFormater dateFromString:self.Date];
//	
//	[dateFormater setDateFormat:@"yyyy-MM-dd"];
//	
//	return [dateFormater stringFromDate:date];
}

@end

