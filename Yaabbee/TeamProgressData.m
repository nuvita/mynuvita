//
//  claimActivityOBJ.m
//  Acclaris
//
//  Created by Sarfaraj Biswas on 09/11/10.
//  Copyright 2010 ObjectSol Technologies. All rights reserved.
//

#import "TeamProgressData.h"

#define ERROR_STATUS @"a:ErrorStatus"
#define ERROR_MSG @"a:ErrorMessage"

#define TEAM_ID @"a:TeamId"
#define TEAM_MEMBER @"a:TeamMember"

#define TEAM_AVATAR @"a:Avatar"
#define EMAIL_ID @"a:EmailAddress"
#define F_NAME @"a:FirstName"
#define P_PERCENT @"a:ProgressPercent"
#define TEAM_NAME @"a:TeamName"


//static TeamProgressData *_sharedInstance = nil;

@implementation TeamProgressData

@synthesize TeamId;
@synthesize TeamName;

@synthesize TeamMembers;

@synthesize responseStatus;

//#pragma mark Singleton Methods
//
//+ (TeamProgressData *) sharedInstance
//{
//	@synchronized(self)
//	{
//		if (!_sharedInstance)
//		{
//			_sharedInstance = [[TeamProgressData alloc] init];
//		}
//	}
//	return _sharedInstance;
//}
//
//
//- (id)init {
//	if (self = [super init]) {
//		;//uploadImageQ = [[UploadImageQ alloc] init] ;//]WithString:@"Default Property Value"];
//	}
//	return self;
//}


//pasring data

//
//  UserresponcePerser.m
//  Acclaris
//
//  Created by Sarfaraj Biswas on 02/10/10.
//  Copyright 2010 ObjectSol Technologies. All rights reserved.
//


#pragma mark Parse Xml Data

- (void)parseXMLFileAtData:(NSMutableData *)data parseError:(NSError **)error
{
	NSXMLParser *parser;
	parser = [[NSXMLParser alloc] initWithData:data];
	[parser setDelegate:self];
	[parser setShouldProcessNamespaces:NO];
	[parser setShouldReportNamespacePrefixes:NO];
	[parser setShouldResolveExternalEntities:NO];
	[parser parse];
	
	NSError *parseError = [parser parserError];
	
	if (parseError && error) 
	{
		*error = parseError;
	}
	
	[parser release];
}

- (void)parserDidStartDocument:(NSXMLParser *)parser
{
	responseStatus = [[ResponseStatus alloc]init];
	TeamMembers = [[NSMutableArray alloc]init];
}		

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
	if (qName)
	{
		elementName = qName;
	}
	else 
		if([elementName isEqualToString:ERROR_STATUS])
		{
			contentOfString=[NSMutableString string];
			[contentOfString retain];
			return;		
				
		}
	else 
		if([elementName isEqualToString:ERROR_MSG])
		{
			contentOfString=[NSMutableString string];
			[contentOfString retain];
			return;		
		}
	else 
		if([elementName isEqualToString:TEAM_ID])
		{
			contentOfString=[NSMutableString string];
			[contentOfString retain];
			return;		
			
		}
		else 
			if([elementName isEqualToString:TEAM_MEMBER])
			{
				teamProgressItem = [[TeamProgressItem alloc]init];
				return;		
				
			}
            else 
				if([elementName isEqualToString:TEAM_AVATAR])
				{
					contentOfString=[NSMutableString string];
					[contentOfString retain];
					return;		
					
				}
			else 
				if([elementName isEqualToString:EMAIL_ID])
				{
					contentOfString=[NSMutableString string];
					[contentOfString retain];
					return;		
					
				}
				else
					if([elementName isEqualToString:F_NAME])
					{
						contentOfString=[NSMutableString string];
						[contentOfString retain];
						return;		
						
					}
					else
						if([elementName isEqualToString:P_PERCENT])
						{
							contentOfString=[NSMutableString string];
							[contentOfString retain];
							return;		
							
						}
						else
							if([elementName isEqualToString:TEAM_NAME])
							{
								contentOfString=[NSMutableString string];
								[contentOfString retain];
								return;		
								
							}
	
	
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName     
{	
	if (qName)
	{
		elementName = qName;
	}
	else 
			if([elementName isEqualToString:ERROR_STATUS])
			{
				if(contentOfString)
				{
					responseStatus.returnCode = contentOfString;
					[contentOfString release];
					contentOfString = nil;
				}				
			}
			else 
				if([elementName isEqualToString:ERROR_MSG])
				{
					if(contentOfString)
					{
						responseStatus.errorText = contentOfString;
						[contentOfString release];
						contentOfString = nil;
					}		
				}
	else 
		if([elementName isEqualToString:TEAM_ID])
		{
			if(contentOfString)
			{
				self.TeamId = contentOfString;
				[contentOfString release];
				contentOfString = nil;
			}		
		}
		else 
			if([elementName isEqualToString:TEAM_MEMBER])
			{
				[TeamMembers addObject:teamProgressItem];//
				
				teamProgressItem = nil;
			}
            else 
                if([elementName isEqualToString:TEAM_AVATAR])
				{
					if(contentOfString)
					{
						teamProgressItem.Avatar = contentOfString;
						[contentOfString release];
						contentOfString = nil;
					}		
				}	
		else 
			if([elementName isEqualToString:EMAIL_ID])
				{
					if(contentOfString)
					{
						teamProgressItem.EmailAddress = contentOfString;
						[contentOfString release];
						contentOfString = nil;
					}		
				}	
		else 
			if([elementName isEqualToString:F_NAME])
			{
				if(contentOfString)
				{
					teamProgressItem.FirstName = contentOfString;
					[contentOfString release];
					contentOfString = nil;
				}					
			}
			else 
				if([elementName isEqualToString:P_PERCENT])
				{
					if(contentOfString)
					{
						teamProgressItem.ProgressPercent = contentOfString;
						[contentOfString release];
						contentOfString = nil;
					}						
				}
				else 
					if([elementName isEqualToString:TEAM_NAME])
					{
						if(contentOfString)
						{
							self.TeamName = contentOfString;
							[contentOfString release];
							contentOfString = nil;
						}							
					}
	
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
	if(contentOfString)
		[contentOfString appendString:string];
}

- (void)parserDidEndDocument:(NSXMLParser *)parse
{
	//myerrorcodeOBJ = (errorcodeOBJ *)[errordetails objectAtIndex:0];
//	responseStatus.returnCode = [NSString stringWithFormat:@"0"];
//	responseStatus.errorText = [NSString stringWithFormat:@""];
	//NSLog(@"**********\n\n\n\n\n\n%@\n\nreturnCode %@\nerrorText %@", responseStatus.returnCode, responseStatus.errorText);
}	

- (NSMutableArray*)getTeamsEmail
{
	NSMutableArray* array = [[NSMutableArray alloc]init];
	[array retain];
	
	int size = [TeamMembers count];
	//NSString* emails = @"";
	
	for (int ii = 0; ii < size; ii++) {
		TeamProgressItem *obj = (TeamProgressItem *)[TeamMembers objectAtIndex:ii];
		[array addObject:obj.EmailAddress];
		//emails = [emails stringByAppendingFormat:teamProgressItem.EmailAddress];
		//emails = [emails stringByAppendingFormat:@","];
	}
	
	return array;
}

@end


/*
 
 <s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">
 <s:Body>
 <GetTeamProgressResponse xmlns="http://tempuri.org/">
 <GetTeamProgressResult xmlns:a="http://schemas.datacontract.org/2004/07/NuvitaMobileService" xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
 <a:ErrorMessage i:nil="true"/>
 <a:ErrorStatus>false</a:ErrorStatus>
 <a:TeamId>0f0a7226-6e59-4719-913d-79041c836299</a:TeamId> 
 <a:TeamMembers>
    <a:TeamMember>
    <a:EmailAddress>afsutherlin@gmail.com</a:EmailAddress>
    <a:FirstName>Fran</a:FirstName>
    <a:ProgressPercent>100</a:ProgressPercent>
 </a:TeamMember>
 <a:TeamMember>
    <a:EmailAddress>fithappenstx@gmail.com</a:EmailAddress>
    <a:FirstName>Lauren</a:FirstName>
    <a:ProgressPercent>38</a:ProgressPercent>
 </a:TeamMember>
 <a:TeamMember>
    <a:EmailAddress>ron.mcphee@nuvita.com</a:EmailAddress>
    <a:FirstName>Ron</a:FirstName>
    <a:ProgressPercent>35</a:ProgressPercent>
 </a:TeamMember>
 <a:TeamMember>
    <a:EmailAddress>terry.bailey@nuvita.com</a:EmailAddress>
    <a:FirstName>Terry</a:FirstName>
    <a:ProgressPercent>93</a:ProgressPercent>
 </a:TeamMember>
 <a:TeamMember>
    <a:EmailAddress>biggs.jess@gmail.com</a:EmailAddress>
    <a:FirstName>Jess</a:FirstName>
    <a:ProgressPercent>83</a:ProgressPercent>
 </a:TeamMember>
 <a:TeamMember>
    <a:EmailAddress>beafitnut@hotmail.com</a:EmailAddress>
    <a:FirstName>Karen</a:FirstName>
    <a:ProgressPercent>17</a:ProgressPercent>
 </a:TeamMember>
 <a:TeamMember>
    <a:EmailAddress>alanantin@msn.com</a:EmailAddress>
    <a:FirstName>Alan</a:FirstName>
    <a:ProgressPercent>100</a:ProgressPercent>
 </a:TeamMember>
 <a:TeamMember>
    <a:EmailAddress>josevo2max@gmail.com</a:EmailAddress>
    <a:FirstName>Jose</a:FirstName>
    <a:ProgressPercent>44</a:ProgressPercent>
 </a:TeamMember>
 <a:TeamMember>
    <a:EmailAddress>dfisher175@yahoo.com</a:EmailAddress>
    <a:FirstName>Deanna</a:FirstName>
    <a:ProgressPercent>17</a:ProgressPercent>
 </a:TeamMember>
 <a:TeamMember>
    <a:EmailAddress>kinlaw@kinlaw.com</a:EmailAddress>
    <a:FirstName>Michelle</a:FirstName>
    <a:ProgressPercent>17</a:ProgressPercent>
 </a:TeamMember>
 <a:TeamMember>
    <a:EmailAddress>paul.chavez@nuvita.com</a:EmailAddress>
    <a:FirstName>Paul</a:FirstName>
    <a:ProgressPercent>100</a:ProgressPercent>
 </a:TeamMember>
 <a:TeamMember>
    <a:EmailAddress>kingangm@gmail.com</a:EmailAddress>
    <a:FirstName>Angie</a:FirstName>
    <a:ProgressPercent>81</a:ProgressPercent>
 </a:TeamMember>
 </a:TeamMembers>
    <a:TeamName>Live Lean Nation</a:TeamName>
 </GetTeamProgressResult>
 </GetTeamProgressResponse>
 </s:Body>
 </s:Envelope>
 
 */

