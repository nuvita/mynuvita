//
//  claimActivityOBJ.m
//  Acclaris
//
//  Created by Sarfaraj Biswas on 09/11/10.
//  Copyright 2010 ObjectSol Technologies. All rights reserved.
//

#import "MobilityWeekData.h"

#define ERROR_STATUS @"a:ErrorStatus"
#define ERROR_MSG @"a:ErrorMessage"

#define MEMBER_ID @"a:MemberId"
#define DATE @"a:Date"

#define FRIDAY_SELECT @"a:FridaySelect"
#define FRIDAY_TEXT @"a:FridayText"

#define FRIDAY_SELECT @"a:FridaySelect"
#define FRIDAY_TEXT @"a:FridayText"

#define MONDAY_SELECT @"a:MondaySelect"
#define MONDAY_TEXT @"a:MondayText"

#define SATURDAY_SELECT @"a:SaturdaySelect"
#define SATURDAY_TEXT @"a:SaturdayText"

#define SUNDAY_SELECT @"a:SundaySelect"
#define SUNDAY_TEXT @"a:SundayText"

#define THURSDAY_SELECT @"a:ThursdaySelect"
#define THURSDAY_TEXT @"a:ThursdayText"

#define TUESDAY_SELECT @"a:TuesdaySelect"
#define TUESDAY_TEXT @"a:TuesdayText"

#define WEDNESSDAY_SELECT @"a:WednesdaySelect"
#define WEDNESSDAY_TEXT @"a:WednesdayText"
#define ISMOBILITY_X @"a:isMobilityX"

#define WEEK_LABEL @"a:WeekLabel"
//static TeamProgressData *_sharedInstance = nil;
////<a:isMobilityX>true</a:isMobilityX>

@implementation MobilityWeekData

@synthesize Date, FridaySelect, FridayText, MondaySelect, MondayText, SaturdaySelect, SaturdayText, SundaySelect, SundayText, ThursdaySelect, ThursdayText,TuesdaySelect, TuesdayText,WednesdaySelect, WednesdayText,isMobilityX;

@synthesize WeekLabel, MemberId;

@synthesize responseStatus;

#pragma mark Parse Xml Data

- (void)parseXMLFileAtData:(NSMutableData *)data parseError:(NSError **)error
{
	NSXMLParser *parser;
	parser = [[NSXMLParser alloc] initWithData:data];
	[parser setDelegate:self];
	[parser setShouldProcessNamespaces:NO];
	[parser setShouldReportNamespacePrefixes:NO];
	[parser setShouldResolveExternalEntities:NO];
	[parser parse];
	
	NSError *parseError = [parser parserError];
	
	if (parseError && error) 
	{
		*error = parseError;
	}
	
	[parser release];
}

- (void)parserDidStartDocument:(NSXMLParser *)parser
{
	responseStatus = [[ResponseStatus alloc]init];
	//self.ItemsArray = [[NSMutableArray alloc]init];
}		

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
	if (qName)
	{
		elementName = qName;
	}
	else 
		if([elementName isEqualToString:ERROR_STATUS])
		{
			contentOfString=[NSMutableString string];
			[contentOfString retain];
			return;		
				
		}
	else 
		if([elementName isEqualToString:ERROR_MSG])
		{
			contentOfString=[NSMutableString string];
			[contentOfString retain];
			return;		
		}
	else //		
		if([elementName isEqualToString:DATE])
		{
			contentOfString=[NSMutableString string];
			[contentOfString retain];
			return;		
			
		}
		else //		
			if([elementName isEqualToString:MEMBER_ID])
			{
				contentOfString=[NSMutableString string];
				[contentOfString retain];
				return;		
				
			}else 		
				if([elementName isEqualToString:MONDAY_SELECT])
				{
					contentOfString=[NSMutableString string];
					[contentOfString retain];
					return;		
					
				}else //		
					if([elementName isEqualToString:MONDAY_TEXT])
					{
						contentOfString=[NSMutableString string];
						[contentOfString retain];
						return;		
						
					}else //		
						if([elementName isEqualToString:TUESDAY_SELECT])
						{
							contentOfString=[NSMutableString string];
							[contentOfString retain];
							return;		
							
						}else //		
							if([elementName isEqualToString:TUESDAY_TEXT])
							{
								contentOfString=[NSMutableString string];
								[contentOfString retain];
								return;		
								
							}else //		
								if([elementName isEqualToString:WEDNESSDAY_SELECT])
								{
									contentOfString=[NSMutableString string];
									[contentOfString retain];
									return;		
									
								}
								else 
									if([elementName isEqualToString:WEDNESSDAY_TEXT])
									{
										contentOfString=[NSMutableString string];
										[contentOfString retain];
										return;		
										
									}
									else //		
										if([elementName isEqualToString:THURSDAY_SELECT])
										{
											contentOfString=[NSMutableString string];
											[contentOfString retain];
											return;		
											
										}else //		
											if([elementName isEqualToString:THURSDAY_TEXT])
											{
												contentOfString=[NSMutableString string];
												[contentOfString retain];
												return;		
												
											}else //		
												if([elementName isEqualToString:FRIDAY_SELECT])
												{
													contentOfString=[NSMutableString string];
													[contentOfString retain];
													return;		
													
												}
												else //		
													if([elementName isEqualToString:FRIDAY_TEXT])
													{
														contentOfString=[NSMutableString string];
														[contentOfString retain];
														return;		
														
													}
	
		
			else 
				if([elementName isEqualToString:SATURDAY_SELECT])
				{
					contentOfString=[NSMutableString string];
					[contentOfString retain];
					return;		
					
				}
				else
					if([elementName isEqualToString:SATURDAY_TEXT])
					{
						contentOfString=[NSMutableString string];
						[contentOfString retain];
						return;		
						
					}
					else
						if([elementName isEqualToString:SUNDAY_SELECT])
						{
							contentOfString=[NSMutableString string];
							[contentOfString retain];
							return;		
							
						}//
						else
							if([elementName isEqualToString:SUNDAY_TEXT])
							{
								contentOfString=[NSMutableString string];
								[contentOfString retain];
								return;		
								
							}
							else
								if([elementName isEqualToString:ISMOBILITY_X])
								{
									contentOfString=[NSMutableString string];
									[contentOfString retain];
									return;		
									
								}
								else
										if([elementName isEqualToString:WEEK_LABEL])
										{
											contentOfString=[NSMutableString string];
											[contentOfString retain];
											return;		
											
										}
	
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName     
{	
	if (qName)
	{
		elementName = qName;
	}
	else 
			if([elementName isEqualToString:ERROR_STATUS])
			{
				if(contentOfString)
				{
					responseStatus.returnCode = contentOfString;
					[contentOfString release];
					contentOfString = nil;
				}				
			}
			else 
				if([elementName isEqualToString:ERROR_MSG])
				{
					if(contentOfString)
					{
						responseStatus.errorText = contentOfString;
						[contentOfString release];
						contentOfString = nil;
					}		
				}
	else 
		if([elementName isEqualToString:DATE])
		{
			if(contentOfString)
			{
				self.Date = contentOfString;
				[contentOfString release];
				contentOfString = nil;
			}
		}
		else 
			if([elementName isEqualToString:MEMBER_ID])
			{
				if(contentOfString)
				{
					self.MemberId = contentOfString;
					[contentOfString release];
					contentOfString = nil;
				}
			}
			else 
				if([elementName isEqualToString:MONDAY_SELECT])
				{
					if(contentOfString)
					{
						self.MondaySelect = contentOfString;
						[contentOfString release];
						contentOfString = nil;
					}
				}
				else 
					if([elementName isEqualToString:MONDAY_TEXT])
					{
						if(contentOfString)
						{
							self.MondayText = contentOfString;
							[contentOfString release];
							contentOfString = nil;
						}
					}
					else 
						if([elementName isEqualToString:TUESDAY_SELECT])
						{
							if(contentOfString)
							{
								self.TuesdaySelect = contentOfString;
								[contentOfString release];
								contentOfString = nil;
							}
						}else 
							if([elementName isEqualToString:TUESDAY_TEXT])
							{
								if(contentOfString)
								{
									self.TuesdayText = contentOfString;
									[contentOfString release];
									contentOfString = nil;
								}
							}else 
								if([elementName isEqualToString:WEDNESSDAY_SELECT])
								{
									if(contentOfString)
									{
										self.WednesdaySelect = contentOfString;
										[contentOfString release];
										contentOfString = nil;
									}
								}else 
									if([elementName isEqualToString:WEDNESSDAY_TEXT])
									{
										if(contentOfString)
										{
											self.WednesdayText = contentOfString;
											[contentOfString release];
											contentOfString = nil;
										}
									}
									else 
										if([elementName isEqualToString:THURSDAY_SELECT])
										{
											if(contentOfString)
											{
												self.ThursdaySelect = contentOfString;
												[contentOfString release];
												contentOfString = nil;
											}
										}else 
											if([elementName isEqualToString:THURSDAY_TEXT])
											{
												if(contentOfString)
												{
													self.ThursdayText = contentOfString;
													[contentOfString release];
													contentOfString = nil;
												}
											}else 
												if([elementName isEqualToString:FRIDAY_SELECT])
												{
													if(contentOfString)
													{
														self.FridaySelect= contentOfString;
														[contentOfString release];
														contentOfString = nil;
													}
												}														
					else 
						if([elementName isEqualToString:FRIDAY_TEXT])
						{
							if(contentOfString)
							{
								self.FridayText = contentOfString;
								[contentOfString release];
								contentOfString = nil;
							}							
						}
						else 
							if([elementName isEqualToString:SATURDAY_SELECT])
							{
								if(contentOfString)
								{
									self.SaturdaySelect = contentOfString;
									[contentOfString release];
									contentOfString = nil;
								}							
							}else 
								if([elementName isEqualToString:SATURDAY_TEXT])
								{
									if(contentOfString)
									{
										self.SaturdayText = contentOfString;
										[contentOfString release];
										contentOfString = nil;
									}							
								}
								else 
									if([elementName isEqualToString:SUNDAY_SELECT])
									{
										if(contentOfString)
										{
											self.SundaySelect = contentOfString;
											[contentOfString release];
											contentOfString = nil;
										}							
									}else 
										if([elementName isEqualToString:SUNDAY_TEXT])
										{
											if(contentOfString)
											{
												self.SundayText = contentOfString;
												[contentOfString release];
												contentOfString = nil;
											}							
										}
								else 
									if([elementName isEqualToString:ISMOBILITY_X])
									{
										if(contentOfString)
										{
											self.isMobilityX = contentOfString;
											[contentOfString release];
											contentOfString = nil;
										}							
									}
								else 
									if([elementName isEqualToString:WEEK_LABEL])
									{
										if(contentOfString)
										{
											self.WeekLabel = contentOfString;
											[contentOfString release];
											contentOfString = nil;
										}
									}
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
	if(contentOfString)
		[contentOfString appendString:string];
}

- (void)parserDidEndDocument:(NSXMLParser *)parse
{
	//myerrorcodeOBJ = (errorcodeOBJ *)[errordetails objectAtIndex:0];
//	responseStatus.returnCode = [NSString stringWithFormat:@"0"];
//	responseStatus.errorText = [NSString stringWithFormat:@""];
	//NSLog(@"**********\n\n\n\n\n\n%@\n\nreturnCode %@\nerrorText %@", responseStatus.returnCode, responseStatus.errorText);
}	

- (BOOL)getBoolVal:(NSString*)val
{
	if([val isEqualToString:@"true"])
		return YES;
	else
		return NO;
}



- (NSString*)getMobilityXml:(NSString*)date
{
	NSMutableString *sRequest=[[NSMutableString alloc] init];
	
	[sRequest appendString:@"<nuv:Date>"];
 	[sRequest appendString:date];
	[sRequest appendString:@"</nuv:Date>"];
	
	
	[sRequest appendString:@"<nuv:ErrorMessage>"];
 	[sRequest appendString:@""];
	[sRequest appendString:@"</nuv:ErrorMessage>"];
	
	[sRequest appendString:@"<nuv:ErrorStatus>"];
 	[sRequest appendString:@"false"];
	[sRequest appendString:@"</nuv:ErrorStatus>"];
	
	//F
	[sRequest appendString:@"<nuv:FridaySelect>"];
 	[sRequest appendString:self.FridaySelect];
	[sRequest appendString:@"</nuv:FridaySelect>"];
	
	[sRequest appendString:@"<nuv:FridayText>"];
 	[sRequest appendString:@"Friday"];
	[sRequest appendString:@"</nuv:FridayText>"];
	
	[sRequest appendString:@"<nuv:MemberId>"];
 	[sRequest appendString:self.MemberId];
	[sRequest appendString:@"</nuv:MemberId>"];
	
	//M
	[sRequest appendString:@"<nuv:MondaySelect>"];
 	[sRequest appendString:self.MondaySelect];
	[sRequest appendString:@"</nuv:MondaySelect>"];
	
	[sRequest appendString:@"<nuv:MondayText>"];
 	[sRequest appendString:@"Monday"];
	[sRequest appendString:@"</nuv:MondayText>"];
	
	//Sat
	[sRequest appendString:@"<nuv:SaturdaySelect>"];
 	[sRequest appendString:self.SaturdaySelect];
	[sRequest appendString:@"</nuv:SaturdaySelect>"];
	
	[sRequest appendString:@"<nuv:SaturdayText>"];
 	[sRequest appendString:@"Saturday"];
	[sRequest appendString:@"</nuv:SaturdayText>"];
	
	//Sunday
	[sRequest appendString:@"<nuv:SundaySelect>"];
 	[sRequest appendString:self.SundaySelect];
	[sRequest appendString:@"</nuv:SundaySelect>"];
	
	[sRequest appendString:@"<nuv:SundayText>"];
 	[sRequest appendString:@"Sunday"];
	[sRequest appendString:@"</nuv:SundayText>"];
	
	//ThursdaySelect
	[sRequest appendString:@"<nuv:ThursdaySelect>"];
 	[sRequest appendString:self.ThursdaySelect];
	[sRequest appendString:@"</nuv:ThursdaySelect>"];
	
	[sRequest appendString:@"<nuv:ThursdayText>"];
 	[sRequest appendString:@"Thursday"];
	[sRequest appendString:@"</nuv:ThursdayText>"];
	
		
	//TuesdaySelect
	[sRequest appendString:@"<nuv:TuesdaySelect>"];
 	[sRequest appendString:self.TuesdaySelect];
	[sRequest appendString:@"</nuv:TuesdaySelect>"];
	
	[sRequest appendString:@"<nuv:TuesdayText>"];
 	[sRequest appendString:@"Tuesday"];
	[sRequest appendString:@"</nuv:TuesdayText>"];
	

	//WednesdaySelect
	[sRequest appendString:@"<nuv:WednesdaySelect>"];
 	[sRequest appendString:self.WednesdaySelect];
	[sRequest appendString:@"</nuv:WednesdaySelect>"];
	
	[sRequest appendString:@"<nuv:WednesdayText>"];
 	[sRequest appendString:@"Wednesday"];
	[sRequest appendString:@"</nuv:WednesdayText>"];
	
	[sRequest appendString:@"<nuv:WeekLabel>"];
 	[sRequest appendString:self.WeekLabel];
	[sRequest appendString:@"</nuv:WeekLabel>"];
	
	return sRequest;
}

@end


/*
 
 <GetMobilityWeekResult xmlns:a="http://schemas.datacontract.org/2004/07/NuvitaMobileService" xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
 <a:Date>2012-01-03T00:00:00</a:Date>
 <a:ErrorMessage i:nil="true"/>
 <a:ErrorStatus>false</a:ErrorStatus>
 <a:FridaySelect>false</a:FridaySelect>
 <a:FridayText/>
 <a:MemberId>8b5ed874-f43d-44f4-a8eb-2884f79e2acd</a:MemberId>
 <a:MondaySelect>true</a:MondaySelect>
 <a:MondayText/>
 <a:SaturdaySelect>false</a:SaturdaySelect>
 <a:SaturdayText/>
 <a:SundaySelect>false</a:SundaySelect>
 <a:SundayText/>
 <a:ThursdaySelect>false</a:ThursdaySelect>
 <a:ThursdayText/>
 <a:TuesdaySelect>true</a:TuesdaySelect>
 <a:TuesdayText/>
 <a:WednesdaySelect>true</a:WednesdaySelect>
 <a:WednesdayText/>
 <a:WeekLabel>Week of 1/2</a:WeekLabel>
 </GetMobilityWeekResult>
 
 <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/" xmlns:nuv="http://schemas.datacontract.org/2004/07/NuvitaMobileService">
 <soapenv:Header/>
 <soapenv:Body>
 <tem:SaveMobilityWeek>
 <tem:mobilityWeek>
 
 <nuv:Date>2012-01-28</nuv:Date>
 <nuv:ErrorMessage></nuv:ErrorMessage>
 <nuv:ErrorStatus>false</nuv:ErrorStatus>
 <nuv:FridaySelect>true</nuv:FridaySelect>
 <nuv:FridayText>Friday</nuv:FridayText>
 <nuv:MemberId>8b5ed874-f43d-44f4-a8eb-2884f79e2acd</nuv:MemberId>
 <nuv:MondaySelect>true</nuv:MondaySelect>
 <nuv:MondayText></nuv:MondayText>
 <nuv:SaturdaySelect>true</nuv:SaturdaySelect>
 <nuv:SaturdayText>Saturday</nuv:SaturdayText>
 <nuv:SundaySelect>true</nuv:SundaySelect>
 <nuv:SundayText>Sunday</nuv:SundayText>
 <nuv:ThursdaySelect>true</nuv:ThursdaySelect>
 <nuv:ThursdayText>Thursday</nuv:ThursdayText>
 <nuv:TuesdaySelect>true</nuv:TuesdaySelect>
 <nuv:TuesdayText>Tuesday</nuv:TuesdayText>
 <nuv:WednesdaySelect>true</nuv:WednesdaySelect>
 <nuv:WednesdayText>Wednesday</nuv:WednesdayText>
 <nuv:WeekLabel>Week of 1/2</nuv:WeekLabel>
 
 </tem:mobilityWeek>
 </tem:SaveMobilityWeek>
 </soapenv:Body>
 </soapenv:Envelope>
 
 */

