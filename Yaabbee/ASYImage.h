//
//  ASYImage.h
//  newMillionaire
//
//  Created by Sumit Prasad on 12/12/09.
//  Copyright 2009 Objectsol Technologes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NuvitaAppDelegate.h"

@interface ASYImage : UIView 
{
	NuvitaAppDelegate *app;
	NSURLConnection* connection;
       NSMutableData* data;
	UIImage* myImageLoaded;
	UIActivityIndicatorView *networkIndicator;
}

@property (nonatomic, retain) NSURLConnection* connection;
@property (nonatomic, retain) UIImage* myImageLoaded;

- (void)loadImageFromURL:(NSURL*)url indicator:(UIActivityIndicatorView *)activeIndicator;
- (UIImage*) image;


@end
