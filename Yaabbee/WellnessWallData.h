//
//  claimActivityOBJ.h
//  Acclaris
//
//  Created by Sarfaraj Biswas on 09/11/10.
//  Copyright 2010 ObjectSol Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WellnessWallItem.h"
#import "ResponseStatus.h"

//@class LessonItem;

@interface WellnessWallData : NSObject<NSXMLParserDelegate> {
	//NSMutableArray *ItemsArray_;
	NSMutableString *contentOfString;
	
	ResponseStatus *responseStatus;
	NSString *htmlData;
    NSString *PageNumber;
    NSMutableArray *itemArr;
}

@property(nonatomic,retain)NSString *MemberId, *WeekLabel;
@property(nonatomic,retain)NSMutableArray *itemArr;
@property(nonatomic,retain)NSString *htmlData;
@property(nonatomic,retain)NSString *PageNumber;
//
@property(nonatomic,retain)ResponseStatus *responseStatus;

- (void)parseXMLFileAtData:(NSMutableData *)data parseError:(NSError **)error;

@end


//<a:TeamId>0f0a7226-6e59-4719-913d-79041c836299</a:TeamId>
//<a:TeamName>Live Lean Nation</a:TeamName>