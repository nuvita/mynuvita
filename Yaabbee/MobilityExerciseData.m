//
//  claimActivityOBJ.m
//  Acclaris
//
//  Created by Sarfaraj Biswas on 09/11/10.
//  Copyright 2010 ObjectSol Technologies. All rights reserved.
//

#import "MobilityExerciseData.h"
#import "constant.h"

#define MOBILITY_ERROR_STATUS @"a:ErrorStatus"
#define MOBILITY_ERROR_MSG @"a:ErrorMessage"

//#define MOBILITY_MEMBER_ID @"a:MemberId"
//#define MOBILITY_DATE @"a:Date"

//workout
#define MOBILITY_WORKOUT_CODE @"a:WorkoutCode"
#define MOBILITY_WORKOUT_ID @"a:WorkoutId"
#define MOBILITY_WORKOUT_NAME @"a:WorkoutName"


//


#define ELTYPE(typeName) ([elementName caseInsensitiveCompare:#typeName] == NSOrderedSame)

//static TeamProgressData *_sharedInstance = nil;

@implementation MobilityExerciseData

@synthesize dictMobilityExercise;
@synthesize WorkoutName, WorkoutId, WorkoutCode;
@synthesize responseStatus;

#pragma mark Parse Xml Data

- (void)parseXMLFileAtData:(NSMutableData *)data parseError:(NSError **)error
{
	NSXMLParser *parser;
	parser = [[NSXMLParser alloc] initWithData:data];
	[parser setDelegate:self];
	[parser setShouldProcessNamespaces:NO];
	[parser setShouldReportNamespacePrefixes:NO];
	[parser setShouldResolveExternalEntities:NO];
	[parser parse];
	
	NSError *parseError = [parser parserError];
	
	if (parseError && error) 
	{
		*error = parseError;
	}
	
	[parser release];
}

- (void)parserDidStartDocument:(NSXMLParser *)parser
{
	responseStatus = [[ResponseStatus alloc]init];
	itemsArray = [[NSMutableArray alloc]init];
	
	self.dictMobilityExercise = [NSMutableDictionary dictionary];
//    [self.dictMobilityExercise retain];
}		

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
	if (qName)
	{
		elementName = qName;
	}
	else 
		if([elementName isEqualToString:MOBILITY_ERROR_STATUS])
		{
			contentOfString=[NSMutableString string];
			[contentOfString retain];
			return;		
				
		}
	else 
		if([elementName isEqualToString:MOBILITY_ERROR_MSG])
		{
			contentOfString=[NSMutableString string];
			[contentOfString retain];
			return;		
		}
	else //		
		if([elementName isEqualToString:MOBILITY_WORKOUT_CODE])
		{
			contentOfString=[NSMutableString string];
			[contentOfString retain];
			return;		
			
		}
		else //		
			if([elementName isEqualToString:MOBILITY_WORKOUT_ID])
			{
				contentOfString=[NSMutableString string];
				[contentOfString retain];
				return;		
				
			}else //		
                if([elementName isEqualToString:MOBILITY_WORKOUT_NAME])
                {
                    contentOfString=[NSMutableString string];
                    [contentOfString retain];
                    return;		
                    
                }
            else 
				if([elementName isEqualToString:MOBILITY_EXERCISES])
				{
					itemsArray = [[NSMutableArray alloc]init];
					return;		
					
				}else //		
					if([elementName isEqualToString:MOBILITY_EXERCISE])
					{
						dictItem = [NSMutableDictionary dictionary];
						[dictItem retain];
						return;		
					}else //		
						if([elementName isEqualToString:EXERCISE_ID])
						{
							contentOfString=[NSMutableString string];
							[contentOfString retain];
							return;		
							
						}else //		
							if([elementName isEqualToString:EXERCISE_NAME])
							{
								contentOfString=[NSMutableString string];
								[contentOfString retain];
								return;		
								
							}else //		
								if([elementName isEqualToString:EXERCISE_IMAGE_URL])
								{
									contentOfString=[NSMutableString string];
									[contentOfString retain];
									return;		
									
								}
								else
										if([elementName isEqualToString:EXERCISE_REPEAT])
										{
											contentOfString=[NSMutableString string];
											[contentOfString retain];
											return;		
											
										}
                                        else
                                            if([elementName isEqualToString:EXERCISE_REPS])
                                            {
                                                contentOfString=[NSMutableString string];
                                                [contentOfString retain];
                                                return;		
                                                
                                            }
                                            else
                                                if([elementName isEqualToString:EXERCISE_REST])
                                                {
                                                    contentOfString=[NSMutableString string];
                                                    [contentOfString retain];
                                                    return;		
                                                    
                                                }
                                                else
                                                    if([elementName isEqualToString:EXERCISE_VIDEO_URL])
                                                    {
                                                        contentOfString=[NSMutableString string];
                                                        [contentOfString retain];
                                                        return;		
                                                        
                                                    }
    }

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName     
{	
	if (qName)
	{
		elementName = qName;
	}
	else 
			if([elementName isEqualToString:MOBILITY_ERROR_STATUS])
			{
				if(contentOfString)
				{
					responseStatus.returnCode = contentOfString;
					[contentOfString release];
					contentOfString = nil;
				}				
			}
			else 
				if([elementName isEqualToString:MOBILITY_ERROR_MSG])
				{
					if(contentOfString)
					{
						responseStatus.errorText = contentOfString;
						[contentOfString release];
						contentOfString = nil;
					}		
				}
	else 
		if([elementName isEqualToString:MOBILITY_WORKOUT_CODE])
		{
			if(contentOfString)
			{
				self.WorkoutCode = contentOfString;
				[contentOfString release];
				contentOfString = nil;
			}
		}
		else 
			if([elementName isEqualToString:MOBILITY_WORKOUT_ID])
			{
				if(contentOfString)
				{
					self.WorkoutId = contentOfString;
					[contentOfString release];
					contentOfString = nil;
				}
			}
            else 
                if([elementName isEqualToString:MOBILITY_WORKOUT_NAME])
                {
                    if(contentOfString)
                    {
                        self.WorkoutName = contentOfString;
                        [contentOfString release];
                        contentOfString = nil;
                    }
                }
			else  
				if([elementName isEqualToString:MOBILITY_EXERCISES])
				{
					if(itemsArray)
					{
						[self.dictMobilityExercise setObject:itemsArray forKey:MOBILITY_EXERCISES];
						[itemsArray release];
						itemsArray = nil;
					}
				}
				else 
					if([elementName isEqualToString:MOBILITY_EXERCISE])
					{
						if(itemsArray && dictItem)
						{
							[itemsArray addObject:dictItem];
							[dictItem release];
							dictItem = nil;
						}
					}
					else 
						if([elementName isEqualToString:EXERCISE_ID])
						{
							if(contentOfString)
							{
								[dictItem setObject:contentOfString forKey:EXERCISE_ID];
								[contentOfString release];
								contentOfString = nil;
							}
						}else 
							if([elementName isEqualToString:EXERCISE_NAME])
							{
								if(contentOfString)
								{
									[dictItem setObject:contentOfString forKey:EXERCISE_NAME];
									[contentOfString release];
									contentOfString = nil;
								}
							}else 
								if([elementName isEqualToString:EXERCISE_IMAGE_URL])
								{
									if(contentOfString)
									{
										[dictItem setObject:contentOfString forKey:EXERCISE_IMAGE_URL];
										[contentOfString release];
										contentOfString = nil;
									}
								}
                                else 
                                    if([elementName isEqualToString:EXERCISE_REPEAT])
                                    {
                                        if(contentOfString)
                                        {
                                            [dictItem setObject:contentOfString forKey:EXERCISE_REPEAT];
                                            [contentOfString release];
                                            contentOfString = nil;
                                        }
                                    }
                                    else 
                                        if([elementName isEqualToString:EXERCISE_REPS])
                                        {
                                            if(contentOfString)
                                            {
                                                [dictItem setObject:contentOfString forKey:EXERCISE_REPS];
                                                [contentOfString release];
                                                contentOfString = nil;
                                            }
                                        }
                                        else 
                                            if([elementName isEqualToString:EXERCISE_REST])
                                            {
                                                if(contentOfString)
                                                {
                                                    [dictItem setObject:contentOfString forKey:EXERCISE_REST];
                                                    [contentOfString release];
                                                    contentOfString = nil;
                                                }
                                            }
                                            else 
                                                if([elementName isEqualToString:EXERCISE_VIDEO_URL])
                                                {
                                                    if(contentOfString)
                                                    {
                                                        [dictItem setObject:contentOfString forKey:EXERCISE_VIDEO_URL];
                                                        [contentOfString release];
                                                        contentOfString = nil;
                                                    }
                                                }

}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
	if(contentOfString)
		[contentOfString appendString:string];
}

- (void)parserDidEndDocument:(NSXMLParser *)parse
{
	
	//myerrorcodeOBJ = (errorcodeOBJ *)[errordetails objectAtIndex:0];
//	responseStatus.returnCode = [NSString stringWithFormat:@"0"];
//	responseStatus.errorText = [NSString stringWithFormat:@""];
	//NSLog(@"**********\n\n\n\n\n\n%@\n\nreturnCode %@\nerrorText %@", responseStatus.returnCode, responseStatus.errorText);
}	

- (BOOL)getBoolVal:(NSString*)val
{
	if([val isEqualToString:@"true"])
		return YES;
	else
		return NO;
}



- (NSString*)getMobilityXml:(NSString*)date
{
	NSMutableString *sRequest=[[NSMutableString alloc] init];
	
	[sRequest appendString:@"<nuv:Date>"];
 	[sRequest appendString:date];
	[sRequest appendString:@"</nuv:Date>"];
	
	return sRequest;
}

@end


/*
 
 <a:Date>2012-03-04T00:00:00</a:Date>
 <a:DayLabel>Sunday 3/4</a:DayLabel>
 <a:ErrorMessage>10</a:ErrorMessage>
 <a:ErrorStatus>false</a:ErrorStatus>
 <a:MemberId>8b5ed874-f43d-44f4-a8eb-2884f79e2acd</a:MemberId>
 
 <a:MobilityWorkouts>
 <a:MobilityWorkout>
 <a:WorkoutCode>NA</a:WorkoutCode>
 <a:WorkoutId>0</a:WorkoutId>
 <a:WorkoutName>-- Select Workout --</a:WorkoutName>
 
 </a:MobilityWorkout>
 <a:MobilityWorkout>
 <a:WorkoutCode>W1</a:WorkoutCode>
 <a:WorkoutId>30</a:WorkoutId>
 <a:WorkoutName>Workout 1</a:WorkoutName>
 </a:MobilityWorkout>
 <a:MobilityWorkout>
 <a:WorkoutCode>W2</a:WorkoutCode>
 <a:WorkoutId>31</a:WorkoutId>
 <a:WorkoutName>Workout 2</a:WorkoutName>
 </a:MobilityWorkout>
 <a:MobilityWorkout>
 <a:WorkoutCode>W3</a:WorkoutCode>
 <a:WorkoutId>32</a:WorkoutId>
 <a:WorkoutName>Workout 3</a:WorkoutName>
 </a:MobilityWorkout>
 </a:MobilityWorkouts>
 
 <a:MobilityXHRs>
 <a:MobilityXHR>
 <a:Above>5</a:Above>
 <a:Below>45</a:Below>
 <a:Calories>453</a:Calories>
 <a:ErrorMessage i:nil="true"/>
 <a:ErrorStatus>false</a:ErrorStatus>
 <a:HRDateId>3/4/2012 4:46:01 PM</a:HRDateId>
 <a:InZone>19</a:InZone>
 <a:MemberId>8b5ed874-f43d-44f4-a8eb-2884f79e2acd</a:MemberId>
 <a:Total>69</a:Total>
 <a:WorkoutId>0</a:WorkoutId>
 <a:WorkoutName/>
 <a:WorkoutRounds>0</a:WorkoutRounds>
 
 </a:MobilityXHR>
 </a:MobilityXHRs>
 </GetMobilityXHRSessionsResult> 
 */

