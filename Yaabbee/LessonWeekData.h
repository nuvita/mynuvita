//
//  claimActivityOBJ.h
//  Acclaris
//
//  Created by Sarfaraj Biswas on 09/11/10.
//  Copyright 2010 ObjectSol Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LessonItem.h"
#import "ResponseStatus.h"

@class LessonItem;

@interface LessonWeekData : NSObject<NSXMLParserDelegate> {
		
	//NSMutableArray *ItemsArray_;
	
	NSMutableString *contentOfString;
	
	ResponseStatus *responseStatus;
	LessonItem *lessonItem;
}

@property(nonatomic,retain)NSString *MemberId, *WeekLabel;
//@property(nonatomic,retain)NSString *TeamName;

@property(nonatomic,retain)NSMutableArray *ItemsArray;

//
@property(nonatomic,retain)ResponseStatus *responseStatus;

- (void)parseXMLFileAtData:(NSMutableData *)data parseError:(NSError **)error;

@end


//<a:TeamId>0f0a7226-6e59-4719-913d-79041c836299</a:TeamId>
//<a:TeamName>Live Lean Nation</a:TeamName>