//
//  request.m
//  Trail
//
//  Created by mmandal on 02/10/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//
#import "NuvitaAppDelegate.h"
#import "request.h"
//#import "SSCrypto.h"
#import "DateFormate.h"
#import "LoginPerser.h"


#define METHOD_URL @"http://tempuri.org/IMobileService/"
#define METHOD_URL2 @"http://tempuri.org/IUploadService/"

#define url @ "http://ws.mynuvita.com/MobileService.svc"
#define url2 @ "http://upload.mynuvita.com/UploadService.svc/mex"

#define urlbeta @"http://beta.ws.mynuvita.com/MobileService.svc"

#define HEALTH_MENU @"GetHealthMeasurementMenu"
#define ACTIVITY_LEVEL @"GetVo2MaxFromRRActivityQuestion"
#define GET_VO2MAX_RR @"GetVo2MaxFromRR"
#define GET_MEASUREMENT_TRENDS @"GetHealthMeasurementCurrentTrends"
#define GET_MEASUREMENT_GROUP @"GetHealthMeasurementGroup"
#define GET_NFL_TEAM @"GetNFLTeam"
#define GET_NFL_WEEK @"GetNFLWeek"
#define GET_NFL_STANDINGS @"GetNFLStandings"
#define GET_NFL_SCHEDULE @"GetNFLSchedule"

#define LOGIN_METHOD  @"Login"
#define CARDIO_WEEK_METHOD  @"GetCardioWeek"
#define EDU_LESSON_METHOD  @"GetEducationLesson"
#define EDU_WEEK_METHOD  @"GetEducationWeek"
#define MEMBER_PROGRESS_METHOD  @"GetMemberProgress"
#define MOBILITY_METHOD  @"GetMobilityWeek"
#define MOBILITY_X_METHOD  @"GetMobilityXHRSessions"
#define SAVE_MOBILITY_X_METHOD  @"SaveMobilityXWorkout"
#define NUTRITION_METHOD  @"GetNutritionDay"
#define STAR_METHOD  @"GetStars"
#define GSTAR_METHOD  @"GetGlobalCardioStars"

#define TEAM_PROGRESS_METHOD  @"GetTeamProgress"

#define SAVE_MEMBER_QUIZ_METHOD  @"SaveMemberQuiz"
#define SAVE_NUTRITION_DAY_METHOD  @"SaveNutritionDay"
#define SAVE_NUTRITION_MEAL_METHOD  @"SaveNutritionMeal"

#define SAVE_MOBILITY_WEEK_METHOD  @"SaveMobilityWeek"
#define WELLNESSWALL_METHOD @"GetWellnessWall"
#define SAVE_WELLNESSWALL_METHOD @"SaveWellnessWall"
#define SAVE_WELLNESSWALLPHOTO_METHOD @"UploadWellnessPhoto"
#define SAVE_MEALS_CAMERA_METHOD @"UploadMealPhoto"

#define MOBILITY_WORKOUTS_METHOD  @"GetMobilityWOList"
#define MOBILITY_EXERCISES_METHOD @"GetMemberMobilityExercises"//GetMobilityExercises"

@implementation request

- (id)initWithTarget:(id)actionTarget
	  SuccessAction:(SEL)successAction 
	  FailureAction:(SEL)failureAction
{
	self=[super init];
	if(self==nil)
	{
		return nil;
	}
	
	target=actionTarget;
	successHandler=successAction;
	failureHandler=failureAction;
	
	return self;
}


- (NSString *) stringToHex:(NSString *)str
{   
    NSUInteger len = [str length];
    unichar *chars = malloc(len * sizeof(unichar));
    [str getCharacters:chars];
    
    NSMutableString *hexString = [[NSMutableString alloc] init];
    
    for(NSUInteger i = 0; i < len; i++ )
    {
        [hexString appendString:[NSString stringWithFormat:@"%x", chars[i]]];
    }
    free(chars);
    
    return [hexString autorelease];
}

- (NSString *) stringFromHex:(NSString *)str 
{   
    NSMutableData *stringData = [[[NSMutableData alloc] init] autorelease];
    unsigned char whole_byte;
    char byte_chars[3] = {'\0','\0','\0'};
    int i;
    for (i=0; i < [str length] / 2; i++) {
        byte_chars[0] = [str characterAtIndex:i*2];
        byte_chars[1] = [str characterAtIndex:i*2+1];
        whole_byte = strtol(byte_chars, NULL, 16);
        [stringData appendBytes:&whole_byte length:1]; 
    }
    
    return [[[NSString alloc] initWithData:stringData encoding:NSASCIIStringEncoding] autorelease];
}

- (NSString *)getUTCFormateDate:(NSDate *)localDate
{
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
	[dateFormatter setTimeZone:timeZone];
	[dateFormatter setDateFormat:@"YYYY-MM-dd'T'HH:mm:ss.S"];
	NSString *dateString = [dateFormatter stringFromDate:localDate];
	
	return dateString;
}

#pragma mark -
#pragma mark loginRequest

- (BOOL)isNetAvalaible {
	if([NuvitaAppDelegate isNetworkAvailable]) {
		return YES;
	} else {	[target performSelector:failureHandler];
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Info"
                                                        message:@"Network or Wireless not available, please try again later."
                                                       delegate:nil
                                              cancelButtonTitle:nil
                                              otherButtonTitles:@"OK",nil];
		[alert show];
	}
	
	return NO;
}

#pragma mark - GetVo2MaxFromRR

- (void)getNFLData:(NSDictionary *)info withService:(NSString *)service {
    if (![self isNetAvalaible]) return;

    whichAPI = service;
    NSMutableString *sRequest = [[NSMutableString alloc] init];
    [sRequest appendString:@"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\" xmlns:nuv=\"http://schemas.datacontract.org/2004/07/NuvitaMobileService\">"];

    [sRequest appendString:@"<soapenv:Header/>"];
    [sRequest appendString:@"<soapenv:Body>"];
    [sRequest appendFormat:@"<tem:%@>", service];

    [sRequest appendFormat:@"<tem:memberId>%@</tem:memberId>", [info objectForKey:@"memberId"]];
    [sRequest appendFormat:@"<tem:date>%@</tem:date>", [info objectForKey:@"Date"]];

    [sRequest appendFormat:@"</tem:%@>", service];
    [sRequest appendString:@"</soapenv:Body>"];
    [sRequest appendString:@"</soapenv:Envelope>"];

    NSLog(@"request: %@", sRequest);
    [self callPostMethod:sRequest Action:[METHOD_URL stringByAppendingString:whichAPI] API:url];
}

- (void)getNFLWeek:(NSDictionary *)data {
    if(![self isNetAvalaible]) return;

	whichAPI = GET_NFL_WEEK;
//	app = (NuvitaAppDelegate *)[[UIApplication sharedApplication]delegate];

	NSMutableString *sRequest = [[NSMutableString alloc] init];
    [sRequest appendString:@"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\" xmlns:nuv=\"http://schemas.datacontract.org/2004/07/NuvitaMobileService\">"];

    [sRequest appendString:@"<soapenv:Header/>"];
    [sRequest appendString:@"<soapenv:Body>"];
    [sRequest appendString:@"<tem:GetNFLWeek>"];

    [sRequest appendFormat:@"<tem:memberId>%@</tem:memberId>", [data objectForKey:@"memberId"]];
    [sRequest appendFormat:@"<tem:date>%@</tem:date>", [data objectForKey:@"Date"]];

    [sRequest appendString:@"</tem:GetNFLWeek>"];
    [sRequest appendString:@"</soapenv:Body>"];
    [sRequest appendString:@"</soapenv:Envelope>"];

    NSLog(@"request: %@", sRequest);
    [self callPostMethod:sRequest Action:[METHOD_URL stringByAppendingString:whichAPI] API:url];
}

- (void)getNFLStandings:(NSDictionary *)data {
    if(![self isNetAvalaible]) return;

	whichAPI = GET_NFL_STANDINGS;
//	app = (NuvitaAppDelegate *)[[UIApplication sharedApplication]delegate];

	NSMutableString *sRequest = [[NSMutableString alloc] init];
    [sRequest appendString:@"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\" xmlns:nuv=\"http://schemas.datacontract.org/2004/07/NuvitaMobileService\">"];

    [sRequest appendString:@"<soapenv:Header/>"];
    [sRequest appendString:@"<soapenv:Body>"];
    [sRequest appendString:@"<tem:GetNFLStandings>"];

    [sRequest appendFormat:@"<tem:memberId>%@</tem:memberId>", [data objectForKey:@"memberId"]];
    [sRequest appendFormat:@"<tem:date>%@</tem:date>", [data objectForKey:@"Date"]];

    [sRequest appendString:@"</tem:GetNFLStandings>"];
    [sRequest appendString:@"</soapenv:Body>"];
    [sRequest appendString:@"</soapenv:Envelope>"];

    NSLog(@"request: %@", sRequest);
    [self callPostMethod:sRequest Action:[METHOD_URL stringByAppendingString:whichAPI] API:url];
}

- (void)getNFLSchedule:(NSDictionary *)data {
    if(![self isNetAvalaible]) return;

	whichAPI = GET_NFL_SCHEDULE;
//	app = (NuvitaAppDelegate *)[[UIApplication sharedApplication]delegate];

	NSMutableString *sRequest = [[NSMutableString alloc] init];
    [sRequest appendString:@"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\" xmlns:nuv=\"http://schemas.datacontract.org/2004/07/NuvitaMobileService\">"];

    [sRequest appendString:@"<soapenv:Header/>"];
    [sRequest appendString:@"<soapenv:Body>"];
    [sRequest appendString:@"<tem:GetNFLSchedule>"];

    [sRequest appendFormat:@"<tem:memberId>%@</tem:memberId>", [data objectForKey:@"memberId"]];
    [sRequest appendFormat:@"<tem:date>%@</tem:date>", [data objectForKey:@"Date"]];

    [sRequest appendString:@"</tem:GetNFLSchedule>"];
    [sRequest appendString:@"</soapenv:Body>"];
    [sRequest appendString:@"</soapenv:Envelope>"];

    NSLog(@"request: %@", sRequest);
    [self callPostMethod:sRequest Action:[METHOD_URL stringByAppendingString:whichAPI] API:url];
}

- (void)getNFLTeam:(NSDictionary *)data {
    if(![self isNetAvalaible]) return;

	whichAPI = GET_NFL_TEAM;
//	app = (NuvitaAppDelegate *)[[UIApplication sharedApplication]delegate];

	NSMutableString *sRequest = [[NSMutableString alloc] init];
    [sRequest appendString:@"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\" xmlns:nuv=\"http://schemas.datacontract.org/2004/07/NuvitaMobileService\">"];

    [sRequest appendString:@"<soapenv:Header/>"];
    [sRequest appendString:@"<soapenv:Body>"];
    [sRequest appendString:@"<tem:GetNFLTeam>"];

    [sRequest appendFormat:@"<tem:memberId>%@</tem:memberId>", [data objectForKey:@"memberId"]];
    [sRequest appendFormat:@"<tem:date>%@</tem:date>", [data objectForKey:@"Date"]];

    [sRequest appendString:@"</tem:GetNFLTeam>"];
    [sRequest appendString:@"</soapenv:Body>"];
    [sRequest appendString:@"</soapenv:Envelope>"];

    NSLog(@"request: %@", sRequest);
    [self callPostMethod:sRequest Action:[METHOD_URL stringByAppendingString:whichAPI] API:url];
}

- (void)getHealthMeasurementCurrentTrends:(NSDictionary *)data {
    
    whichAPI = GET_MEASUREMENT_TRENDS;
//	app = (NuvitaAppDelegate *)[[UIApplication sharedApplication]delegate];

	NSMutableString *sRequest = [[NSMutableString alloc] init];
    [sRequest appendString:@"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\" xmlns:nuv=\"http://schemas.datacontract.org/2004/07/NuvitaMobileService\">"];
    
    [sRequest appendString:@"<soapenv:Header/>"];
    [sRequest appendString:@"<soapenv:Body>"];
    [sRequest appendString:@"<tem:GetHealthMeasurementCurrentTrends>"];
    
    [sRequest appendFormat:@"<tem:memberId>%@</tem:memberId>", [data valueForKey:@"memberId"]];
    [sRequest appendFormat:@"<tem:groupId>%@</tem:groupId>", @"6"];
    
    [sRequest appendString:@"</tem:GetHealthMeasurementCurrentTrends>"];
    [sRequest appendString:@"</soapenv:Body>"];
    [sRequest appendString:@"</soapenv:Envelope>"];
    [self callPostMethod:sRequest Action:[METHOD_URL stringByAppendingString:whichAPI] API:url];
}

#pragma mark - getHealthMeasurementMenu

- (void)getHealthMeasurementMenu {

	if(![self isNetAvalaible]) return;
	
	whichAPI = HEALTH_MENU;//@"senduserPass";
//	app = (NuvitaAppDelegate *)[[UIApplication sharedApplication]delegate];

    NSMutableString *sRequest=[[NSMutableString alloc] init];
	[sRequest appendString:@"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\"><soapenv:Header/><soapenv:Body><tem:GetHealthMeasurementMenu>"];
	[sRequest appendString:@"</tem:GetHealthMeasurementMenu></soapenv:Body></soapenv:Envelope>"];
    [self callPostMethod:sRequest Action:[METHOD_URL stringByAppendingString:whichAPI] API:urlbeta];
}

#pragma mark -
#pragma mark loginRequest


- (void)loginRequest:(NSString *)user pass:(NSString *)password
{

	if(![self isNetAvalaible]) return;
	
	whichAPI = LOGIN_METHOD;//@"senduserPass";
//	app=(NuvitaAppDelegate *)[[UIApplication sharedApplication]delegate];

	NSMutableString *sRequest=[[NSMutableString alloc] init];
	//sRequest=[self createHeader];
	[sRequest appendString:@"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\"><soapenv:Header/><soapenv:Body><tem:Login><tem:userId>"];
 	[sRequest appendString:user];//append Username
	[sRequest appendString:@"</tem:userId><tem:password>"];
	[sRequest appendString:password];//append Password
	[sRequest appendString:@"</tem:password></tem:Login></soapenv:Body></soapenv:Envelope>"];
	NSLog(@"request string: %@",sRequest);
	//NSString *link=url;
	//link=[link stringByAppendingString:@"AuthenticateUserMAService"];
	[self callPostMethod:sRequest Action:[METHOD_URL stringByAppendingString:whichAPI] API:urlbeta];
}

#pragma mark -
#pragma mark getTeamProgress

- (void)getTeamProgress:(NSString *)memberID date:(NSString *)weekDate
{
	if(![self isNetAvalaible]) return;
	
	whichAPI = TEAM_PROGRESS_METHOD;//@"senduserPass";
//	app=(NuvitaAppDelegate *)[[UIApplication sharedApplication]delegate];

	NSMutableString *sRequest=[[NSMutableString alloc] init];
	//sRequest=[self createHeader];
	[sRequest appendString:@"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\"><soapenv:Header/><soapenv:Body>"];
	[sRequest appendString:@"<tem:GetTeamProgress><tem:memberID>"];
 	[sRequest appendString:memberID];//append Username
	[sRequest appendString:@"</tem:memberID><tem:weekDate>"];
	[sRequest appendString:weekDate];//append Password
	[sRequest appendString:@"</tem:weekDate></tem:GetTeamProgress>"];
	[sRequest appendString:@"</soapenv:Body></soapenv:Envelope>"];
	NSLog(@"request string: %@",sRequest);
	[self callPostMethod:sRequest Action:[METHOD_URL stringByAppendingString:whichAPI] API:url];
}


#pragma mark -
#pragma mark getMemberProgress

- (void)getMemberProgress:(NSString *)memberID date:(NSString *)weekDate
{
	if(![self isNetAvalaible]) return;
	
	whichAPI = MEMBER_PROGRESS_METHOD;//@"senduserPass";
//	app=(NuvitaAppDelegate *)[[UIApplication sharedApplication]delegate];

		 
	NSMutableString *sRequest=[[NSMutableString alloc] init];
	//sRequest=[self createHeader];
	[sRequest appendString:@"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\"><soapenv:Header/><soapenv:Body>"];
	[sRequest appendString:@"<tem:GetMemberProgress><tem:memberID>"];
 	[sRequest appendString:memberID];//append Username
	[sRequest appendString:@"</tem:memberID><tem:weekDate>"];
	[sRequest appendString:weekDate];//append Password
	[sRequest appendString:@"</tem:weekDate></tem:GetMemberProgress>"];
	[sRequest appendString:@"</soapenv:Body></soapenv:Envelope>"];
	NSLog(@"request string: %@",sRequest);
	[self callPostMethod:sRequest Action:[METHOD_URL stringByAppendingString:whichAPI] API:url];
}


#pragma mark -
#pragma mark getNutritionDay

- (void)getNutritionData:(NSDictionary *)info withService:(NSString *)service {
    if (![self isNetAvalaible]) return;

    whichAPI = service;
    NSMutableString *sRequest = [[NSMutableString alloc] init];
    [sRequest appendString:@"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\" xmlns:nuv=\"http://schemas.datacontract.org/2004/07/NuvitaMobileService\">"];

    [sRequest appendString:@"<soapenv:Header/>"];
    [sRequest appendString:@"<soapenv:Body>"];
    [sRequest appendFormat:@"<tem:%@>", service];
    [sRequest appendFormat:@"<tem:memberID>%@</tem:memberID>", [info objectForKey:@"memberId"]];
    [sRequest appendFormat:@"<tem:date>%@</tem:date>", [info objectForKey:@"Date"]];
    [sRequest appendFormat:@"</tem:%@>", service];
    [sRequest appendString:@"</soapenv:Body>"];
    [sRequest appendString:@"</soapenv:Envelope>"];

    NSLog(@"request: %@", sRequest);
    [self callPostMethod:sRequest Action:[METHOD_URL stringByAppendingString:whichAPI] API:url];
}

- (void)getNutritionDay:(NSString *)memberID date:(NSString *)date
{
	if(![self isNetAvalaible]) return;
	
	whichAPI = NUTRITION_METHOD;//@"senduserPass";
//	app=(NuvitaAppDelegate *)[[UIApplication sharedApplication]delegate];


	
	NSMutableString *sRequest=[[NSMutableString alloc] init];
	//sRequest=[self createHeader];
	[sRequest appendString:@"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\"><soapenv:Header/><soapenv:Body>"];
	[sRequest appendString:@"<tem:GetNutritionDay><tem:memberID>"];
 	[sRequest appendString:memberID];
	[sRequest appendString:@"</tem:memberID><tem:date>"];
	[sRequest appendString:date];
	[sRequest appendString:@"</tem:date></tem:GetNutritionDay>"];
	[sRequest appendString:@"</soapenv:Body></soapenv:Envelope>"];
	NSLog(@"request string: %@",sRequest);
	[self callPostMethod:sRequest Action:[METHOD_URL stringByAppendingString:whichAPI] API:url];
}

/*
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/">
<soapenv:Header/>
<soapenv:Body>
<tem:GetNutritionDay>
<!--Optional:-->
<tem:memberID>8b5ed874-f43d-44f4-a8eb-2884f79e2acd</tem:memberID>
<!--Optional:-->
<tem:date>01-25-2012</tem:date>
</tem:GetNutritionDay>
</soapenv:Body>
</soapenv:Envelope>
*/

#pragma mark -
#pragma mark getCardioWeek

- (void)getCardioWeek:(NSString *)memberID date:(NSString *)date
{
	if(![self isNetAvalaible]) return;
	
	whichAPI = CARDIO_WEEK_METHOD;
//	app=(NuvitaAppDelegate *)[[UIApplication sharedApplication]delegate];

	NSMutableString *sRequest=[[NSMutableString alloc] init];
	//sRequest=[self createHeader];
	[sRequest appendString:@"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\"><soapenv:Header/><soapenv:Body>"];
	[sRequest appendString:@"<tem:GetCardioWeek><tem:memberID>"];
 	[sRequest appendString:memberID];//append Username
	[sRequest appendString:@"</tem:memberID><tem:weekDate>"];
	[sRequest appendString:date];//append Password
	[sRequest appendString:@"</tem:weekDate></tem:GetCardioWeek>"];
	[sRequest appendString:@"</soapenv:Body></soapenv:Envelope>"];
	NSLog(@"request string: %@",sRequest);
	[self callPostMethod:sRequest Action:[METHOD_URL stringByAppendingString:whichAPI] API:url];
}


#pragma mark -
#pragma mark GetStars

- (void)getStars:(NSString *)memberID date:(NSString *)weekDate
{
	if(![self isNetAvalaible]) return;
	
	whichAPI = STAR_METHOD;//@"senduserPass";
//	app=(NuvitaAppDelegate *)[[UIApplication sharedApplication]delegate];

	NSMutableString *sRequest=[[NSMutableString alloc] init];
	//sRequest=[self createHeader];
	[sRequest appendString:@"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\"><soapenv:Header/><soapenv:Body>"];
	[sRequest appendString:@"<tem:GetStars><tem:memberID>"];
 	[sRequest appendString:memberID];//append Username
	[sRequest appendString:@"</tem:memberID><tem:weekDate>"];
	[sRequest appendString:weekDate];//append Password
	[sRequest appendString:@"</tem:weekDate></tem:GetStars>"];
	[sRequest appendString:@"</soapenv:Body></soapenv:Envelope>"];
	NSLog(@"request string: %@",sRequest);
	[self callPostMethod:sRequest Action:[METHOD_URL stringByAppendingString:whichAPI] API:url];
}

#pragma mark -
#pragma mark GetGlobalStars

- (void)getGlobalStars:(NSString *)pageNum date:(NSString *)weekDate
{
	if(![self isNetAvalaible]) return;
	
	whichAPI = GSTAR_METHOD;//@"senduserPass";
//	app=(NuvitaAppDelegate *)[[UIApplication sharedApplication]delegate];

	NSMutableString *sRequest=[[NSMutableString alloc] init];
	//sRequest=[self createHeader];
	[sRequest appendString:@"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\"><soapenv:Header/><soapenv:Body>"];
	[sRequest appendString:@"<tem:GetGlobalCardioStars><tem:weekDate>"];
 	[sRequest appendString:weekDate];//append Username
	[sRequest appendString:@"</tem:weekDate><tem:PageNumber>"];
	[sRequest appendString:pageNum];//append Password
	[sRequest appendString:@"</tem:PageNumber></tem:GetGlobalCardioStars>"];
	[sRequest appendString:@"</soapenv:Body></soapenv:Envelope>"];
	NSLog(@"request string: %@",sRequest);
	[self callPostMethod:sRequest Action:[METHOD_URL stringByAppendingString:whichAPI] API:url];
}



#pragma mark -
#pragma mark GetMobilityWeek

- (void)getMobilityWeek:(NSString *)memberID date:(NSString *)weekDate
{
	if(![self isNetAvalaible]) return;
	
	whichAPI = MOBILITY_METHOD;//@"senduserPass";
//	app=(NuvitaAppDelegate *)[[UIApplication sharedApplication]delegate];

	NSMutableString *sRequest=[[NSMutableString alloc] init];
	//sRequest=[self createHeader];
	[sRequest appendString:@"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\"><soapenv:Header/><soapenv:Body>"];
	[sRequest appendString:@"<tem:GetMobilityWeek><tem:memberID>"];
 	[sRequest appendString:memberID];//append Username
	[sRequest appendString:@"</tem:memberID><tem:weekDate>"];
	[sRequest appendString:weekDate];//append Password
	[sRequest appendString:@"</tem:weekDate></tem:GetMobilityWeek>"];
	[sRequest appendString:@"</soapenv:Body></soapenv:Envelope>"];
	NSLog(@"request string: %@",sRequest);
	[self callPostMethod:sRequest Action:[METHOD_URL stringByAppendingString:whichAPI] API:url];
}

#pragma mark -
#pragma mark getMobilityWorkouts

- (void)getMobilityWorkouts:(NSString *)memberID date:(NSString *)weekDate
{
	if(![self isNetAvalaible]) return;
	
	whichAPI = MOBILITY_WORKOUTS_METHOD;//@"senduserPass";
//	app=(NuvitaAppDelegate *)[[UIApplication sharedApplication]delegate];

	NSMutableString *sRequest=[[NSMutableString alloc] init];
	//sRequest=[self createHeader];
	[sRequest appendString:@"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\"><soapenv:Header/><soapenv:Body>"];
	[sRequest appendString:@"<tem:GetMobilityWOList><tem:memberId>"];
 	[sRequest appendString:memberID];//append Username
	[sRequest appendString:@"</tem:memberId><tem:date>"];
	[sRequest appendString:weekDate];//append Password
	[sRequest appendString:@"</tem:date></tem:GetMobilityWOList>"];
	[sRequest appendString:@"</soapenv:Body></soapenv:Envelope>"];
	NSLog(@"request string: %@",sRequest);
	[self callPostMethod:sRequest Action:[METHOD_URL stringByAppendingString:whichAPI] API:url];
}

/*
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/">
<soapenv:Header/>
<soapenv:Body>
<tem:GetMemberMobilityExercises>
<!--Optional:-->
<tem:memberId>?</tem:memberId>
<!--Optional:-->
<tem:WorkoutId>?</tem:WorkoutId>
</tem:GetMemberMobilityExercises>
</soapenv:Body>
</soapenv:Envelope>
*/

#pragma mark -
#pragma mark getMobilityExercises

- (void)getMobilityExercises:(NSString *)memberID workoutId:(NSString *)workoutId
{
    if(![self isNetAvalaible]) return;
	
	whichAPI = MOBILITY_EXERCISES_METHOD;//@"senduserPass";
//	app=(NuvitaAppDelegate *)[[UIApplication sharedApplication]delegate];

	NSMutableString *sRequest=[[NSMutableString alloc] init];
	//sRequest=[self createHeader];
	[sRequest appendString:@"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\"><soapenv:Header/><soapenv:Body>"];
    
    [sRequest appendString:@"<tem:GetMemberMobilityExercises><tem:memberId>"];
 	[sRequest appendString:memberID];//append Username
	[sRequest appendString:@"</tem:memberId>"];
	[sRequest appendString:@"<tem:WorkoutId>"];
 	[sRequest appendString:workoutId];//append Username
	[sRequest appendString:@"</tem:WorkoutId></tem:GetMemberMobilityExercises>"];
	[sRequest appendString:@"</soapenv:Body></soapenv:Envelope>"];
	NSLog(@"request string: %@",sRequest);
	[self callPostMethod:sRequest Action:[METHOD_URL stringByAppendingString:whichAPI] API:url];
}

#pragma mark -
#pragma mark GetMobilityXWeek

- (void)getMobilityXWeek:(NSString *)memberID date:(NSString *)date
{
	if(![self isNetAvalaible]) return;
	
	whichAPI = MOBILITY_X_METHOD;//@"senduserPass";
//	app=(NuvitaAppDelegate *)[[UIApplication sharedApplication]delegate];

	NSMutableString *sRequest=[[NSMutableString alloc] init];
	//sRequest=[self createHeader];
	[sRequest appendString:@"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\"><soapenv:Header/><soapenv:Body>"];
	[sRequest appendString:@"<tem:GetMobilityXHRSessions><tem:memberID>"];
 	[sRequest appendString:memberID];//append Username
	[sRequest appendString:@"</tem:memberID><tem:Date>"];
	[sRequest appendString:date];//append Password
	[sRequest appendString:@"</tem:Date></tem:GetMobilityXHRSessions>"];
	[sRequest appendString:@"</soapenv:Body></soapenv:Envelope>"];
	NSLog(@"request string: %@",sRequest);
	[self callPostMethod:sRequest Action:[METHOD_URL stringByAppendingString:whichAPI] API:url];
}

/*
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/">
<soapenv:Header/>
<soapenv:Body>
<tem:GetMobilityXHRSessions>
<!--Optional:-->
<tem:memberID>8b5ed874-f43d-44f4-a8eb-2884f79e2acd</tem:memberID>
<!--Optional:-->
<tem:Date>03-04-2012</tem:Date>
</tem:GetMobilityXHRSessions>
</soapenv:Body>
</soapenv:Envelope>
*/

#pragma mark -
#pragma mark SaveMobilityXWeek

- (void)saveMobilityXWeek:(NSString *)memberID date:(NSString *)date WorkoutId:(NSString*)WorkoutId Rounds:(NSString*)Rounds
{
	if(![self isNetAvalaible]) return;
	
	whichAPI = SAVE_MOBILITY_X_METHOD;//@"senduserPass";
//	app=(NuvitaAppDelegate *)[[UIApplication sharedApplication]delegate];

	NSMutableString *sRequest=[[NSMutableString alloc] init];
	//sRequest=[self createHeader];
	[sRequest appendString:@"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\"><soapenv:Header/><soapenv:Body>"];
	[sRequest appendString:@"<tem:SaveMobilityXWorkout><tem:memberId>"];
 	[sRequest appendString:memberID];//append Username
	[sRequest appendString:@"</tem:memberId><tem:HRDateId>"];
	[sRequest appendString:date];//append Password
	[sRequest appendString:@"</tem:HRDateId>"];
	
	[sRequest appendString:@"<tem:WorkoutId>"];
	[sRequest appendString:WorkoutId];//append Password
	[sRequest appendString:@"</tem:WorkoutId>"];
	
	[sRequest appendString:@"<tem:Rounds>"];
	[sRequest appendString:Rounds];//append Password
	[sRequest appendString:@"</tem:Rounds>"];
	
	[sRequest appendString:@"</tem:SaveMobilityXWorkout>"];
	[sRequest appendString:@"</soapenv:Body></soapenv:Envelope>"];
	NSLog(@"request string: %@",sRequest);
	[self callPostMethod:sRequest Action:[METHOD_URL stringByAppendingString:whichAPI] API:url];
}

/*
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/">
<soapenv:Header/>
<soapenv:Body>
<tem:SaveMobilityXWorkout>
<tem:memberId>8b5ed874-f43d-44f4-a8eb-2884f79e2acd</tem:memberId>
<tem:HRDateId>2/22/2012 7:37:31 PM</tem:HRDateId>
<tem:WorkoutId>1</tem:WorkoutId>
<tem:Rounds>3</tem:Rounds>
</tem:SaveMobilityXWorkout>
</soapenv:Body>
</soapenv:Envelope>
=============================


*/

#pragma mark -
#pragma mark GetEducationWeek

- (void)getEducationWeek:(NSString *)memberID date:(NSString *)weekDate
{
	if(![self isNetAvalaible]) return;
	
	whichAPI = EDU_WEEK_METHOD;//@"senduserPass";
//	app=(NuvitaAppDelegate *)[[UIApplication sharedApplication]delegate];

	NSMutableString *sRequest=[[NSMutableString alloc] init];
	//sRequest=[self createHeader];
	[sRequest appendString:@"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\"><soapenv:Header/><soapenv:Body>"];
	[sRequest appendString:@"<tem:GetEducationWeek><tem:memberID>"];
 	[sRequest appendString:memberID];//append Username
	[sRequest appendString:@"</tem:memberID><tem:weekDate>"];
	[sRequest appendString:weekDate];//append Password
	[sRequest appendString:@"</tem:weekDate></tem:GetEducationWeek>"];
	[sRequest appendString:@"</soapenv:Body></soapenv:Envelope>"];
	NSLog(@"request string: %@",sRequest);
	[self callPostMethod:sRequest Action:[METHOD_URL stringByAppendingString:whichAPI] API:url];
}

#pragma mark -
#pragma mark GetEducationLessonWeek

- (void)getEducationLessonWeek:(NSString *)memberID date:(NSString *)weekDate lessonNumber:(NSString *)lessonNumber
{
	if(![self isNetAvalaible]) return;
	
	whichAPI = EDU_LESSON_METHOD;//@"senduserPass";
//	app=(NuvitaAppDelegate *)[[UIApplication sharedApplication]delegate];

	NSMutableString *sRequest=[[NSMutableString alloc] init];
	//sRequest=[self createHeader];
	[sRequest appendString:@"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\"><soapenv:Header/><soapenv:Body>"];
	[sRequest appendString:@"<tem:GetEducationLesson><tem:memberID>"];
 	[sRequest appendString:memberID];//append Username
	[sRequest appendString:@"</tem:memberID><tem:weekDate>"];
	[sRequest appendString:weekDate];//append Password
	[sRequest appendString:@"</tem:weekDate><tem:lessonNumber>"];
	[sRequest appendString:lessonNumber];//append Password
	[sRequest appendString:@"</tem:lessonNumber></tem:GetEducationLesson>"];
	[sRequest appendString:@"</soapenv:Body></soapenv:Envelope>"];
	NSLog(@"request string: %@",sRequest);
	[self callPostMethod:sRequest Action:[METHOD_URL stringByAppendingString:whichAPI] API:url];
}

#pragma mark -
#pragma mark SAVE_WELLNESSWALL_METHOD

- (void)saveWellnessWall:(NSString *)memberID date:(NSString *)date text:(NSString *)text photo:(NSString *)photo
{
	if(![self isNetAvalaible]) return;
	
	whichAPI = SAVE_WELLNESSWALL_METHOD;//@"senduserPass";
//	app=(NuvitaAppDelegate *)[[UIApplication sharedApplication]delegate];

	NSMutableString *sRequest=[[NSMutableString alloc] init];
	//sRequest=[self createHeader];
	[sRequest appendString:@"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\" xmlns:nuv=\"http://schemas.datacontract.org/2004/07/NuvitaMobileService\"><soapenv:Header/><soapenv:Body>"];
    //<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/" xmlns:nuv="http://schemas.datacontract.org/2004/07/NuvitaMobileService">
    
	[sRequest appendString:@"<tem:SaveWellnessWall><tem:WellnessWallUpload><nuv:Date>"];
 	[sRequest appendString:date];//append Username
	[sRequest appendString:@"</nuv:Date><nuv:Photo>"];
	[sRequest appendString:photo];//append Password
    [sRequest appendString:@"</nuv:Photo><nuv:Text>"];
	[sRequest appendString:text];//append Password
 	[sRequest appendString:@"</nuv:Text><nuv:memberId>"];
	[sRequest appendString:memberID];//append Password
	[sRequest appendString:@"</nuv:memberId></tem:WellnessWallUpload></tem:SaveWellnessWall>"];
    
    
	[sRequest appendString:@"</soapenv:Body></soapenv:Envelope>"];
	NSLog(@"request string: %@",sRequest);
	[self callPostMethod:sRequest Action:[METHOD_URL stringByAppendingString:whichAPI] API:url];
}

#pragma mark -
#pragma mark SAVE_PHOTO_WELLNESSWALL_METHOD

- (void)saveWellnessWallPhoto:(NSString *)memberID date:(NSString *)date text:(NSString *)text photo:(NSData *)photo
{

	if(![self isNetAvalaible]) return;
	
	whichAPI = SAVE_WELLNESSWALLPHOTO_METHOD;//@"senduserPass";
//	app=(NuvitaAppDelegate *)[[UIApplication sharedApplication]delegate];

	NSMutableString *sRequest=[[NSMutableString alloc] init];

    [sRequest appendString:@"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\">"];
    [sRequest appendString:@"<soapenv:Header>"];
    [sRequest appendString:@"<tem:Text>"];
 	[sRequest appendString:text];
	[sRequest appendString:@"</tem:Text>"];
    [sRequest appendString:@"<tem:PhotoSize>"];
 	[sRequest appendString:[NSString stringWithFormat:@"%d", [photo length]]];
	[sRequest appendString:@"</tem:PhotoSize>"];
	[sRequest appendString:@"<tem:MemberId>"];
 	[sRequest appendString:memberID];
	[sRequest appendString:@"</tem:MemberId>"];
    [sRequest appendString:@"<tem:Date>"];
 	[sRequest appendString:date];
	[sRequest appendString:@"</tem:Date>"];
    [sRequest appendString:@"</soapenv:Header>"];
    [sRequest appendString:@"<soapenv:Body>"];
    [sRequest appendString:@"<tem:WellnessWallUpload><tem:Stream>"];
    [sRequest appendString:@"<inc:Include href=\"cid:123456789123456\" xmlns:inc=\"http://www.w3.org/2004/08/xop/include\"/>"];
    [sRequest appendString:@"</tem:Stream></tem:WellnessWallUpload>"];
	[sRequest appendString:@"</soapenv:Body></soapenv:Envelope>"];
    
    NSString *boundary = @"Abcd328726387263872638263809";
    NSMutableData *body = [NSMutableData data];

    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Type: application/xop+xml; charset=UTF-8; type=\"text/xml\"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Transfer-Encoding: 8bit\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-ID: <12345678912345671>\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[sRequest dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Type: application/octet-stream\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Transfer-Encoding: binary\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-ID: <123456789123456>\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[NSData dataWithData:photo]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [self callMultiPostMethod:body Action:[METHOD_URL2 stringByAppendingString:whichAPI] API:url2];
}


#pragma mark -
#pragma mark saveMealsCamera

- (void)saveMealsCamera:(NSString *)memberID date:(NSString *)date mealEnum:(NSString *)mealEnum photo:(NSData *)photo
{
	if(![self isNetAvalaible]) return;
	
	whichAPI = SAVE_MEALS_CAMERA_METHOD;//@"senduserPass";
//	app=(NuvitaAppDelegate *)[[UIApplication sharedApplication]delegate];

	NSMutableString *sRequest=[[NSMutableString alloc] init];

    [sRequest appendString:@"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\">"];
    [sRequest appendString:@"<soapenv:Header>"];
	[sRequest appendString:@"<tem:MemberId>"];
 	[sRequest appendString:memberID];
	[sRequest appendString:@"</tem:MemberId>"];
    //
    [sRequest appendString:@"<tem:MealEnum>"];
 	[sRequest appendString:mealEnum];
	[sRequest appendString:@"</tem:MealEnum>"];
    //
    [sRequest appendString:@"<tem:FileSize>"];
 	[sRequest appendString:[NSString stringWithFormat:@"%d", [photo length]]];
	[sRequest appendString:@"</tem:FileSize>"];
    //
    [sRequest appendString:@"<tem:Date>"];
 	[sRequest appendString:date];
	[sRequest appendString:@"</tem:Date>"];
    
    [sRequest appendString:@"</soapenv:Header>"];
    //body
    [sRequest appendString:@"<soapenv:Body>"];
    [sRequest appendString:@"<tem:MealPhoto><tem:Stream>"];
    [sRequest appendString:@"<inc:Include href=\"cid:123456789123456\" xmlns:inc=\"http://www.w3.org/2004/08/xop/include\"/>"];
    [sRequest appendString:@"</tem:Stream></tem:MealPhoto>"];
	[sRequest appendString:@"</soapenv:Body></soapenv:Envelope>"];
    
    NSString *boundary = @"Abcd328726387263872638263809";
    NSMutableData *body = [NSMutableData data];

    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Type: application/xop+xml; charset=UTF-8; type=\"text/xml\"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Transfer-Encoding: 8bit\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-ID: <12345678912345671>\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[sRequest dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Type: application/octet-stream; name=test.jpg\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Transfer-Encoding: binary\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-ID: <123456789123456>\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[NSData dataWithData:photo]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
 
	[self callMultiPostMethod:body Action:[METHOD_URL2 stringByAppendingString:whichAPI] API:url2];
}

#pragma mark -
#pragma mark GetEducationLessonWeek

- (void)getWellnessWall:(NSString *)memberID pageNumber:(NSString *)pageNumber
{
	if(![self isNetAvalaible]) return;
	
	whichAPI = WELLNESSWALL_METHOD;//@"senduserPass";
//	app=(NuvitaAppDelegate *)[[UIApplication sharedApplication]delegate];

	NSMutableString *sRequest=[[NSMutableString alloc] init];
	//sRequest=[self createHeader];
	[sRequest appendString:@"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\"><soapenv:Header/><soapenv:Body>"];
	[sRequest appendString:@"<tem:GetWellnessWall><tem:memberId>"];
 	[sRequest appendString:memberID];//append Username
	[sRequest appendString:@"</tem:memberId><tem:PageNumber>"];
	[sRequest appendString:pageNumber];//append Password
	[sRequest appendString:@"</tem:PageNumber></tem:GetWellnessWall>"];
	[sRequest appendString:@"</soapenv:Body></soapenv:Envelope>"];
	NSLog(@"request string: %@",sRequest);
	[self callPostMethod:sRequest Action:[METHOD_URL stringByAppendingString:whichAPI] API:url];
}


#pragma mark -
#pragma mark saveQuiz

- (void)saveQuiz:(NSString *)memberID date:(NSString *)weekDate lessonNumber:(NSString *)lessonNumber correctAnswers:(NSString*)correctAnswers totalQuestions:(NSString*)totalQuestions
{
	if(![self isNetAvalaible]) return;
	
	whichAPI = SAVE_MEMBER_QUIZ_METHOD;//@"senduserPass";
//	app=(NuvitaAppDelegate *)[[UIApplication sharedApplication]delegate];

	NSMutableString *sRequest=[[NSMutableString alloc] init];
	//sRequest=[self createHeader];
	[sRequest appendString:@"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\"><soapenv:Header/><soapenv:Body>"];
	[sRequest appendString:@"<tem:SaveMemberQuiz><tem:memberId>"];
 	[sRequest appendString:memberID];//append Username
	[sRequest appendString:@"</tem:memberId><tem:date>"];
	[sRequest appendString:weekDate];//append Password
	[sRequest appendString:@"</tem:date><tem:lessonNumber>"];
	[sRequest appendString:lessonNumber];//append Password
	[sRequest appendString:@"</tem:lessonNumber>"];
	
	[sRequest appendString:@"<tem:correctAnswers>"];
	[sRequest appendString:correctAnswers];//append Password
	[sRequest appendString:@"</tem:correctAnswers>"];
	
	[sRequest appendString:@"</tem:SaveMemberQuiz></soapenv:Body></soapenv:Envelope>"];
	NSLog(@"request string: %@",sRequest);
	[self callPostMethod:sRequest Action:[METHOD_URL stringByAppendingString:whichAPI] API:url];
}


#pragma mark -
#pragma mark SaveMobility

- (void)SaveMobility:(NSString *)xmldata
{
	if(![self isNetAvalaible]) return;
	
	whichAPI = SAVE_MOBILITY_WEEK_METHOD;//@"senduserPass";
//	app=(NuvitaAppDelegate *)[[UIApplication sharedApplication]delegate];

	NSMutableString *sRequest=[[NSMutableString alloc] init];
	//sRequest=[self createHeader];
	[sRequest appendString:@"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\" xmlns:nuv=\"http://schemas.datacontract.org/2004/07/NuvitaMobileService\"><soapenv:Header/><soapenv:Body>"];
	 //<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\"><soapenv:Header/><soapenv:Body>"];
	
	[sRequest appendString:@"<tem:SaveMobilityWeek><tem:mobilityWeek>"];
 	[sRequest appendString:xmldata];//append Password
	[sRequest appendString:@"</tem:mobilityWeek>"];
	
	[sRequest appendString:@"</tem:SaveMobilityWeek></soapenv:Body></soapenv:Envelope>"];
	NSLog(@"request string: %@",sRequest);
	[self callPostMethod:sRequest Action:[METHOD_URL stringByAppendingString:whichAPI] API:url];
}

#pragma mark -
#pragma mark SaveNutritionMeal

- (void)saveNutritionMeal:(NSString *)memberID date:(NSString *)date mealEnum:(NSString *)mealEnum description:(NSString*)description caloriesConsumed:(NSString*)caloriesConsumed healthRanking:(NSString*)healthRanking targetCalories:(NSString*)targetCalories
{
	if(![self isNetAvalaible]) return;
	
	whichAPI = SAVE_NUTRITION_MEAL_METHOD;//@"senduserPass";
//	app=(NuvitaAppDelegate *)[[UIApplication sharedApplication]delegate];

	NSMutableString *sRequest=[[NSMutableString alloc] init];
	[sRequest appendString:@"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\">"];
	[sRequest appendString:@"<soapenv:Header/>"];
	
	[sRequest appendString:@"<soapenv:Body>"];
	
	[sRequest appendString:@"<tem:SaveNutritionMeal><tem:memberId>"];
 	[sRequest appendString:memberID];//append Username
	[sRequest appendString:@"</tem:memberId><tem:date>"];
	[sRequest appendString:date];//append Password
	[sRequest appendString:@"</tem:date><tem:mealEnum>"];
	[sRequest appendString:mealEnum];//append Password
	[sRequest appendString:@"</tem:mealEnum>"];
	
	[sRequest appendString:@"<tem:description>"];
	[sRequest appendString:description];//append Password
	[sRequest appendString:@"</tem:description>"];
	
	[sRequest appendString:@"<tem:caloriesConsumed>"];
	[sRequest appendString:caloriesConsumed];//append Password
	[sRequest appendString:@"</tem:caloriesConsumed>"];	
	
	[sRequest appendString:@"<tem:healthRanking>"];
	[sRequest appendString:healthRanking];//append Password
	[sRequest appendString:@"</tem:healthRanking>"];	
	
	[sRequest appendString:@"<tem:targetCalories>"];
	[sRequest appendString:targetCalories];//append Password
	[sRequest appendString:@"</tem:targetCalories>"];
	
	[sRequest appendString:@"</tem:SaveNutritionMeal></soapenv:Body></soapenv:Envelope>"];
	NSLog(@"request string: %@",sRequest);
	[self callPostMethod:sRequest Action:[METHOD_URL stringByAppendingString:whichAPI] API:url];
}

#pragma mark -
#pragma mark SaveNutritionMeal

- (void)saveNutritionDay:(NSString *)memberID date:(NSString *)date Breakfast:(NSString *)Breakfast AMSnack:(NSString*)AMSnack PMSnack:(NSString*)PMSnack Water:(NSString*)Water Vitamin:(NSString*)Vitamin
{
	if(![self isNetAvalaible]) return;
	
	whichAPI = SAVE_NUTRITION_DAY_METHOD;//@"senduserPass";
//	app=(NuvitaAppDelegate *)[[UIApplication sharedApplication]delegate];

	NSMutableString *sRequest=[[NSMutableString alloc] init];
	//sRequest=[self createHeader];
	[sRequest appendString:@"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\">"];
	[sRequest appendString:@"<soapenv:Header/>"];
	
	[sRequest appendString:@"<soapenv:Body>"];
	
	[sRequest appendString:@"<tem:SaveNutritionDay><tem:memberId>"];
 	[sRequest appendString:memberID];//append Username
	[sRequest appendString:@"</tem:memberId><tem:date>"];
	[sRequest appendString:date];//append Password
	[sRequest appendString:@"</tem:date><tem:breakfast>"];
	[sRequest appendString:Breakfast];//append Password
	[sRequest appendString:@"</tem:breakfast>"];
		
	[sRequest appendString:@"<tem:amSnack>"];
	[sRequest appendString:AMSnack];//append Password
	[sRequest appendString:@"</tem:amSnack>"];
	
	[sRequest appendString:@"<tem:pmSnack>"];
	[sRequest appendString:PMSnack];//append Password
	[sRequest appendString:@"</tem:pmSnack>"];	
	
	[sRequest appendString:@"<tem:water>"];
	[sRequest appendString:Water];//append Password
	[sRequest appendString:@"</tem:water>"];	
	
	[sRequest appendString:@"<tem:vitamin>"];
	[sRequest appendString:Vitamin];//append Password
	[sRequest appendString:@"</tem:vitamin>"];
	
	[sRequest appendString:@"</tem:SaveNutritionDay></soapenv:Body></soapenv:Envelope>"];
	NSLog(@"request string: %@",sRequest);
	[self callPostMethod:sRequest Action:[METHOD_URL stringByAppendingString:whichAPI] API:url];
}


/*
 <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/">
 <soapenv:Header/>
 <soapenv:Body>
 <tem:SaveNutritionDay>
 <tem:memberId>8b5ed874-f43d-44f4-a8eb-2884f79e2acd</tem:memberId>
 <tem:date>02-05-2012</tem:date>
 <tem:Breakfast>false</tem:Breakfast>
 <tem:AMSnack>true</tem:AMSnack>
 <tem:PMSnack>false</tem:PMSnack>
 <tem:Water>true</tem:Water>
 <tem:Vitamin>false</tem:Vitamin>
 </tem:SaveNutritionDay>
 </soapenv:Body>
 </soapenv:Envelope>
*/


#pragma mark -
#pragma mark CallPostMethod

- (void)callPostMethod:(NSMutableString *)sRequest Action:(NSString *)action API:(NSString *)api
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
	
	NSData *postBody = [sRequest dataUsingEncoding:NSUTF8StringEncoding];
	NSURL *apiURL = [NSURL URLWithString:api];
	NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:apiURL];
	
    [request addValue:@"text/xml" forHTTPHeaderField:@"Content-Type"];
	[request addValue:action forHTTPHeaderField:@"SOAPAction"];
    
	[request setHTTPMethod:@"POST"];
	[request setHTTPBody:postBody];

	NSURLConnection *conn =[[NSURLConnection alloc] initWithRequest:request delegate:self];
		
	//NSURL *apiURL=[NSURL URLWithString:@"http://www.yaabbee.com/ws/?f=classified.list&category=0&type=1"];
//	NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:apiURL];
//	[request setHTTPMethod:@"GET"];
//	NSURLConnection *conn=[[NSURLConnection alloc] initWithRequest:request delegate:self];
	
	if (conn)
	{
		;
	}
}

#pragma mark -
#pragma mark CallPostMethod

- (void)callMultiPostMethod:(NSData *)postBody Action:(NSString *)action API:(NSString *)api {
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
	NSURL *apiURL=[NSURL URLWithString:api];
	NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:apiURL];
    NSString *boundary = @"Abcd328726387263872638263809";

    NSString *contentType = [NSString stringWithFormat:@"multipart/related; type=\"application/xop+xml\"; start=\"<12345678912345671>\"; start-info=\"text/xml\"; boundary=%@",boundary];
    
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"%lu",(unsigned long)[postBody length]]  forHTTPHeaderField:@"Content-Length"];
    [request addValue:action forHTTPHeaderField:@"SOAPAction"];
	[request setHTTPMethod:@"POST"];
	[request setHTTPBody:postBody];
    
	NSURLConnection *conn=[[NSURLConnection alloc] initWithRequest:request delegate:self];

	if (conn) {
		;
	}
}


#pragma mark -
#pragma mark Connection Deligate

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	NSLog(@"HERE RESPONSE: %ld",(long)[(NSHTTPURLResponse *) response statusCode]);
	if([(NSHTTPURLResponse *) response statusCode] != 200) {

        NSString *error = [NSHTTPURLResponse localizedStringForStatusCode:[(NSHTTPURLResponse *) response statusCode]];
        isstatus = YES;
        [target performSelector:failureHandler
                     withObject:error
                     withObject:nil];
        return;		
	}

	if(d2)
		[d2 release];
	d2 = [[NSMutableData alloc]init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	[d2 appendData:data];	
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    
    
	if(isstatus == YES) {
		isstatus = NO;
		return;
	} else {

		NSString *data_Response = [[NSString alloc] initWithData:d2 encoding:NSUTF8StringEncoding];
		NSLog(@"Response_of_submit =%@",data_Response);

		NSString *XHTMLsearchForWarning = @"XHTML";
		NSRange rangeXHTMLWarning = [data_Response rangeOfString : XHTMLsearchForWarning];
		
		NSString *HTMLsearchForWarning = @"HTML";
		NSRange rangeHTMLWarning = [data_Response rangeOfString : HTMLsearchForWarning];
		if (rangeXHTMLWarning.location != NSNotFound || rangeHTMLWarning.location != NSNotFound)
		{			
			[target performSelector:failureHandler];
			UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Error" message:@"Wrong response format." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
			[alert show];
			[alert release];
			return ;
		}

       if (_isGenericRequest) {

           NSError *error = nil;
           NuvitaXMLParser *perser = [[NuvitaXMLParser alloc] init];

           [perser parseXMLFileAtData:d2 parseError:&error parseService:whichAPI];
           [target performSelector:successHandler withObject:perser.responseStatus withObject:perser];
           return;
       }
		
		if ([whichAPI isEqualToString:LOGIN_METHOD]) {
			
			NSError *parseError = nil;
			LoginPerser *loginPerser = [[LoginPerser alloc] init];
			[loginPerser parseXMLFileAtData:d2 parseError:&parseError];	
	
			[target performSelector:successHandler withObject:loginPerser.responseStatus withObject:nil];
			[loginPerser release];
            
		} else if ([whichAPI isEqualToString:GET_NFL_WEEK]) {
			
			NSError *parseError = nil;
			NuvitaXMLParser *perser = [[NuvitaXMLParser alloc] init];
            [perser parseXMLFileAtData:d2 parseError:&parseError parseService:GET_NFL_WEEK];

			[target performSelector:successHandler withObject:perser.responseStatus withObject:perser];

		} else if ([whichAPI isEqualToString:GET_NFL_STANDINGS]) {

			NSError *parseError = nil;
			NuvitaXMLParser *perser = [[NuvitaXMLParser alloc] init];
            [perser parseXMLFileAtData:d2 parseError:&parseError parseService:GET_NFL_STANDINGS];

			[target performSelector:successHandler withObject:perser.responseStatus withObject:perser];
            
		} else if ([whichAPI isEqualToString:GET_NFL_SCHEDULE]) {

			NSError *parseError = nil;
			NuvitaXMLParser *perser = [[NuvitaXMLParser alloc] init];
            [perser parseXMLFileAtData:d2 parseError:&parseError parseService:GET_NFL_SCHEDULE];

			[target performSelector:successHandler withObject:perser.responseStatus withObject:perser];
            
		} else if ([whichAPI isEqualToString:GET_NFL_TEAM]) {

			NSError *parseError = nil;
			NuvitaXMLParser *perser = [[NuvitaXMLParser alloc] init];
            [perser parseXMLFileAtData:d2 parseError:&parseError parseService:GET_NFL_TEAM];

			[target performSelector:successHandler withObject:perser.responseStatus withObject:perser];

		} else if ([whichAPI isEqualToString:HEALTH_MENU]) {
			
			NSError *parseError = nil;
			NuvitaXMLParser *perser = [[NuvitaXMLParser alloc] init];
            [perser parseXMLFileAtData:d2 parseError:&parseError parseService:HEALTH_MENU];
			
			[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
			[target performSelector:successHandler withObject:perser.responseStatus withObject:perser];
		
		} else if ([whichAPI isEqualToString:GET_MEASUREMENT_GROUP]) {
            
            NSError *parseError = nil;
			NuvitaXMLParser *perser = [[NuvitaXMLParser alloc] init];
            [perser parseXMLFileAtData:d2 parseError:&parseError parseService:GET_MEASUREMENT_GROUP];
			
			[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
			[target performSelector:successHandler withObject:perser.responseStatus withObject:perser];
            
		} else if ([whichAPI isEqualToString:GET_MEASUREMENT_TRENDS]) {
            
            NSError *parseError = nil;
			NuvitaXMLParser *perser = [[NuvitaXMLParser alloc] init];
            [perser parseXMLFileAtData:d2 parseError:&parseError parseService:GET_MEASUREMENT_TRENDS];
			
			[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
			[target performSelector:successHandler withObject:perser.responseStatus withObject:perser];
		}
		else if ([whichAPI isEqualToString:TEAM_PROGRESS_METHOD]) {
			
			NSError *parseError = nil;
			TeamProgressData *perser = [[TeamProgressData alloc] init];
			[perser parseXMLFileAtData:d2 parseError:&parseError];	
			
			[target performSelector:successHandler withObject:perser.responseStatus withObject:perser];
			
			//[perser release];
		}	
		else if ([whichAPI isEqualToString:MEMBER_PROGRESS_METHOD]) {
			
			NSError *parseError = nil;
			MemberProgressData *perser = [[MemberProgressData alloc] init];
			[perser parseXMLFileAtData:d2 parseError:&parseError];	
			
			//[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
			[target performSelector:successHandler withObject:perser.responseStatus withObject:perser];
			
			//[perser release];
		}	
		else if ([whichAPI isEqualToString:STAR_METHOD] || [whichAPI isEqualToString:GSTAR_METHOD]) {
			
			NSError *parseError = nil;
			StarProgressData *perser = [[StarProgressData alloc] init];
			[perser parseXMLFileAtData:d2 parseError:&parseError];	
			
			//[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
			[target performSelector:successHandler withObject:perser.responseStatus withObject:perser];
			
			//[perser release];
		}	
		else if ([whichAPI isEqualToString:CARDIO_WEEK_METHOD]) {
			
			NSError *parseError = nil;
			CardioWeekData *perser = [[CardioWeekData alloc] init];
			[perser parseXMLFileAtData:d2 parseError:&parseError];	
			
			[target performSelector:successHandler withObject:perser.responseStatus withObject:perser];
		}	
		else if ([whichAPI isEqualToString:MOBILITY_METHOD]) {
			
			NSError *parseError = nil;
			MobilityWeekData *perser = [[MobilityWeekData alloc] init];
			//[perser retain];
			[perser parseXMLFileAtData:d2 parseError:&parseError];	
			
			[target performSelector:successHandler withObject:perser.responseStatus withObject:perser];
		}
		else if ([whichAPI isEqualToString:MOBILITY_X_METHOD]) {
			NSError *parseError = nil;
			
			//NSDictionary* data = [XMLReader dictionaryForXMLData:d2 error:&parseError];
			
			MobilityXData *perser = [[MobilityXData alloc] init];
			//[perser retain];
			[perser parseXMLFileAtData:d2 parseError:&parseError];	
		
			[target performSelector:successHandler withObject:perser.responseStatus withObject:perser];
		}
        else if ([whichAPI isEqualToString:MOBILITY_WORKOUTS_METHOD]) {
			NSError *parseError = nil;
                    
			MobilityWorkoutsData *perser = [[MobilityWorkoutsData alloc] init];
			//[perser retain];
			[perser parseXMLFileAtData:d2 parseError:&parseError];	
            
			[target performSelector:successHandler withObject:perser.responseStatus withObject:perser];
		}
        else if ([whichAPI isEqualToString:MOBILITY_EXERCISES_METHOD]) {
			NSError *parseError = nil;
						
			MobilityExerciseData *perser = [[MobilityExerciseData alloc] init];
			//[perser retain];
			[perser parseXMLFileAtData:d2 parseError:&parseError];	
            
			[target performSelector:successHandler withObject:perser.responseStatus withObject:perser];
		}
		else if ([whichAPI isEqualToString:EDU_WEEK_METHOD]) {
			
			NSError *parseError = nil;
			LessonWeekData *perser = [[LessonWeekData alloc] init];
			[perser parseXMLFileAtData:d2 parseError:&parseError];	
			
			[target performSelector:successHandler withObject:perser.responseStatus withObject:perser];
		}
		else if ([whichAPI isEqualToString:EDU_LESSON_METHOD]) {
			
			NSError *parseError = nil;
			EduLessonWeekData *perser = [[EduLessonWeekData alloc] init];
			[perser parseXMLFileAtData:d2 parseError:&parseError];	
			
			[target performSelector:successHandler withObject:perser.responseStatus withObject:perser];
		}
		else if ([whichAPI isEqualToString:SAVE_MEMBER_QUIZ_METHOD]) {
			
			NSError *parseError = nil;
			SaveQuizData *perser = [[SaveQuizData alloc] init];
			[perser parseXMLFileAtData:d2 parseError:&parseError];	
			
			[target performSelector:successHandler withObject:perser.responseStatus withObject:nil];
		}
        else if ([whichAPI isEqualToString:SAVE_WELLNESSWALL_METHOD]) {
			
			NSError *parseError = nil;
			SaveWellnessWallData *perser = [[SaveWellnessWallData alloc] init];
			[perser parseXMLFileAtData:d2 parseError:&parseError];	
			
			[target performSelector:successHandler withObject:perser.responseStatus withObject:nil];
		}
        else if ([whichAPI isEqualToString:SAVE_MEALS_CAMERA_METHOD]) {
			
			NSError *parseError = nil;
			SaveMealsCameraData *perser = [[SaveMealsCameraData alloc] init];
			[perser parseXMLFileAtData:d2 parseError:&parseError];	
			
			[target performSelector:successHandler withObject:perser.responseStatus withObject:nil];
		}
        else if ([whichAPI isEqualToString:SAVE_WELLNESSWALLPHOTO_METHOD]) {
			
			NSError *parseError = nil;
			SaveMealsCameraData *perser = [[SaveMealsCameraData alloc] init];
			[perser parseXMLFileAtData:d2 parseError:&parseError];	
			
			[target performSelector:successHandler withObject:perser.responseStatus withObject:nil];
		}
		else if ([whichAPI isEqualToString:SAVE_MOBILITY_WEEK_METHOD]) {
			
			NSError *parseError = nil;
			SaveMobilityData *perser = [[SaveMobilityData alloc] init];
			[perser parseXMLFileAtData:d2 parseError:&parseError];	
			
			[target performSelector:successHandler withObject:perser.responseStatus withObject:nil];
		}
		else if ([whichAPI isEqualToString:SAVE_MOBILITY_X_METHOD]) {
			
			NSError *parseError = nil;
			SaveMobilityXWorkout *perser = [[SaveMobilityXWorkout alloc] init];
			[perser parseXMLFileAtData:d2 parseError:&parseError];	
			
			[target performSelector:successHandler withObject:perser.responseStatus withObject:nil];
		}
		else if ([whichAPI isEqualToString:NUTRITION_METHOD]) {
			
			NSError *parseError = nil;
			NutritionDayData *perser = [[NutritionDayData alloc] init];
			[perser parseXMLFileAtData:d2 parseError:&parseError];	
			
			[target performSelector:successHandler withObject:perser.responseStatus withObject:perser];
//            NSError *parseError = nil;
//			NuvitaXMLParser *perser = [[NuvitaXMLParser alloc] init];
//            [perser parseXMLFileAtData:d2 parseError:&parseError parseService:NUTRITION_METHOD];
//
//			[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
//			[target performSelector:successHandler withObject:perser.responseStatus withObject:perser];
		}
		else if ([whichAPI isEqualToString:SAVE_NUTRITION_MEAL_METHOD]) {
			
			NSError *parseError = nil;
			SaveNutritionMealData *perser = [[SaveNutritionMealData alloc] init];
			[perser parseXMLFileAtData:d2 parseError:&parseError];	
			
			[target performSelector:successHandler withObject:perser.responseStatus withObject:nil];
		}
		else if ([whichAPI isEqualToString:SAVE_NUTRITION_DAY_METHOD]) {
			
			NSError *parseError = nil;
			SaveNutritionDayData *perser = [[SaveNutritionDayData alloc] init];
			[perser parseXMLFileAtData:d2 parseError:&parseError];	
			
			[target performSelector:successHandler withObject:perser.responseStatus withObject:nil];
		}
        else if ([whichAPI isEqualToString:WELLNESSWALL_METHOD]) {
			
			NSError *parseError = nil;
			WellnessWallData *perser = [[WellnessWallData alloc] init];
			[perser parseXMLFileAtData:d2 parseError:&parseError];	
			
			[target performSelector:successHandler withObject:perser.responseStatus withObject:perser];
		}
	}
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry"
                                                    message:[error localizedDescription]
                                                   delegate:nil
                                          cancelButtonTitle:@"Ok"
                                          otherButtonTitles: nil];
	[alert show];
	[alert release];
	[target performSelector:failureHandler];
}


@end

/*
 
 <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/">
 <soapenv:Header/>
 <soapenv:Body>
 <tem:GetTeamProgress>
 <!--Optional:-->
 <tem:memberID>8b5ed874-f43d-44f4-a8eb-2884f79e2acd</tem:memberID>
 <!--Optional:-->
 <tem:weekDate>12-18-2010</tem:weekDate>
 </tem:GetTeamProgress>
 </soapenv:Body>
 </soapenv:Envelope>
 
 
 <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/">
 <soapenv:Header/>
 <soapenv:Body>
 <tem:GetMemberProgress>
 <!--Optional:-->
 <tem:memberID>8b5ed874-f43d-44f4-a8eb-2884f79e2acd</tem:memberID>
 <!--Optional:-->
 <tem:weekDate>12-18-2011</tem:weekDate>
 </tem:GetMemberProgress>
 </soapenv:Body>
 </soapenv:Envelope>
 
 
 */

