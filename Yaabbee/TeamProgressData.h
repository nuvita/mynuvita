//
//  claimActivityOBJ.h
//  Acclaris
//
//  Created by Sarfaraj Biswas on 09/11/10.
//  Copyright 2010 ObjectSol Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TeamProgressItem.h"
#import "ResponseStatus.h"

@class TeamProgressItem;

@interface TeamProgressData : NSObject<NSXMLParserDelegate> {
		
	NSString *TeamId;
	NSString *TeamName;
	
	NSMutableArray *TeamMembers;
	
	NSMutableString *contentOfString;
	
	ResponseStatus *responseStatus;
	TeamProgressItem *teamProgressItem;
}

@property(nonatomic,retain)NSString *TeamId;
@property(nonatomic,retain)NSString *TeamName;

@property(nonatomic,retain)NSMutableArray *TeamMembers;

//
@property(nonatomic,retain)ResponseStatus *responseStatus;

- (void)parseXMLFileAtData:(NSMutableData *)data parseError:(NSError **)error;

- (NSMutableArray*)getTeamsEmail;

@end


//<a:TeamId>0f0a7226-6e59-4719-913d-79041c836299</a:TeamId>
//<a:TeamName>Live Lean Nation</a:TeamName>