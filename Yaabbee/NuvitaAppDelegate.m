//
//  NuvitaAppDelegate.m
//  Yaabbee
//
//  Created by Srabati on 7/22/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//
//aab946d88bec290e429d60283d6657e875123db6 - iphone4
//477ed0b7855dce61e80c556f4eddc2058fdbefce -- iphone5


#import "NuvitaAppDelegate.h"
#import "NewLoginViewController.h"
#import "Reachability.h"

#import "HomeViewController.h"
#import "FitnessLeagueViewController.h"
#import "FitnessDetailViewController.h"
#import "CardioViewController.h"
#import "StarViewController.h"
#import "MoreViewController.h"
#import "MealesViewController.h"

@interface NuvitaAppDelegate () <NewLoginViewControllerDelegate>

@end

@implementation NuvitaAppDelegate

@synthesize window = _window;

NetworkStatus remoteHostStatus;
int l = 1;


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    // Add the tab bar controller's current view as a subview of the window
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loadLoginView)
                                                 name:@"LOGOUT"
                                               object:nil];
  
    [self setupNetworkCheck];
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    NewLoginViewController *newLoginViewController = [[NewLoginViewController alloc] initWithNibName:@"NewLoginViewController" bundle:nil];
    newLoginViewController.delegate = self;
    
    _forceNavigationViewController = [[ForceNavigationViewController alloc] initWithRootViewController:newLoginViewController];
    self.window.rootViewController = _forceNavigationViewController;
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
     */
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    /*
     Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
     */
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
}

- (void)applicationWillTerminate:(UIApplication *)application {
    /*
     Called when the application is about to terminate.
     Save data if appropriate.
     See also applicationDidEnterBackground:.
     */
}

- (void)resetViewControllers {
    
    NSInteger count = [[_forceTabBarViewController viewControllers] count];
    for(int index = 0; index < count; index ++)
        [self setTintColorForNavigationControllerWithIndex:index];
}

- (void)setTintColorForNavigationControllerWithIndex:(NSInteger)index{

    UINavigationController *navController = (UINavigationController *)[[self.forceTabBarViewController viewControllers] objectAtIndex:index];
    UIImageView *textView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo.png"]];
    [navController.navigationBar.topItem setTitleView:textView];
    [[UINavigationBar appearance] setTintColor:[UIColor colorWithRed:0.2 green:0.4 blue:0.7 alpha:1.0]];
}

- (void)reloadTabController {
    
    [[[UIApplication sharedApplication].delegate window] setRootViewController:nil];
    self.forceNavigationViewController.viewControllers = nil;

    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    UIViewController *viewController1 = ![[[LoginPerser getLoginResponse] programName] isEqualToString:@"Fitness League"] ? [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil] : [[FitnessLeagueViewController alloc] initWithNibName:@"FitnessLeagueViewController" bundle:nil];
    UIViewController *viewController2 = ![[[LoginPerser getLoginResponse] programName] isEqualToString:@"Fitness League"] ? [[MealesViewController alloc] initWithNibName:@"MealesViewController" bundle:nil] : [[FitnessDetailViewController alloc] initWithNibName:@"FitnessDetailViewController" bundle:nil];
    UIViewController *viewController3 = [[CardioViewController alloc] initWithNibName:@"CardioViewController" bundle:nil];
    UIViewController *viewController4 = [[StarViewController alloc] initWithNibName:@"StarViewController" bundle:nil];
    UIViewController *viewController5 = [[MoreViewController alloc] initWithNibName:@"MoreViewController" bundle:nil];
    
    UINavigationController *navCon1 = [[UINavigationController alloc] initWithRootViewController:viewController1];
    UINavigationController *navCon2 = [[UINavigationController alloc] initWithRootViewController:viewController2];
    UINavigationController *navCon3 = [[UINavigationController alloc] initWithRootViewController:viewController3];
    UINavigationController *navCon4 = [[UINavigationController alloc] initWithRootViewController:viewController4];
    UINavigationController *navCon5 = [[UINavigationController alloc] initWithRootViewController:viewController5];
    
    _forceTabBarViewController = [[ForceTabBarViewController alloc] init];
    _forceTabBarViewController.viewControllers = @[navCon1, navCon2, navCon3, navCon4, navCon5];
    self.window.rootViewController = _forceTabBarViewController;


    [[_forceTabBarViewController.tabBar.items objectAtIndex:1] setEnabled:![LoginPerser isLifeStyle360]];
    
    [self resetViewControllers];
    [self.window makeKeyAndVisible];
}

- (void)loadLoginView {
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    NewLoginViewController *newLoginViewController = [[NewLoginViewController alloc] initWithNibName:@"NewLoginViewController" bundle:nil];
    newLoginViewController.delegate = self;
    
    _forceNavigationViewController = [[ForceNavigationViewController alloc] initWithRootViewController:newLoginViewController];
    self.window.rootViewController = _forceNavigationViewController;
    [self.window makeKeyAndVisible];
}


#pragma mark reachibility 

- (void)setupNetworkCheck {
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:@"kNetworkReachabilityChangedNotification" object:nil];
	[[Reachability sharedReachability] setHostName:@"www.google.com"];
	[NuvitaAppDelegate updateStatus];
}

- (void)reachabilityChanged:(NSNotification*)notification {
	[NuvitaAppDelegate updateStatus];        
}

+ (void)updateStatus {
	remoteHostStatus   = [[Reachability sharedReachability] remoteHostStatus];
}

+ (BOOL)isNetworkAvailable {
	[NuvitaAppDelegate updateStatus];
	if(remoteHostStatus == NotReachable)
		return NO;
	else 
		return YES;
}

+ (UIImage *)captureView:(UIView *)view {

    CALayer *layer = view.layer;
    UIGraphicsBeginImageContext(view.bounds.size);
    CGContextClipToRect(UIGraphicsGetCurrentContext(), view.bounds);
    [layer renderInContext:UIGraphicsGetCurrentContext()];

    UIImage *screenImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return screenImage;
}

#pragma mark -
#pragma mark <Checktouch>

- (void)Checktouch {
	if(_isidlecheck==YES)
	{
		_isidlecheck=NO;
		
	    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(Check) object: nil];
		l=1;
	}
	else
	{
		if(l==1)
		{
			[self performSelector:@selector(Check) withObject:nil afterDelay:300.0];
			l=2;
		}
		
	}
	[self performSelector:@selector(Checktouch) withObject:nil afterDelay:1.0];
}

#pragma mark -
#pragma mark <Checktouch>

- (void)Check
{
	//[self removeTabBar];
}

+ (BOOL)isPhone5
{
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone){
        if ([[UIScreen mainScreen] respondsToSelector: @selector(scale)]) {
            CGSize result = [[UIScreen mainScreen] bounds].size;
            CGFloat scale = [UIScreen mainScreen].scale;
            result = CGSizeMake(result.width * scale, result.height * scale);
            
            if(result.height == 960) {
//                NSLog(@"iPhone 4 Resolution");
            }
            
            if(result.height == 1136) {
//                NSLog(@"iPhone 5 Resolution");
//
                return YES;
            }
        }
        else{
//            NSLog(@"Standard Resolution");
        }
    }
    
    return NO;
}

+ (BOOL)isIOS7 {
    
    NSArray *version = [[UIDevice currentDevice].systemVersion componentsSeparatedByString:@"."];
    if ([[version objectAtIndex:0] intValue] < 7.0) {
//        NSLog(@"Device is running in ios 7 down");
        return NO;
    }
    
//    NSLog(@"Device version is running in ios 7 up");
    return YES;
}

#pragma mark - Reload Orientation

- (void)reloadAppDelegateRootViewController {
    
    [[[UIApplication sharedApplication].delegate window] setRootViewController:nil];
    [(ForceTabBarViewController *)self.forceTabBarViewController setOrientation:UIInterfaceOrientationPortrait];
    [(ForceTabBarViewController *)self.forceTabBarViewController setSupportedInterfaceOrientatoin:UIInterfaceOrientationMaskPortrait];
    [[[UIApplication sharedApplication].delegate window] setRootViewController:self.forceTabBarViewController];
}

- (void)reloadAppDelegateRootViewControllerLandscape {
    
    [[[UIApplication sharedApplication].delegate window] setRootViewController:nil];
    [(ForceTabBarViewController *)self.forceTabBarViewController setOrientation:UIInterfaceOrientationLandscapeRight];
    [(ForceTabBarViewController *)self.forceTabBarViewController setSupportedInterfaceOrientatoin:UIInterfaceOrientationMaskLandscape];
    [[[UIApplication sharedApplication].delegate window] setRootViewController:self.forceTabBarViewController];
}

#pragma mark - NewLoginViewControllerDelegate

- (void)newLoginViewController:(NewLoginViewController *)viewController didFinishLogin:(BOOL)flag {
    if (flag) {
        [self reloadTabController];
    }
}

@end
