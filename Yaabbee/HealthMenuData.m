//
//  HealthMenuData.m
//  ;
//
//  Created by John on 3/26/14.
//
//

#import "HealthMenuData.h"

#define ERROR_MESSAGE       @"a:ErrorMessage"
#define ERROR_STATUS        @"a:ErrorStatus"
#define MENU_ITEM           @"a:HealthMeasurementMenuItem"
#define LABEL               @"a:Label"
#define GROUP_ID            @"a:MeasurementGroupId"

@interface HealthMenuData () <NSXMLParserDelegate>

@property (nonatomic, retain) NSMutableString           *contentOfString;
@property (nonatomic, retain) NSMutableDictionary       *data;

@end

@implementation HealthMenuData

- (void)parseXMLFileAtData:(NSMutableData *)data parseError:(NSError **)error {
    NSXMLParser *parser;
    parser = [[NSXMLParser alloc] initWithData:data];
    [parser setDelegate:self];
    [parser setShouldProcessNamespaces:NO];
    [parser setShouldReportNamespacePrefixes:NO];
    [parser setShouldResolveExternalEntities:NO];
    [parser parse];
    
    NSError *parseError = [parser parserError];
    if (parseError && error) {
        *error = parseError;
    }
    
    [parser release];
}

- (void)parserDidStartDocument:(NSXMLParser *)parser {
	_responseStatus = [[ResponseStatus alloc] init];
    _items = [[NSMutableArray alloc] init];
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
	
    if (qName) {
		elementName = qName;
	}else
        if ([elementName isEqualToString:ERROR_MESSAGE]) {
            _contentOfString = [NSMutableString string];
            [_contentOfString retain];
            return;
        }else
            if ([elementName isEqualToString:ERROR_STATUS]) {
                _contentOfString = [NSMutableString string];
                [_contentOfString retain];
                return;
            }else
                if ([elementName isEqualToString:MENU_ITEM]) {
                    _data = [[NSMutableDictionary alloc] init];
                }else
                    if ([elementName isEqualToString:LABEL]) {
                        _contentOfString = [NSMutableString string];
                        [_contentOfString retain];
                    }else
                        if ([elementName isEqualToString:GROUP_ID]) {
                            _contentOfString = [NSMutableString string];
                            [_contentOfString retain];
                        }

}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
	
    if (qName) {
		elementName = qName;
	} else
        if([elementName isEqualToString:ERROR_STATUS]) {
            if(_contentOfString) {
                
                _responseStatus.returnCode = _contentOfString;
                [_contentOfString release];
                _contentOfString = nil;
            }
        } else
            if([elementName isEqualToString:ERROR_MESSAGE]) {
                if(_contentOfString) {
                    _responseStatus.errorText = _contentOfString;
                    [_contentOfString release];
                    _contentOfString = nil;
                }
            } else
                if([elementName isEqualToString:MENU_ITEM]) {
                    [_items addObject:_data];
                    _data = nil;
                }else
                    if ([elementName isEqualToString:LABEL]) {
                        if (_contentOfString) {
                            [_data setObject:_contentOfString forKey:LABEL];
                            [_contentOfString release];
                            _contentOfString = nil;
                        }
                    }else
                        if (_contentOfString) {
                            if ([elementName isEqualToString:GROUP_ID]) {
                                [_data setObject:_contentOfString forKey:GROUP_ID];
                                [_contentOfString release];
                                _contentOfString = nil;
                            }
                        }
    
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
	if(_contentOfString)
		[_contentOfString appendString:string];
}

- (void)parserDidEndDocument:(NSXMLParser *)parse {
//    NSLog(@"item: %@", _items);
}

@end
