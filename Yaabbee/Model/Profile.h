//
//  Profile.h
//  Yaabbee
//
//  Created by Sarfaraj Biswas on 8/15/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Profile : NSObject


@property (nonatomic,retain)NSString *name;
@property (nonatomic,retain)NSString *email;
@property (nonatomic,retain)NSString *username;
@property (nonatomic,retain)NSNumber *phoneNumber;
@property (nonatomic,retain)NSString *address;
@property (nonatomic,retain)NSString *membershipDate;

@end
