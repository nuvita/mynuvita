//
//  CustTableHeaderView.m
//  Nuvita
//
//  Created by Sayan Chatterjee on 08/12/11.
//  Copyright 2011 ObjectSol. All rights reserved.
//

#import "CustTableHeaderView.h"


@implementation CustTableHeaderView
@synthesize columns;

- (id)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code.
    }
    return self;
}

- (void)numberOfCol:(NSInteger) num width:(NSInteger)wd
{
	if(!self.columns)  self.columns = [[NSMutableArray alloc] init];
	
	float gap = wd/num;
	float pos = 0;
	
	for(int ii = 0; ii < num; ii++)
	{
		pos = ii*gap;
		[self.columns addObject:[NSNumber numberWithFloat:pos]];
	}
}

- (void)drawRect:(CGRect)rect {
	CGContextRef ctx = UIGraphicsGetCurrentContext();
	// Use the same color and width as the default cell separator for now
    CGContextSetStrokeColorWithColor(ctx, [UIColor blackColor].CGColor);
	CGContextSetLineWidth(ctx, 1);


	for (int i = 0; i < [self.columns count]; i++) {
		CGFloat f = [((NSNumber*) [self.columns objectAtIndex:i]) floatValue];
		CGContextMoveToPoint(ctx, f, 0);
		CGContextAddLineToPoint(ctx, f, self.bounds.size.height);
	}
	
	CGContextStrokePath(ctx);
	
	[super drawRect:rect];
}

- (void)dealloc {
    [super dealloc];
}


@end
