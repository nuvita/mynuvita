//
//  Profile.m
//  Yaabbee
//
//  Created by Sarfaraj Biswas on 8/15/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Profile.h"

@implementation Profile

@synthesize name;
@synthesize email;
@synthesize username;
@synthesize phoneNumber;
@synthesize address;
@synthesize membershipDate;

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

- (void)dealloc{
    
    [name release];
    [email release];
    [username release];
    [phoneNumber release];
    [address release]; 
    [membershipDate release];
    [super dealloc];
}
@end
