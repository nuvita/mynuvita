//
//  Imagescall.h
//  mynuvita
//
//  Created by Sarfaraj Biswas on 10/09/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIImage (ImageScale)
- (UIImage*)scaleToSize:(CGSize)size;
@end
