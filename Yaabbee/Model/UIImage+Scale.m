//
//  Imagescall.m
//  mynuvita
//
//  Created by Sarfaraj Biswas on 10/09/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//
#import "UIImage+Scale.h"

@implementation UIImage (ImageScale)

- (UIImage*)scaleToSize:(CGSize)size
{
    UIGraphicsBeginImageContext(size);
    [self drawInRect:CGRectMake(0, 0, size.width, size.height)];
    
    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext() ;
    
    UIGraphicsEndImageContext();
    
    return scaledImage;
}
@end
