//
//  Message.m
//  Yaabbee
//
//  Created by Abhijit Mukherjee on 27/08/11.
//  Copyright 2011 CastleRock Research. All rights reserved.
//

#import "Message.h"

@implementation Message
@synthesize messageText = _messageText;
@synthesize senderName = _sendersName;
@synthesize sentAt = _sentAt;
@synthesize updatedAt = _updatedAt;

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code here.
        self.messageText = nil;
        self.sentAt = [NSDate date];
        self.updatedAt = [NSDate date];
        self.senderName = @"me";
    }
    
    return self;
}

- (CGSize)computeMessageLengthToFit{
    return [self computeMessageLengthToFit:defaultConstraint withFontSize:defaultFontSize];
}

- (CGSize)computeMessageLengthToFit:(CGSize)restrainSize withFontSize:(CGFloat)fontSize{
    return [self.messageText sizeWithFont:[UIFont systemFontOfSize:fontSize] constrainedToSize:restrainSize lineBreakMode:NSLineBreakByWordWrapping];
}

- (void)dealloc{
    [_messageText release];
    [_sentAt release];
    [_sendersName release];
    [_updatedAt release];
    [super dealloc];
}
@end
