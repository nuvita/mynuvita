//
//  PhotoShareItemCell.h
//  syncClient
//
//  Created by Sayan Chatterjee on 02/11/11.
//  Copyright 2011 ObjectSol. All rights reserved.
//

#import <UIKit/UIKit.h>


//status queue cell
@interface CustTableCell : UITableViewCell {
	NSMutableArray *columns;
}
@property (nonatomic, retain) NSMutableArray *columns;
- (void)addColumn:(CGFloat)position;
- (void)numberOfCol:(NSInteger) num width:(NSInteger)wd ;

@end
