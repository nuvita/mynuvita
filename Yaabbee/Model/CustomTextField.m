//
//  NSString+AESCrypt.m
//
//  Created by Michael Sedlaczek, Gone Coding on 2011-02-22
//

#import "CustomTextField.h"



@implementation CustomTextField

- (CGRect)textRectForBounds:(CGRect)bounds {
	return CGRectMake(bounds.origin.x + 4, bounds.origin.y ,
					  bounds.size.width - 4*2, bounds.size.height);
}
- (CGRect)editingRectForBounds:(CGRect)bounds {
	return [self textRectForBounds:bounds];
}

/*
- (CGRect)placeholderRectForBounds:(CGRect)bounds {
    return bounds;//CGRectMake(x,y,width,height);
}


- (void)drawPlaceholderInRect:(CGRect)rect {
    // Your drawing code.
	[self changePlaceholderColor:[UIColor blackColor]];
	//[[UIColor redColor] setFill];

	//[super drawPlaceholderInRect:rect];
}


- (void) changePlaceholderColor:(UIColor*)color
{    
    	
    // Need to place the overlay placeholder exactly above the original placeholder
    UILabel *overlayPlaceholderLabel = [[[UILabel alloc] initWithFrame:CGRectMake(self.frame.origin.x + 4, self.frame.origin.y + 4, self.frame.size.width - 8, self.frame.size.height - 8)] autorelease];
    overlayPlaceholderLabel.backgroundColor = [UIColor clearColor];
    overlayPlaceholderLabel.opaque = YES;
    overlayPlaceholderLabel.text = self.placeholder;
    overlayPlaceholderLabel.textColor = color;
    overlayPlaceholderLabel.font = self.font;
    // Need to add it to the superview, as otherwise we cannot overlay the buildin text label.
    [self.superview addSubview:overlayPlaceholderLabel];
    self.placeholder = nil;
}
*/

@end
