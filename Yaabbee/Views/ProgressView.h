

#import <UIKit/UIKit.h>


@interface ProgressView : UIView {
  int numerator, denominator;
}

@property (assign) int numerator;
@property (assign) int denominator;

@end
