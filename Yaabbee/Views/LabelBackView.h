//
//  LabelBackVIew.h
//  Yaabbee
//
//  Created by Sarfaraj Biswas on 7/23/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface LabelBackView : UIView{
    UILabel *label;
    NSString *labelText;
   
}

@property (nonatomic,retain)NSString *labelText;
@end


@interface PatternBackView : UIView {
    UILabel *label;
    NSString *labelText;

}
@property (nonatomic)CGFloat lineGap;
@property (nonatomic,retain)NSString *labelText;
@end 