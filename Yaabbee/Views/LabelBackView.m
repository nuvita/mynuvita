//
//  LabelBackVIew.m
//  Yaabbee
//
//  Created by Sarfaraj Biswas on 7/23/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "LabelBackView.h"
#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>

@implementation LabelBackView
@synthesize labelText;


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        self.backgroundColor = [UIColor colorWithRed:0.945 green:0.945 blue:0.945 alpha:1];
        
        CAGradientLayer *dividerGradient = [[CAGradientLayer alloc] init];
        dividerGradient.colors = [NSArray arrayWithObjects:
                                  (id)[[UIColor colorWithRed:0.3 green:0.3 blue:0.3 alpha:0.2] CGColor],
                                  (id)[[UIColor colorWithRed:0.3 green:0.3 blue:0.3 alpha:0.2] CGColor],
                                  (id)[[UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:0.4] CGColor],
                                  (id)[[UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.4] CGColor],
                                  (id)[[UIColor colorWithRed:0.1 green:0.1 blue:0.1 alpha:0.6] CGColor],
                                  nil]; 
        
        dividerGradient.frame = self.bounds;
        [self.layer addSublayer:dividerGradient];
        [dividerGradient release];

        
        CGRect lblFrame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width,frame.size.height - 10);
        label = [[UILabel alloc] initWithFrame:lblFrame];
        label.backgroundColor = [UIColor clearColor];
        label.textColor = [UIColor darkGrayColor];
        label.textAlignment = NSTextAlignmentCenter;
        label.font = [UIFont boldSystemFontOfSize:17.0];
        
        [self addSubview:label];
        
         }
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    label.text = self.labelText;
    // Drawing code
    // Drawing lines with a white stroke color
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetRGBStrokeColor(context, 0.4, 0.4, 0.4, 1.0);
    CGContextSetLineWidth(context, 0.5);
         // Draw a series of line segments. Each pair of points is a segment
  
    int lineGap = 50;
    for(int i = -lineGap; i < rect.size.width; i+=5){
        CGFloat nextPoint = i + lineGap;
        CGContextMoveToPoint(context, nextPoint, 0);
        CGContextAddLineToPoint(context, i, rect.size.height);
        CGContextStrokePath(context);
    }
 }

- (void)dealloc{
    [labelText release];
    [label release];
    [super dealloc];
}

@end

@implementation PatternBackView

@synthesize labelText;
@synthesize lineGap;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = [UIColor whiteColor];
        CAGradientLayer *dividerGradient = [[CAGradientLayer alloc] init];
        dividerGradient.colors = [NSArray arrayWithObjects:
                                 // (id)[[UIColor colorWithRed:1.0 green:0.0 blue:0.0 alpha:1.0] CGColor],
                                  (id)[[UIColor colorWithRed:0.1 green:0.1 blue:0.1 alpha:0.3] CGColor],
                                  (id)[[UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.4] CGColor],                                
                                  (id)[[UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.0] CGColor],                                
                                  nil]; 
        
        dividerGradient.frame = CGRectMake(self.bounds.origin.x, self.bounds.origin.y, self.bounds.size.width, self.bounds.size.height/8);// self.bounds;
        [self.layer addSublayer:dividerGradient];
        [dividerGradient release];
        
        
        CGRect lblFrame = CGRectMake(frame.origin.x, frame.origin.y + 130, frame.size.width,50);
        
    //    CGPoint center = self.center;
    //    label.center = self.center;
        label = [[UILabel alloc] initWithFrame:lblFrame];
        label.backgroundColor = [UIColor clearColor];
        label.textColor = [UIColor darkGrayColor];
        label.textAlignment = NSTextAlignmentCenter;
        label.font = [UIFont boldSystemFontOfSize:17.0];
        
        [self addSubview:label];

    }
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    label.text = self.labelText;
    // Drawing code
    // Drawing lines with a white stroke color
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetRGBStrokeColor(context, 0.9, 0.9, 0.9, 1.0);
    CGContextSetLineWidth(context, 0.5);
    // Draw a series of line segments. Each pair of points is a segment
    
    // int gap = 50;
    for(int i = -lineGap; i < rect.size.width; i+=4){
        CGFloat nextPoint = i + lineGap;
        CGContextMoveToPoint(context, nextPoint, 0);
        CGContextAddLineToPoint(context, i, rect.size.height);
        CGContextStrokePath(context);
    }

}

- (void)dealloc{
    
    [labelText release];
    [label release];
    [super dealloc];

    [super dealloc];
}

@end