

#import "ProgressView.h"


@implementation ProgressView

@synthesize numerator, denominator;

- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        // Initialization code
        self.backgroundColor = [UIColor clearColor];
        //[self backgroundColor:[UIColor clearColor]];
    }
    return self;
}


- (void)drawRect:(CGRect)rect {
  // Drawing code
  CGContextRef context = UIGraphicsGetCurrentContext();

  // set up transformation

    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:rect cornerRadius:12];

    [[UIColor colorWithRed:147.0/255.0 green:147.0/255.0 blue:147.0/255.0 alpha:1] setFill];
    [path fill];

    
    NSString *text = [[NSString alloc] initWithString:@"1 of 5"];
    NSString *text2 = [[NSString alloc] initWithString:@"Images"];
    
    CGSize size = [text sizeWithFont:[UIFont boldSystemFontOfSize:12.0]];


    CGPoint pt1 = CGPointMake(14, 0);
    CGPoint pt2 = CGPointMake(11, size.height);
   CGContextSetRGBFillColor(context, 1.0, 1.0, 1.0, 1);
  [text drawAtPoint:pt1 withFont:[UIFont boldSystemFontOfSize:12.0]];
  CGContextSetRGBFillColor(context, 1.0, 1.0, 1.0, 1);
  [text2 drawAtPoint:pt2 withFont:[UIFont boldSystemFontOfSize:12.0]];
  [text release];
    [text2 release];


}


- (void)dealloc {
    [super dealloc];
}


@end
