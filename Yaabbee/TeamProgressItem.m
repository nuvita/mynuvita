//
//  claimActivityOBJ.m
//  Acclaris
//
//  Created by Sarfaraj Biswas on 09/11/10.
//  Copyright 2010 ObjectSol Technologies. All rights reserved.
//

#import "TeamProgressItem.h"

@implementation TeamProgressItem

@synthesize Avatar;
@synthesize EmailAddress;
@synthesize FirstName;
@synthesize ProgressPercent;

@end
