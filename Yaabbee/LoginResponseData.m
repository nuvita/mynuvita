//
//  claimActivityOBJ.m
//  Acclaris
//
//  Created by Sarfaraj Biswas on 09/11/10.
//  Copyright 2010 ObjectSol Technologies. All rights reserved.
//

#import "LoginResponseData.h"

@implementation LoginResponseData

@synthesize firstName;
@synthesize lastName;
@synthesize memberID;
@synthesize programName;
@synthesize today;
@synthesize avator;
@synthesize weight;
@synthesize useCameraMeals;

@end
