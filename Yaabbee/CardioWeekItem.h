

#import <Foundation/Foundation.h>


@interface CardioWeekItem : NSObject  {

}


@property(nonatomic,retain)NSString *Above;
@property(nonatomic,retain)NSString *Below;
@property(nonatomic,retain)NSString *Calories;
@property(nonatomic,retain)NSString *Date;

@property(nonatomic,retain)NSString *InAbove;
@property(nonatomic,retain)NSString *InZone;
@property(nonatomic,retain)NSString *MemberId;
@property(nonatomic,retain)NSString *Total;

- (NSString*)getDate;

@end


//<a:CardioSession>
//<a:Above>0</a:Above>
//<a:Below>45</a:Below>
//<a:Calories>490</a:Calories>
//<a:Date>2011-12-31T12:44:02</a:Date>

//<a:InAbove>26</a:InAbove>
//<a:InZone>26</a:InZone>
//<a:MemberId>8b5ed874-f43d-44f4-a8eb-2884f79e2acd</a:MemberId>
//<a:Total>71</a:Total>
//</a:CardioSession>