//
//  DateFormate.h
//  Trail
//
//  Created by mmandal on 02/10/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface DateFormate : NSObject {

}
+ (NSString *)stringFromDate:(NSDate *)date ;
+ (NSDate *)dateFromString:(NSString *)formattedDate;
@end
