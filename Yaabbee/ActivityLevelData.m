//
//  ActivityLevelData.m
//  mynuvita
//
//  Created by John on 4/9/14.
//
//

#import "ActivityLevelData.h"

@interface ActivityLevelData () <NSXMLParserDelegate> {

    NSString          *contentOfString;
}

@property (nonatomic, retain) NSString                  *parseService;
@property (nonatomic, retain) NSMutableDictionary       *elementDictionary;
@property (nonatomic, retain) NSMutableArray            *elementArray;

@end

@implementation ActivityLevelData

- (void)parseXMLFileAtData:(NSMutableData *)data parseError:(NSError **)error parseService:(NSString *)service {
    
    _parseService = service;
    
    NSXMLParser *parser;
    parser = [[NSXMLParser alloc] initWithData:data];
    [parser setDelegate:self];
    [parser setShouldProcessNamespaces:NO];
    [parser setShouldReportNamespacePrefixes:NO];
    [parser setShouldResolveExternalEntities:NO];
    [parser parse];
    
    NSError *parseError = [parser parserError];
    if (parseError && error) {
        *error = parseError;
    }
}

- (void)parserDidStartDocument:(NSXMLParser *)parser {
	_responseStatus = [[ResponseStatus alloc] init];
    _info = [[NSMutableDictionary alloc] init];
    
    _elementArray = [[NSMutableArray alloc] init];
    _elementDictionary = [[NSMutableDictionary alloc] init];
    contentOfString = @"";
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
	
    if (qName) {
		elementName = qName;
	}
    
    if (elementName) {
        [_elementArray addObject:elementName];
        contentOfString = @"";
    }
    
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
	

    if (qName) {
		elementName = qName;
	}

    if ([[elementName lowercaseString] rangeOfString:@"response"].location != NSNotFound) {
        _info = [_elementArray lastObject];
        [_elementArray removeAllObjects];
        return;
    }
  
    if ([(id)[_elementArray lastObject] isEqual:(id)elementName]) {
    
        NSDictionary *item = [NSDictionary dictionaryWithObjectsAndKeys:contentOfString, elementName, nil];
        [_elementArray replaceObjectAtIndex:[_elementArray count] - 1 withObject:item];
        return;
    
    } else {
    
        [_elementArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
           
            if ([obj isEqual:(id)elementName]) {
       
                NSUInteger location = idx + 1;
                NSRange range = NSMakeRange(location, [_elementArray count] - location);
  
                //...make array of dictionary
                NSArray *list = [_elementArray objectsAtIndexes:[NSIndexSet indexSetWithIndexesInRange:range]];
                NSMutableDictionary *dictionaryMerger = [[NSMutableDictionary alloc] init];
                for (id dictionary in list) {
                    if ([[dictionary allKeys] count] == 1)
                        [dictionaryMerger addEntriesFromDictionary:dictionary];
                }
                
                NSUInteger itemCount = [[dictionaryMerger allKeys] count];
                [_elementArray removeObjectsInRange:range];
                [_elementArray replaceObjectAtIndex:idx withObject:(itemCount == 0) ? [NSDictionary dictionaryWithObjectsAndKeys:list, elementName, nil] : dictionaryMerger];
            }

        }];

    }
    
    contentOfString = @"";

}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    contentOfString = string;
}

- (void)parserDidEndDocument:(NSXMLParser *)parse {
//    NSLog(@"info: %@", _info);
}


@end
