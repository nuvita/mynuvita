//
//  claimActivityOBJ.m
//  Acclaris
//
//  Created by Sarfaraj Biswas on 09/11/10.
//  Copyright 2010 ObjectSol Technologies. All rights reserved.
//

#import "LessonWeekData.h"

#define ERROR_STATUS @"a:ErrorStatus"
#define ERROR_MSG @"a:ErrorMessage"

#define MEMBER_ID @"a:MemberId"
#define LESSON_SUMM @"a:LessonSummary"

//
#define DESC @"a:Desc"
#define LESSON_NUMER @"a:LessonNumber"
#define PASSED @"a:Passed"
#define TITLE @"a:Title"

#define WEEK_LABEL @"a:WeekLabel"



@implementation LessonWeekData

@synthesize MemberId,WeekLabel;
//@synthesize TeamName;

@synthesize ItemsArray;

@synthesize responseStatus;


#pragma mark Parse Xml Data

- (void)parseXMLFileAtData:(NSMutableData *)data parseError:(NSError **)error
{
	NSXMLParser *parser;
	parser = [[NSXMLParser alloc] initWithData:data];
	[parser setDelegate:self];
	[parser setShouldProcessNamespaces:NO];
	[parser setShouldReportNamespacePrefixes:NO];
	[parser setShouldResolveExternalEntities:NO];
	[parser parse];
	
	NSError *parseError = [parser parserError];
	
	if (parseError && error) 
	{
		*error = parseError;
	}
	
	[parser release];
}

- (void)parserDidStartDocument:(NSXMLParser *)parser
{
	responseStatus = [[ResponseStatus alloc]init];
	self.ItemsArray = [[NSMutableArray alloc]init];
}		

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
	if (qName)
	{
		elementName = qName;
	}
	else 
		if([elementName isEqualToString:ERROR_STATUS])
		{
			contentOfString=[NSMutableString string];
			[contentOfString retain];
			return;		
				
		}
	else 
		if([elementName isEqualToString:ERROR_MSG])
		{
			contentOfString=[NSMutableString string];
			[contentOfString retain];
			return;		
		}
	else 
		if([elementName isEqualToString:MEMBER_ID])
		{
			contentOfString=[NSMutableString string];
			[contentOfString retain];
			return;		
			
		}
		else
			if([elementName isEqualToString:WEEK_LABEL])
			{
				contentOfString=[NSMutableString string];
				[contentOfString retain];
				return;		
				
			}
		else 
			if([elementName isEqualToString:LESSON_SUMM])
			{
				lessonItem = [[LessonItem alloc]init];
				[lessonItem retain];
				return;		
			}
			else 
				if([elementName isEqualToString:DESC])
				{
					contentOfString=[NSMutableString string];
					[contentOfString retain];
					return;		
					
				}
				else
					if([elementName isEqualToString:LESSON_NUMER])
					{
						contentOfString=[NSMutableString string];
						[contentOfString retain];
						return;		
						
					}
					else
						if([elementName isEqualToString:PASSED])
						{
							contentOfString=[NSMutableString string];
							[contentOfString retain];
							return;		
							
						}
						else
							if([elementName isEqualToString:TITLE])
							{
								contentOfString=[NSMutableString string];
								[contentOfString retain];
								return;		
								
							}
							
		
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName     
{	
	if (qName)
	{
		elementName = qName;
	}
	else 
			if([elementName isEqualToString:ERROR_STATUS])
			{
				if(contentOfString)
				{
					responseStatus.returnCode = contentOfString;
					[contentOfString release];
					contentOfString = nil;
				}				
			}
			else 
				if([elementName isEqualToString:ERROR_MSG])
				{
					if(contentOfString)
					{
						responseStatus.errorText = contentOfString;
						[contentOfString release];
						contentOfString = nil;
					}		
				}
	else 
		if([elementName isEqualToString:MEMBER_ID])
		{
			if(contentOfString)
			{
				self.MemberId = contentOfString;
				[contentOfString release];
				contentOfString = nil;
			}
		}
		else 
			if([elementName isEqualToString:WEEK_LABEL])
			{
				if(contentOfString)
				{
					self.WeekLabel = contentOfString;
					[contentOfString release];
					contentOfString = nil;
				}							
			}
		else 
			if([elementName isEqualToString:LESSON_SUMM])
			{
				[self.ItemsArray addObject:lessonItem];//
				
				lessonItem = nil;
			}
		else 
			if([elementName isEqualToString:DESC])
				{
					if(contentOfString)
					{
						lessonItem.Desc = contentOfString;
						[contentOfString release];
						contentOfString = nil;
					}		
				}	
		else 
			if([elementName isEqualToString:LESSON_NUMER])
			{
				if(contentOfString)
				{
					lessonItem.LessonNumber = contentOfString;
					[contentOfString release];
					contentOfString = nil;
				}					
			}
			else 
				if([elementName isEqualToString:PASSED])
				{
					if(contentOfString)
					{
						lessonItem.Passed = contentOfString;
						[contentOfString release];
						contentOfString = nil;
					}						
				}
				else 
					if([elementName isEqualToString:TITLE])
					{
						if(contentOfString)
						{
							lessonItem.Title = contentOfString;
							[contentOfString release];
							contentOfString = nil;
						}							
					}
					
	
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
	if(contentOfString)
		[contentOfString appendString:string];
}

- (void)parserDidEndDocument:(NSXMLParser *)parse
{
	
	//myerrorcodeOBJ = (errorcodeOBJ *)[errordetails objectAtIndex:0];
//	responseStatus.returnCode = [NSString stringWithFormat:@"0"];
//	responseStatus.errorText = [NSString stringWithFormat:@""];
	//NSLog(@"**********\n\n\n\n\n\n%@\n\nreturnCode %@\nerrorText %@", responseStatus.returnCode, responseStatus.errorText);
}	

@end


/*
 
 <a:ErrorMessage i:nil="true"/>
 <a:ErrorStatus>false</a:ErrorStatus>
 <a:Lessons>
 <a:LessonSummary>
 <a:Desc>Most likley, you belong to one of two camps: You set resolutions every New Year or you don’t.</a:Desc>
 <a:LessonNumber>160</a:LessonNumber>
 <a:Passed>false</a:Passed>
 <a:Title>Reset your Resolutions – A 3-Step Plan Part I</a:Title>
 </a:LessonSummary>
 <a:LessonSummary>
 <a:Desc>Most likley, you belong to one of two camps: You set resolutions every New Year or you don’t.</a:Desc>
 <a:LessonNumber>161</a:LessonNumber>
 <a:Passed>false</a:Passed>
 <a:Title>Reset your Resolutions – A 3-Step Plan Part II</a:Title>
 </a:LessonSummary>
 </a:Lessons>
 <a:MemberId>8b5ed874-f43d-44f4-a8eb-2884f79e2acd</a:MemberId>
 <a:WeekLabel>Week of 1/2</a:WeekLabel>
 
 */

