//
//  claimActivityOBJ.m
//  Acclaris
//
//  Created by Sarfaraj Biswas on 09/11/10.
//  Copyright 2010 ObjectSol Technologies. All rights reserved.
//

#import "CardioWeekData.h"

#define ERROR_STATUS @"a:ErrorStatus"
#define ERROR_MSG @"a:ErrorMessage"

#define ABOVE_RANGE_HR @"a:AboveRangeHR"
#define ABOVE_RANGE_PCT @"a:AboveRangePct"
#define BELOW_RANGE_HR @"a:BelowRangeHR"
#define BELOW_RANGE_PCT @"a:BelowRangePct"

#define GOAL @"a:Goal"
#define GOAL_NAME @"a:GoalName"
#define INZONE_RANGE_HR @"a:InZoneRangeHR"
#define INZONE_RANGE_PCT @"a:InZoneRangePct"

#define MEMBER_ID @"a:MemberId"
#define PROGRESS @"a:Progress"
#define PROGRESS_PERC @"a:ProgressPerecent"
#define PROGRESS_VAL @"a:ProgressValue"

//
#define CARDIO_SESSION @"a:CardioSession"

#define ABOVE @"a:Above"
#define BELOW @"a:Below"

#define CALORIES @"a:Calories"
#define DATE @"a:Date"

#define INABOVE @"a:InAbove"
#define INZONE @"a:InZone"

#define MEMBER_ID @"a:MemberId"
#define TOTAL @"a:Total"

#define WEEK_LABEL @"a:WeekLabel"
//static TeamProgressData *_sharedInstance = nil;

@implementation CardioWeekData

@synthesize AboveRangeHR, AboveRangePct, BelowRangeHR, BelowRangePct, Goal, GoalName, InZoneRangeHR, InZoneRangePct, MemberId, ProgressPerecent, ProgressValue,Progress;
@synthesize WeekLabel;

@synthesize ItemsArray;

@synthesize responseStatus;


#pragma mark Parse Xml Data

- (void)parseXMLFileAtData:(NSMutableData *)data parseError:(NSError **)error
{
	NSXMLParser *parser;
	parser = [[NSXMLParser alloc] initWithData:data];
	[parser setDelegate:self];
	[parser setShouldProcessNamespaces:NO];
	[parser setShouldReportNamespacePrefixes:NO];
	[parser setShouldResolveExternalEntities:NO];
	[parser parse];
	
	NSError *parseError = [parser parserError];
	
	if (parseError && error) 
	{
		*error = parseError;
	}
	
	[parser release];
}

- (void)parserDidStartDocument:(NSXMLParser *)parser
{
	responseStatus = [[ResponseStatus alloc]init];
	self.ItemsArray = [[NSMutableArray alloc]init];
}		

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
	if (qName)
	{
		elementName = qName;
	}
	else 
		if([elementName isEqualToString:ERROR_STATUS])
		{
			contentOfString=[NSMutableString string];
			[contentOfString retain];
			return;		
				
		}
	else 
		if([elementName isEqualToString:ERROR_MSG])
		{
			contentOfString=[NSMutableString string];
			[contentOfString retain];
			return;		
		}
	else //		
		if([elementName isEqualToString:ABOVE_RANGE_HR])
		{
			contentOfString=[NSMutableString string];
			[contentOfString retain];
			return;		
			
		}
		else //		
			if([elementName isEqualToString:ABOVE_RANGE_PCT])
			{
				contentOfString=[NSMutableString string];
				[contentOfString retain];
				return;		
				
			}else //		
				if([elementName isEqualToString:BELOW_RANGE_HR])
				{
					contentOfString=[NSMutableString string];
					[contentOfString retain];
					return;		
					
				}else //		
					if([elementName isEqualToString:BELOW_RANGE_PCT])
					{
						contentOfString=[NSMutableString string];
						[contentOfString retain];
						return;		
						
					}else //		
						if([elementName isEqualToString:GOAL])
						{
							contentOfString=[NSMutableString string];
							[contentOfString retain];
							return;		
							
						}else //		
							if([elementName isEqualToString:GOAL_NAME])
							{
								contentOfString=[NSMutableString string];
								[contentOfString retain];
								return;		
								
							}else //		
								if([elementName isEqualToString:INZONE_RANGE_HR])
								{
									contentOfString=[NSMutableString string];
									[contentOfString retain];
									return;		
									
								}
								else 
									if([elementName isEqualToString:INZONE_RANGE_PCT])
									{
										contentOfString=[NSMutableString string];
										[contentOfString retain];
										return;		
										
									}
									else //		
										if([elementName isEqualToString:MEMBER_ID])
										{
											contentOfString=[NSMutableString string];
											[contentOfString retain];
											return;		
											
										}else //
											if([elementName isEqualToString:PROGRESS])
											{
												contentOfString=[NSMutableString string];
												[contentOfString retain];
												return;		
												
											}else
											if([elementName isEqualToString:PROGRESS_PERC])
											{
												contentOfString=[NSMutableString string];
												[contentOfString retain];
												return;		
												
											}else //		
												if([elementName isEqualToString:PROGRESS_VAL])
												{
													contentOfString=[NSMutableString string];
													[contentOfString retain];
													return;		
													
												}
												else //		
													if([elementName isEqualToString:WEEK_LABEL])
													{
														contentOfString=[NSMutableString string];
														[contentOfString retain];
														return;		
														
													}
	
		else //node
			if([elementName isEqualToString:CARDIO_SESSION])
			{
				item = [[CardioWeekItem alloc]init];
				[item retain];
				return;		
			}
			else 
				if([elementName isEqualToString:ABOVE])
				{
					contentOfString=[NSMutableString string];
					[contentOfString retain];
					return;		
					
				}
				else
					if([elementName isEqualToString:BELOW])
					{
						contentOfString=[NSMutableString string];
						[contentOfString retain];
						return;		
						
					}
					else
						if([elementName isEqualToString:CALORIES])
						{
							contentOfString=[NSMutableString string];
							[contentOfString retain];
							return;		
							
						}
						else
							if([elementName isEqualToString:DATE])
							{
								contentOfString=[NSMutableString string];
								[contentOfString retain];
								return;		
								
							}
							else
								if([elementName isEqualToString:INABOVE])
								{
									contentOfString=[NSMutableString string];
									[contentOfString retain];
									return;		
									
								}
								else
									if([elementName isEqualToString:INZONE])
									{
										contentOfString=[NSMutableString string];
										[contentOfString retain];
										return;		
										
									}else
										if([elementName isEqualToString:MEMBER_ID])
										{
											contentOfString=[NSMutableString string];
											[contentOfString retain];
											return;		
											
										}else
											if([elementName isEqualToString:TOTAL])
											{
												contentOfString=[NSMutableString string];
												[contentOfString retain];
												return;		
												
											}
	
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName     
{	
	if (qName)
	{
		elementName = qName;
	}
	else 
			if([elementName isEqualToString:ERROR_STATUS])
			{
				if(contentOfString)
				{
					responseStatus.returnCode = contentOfString;
					[contentOfString release];
					contentOfString = nil;
				}				
			}
			else 
				if([elementName isEqualToString:ERROR_MSG])
				{
					if(contentOfString)
					{
						responseStatus.errorText = contentOfString;
						[contentOfString release];
						contentOfString = nil;
					}		
				}
	else 
		if([elementName isEqualToString:ABOVE_RANGE_HR])
		{
			if(contentOfString)
			{
				self.AboveRangeHR = contentOfString;
				[contentOfString release];
				contentOfString = nil;
			}
		}
		else 
			if([elementName isEqualToString:ABOVE_RANGE_PCT])
			{
				if(contentOfString)
				{
					self.AboveRangePct = contentOfString;
					[contentOfString release];
					contentOfString = nil;
				}
			}
			else 
				if([elementName isEqualToString:BELOW_RANGE_HR])
				{
					if(contentOfString)
					{
						self.BelowRangeHR = contentOfString;
						[contentOfString release];
						contentOfString = nil;
					}
				}
				else 
					if([elementName isEqualToString:BELOW_RANGE_PCT])
					{
						if(contentOfString)
						{
							self.BelowRangePct = contentOfString;
							[contentOfString release];
							contentOfString = nil;
						}
					}
					else 
						if([elementName isEqualToString:GOAL])
						{
							if(contentOfString)
							{
								self.Goal = contentOfString;
								[contentOfString release];
								contentOfString = nil;
							}
						}else 
							if([elementName isEqualToString:GOAL_NAME])
							{
								if(contentOfString)
								{
									self.GoalName = contentOfString;
									[contentOfString release];
									contentOfString = nil;
								}
							}else 
								if([elementName isEqualToString:INZONE_RANGE_HR])
								{
									if(contentOfString)
									{
										self.InZoneRangeHR = contentOfString;
										[contentOfString release];
										contentOfString = nil;
									}
								}else 
									if([elementName isEqualToString:INZONE_RANGE_PCT])
									{
										if(contentOfString)
										{
											self.InZoneRangePct = contentOfString;
											[contentOfString release];
											contentOfString = nil;
										}
									}
									else 
										if([elementName isEqualToString:MEMBER_ID])
										{
											if(contentOfString)
											{
												self.MemberId = contentOfString;
												[contentOfString release];
												contentOfString = nil;
											}
										}else 
											if([elementName isEqualToString:PROGRESS])
											{
												if(contentOfString)
												{
													self.Progress = contentOfString;
													[contentOfString release];
													contentOfString = nil;
												}
											}else
											if([elementName isEqualToString:PROGRESS_PERC])
											{
												if(contentOfString)
												{
													self.ProgressPerecent = contentOfString;
													[contentOfString release];
													contentOfString = nil;
												}
											}else 
												if([elementName isEqualToString:PROGRESS_VAL])
												{
													if(contentOfString)
													{
														self.ProgressValue = contentOfString;
														[contentOfString release];
														contentOfString = nil;
													}
												}
												else 
													if([elementName isEqualToString:WEEK_LABEL])
													{
														if(contentOfString)
														{
															self.WeekLabel = contentOfString;
															[contentOfString release];
															contentOfString = nil;
														}
													}
		else //node
			if([elementName isEqualToString:CARDIO_SESSION])
			{
				[self.ItemsArray addObject:item];
				
				item = nil;
			}
		else 			
			if([elementName isEqualToString:ABOVE])
				{
					if(contentOfString)
					{
						item.Above = contentOfString;
						[contentOfString release];
						contentOfString = nil;
					}		
				}	
		else 
			if([elementName isEqualToString:BELOW])
			{
				if(contentOfString)
				{
					item.Below = contentOfString;
					[contentOfString release];
					contentOfString = nil;
				}					
			}
			else 
				if([elementName isEqualToString:CALORIES])
				{
					if(contentOfString)
					{
						item.Calories = contentOfString;
						[contentOfString release];
						contentOfString = nil;
					}						
				}
				else 
					if([elementName isEqualToString:DATE])
					{
						if(contentOfString)
						{
							item.Date = contentOfString;
							[contentOfString release];
							contentOfString = nil;
						}							
					}
					else 
						if([elementName isEqualToString:INABOVE])
						{
							if(contentOfString)
							{
								item.InAbove = contentOfString;
								[contentOfString release];
								contentOfString = nil;
							}							
						}
						else 
							if([elementName isEqualToString:INZONE])
							{
								if(contentOfString)
								{
									item.InZone = contentOfString;
									[contentOfString release];
									contentOfString = nil;
								}							
							}else 
								if([elementName isEqualToString:MEMBER_ID])
								{
									if(contentOfString)
									{
										item.MemberId = contentOfString;
										[contentOfString release];
										contentOfString = nil;
									}							
								}else 
									if([elementName isEqualToString:TOTAL])
									{
										if(contentOfString)
										{
											item.Total = contentOfString;
											[contentOfString release];
											contentOfString = nil;
										}							
									}
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
	if(contentOfString)
		[contentOfString appendString:string];
}

- (void)parserDidEndDocument:(NSXMLParser *)parse
{
	
	//myerrorcodeOBJ = (errorcodeOBJ *)[errordetails objectAtIndex:0];
//	responseStatus.returnCode = [NSString stringWithFormat:@"0"];
//	responseStatus.errorText = [NSString stringWithFormat:@""];
	//NSLog(@"**********\n\n\n\n\n\n%@\n\nreturnCode %@\nerrorText %@", responseStatus.returnCode, responseStatus.errorText);
}	

- (NSString*)getTotalCals
{
	float val = 0;
	int size = [ItemsArray count];
	
	for(int ii = 0; ii < size; ii++)
	{
		CardioWeekItem* cardioWeekItem = (CardioWeekItem*)[ItemsArray objectAtIndex:ii];
		
		val += [cardioWeekItem.Calories floatValue]; 
	}
	
	return [NSString stringWithFormat:@"%.f", val] ;
}

- (NSString*)getTotalBelow
{
	float val = 0;
	int size = [ItemsArray count];
	
	for(int ii = 0; ii < size; ii++)
	{
		CardioWeekItem* cardioWeekItem = (CardioWeekItem*)[ItemsArray objectAtIndex:ii];
		
		val += [cardioWeekItem.Below floatValue]; 
	}
	
	return [NSString stringWithFormat:@"%.f", val] ;
}

- (NSString*)getTotalInZone
{
	float val = 0;
	int size = [ItemsArray count];
	
	for(int ii = 0; ii < size; ii++)
	{
		CardioWeekItem* cardioWeekItem = (CardioWeekItem*)[ItemsArray objectAtIndex:ii];
		
		val += [cardioWeekItem.InZone floatValue]; 
	}
	
	return [NSString stringWithFormat:@"%.f", val] ;
}

- (NSString*)getTotalAbove
{
	float val = 0;
	int size = [ItemsArray count];
	
	for(int ii = 0; ii < size; ii++)
	{
		CardioWeekItem* cardioWeekItem = (CardioWeekItem*)[ItemsArray objectAtIndex:ii];
		
		val += [cardioWeekItem.Above floatValue]; 
	}
	
	return [NSString stringWithFormat:@"%.f", val] ;
}

- (NSString*)getTotalVal
{
	float val = 0;
	int size = [ItemsArray count];
	
	for(int ii = 0; ii < size; ii++)
	{
		CardioWeekItem* cardioWeekItem = (CardioWeekItem*)[ItemsArray objectAtIndex:ii];
		
		val += [cardioWeekItem.Total floatValue]; 
	}
	
	return [NSString stringWithFormat:@"%.f", val] ;
}


@end


/*
 
 <s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">
 <s:Body>
 <GetCardioWeekResponse xmlns="http://tempuri.org/">
 <GetCardioWeekResult xmlns:a="http://schemas.datacontract.org/2004/07/NuvitaMobileService" xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
 <a:AboveRangeHR>146-146 bpm</a:AboveRangeHR>
 <a:AboveRangePct>81-81%</a:AboveRangePct>
 <a:BelowRangeHR>0-0 bpm</a:BelowRangeHR>
 <a:BelowRangePct>0-0%</a:BelowRangePct>
 <a:ErrorMessage i:nil="true"/>
 <a:ErrorStatus>false</a:ErrorStatus>
 <a:Goal>120</a:Goal>
 <a:GoalName>Heart Rate</a:GoalName>
 <a:InZoneRangeHR>126-126 bpm</a:InZoneRangeHR>
 <a:InZoneRangePct>70-70%</a:InZoneRangePct>
 <a:MemberId>8b5ed874-f43d-44f4-a8eb-2884f79e2acd</a:MemberId>
 <a:ProgressPerecent>0</a:ProgressPerecent>
 <a:ProgressValue>0</a:ProgressValue>
 <a:Sessions>
 <a:CardioSession>
 <a:Above>0</a:Above>
 <a:Below>2</a:Below>
 <a:Calories>11</a:Calories>
 <a:Date>2011-12-27T08:19:12</a:Date>
 <a:InAbove>0</a:InAbove>
 <a:InZone>0</a:InZone>
 <a:MemberId>8b5ed874-f43d-44f4-a8eb-2884f79e2acd</a:MemberId>
 <a:Total>2</a:Total>
 </a:CardioSession>
 <a:CardioSession>
 <a:Above>0</a:Above>
 <a:Below>16</a:Below>
 <a:Calories>413</a:Calories>
 <a:Date>2011-12-27T19:17:27</a:Date>
 <a:InAbove>30</a:InAbove>
 <a:InZone>30</a:InZone>
 <a:MemberId>8b5ed874-f43d-44f4-a8eb-2884f79e2acd</a:MemberId>
 <a:Total>46</a:Total>
 </a:CardioSession>
 <a:CardioSession>
 <a:Above>0</a:Above>
 <a:Below>46</a:Below>
 <a:Calories>147</a:Calories>
 <a:Date>2011-12-28T17:11:04</a:Date>
 <a:InAbove>3</a:InAbove>
 <a:InZone>3</a:InZone>
 <a:MemberId>8b5ed874-f43d-44f4-a8eb-2884f79e2acd</a:MemberId>
 <a:Total>49</a:Total>
 </a:CardioSession>
 <a:CardioSession>
 <a:Above>0</a:Above>
 <a:Below>4</a:Below>
 <a:Calories>369</a:Calories>
 <a:Date>2011-12-29T08:14:04</a:Date>
 <a:InAbove>29</a:InAbove>
 <a:InZone>29</a:InZone>
 <a:MemberId>8b5ed874-f43d-44f4-a8eb-2884f79e2acd</a:MemberId>
 <a:Total>33</a:Total>
 </a:CardioSession>
 <a:CardioSession>
 <a:Above>0</a:Above>
 <a:Below>24</a:Below>
 <a:Calories>505</a:Calories>
 <a:Date>2011-12-30T10:28:56</a:Date>
 <a:InAbove>31</a:InAbove>
 <a:InZone>31</a:InZone>
 <a:MemberId>8b5ed874-f43d-44f4-a8eb-2884f79e2acd</a:MemberId>
 <a:Total>55</a:Total>
 </a:CardioSession>
 <a:CardioSession>
 <a:Above>0</a:Above>
 <a:Below>45</a:Below>
 <a:Calories>490</a:Calories>
 <a:Date>2011-12-31T12:44:02</a:Date>
 <a:InAbove>26</a:InAbove>
 <a:InZone>26</a:InZone>
 <a:MemberId>8b5ed874-f43d-44f4-a8eb-2884f79e2acd</a:MemberId>
 <a:Total>71</a:Total>
 </a:CardioSession>
 </a:Sessions>
 </GetCardioWeekResult>
 </GetCardioWeekResponse>
 </s:Body>
 </s:Envelope> 
 */

