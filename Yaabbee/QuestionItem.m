//
//  claimActivityOBJ.m
//  Acclaris
//
//  Created by Sarfaraj Biswas on 09/11/10.
//  Copyright 2010 ObjectSol Technologies. All rights reserved.
//

#import "QuestionItem.h"

@implementation QuestionItem

@synthesize Text;
@synthesize CorrectAnswer;
@synthesize Answers;

@synthesize isAnswered;
@synthesize answeredIndex;

- (BOOL)isAnsweredCorrect
{
	AnswerItem* item = (AnswerItem*)[Answers objectAtIndex:answeredIndex] ;
	
	return [item.isCorrect boolValue];
}

- (NSDictionary *)getItems {
    NSDictionary *items = [NSDictionary dictionaryWithObjectsAndKeys:
                           [NSNumber numberWithBool:self.isAnswered], @"isAnswered",
                           [NSNumber numberWithInt:self.answeredIndex], @"answeredIndex",
                           [self Text], @"Text",
                           [self CorrectAnswer], @"CorrectAnswer",
                           [self Answers], @"Answers",
                            nil];

    return items;
}

@end

